#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp

    printf "%b\n" "\nCleaning directory: ${temp_dir}"
    rm -r -v "${temp_dir}" || true

    printf "%b\n" "\nInfo: Cleanup finished ..."
}

# Under Linux you use `home` under macOS `Users`
HOMEDIR="home"
readonly HOMEDIR
printf "%b\n" "\nHOMEDIR: ${HOMEDIR}"

# Your user ! in which context it SHOULD run
USERSCRIPT="vagrant"
readonly USERSCRIPT
printf "%b\n" "\nUSERSCRIPT: ${USERSCRIPT}"

DIRDATE="$(date +"%Y-%m-%d")"
readonly DIRDATE
printf "%b\n" "\nCurrent date: ${DIRDATE}"

# Specify the target directory for the compressed archive
target_directory="/home/vagrant"
printf "%b\n" "\ntarget_directory: ${target_directory}"

# https://www.cyberciti.biz/faq/linux-log-files-location-and-how-do-i-view-logs-files/

# Specify the source directory of log files
log_directory="/var/log"
printf "%b\n" "\nlog_directory: ${log_directory}"

# Specify the log files you want to include in the archive
log_files=("kern.log" "syslog" "auth.log" "other.log" "cron.log" "dpkg.log" "user.log" "mail.log" "maillog" "utmp" "wtmp"  "btmp")
printf "%b\n" "\nlog_files: ${log_files}"

log_files_tomcat=("access_log*", "error_log*", "httpd-access.log*", "httpd-error.log*", "catalina.out")
printf "%b\n" "\nlog_files_tomcat: ${log_files_tomcat}"

log_files_apache=("access_log*", "error_log*", "httpd-access.log*", "httpd-error.log*", "catalina.out")
printf "%b\n" "\nlog_files_apache: ${log_files_apache}"

log_files_nginx=("*access_log*", "*access.log*", "*error_log*", "*error.log*")
printf "%b\n" "\nlog_files_nginx: ${log_files_nginx}"

log_files_lighttpd=("*access_log*", "*access.log*", "*error_log*", "*error.log*")
printf "%b\n" "\nlog_files_lighttpd: ${log_files_lighttpd}"

# https://www.jenkins.io/blog/2022/03/25/systemd-migration/
log_files_jenkins=("jenkins.log*", "*error_log*", "*error.log*")
printf "%b\n" "\nlog_files_jenkins: ${log_files_jenkins}"

log_files_qmail=("*", "*error_log*", "*error.log*")
printf "%b\n" "\nlog_files_qmail: ${log_files_qmail}"

log_files_packages=("apt_log*", "yum.log*", "dnf.log*")
printf "%b\n" "\nlog_files_nginx: ${log_files_packages}"

# Create a timestamp for the archive filename
timestamp="$(date +"%Y-%m-%d-%H-%M")"
printf "%b\n" "\ntimestamp: ${timestamp}"

# Create a temporary directory to store the log files
temp_dir=$(mktemp -d)
printf "%b\n" "\ntemp_dir: ${temp_dir}"

# Function to handle errors
handle_error() {
    printf "%b\n" "\nError occurred. Cleaning up...\n"
    cleanup
    exit 1
}

trap cleanup EXIT
trap handle_error ERR

printf "%b\n" "\nCopy log files to the temporary directory ${temp_dir}"
for log_file in "${log_files[@]}"; do
    printf "%b\n" "\nCopying ${log_directory}/${log_file} ${temp_dir}"
    cp -f -p -v "${log_directory}/${log_file}" "${temp_dir}" || true
done

printf "%b\n" "\nCopy Apache log files to the temporary directory ${temp_dir}\n"
for log_file_apache in "${log_files_apache[@]}"; do
    DIRECTORY="${temp_dir}/apache"
    printf "%b\n" "\n${DIRECTORY}"

    if [ ! -d "${DIRECTORY}" ]; then
        # Control will enter here if ${DESTINATION} doesn't exist.
        mkdir -p -v "${DIRECTORY}"
        printf "%b\n" "\nThe directory ${DIRECTORY} is created\n"
    fi
    printf "%b\n" "\nCopying ${log_directory}/${log_file_apache} ${temp_dir}/apache\n"
    cp -f -p -v "${log_directory}/${log_file_apache}" "${temp_dir}/apache" || true
    printf "%b\n" "\nCopying ${log_directory}/apache/${log_file_apache} ${temp_dir}/apache\n"
    cp -f -p -v "${log_directory}/apache/${log_file_apache}" "${temp_dir}/apache" || true
    printf "%b\n" "\nCopying ${log_directory}/apache2/${log_file_apache} ${temp_dir}/apache\n"
    cp -f -p -v "${log_directory}/apache2/${log_file_apache}" "${temp_dir}/apache" || true
    printf "%b\n" "\nCopying ${log_directory}/httpd/${log_file_apache} ${temp_dir}/apache\n"
    cp -f -p -v "${log_directory}/httpd/${log_file_apache}" "${temp_dir}/apache" || true
done

printf "%b\n" "\nCopy nginx log files to the temporary directory ${temp_dir}\n"
for log_file_nginx in "${log_files_nginx[@]}"; do
    DIRECTORY="${temp_dir}/nginx"
    printf "%b\n" "\n${DIRECTORY}"

    if [ ! -d "${DIRECTORY}" ]; then
        # Control will enter here if ${DESTINATION} doesn't exist.
        mkdir -p -v "${DIRECTORY}"
        printf "%b\n" "\nThe directory ${DIRECTORY} is created\n"
    fi
    printf "%b\n" "\nCopying ${log_directory}/${log_file_nginx} ${temp_dir}/nginx\n"
    cp -f -p -v "${log_directory}/${log_file_nginx}" "${temp_dir}/nginx" || true
    printf "%b\n" "\nCopying ${log_directory}/nginx/${log_file_nginx} ${temp_dir}/nginx\n"
    cp -f -p -v "${log_directory}/nginx/${log_file_nginx}" "${temp_dir}/nginx" || true
done

printf "%b\n" "\nCopy lighttpd log files to the temporary directory ${temp_dir}\n"
for log_file_lighttpd in "${log_files_lighttpd[@]}"; do
    DIRECTORY="${temp_dir}/lighttpd"
    printf "%b\n" "\n${DIRECTORY}"

    if [ ! -d "${DIRECTORY}" ]; then
        # Control will enter here if ${DESTINATION} doesn't exist.
        mkdir -p -v "${DIRECTORY}"
        printf "%b\n" "\nThe directory ${DIRECTORY} is created\n"
    fi
    printf "%b\n" "\nCopying ${log_directory}/lighttpd/${log_file_lighttpd} ${temp_dir}/lighttpd\n"
    cp -f -p -v "${log_directory}/lighttpd/${log_file_lighttpd}" "${temp_dir}/lighttpd" || true
done

printf "%b\n" "\nCopy tomcat log files to the temporary directory ${temp_dir}\n"
for log_file_tomcat in "${log_files_tomcat[@]}"; do
    printf "%b\n" "\nCopying /${log_file_tomcat} ${temp_dir}"
    cp -f -p -r -v "${log_directory}/${log_file_tomcat}" "${temp_dir}" || true
done

printf "%b\n" "\nCopy Jenkins log files to the temporary directory ${temp_dir}\n"
for log_file_jenkins in "${log_files_jenkins[@]}"; do
    DIRECTORY="${temp_dir}/jenkins"
    printf "%b\n" "\n${DIRECTORY}"

    if [ ! -d "${DIRECTORY}" ]; then
        # Control will enter here if ${DESTINATION} doesn't exist.
        mkdir -p -v "${DIRECTORY}"
        printf "%b\n" "\nThe directory ${DIRECTORY} is created\n"
    fi
    printf "%b\n" "\nCopying ${log_directory}/jenkins/${log_file_jenkins} ${temp_dir}/jenkins\n"
    cp -f -p -v "${log_directory}/jenkins/${log_file_jenkins}" "${temp_dir}/jenkins" || true
done

printf "%b\n" "\nCopy qmail log files to the temporary directory ${temp_dir}\n"
for log_file_qmail in "${log_files_qmail[@]}"; do
    DIRECTORY="${temp_dir}/qmail"
    printf "%b\n" "\n${DIRECTORY}"

    if [ ! -d "${DIRECTORY}" ]; then
        # Control will enter here if ${DESTINATION} doesn't exist.
        mkdir -p -v "${DIRECTORY}"
        printf "%b\n" "\nThe directory ${DIRECTORY} is created\n"
    fi
    printf "%b\n" "\nCopying ${log_directory}/qmail/${log_file_qmail} ${temp_dir}/qmail\n"
    cp -f -p -v "${log_directory}/qmail/${log_file_qmail}" "${temp_dir}/qmail" || true
done

printf "%b\n" "\nCopy packages log files to the temporary directory ${temp_dir}"
for log_file_packages in "${log_files_packages[@]}"; do
    printf "%b\n" "\nCopying ${log_directory}/${log_file_packages} ${temp_dir}"
    cp -f -p -v "${log_directory}/${log_file_packages}" "${temp_dir}" || true
done

DIRECTORY="${temp_dir}/var/logs"
printf "%b\n" "\n${DIRECTORY}"

if [ ! -d "${DIRECTORY}" ]; then
        # Control will enter here if ${DESTINATION} doesn't exist.
        mkdir -p -v "${DIRECTORY}"
        printf "%b\n" "\nThe directory ${DIRECTORY} is created\n"
fi

printf "%b\n" "\nCopying ${log_directory} to ${temp_dir}/var/logs"
cp -f -p -r -v "${log_directory}"/* "${temp_dir}/var/logs" || true

printf "%b\n" "\nCopying ${target_directory}/log_lynis_* to ${temp_dir}"
cp -f -p -v "${target_directory}"/log_lynis_* "${temp_dir}" || true
printf "%b\n" "\nRedirect history to ${temp_dir}/history_${DIRDATE}.txt"
history | sort > "${temp_dir}/history_${DIRDATE}.txt" || true
printf "%b\n" "\ncat ${temp_dir}/history_${DIRDATE}.txt"
cat "${temp_dir}/history_${DIRDATE}.txt" || true

printf "%b\n" "\nCopying installed_packages* to ${temp_dir}"
cp -f -p --v installed_packages* "${temp_dir}" || true

# Remove existing archives in the target directory
printf "%b\n" "\nCleanup existing *.tar.gz files in ${target_directory}"
rm -f -v "${target_directory}/"*.tar.gz || true

printf "%b\n" "\nCleanup existing *.tgz files in ${target_directory}"
rm -f -v "${target_directory}/"*.tgz || true

# Compress the /etc files into a tar.gz archive
archive_filename="etc_${timestamp}.tar.gz"
printf "%b\n" "\nCompressing /etc files into ${archive_filename}"
tar -cvzf "${archive_filename}" -C "${temp_dir}" "/etc"

# Compress the log files into a tar.gz archive
archive_filename="logs_${timestamp}.tar.gz"
printf "%b\n" "\nCompressing log files into ${archive_filename}"
tar -cvzf "${archive_filename}" -C "${temp_dir}" .

# Move the archive to the target directory
printf "%b\n" "\nMoving ${archive_filename} to ${target_directory}"
mv -v "${archive_filename}" "${target_directory}" || true

printf "%b\n" "\nLog files archived and moved to ${target_directory}/${archive_filename}"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
