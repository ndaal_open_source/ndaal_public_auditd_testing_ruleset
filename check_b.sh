#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string,
# which may trigger antivirus software. Use with caution and only
# in controlled environments for legitimate testing purposes.
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
	trap - SIGINT SIGTERM ERR EXIT
	printf "%b\n" "\nInfo: Cleanup is running ..."
	# Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

DIRDATE="$(date +"%Y-%m-%d")"
readonly DIRDATE
printf "Info: Current date: %s\n" "${DIRDATE}"

# Function to increment the number in the audit.rules line starting with '-b'
increment_backlog_limit() {
    local filename="$1"
    local increment="$2"
    echo "filename: ${filename}"
    echo "increment: ${increment}"

    #sed -i 's/^\(-b \)[0-9]\+/\1'"$((increment + 1))"'/' "${filename}"
    #awk -v inc="$((increment + 1))" '/^-b [0-9]+$/ { sub(/^[0-9]+$/, inc); } { print }' "${filename}" > "${filename}.tmp" && mv "${filename}.tmp" "${filename}"
    #awk -v inc="$((increment + 1))" '/^-b [0-9]+$/ { sub(/[0-9]+/, inc, $2); } { print }' "${filename}" > "${filename}.tmp" && mv "${filename}.tmp" "${filename}"
    #cat "${filename}" | grep -m 1 "-b "
    #awk -v inc="$((increment + 1))" '/^-b [0-9]+$/ { sub(/[0-9]+/, inc); } { print }' "${filename}" > "${filename}.tmp" && mv "${filename}.tmp" "${filename}"
    #sed -E -i 's/^(-b )[0-9]+/\1'"$((increment + 1))"'/' "${filename}"
    #sed -i -E 's/^(-b )[0-9]+/\1'"$((increment + 1))"'/' "${filename}"
    #awk -v inc="$((increment + 1))" '/^(-b )[0-9]+/ { sub(/^[0-9]+/, inc); } 1' "${filename}" > "${filename}.tmp" && mv "${filename}.tmp" "${filename}"
    #awk -v inc="$increment" '/^(-b )[0-9]+/ { sub(/^[0-9]+/, $2 + inc); } 1' "${filename}" > "${filename}.tmp" && mv "${filename}.tmp" "${filename}"
    #sed -E -i.bak 's/^(-b )[0-9]+/\1'"$((increment + 1))"'/' "${filename}"
    #awk -v inc="$((increment + 1))" '/^-b [0-9]+/ {sub(/^[^0-9]+/, "-b " inc)} 1' "${filename}" > "${filename}.tmp" && mv "${filename}.tmp" "${filename}"
    #sed -Ei 's/^(-b )[0-9]+/\1'"$((increment + 1))"'/' "${filename}"
    #sed -i -E "s/^(-b )[0-9]+/\1$((increment + 1))/" "${filename}"
    #sed -Ei "s/^(-b )([0-9]+)/printf '%s%s' \"\1\" \"\$((\2 + increment))\"/e" "${filename}"
    #awk -v inc="${increment}" '/^(-b )[0-9]+/ {$2 = $2 + inc} 1' "${filename}" > "${filename}.tmp" && mv "${filename}.tmp" "${filename}"
    # shellcheck disable=SC2004
    sed -i 's/^\(-b \)[0-9]\+/\1'"$((i + ${start_value}))"'/' "${filename}"
    echo "${filename} updated"
    #echo "Add a longer delay (9 seconds) to allow changes to take effect"
    #sleep 9  # Add a longer delay (3 seconds) to allow changes to take effect
}

# Function to perform the audit rules replacement and restart auditd
#replace_and_restart_auditd "${audit_rules_file}"
#sleep 1  # Add a short delay (1 second) to allow changes to take effect

replace_and_restart_auditd() {
    local source_file="$1"
    local destination_file="/etc/audit/rules.d/audit.rules"
    echo "source_file: ${source_file}"
    echo "destination_file: ${destination_file}"

    sudo cp -v -f "${source_file}" "${destination_file}"
    sudo cp -v -f /home/vagrant/auditd.conf /etc/audit/auditd.conf
    sudo systemctl restart auditd || true
    echo "Add a longer delay (5 seconds) to allow changes to take effect"
    sleep 5  # Add a longer delay (5 seconds) to allow changes to take effect
    sudo auditctl -l || true
    echo "Add a longer delay (5 seconds) to allow changes to take effect"
    sleep 5  # Add a longer delay (5 seconds) to allow changes to take effect
    #sudo auditctl -s || true
}

# Function to search for the backlog_limit line and write to a file
search_backlog_limit() {
    local filename="$1"
    local output_file="$2"
    local value="$3"
    echo "filename: ${filename}"
    echo "output_file: ${output_file}"
    echo "value: ${value}"

    for ((i = 1; i <= value; i++)); do
        #sudo auditctl -s || true
        echo "sudo auditctl -s | grep -m 1 backlog_limit"
        backlog_line=$(sudo auditctl -s | grep -m 1 "backlog_limit") || true
        echo "Backlog Limit (Iteration $i): ${backlog_line}" >> "${output_file}"
        sudo auditctl -s >> "${output_file}" || true
    done
}

# Specify the input audit.rules file
audit_rules_file="/home/vagrant/audit.rules"

# Set the desired loop value
start_value=8192
loop_value=1000000
increment_value=10000
echo "Start Value: ${start_value}"
echo "Loop Value: ${loop_value}"
echo "Increment Value: ${increment_value}"

# Specify the output file for backlog_limit results
output_file="backlog_limit_results.txt"
echo "Output File: ${output_file}"

# rm -f -v "${output_file}"
# touch "${output_file}"
echo "We are starting at ${DIRDATE}" > "${output_file}"

# Search for backlog_limit in a loop and write results to a file
for ((i = 1; i < loop_value; i++)); do
    #increment_backlog_limit "${audit_rules_file}" "${i}"
    increment_backlog_limit "${audit_rules_file}" "${increment_value}"
    replace_and_restart_auditd "${audit_rules_file}"
    echo "Add a short delay (1 second) to allow changes to take effect"
    sleep 1  # Add a short delay (1 second) to allow changes to take effect
    #search_backlog_limit "${audit_rules_file}" "${output_file}" "${i}"
    search_backlog_limit "${audit_rules_file}" "${output_file}" "${increment_value}"
done


cleanup

script_name1="$(basename "${0}")"
printf '\n%s\n' "Info: script_name1: ${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf '\n%s\n' "Info: script_path1: ${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf '\n%s\n' "Info: Script path with name: ${script_path_with_name}"
printf '\n%s\n' "Info: Script finished"
exit 0
