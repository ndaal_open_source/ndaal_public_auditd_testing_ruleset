#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}
# 
echo "a2disconf
echo "Disable an Apache configuration file on Debian-based OSes.
echo "More information: <https://manpages.debian.org/latest/apache2/a2disconf.8.en.html>.
echo "Disable a configuration file:
sudo a2disconf {{configuration_file}}
echo "Don't show informative messages:
sudo a2disconf --quiet {{configuration_file}}
echo "a2dismod
echo "Disable an Apache module on Debian-based OSes.
echo "More information: <https://manpages.debian.org/latest/apache2/a2dismod.8.en.html>.
echo "Disable a module:
sudo a2dismod {{module}}
echo "Don't show informative messages:
sudo a2dismod --quiet {{module}}
echo "a2dissite
echo "Disable an Apache virtual host on Debian-based OSes.
echo "More information: <https://manpages.debian.org/latest/apache2/a2dissite.8.en.html>.
echo "Disable a virtual host:
sudo a2dissite {{virtual_host}}
echo "Don't show informative messages:
sudo a2dissite --quiet {{virtual_host}}
echo "a2enconf
echo "Enable an Apache configuration file on Debian-based OSes.
echo "More information: <https://manpages.debian.org/latest/apache2/a2enconf.8.en.html>.
echo "Enable a configuration file:
sudo a2enconf {{configuration_file}}
echo "Don't show informative messages:
sudo a2enconf --quiet {{configuration_file}}
echo "a2enmod
echo "Enable an Apache module on Debian-based OSes.
echo "More information: <https://manpages.debian.org/latest/apache2/a2enmod.8.en.html>.
echo "Enable a module:
sudo a2enmod {{module}}
echo "Don't show informative messages:
sudo a2enmod --quiet {{module}}
echo "a2ensite
echo "Enable an Apache virtual host on Debian-based OSes.
echo "More information: <https://manpages.debian.org/latest/apache2/a2ensite.8.en.html>.
echo "Enable a virtual host:
sudo a2ensite {{virtual_host}}
echo "Don't show informative messages:
sudo a2ensite --quiet {{virtual_host}}
echo "a2query
echo "Retrieve runtime configuration from Apache on Debian-based OSes.
echo "More information: <https://manpages.debian.org/latest/apache2/a2query.html>.
echo "List enabled Apache modules:
sudo a2query -m
echo "Check if a specific module is installed:
sudo a2query -m {{module_name}}
echo "List enabled virtual hosts:
sudo a2query -s
echo "Display the currently enabled Multi Processing Module:
sudo a2query -M
echo "Display the Apache version:
sudo a2query -v
echo "aa-complain
echo "Set an AppArmor policy to complain mode.
echo "See also: aa-disable`, `aa-enforce`, `aa-status.
echo "More information: <https://gitlab.com/apparmor/apparmor/-/wikis/manpage_aa-complain.8>.
echo "Set policy to complain mode:
sudo aa-complain {{path/to/profile}}
echo "Set policies to complain mode:
sudo aa-complain --dir {{path/to/profiles}}
echo "aa-disable
echo "Disable AppArmor security policy.
echo "See also: aa-complain`, `aa-enforce`, `aa-status.
echo "More information: <https://gitlab.com/apparmor/apparmor/-/wikis/manpage_aa-disable.8>.
echo "Disable profile:
sudo aa-disable {{path/to/profile}}
echo "Disable profiles:
sudo aa-disable --dir {{path/to/profiles}}
echo "aa-enforce
echo "Set an AppArmor profile to enforce mode.
echo "See also: aa-complain`, `aa-disable`, `aa-status.
echo "More information: <https://gitlab.com/apparmor/apparmor/-/wikis/manpage_aa-enforce.8>.
echo "Enable profile:
sudo aa-enforce {{path/to/profile}}
echo "Enable profiles:
sudo aa-enforce --dir {{path/to/profile}}
echo "aa-status
echo "List currently loaded AppArmor modules.
echo "See also: aa-complain`, `aa-disable`, `aa-enforce.
echo "More information: <https://gitlab.com/apparmor/apparmor/-/wikis/manpage_aa-status.8>.
echo "Check status:
sudo aa-status
echo "Display the number of loaded policies:
sudo aa-status --profiled
echo "Display the number of loaded enforicing policies:
sudo aa-status --enforced
echo "Display the number of loaded non-enforcing policies:
sudo aa-status --complaining
echo "Display the number of loaded enforcing policies that kill tasks:
sudo aa-status --kill
echo "abbr
echo "Manage abbreviations for the fish shell.
echo "User-defined words are replaced with longer phrases after they are entered.
echo "More information: <https://fishshell.com/docs/current/cmds/abbr.html>.
echo "Add a new abbreviation:
abbr --add {{abbreviation_name}} {{command}} {{command_arguments}}
echo "Rename an existing abbreviation:
abbr --rename {{old_name}} {{new_name}}
echo "Erase an existing abbreviation:
abbr --erase {{abbreviation_name}}
echo "Import the abbreviations defined on another host over SSH:
ssh {{host_name}} abbr --show | source
echo "abroot
echo "Utility providing full immutability and atomicity by transacting between 2 root partition states (A⟺B).
echo "Updates are performed using OCI images, to ensure that the system is always in a consistent state.
echo "More information: <https://github.com/Vanilla-OS/ABRoot>.
echo "Add packages to the local image (Note: after executing this command, you need to apply these changes.):
sudo abroot pkg add {{package}}
echo "Remove packages from the local image (Note: after executing this command, you need to apply these changes.):
sudo abroot pkg remove {{package}}
echo "List packages in the local image:
sudo abroot pkg list
echo "Apply changes in the local image (Note: you need to reboot your system for these changes to be applied):
sudo abroot pkg apply
echo "Rollback your system to previous state:
sudo abroot rollback
echo "Edit/View kernel parameters:
sudo abroot kargs {{edit|show}}
echo "Display status:
sudo abroot status
echo "Display help:
abroot --help
echo "ac
echo "Print statistics on how long users have been connected.
echo "More information: <https://www.gnu.org/software/acct/manual/accounting.html#ac>.
echo "Print how long the current user has been connected in hours:
ac
echo "Print how long users have been connected in hours:
ac --individual-totals
echo "Print how long a particular user has been connected in hours:
ac --individual-totals {{username}}
echo "Print how long a particular user has been connected in hours per day (with total):
ac --daily-totals --individual-totals {{username}}
echo "Also display additional details:
ac --compatibility
echo "acountry
echo "Print the country where an IPv4 address or hostname is located.
echo "More information: <https://manned.org/acountry>.
echo "Print a country where an IPv4 address or host is located:
acountry {{example.com}}
echo "Print extra [d]ebugging output:
acountry -d {{example.com}}
echo "Print more [v]erbose information:
acountry -v {{example.com}}
echo "acpi
echo "Shows battery status or thermal information.
echo "More information: <https://sourceforge.net/projects/acpiclient/files/acpiclient/>.
echo "Show battery information:
acpi
echo "Show thermal information:
acpi -t
echo "Show cooling device information:
acpi -c
echo "Show thermal information in Fahrenheit:
acpi -tf
echo "Show all information:
acpi -V
echo "Extract information from /proc` instead of `/sys:
acpi -p
echo "add-apt-repository
echo "Manages apt repository definitions.
echo "More information: <https://manned.org/apt-add-repository>.
echo "Add a new apt repository:
add-apt-repository {{repository_spec}}
echo "Remove an apt repository:
add-apt-repository --remove {{repository_spec}}
echo "Update the package cache after adding a repository:
add-apt-repository --update {{repository_spec}}
echo "Allow source packages to be downloaded from the repository:
add-apt-repository --enable-source {{repository_spec}}
echo "addpart
echo "Tell the Linux kernel about the existence of the specified partition.
echo "A simple wrapper around the add partition ioctl.
echo "More information: <https://manned.org/addpart>.
echo "Tell the kernel about the existence of the specified partition:
addpart {{device}} {{partition}} {{start}} {{length}}
echo "addr2line
echo "Convert addresses of a binary into file names and line numbers.
echo "More information: <https://manned.org/addr2line>.
echo "Display the filename and line number of the source code from an instruction address of an executable:
addr2line --exe={{path/to/executable}} {{address}}
echo "Display the function name, filename and line number:
addr2line --exe={{path/to/executable}} --functions {{address}}
echo "Demangle the function name for C++ code:
addr2line --exe={{path/to/executable}} --functions --demangle {{address}}
echo "adduser
echo "User addition utility.
echo "More information: <https://manpages.debian.org/latest/adduser/adduser.html>.
echo "Create a new user with a default home directory and prompt the user to set a password:
adduser {{username}}
echo "Create a new user without a home directory:
adduser --no-create-home {{username}}
echo "Create a new user with a home directory at the specified path:
adduser --home {{path/to/home}} {{username}}
echo "Create a new user with the specified shell set as the login shell:
adduser --shell {{path/to/shell}} {{username}}
echo "Create a new user belonging to the specified group:
adduser --ingroup {{group}} {{username}}
echo "adig
echo "Print information received from Domain Name System (DNS) servers.
echo "More information: <https://manned.org/adig>.
echo "Display A (default) record from DNS for hostname(s):
adig {{example.com}}
echo "Display extra [d]ebugging output:
adig -d {{example.com}}
echo "Connect to a specific DNS [s]erver:
adig -s {{1.2.3.4}} {{example.com}}
echo "Use a specific TCP port to connect to a DNS server:
adig -T {{port}} {{example.com}}
echo "Use a specific UDP port to connect to a DNS server:
adig -U {{port}} {{example.com}}
echo "ahost
echo "DNS lookup utility to display the A or AAAA record linked with a hostname or IP address.
echo "More information: <https://manned.org/ahost>.
echo "Print an A` or `AAAA record associated with a hostname or IP address:
ahost {{example.com}}
echo "Display some extra debugging output:
ahost -d {{example.com}}
echo "Display the record with a specified type:
ahost -t {{a|aaaa|u}} {{example.com}}
echo "alien
echo "Convert different installation packages to other formats.
echo "More information: <https://manned.org/alien>.
echo "Convert a specific installation file to Debian format (.deb extension):
sudo alien --to-deb {{path/to/file}}
echo "Convert a specific installation file to Red Hat format (.rpm extension):
sudo alien --to-rpm {{path/to/file}}
echo "Convert a specific installation file to a Slackware installation file (.tgz extension):
sudo alien --to-tgz {{path/to/file}}
echo "Convert a specific installation file to Debian format and install on the system:
sudo alien --to-deb --install {{path/to/file}}
echo "alpine
echo "An email client and Usenet newsgroup program with a pico/nano-inspired interface.
echo "Supports most modern email services through IMAP.
echo "More information: <https://manned.org/alpine>.
echo "Open alpine normally:
alpine
echo "Open alpine directly to the message composition screen to send an email to a given email address:
alpine {{email@example.net}}
echo "Quit alpine:
q + y
echo "alternatives
echo "This command is an alias of update-alternatives.
echo "More information: <https://manned.org/alternatives>.
echo "View documentation for the original command:
tldr update-alternatives
echo "amixer
echo "Mixer for ALSA soundcard driver.
echo "More information: <https://manned.org/amixer>.
echo "Turn up the master volume by 10%:
amixer -D pulse sset Master {{10%+}}
echo "Turn down the master volume by 10%:
amixer -D pulse sset Master {{10%-}}
echo "anbox
echo "Run Android applications on any GNU/Linux operating system.
echo "More information: <https://manned.org/anbox>.
echo "Launch Anbox into the app manager:
anbox launch --package={{org.anbox.appmgr}} --component={{org.anbox.appmgr.AppViewActivity}}
echo "apache2ctl
echo "Administrate the Apache HTTP web server.
echo "This command comes with Debian based OSes, for RHEL based ones see httpd.
echo "More information: <https://manpages.debian.org/latest/apache2/apache2ctl.8.en.html>.
echo "Start the Apache daemon. Throw a message if it is already running:
sudo apache2ctl start
# echo "Stop the Apache daemon:
# sudo apache2ctl stop
echo "Restart the Apache daemon:
sudo apache2ctl restart
echo "Test syntax of the configuration file:
sudo apache2ctl -t
echo "List loaded modules:
sudo apache2ctl -M
echo "apk
echo "Alpine Linux package management tool.
echo "More information: <https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management>.
echo "Update repository indexes from all remote repositories:
apk update
echo "Install a new package:
apk add {{package}}
echo "Remove a package:
apk del {{package}}
echo "Repair a package or upgrade it without modifying main dependencies:
apk fix {{package}}
echo "Search for a package via keywords:
apk search {{keywords}}
echo "Display information about a specific package:
apk info {{package}}
echo "aplay
echo "Command-line sound player for ALSA soundcard driver.
echo "More information: <https://manned.org/aplay>.
echo "Play a specific file (sampling rate, bit depth, etc. will be automatically determined for the file format):
aplay {{path/to/file}}
echo "Play the first 10 seconds of a specific file at 2500 Hz:
aplay --duration={{10}} --rate={{2500}} {{path/to/file}}
echo "Play the raw file as a 22050 Hz, mono, 8-bit, Mu-Law .au file:
aplay --channels={{1}} --file-type {{raw}} --rate={{22050}} --format={{mu_law}} {{path/to/file}}
echo "apport-bug
echo "File a bug report on Ubuntu.
echo "More information: <https://wiki.ubuntu.com/Apport>.
echo "Report a bug about the whole system:
apport-bug
echo "Report a bug about a specific package:
apport-bug {{package}}
echo "Report a bug about a specific executable:
apport-bug {{path/to/executable}}
echo "Report a bug about a specific process:
apport-bug {{PID}}
echo "apt-add-repository
echo "Manages apt repository definitions.
echo "More information: <https://manpages.debian.org/latest/software-properties-common/apt-add-repository.1.html>.
echo "Add a new apt repository:
apt-add-repository {{repository_spec}}
echo "Remove an apt repository:
apt-add-repository --remove {{repository_spec}}
echo "Update the package cache after adding a repository:
apt-add-repository --update {{repository_spec}}
echo "Enable source packages:
apt-add-repository --enable-source {{repository_spec}}
echo "apt-cache
echo "Debian and Ubuntu package query tool.
echo "More information: <https://manpages.debian.org/latest/apt/apt-cache.8.html>.
echo "Search for a package in your current sources:
apt-cache search {{query}}
echo "Show information about a package:
apt-cache show {{package}}
echo "Show whether a package is installed and up to date:
apt-cache policy {{package}}
echo "Show dependencies for a package:
apt-cache depends {{package}}
echo "Show packages that depend on a particular package:
apt-cache rdepends {{package}}
echo "apt-file
echo "Search for files in apt packages, including ones not yet installed.
echo "More information: <https://manpages.debian.org/latest/apt-file/apt-file.1.html>.
echo "Update the metadata database:
sudo apt update
echo "Search for packages that contain the specified file or path:
apt-file {{search|find}} {{partial_path/to/file}}
echo "List the contents of a specific package:
apt-file {{show|list}} {{package}}
echo "Search for packages that match the regular_expression:
apt-file {{search|find}} --regexp {{regular_expression}}
echo "apt-get
echo "Debian and Ubuntu package management utility.
echo "Search for packages using apt-cache.
echo "More information: <https://manpages.debian.org/latest/apt/apt-get.8.html>.
echo "Update the list of available packages and versions (it's recommended to run this before other apt-get commands):
apt-get update
echo "Install a package, or update it to the latest available version:
apt-get install {{package}}
echo "Remove a package:
apt-get remove {{package}}
echo "Remove a package and its configuration files:
apt-get purge {{package}}
echo "Upgrade all installed packages to their newest available versions:
apt-get upgrade
echo "Clean the local repository - removing package files (.deb) from interrupted downloads that can no longer be downloaded:
apt-get autoclean
echo "Remove all packages that are no longer needed:
apt-get autoremove
echo "Upgrade installed packages (like upgrade), but remove obsolete packages and install additional packages to meet new dependencies:
apt-get dist-upgrade
echo "apt-key
echo "Key management utility for the APT Package Manager on Debian and Ubuntu.
echo "Note: apt-key` is now deprecated (except for the use of `apt-key del in maintainer scripts).
echo "More information: <https://manpages.debian.org/latest/apt/apt-key.8.html>.
echo "List trusted keys:
apt-key list
echo "Add a key to the trusted keystore:
apt-key add {{public_key_file.asc}}
echo "Delete a key from the trusted keystore:
apt-key del {{key_id}}
echo "Add a remote key to the trusted keystore:
wget -qO - {{https://host.tld/filename.key}} | apt-key add -
echo "Add a key from keyserver with only key id:
apt-key adv --keyserver {{pgp.mit.edu}} --recv {{KEYID}}
echo "apt-mark
echo "Utility to change the status of installed packages.
echo "More information: <https://manpages.debian.org/latest/apt/apt-mark.8.html>.
echo "Mark a package as automatically installed:
sudo apt-mark auto {{package}}
echo "Hold a package at its current version and prevent updates to it:
sudo apt-mark hold {{package}}
echo "Allow a package to be updated again:
sudo apt-mark unhold {{package}}
echo "Show manually installed packages:
apt-mark showmanual
echo "Show held packages that aren't being updated:
apt-mark showhold
echo "apt moo
echo "An APT easter egg.
echo "More information: <https://manpages.debian.org/latest/apt/apt.8.html>.
echo "Print a cow easter egg:
apt moo
echo "apt
echo "Package management utility for Debian based distributions.
echo "Recommended replacement for apt-get when used interactively in Ubuntu versions 16.04 and later.
echo "For equivalent commands in other package managers, see <https://wiki.archlinux.org/title/Pacman/Rosetta>.
echo "More information: <https://manpages.debian.org/latest/apt/apt.8.html>.
echo "Update the list of available packages and versions (it's recommended to run this before other apt commands):
sudo apt update
echo "Search for a given package:
apt search {{package}}
echo "Show information for a package:
apt show {{package}}
echo "Install a package, or update it to the latest available version:
sudo apt install {{package}}
echo "Remove a package (using purge instead also removes its configuration files):
sudo apt remove {{package}}
echo "Upgrade all installed packages to their newest available versions:
sudo apt upgrade
echo "List all packages:
apt list
echo "List installed packages:
apt list --installed
echo "aptitude
echo "Debian and Ubuntu package management utility.
echo "More information: <https://manpages.debian.org/latest/aptitude/aptitude.8.html>.
echo "Synchronize list of packages and versions available. This should be run first, before running subsequent aptitude commands:
aptitude update
echo "Install a new package and its dependencies:
aptitude install {{package}}
echo "Search for a package:
aptitude search {{package}}
echo "Search for an installed package (?installed` is an `aptitude search term):
aptitude search '?installed({{package}})'
echo "Remove a package and all packages depending on it:
aptitude remove {{package}}
echo "Upgrade installed packages to the newest available versions:
aptitude upgrade
echo "Upgrade installed packages (like aptitude upgrade) including removing obsolete packages and installing additional packages to meet new package dependencies:
aptitude full-upgrade
echo "Put an installed package on hold to prevent it from being automatically upgraded:
aptitude hold '?installed({{package}})'
echo "apx pkgmanagers
echo "Manage package managers in apx.
echo "Note: user-created package manager configurations are stored in ~/.local/share/apx/pkgmanagers.
echo "More information: <https://github.com/Vanilla-OS/apx>.
echo "Interactively create a new package manager configuration:
apx pkgmanagers create
echo "List all available package manager confirgurations:
apx pkgmanagers list
echo "Remove a package manager configuration:
apx pkgmanagers rm --name {{string}}
echo "Display information about a specific package manager:
apx pkgmanagers show {{name}}
echo "apx stacks
echo "Manage stacks in apx.
echo "Note: user-created stack configurations are stored in ~/.local/share/apx/stacks.
echo "More information: <https://github.com/Vanilla-OS/apx>.
echo "Interactively create a new stack configuration:
apx stacks new
echo "Interactively update a stack configuration:
apx stacks update {{name}}
echo "List all available stack configurations:
apx stacks list
echo "Remove a specified stack configuration:
apx stacks rm --name {{string}}
echo "Import a stack configuration:
apx stacks import --input {{path/to/stack.yml}}
echo "Export the stack configuration (Note: the output flag is optional, it is exported to the current working directory by default):
apx stacks export --name {{string}} --output {{path/to/output_file}}
echo "apx subsystems
echo "Manage subsystems in apx.
echo "Subsystems are containers that can be created based on pre-existing stacks.
echo "More information: <https://github.com/Vanilla-OS/apx>.
echo "Interactively create a new subsystem:
apx subsystems new
echo "List all available subsystems:
apx subsystems list
echo "Reset a specific subsystem to its initial state:
apx subsystems reset --name {{string}}
echo "[f]orce reset a specific subsystem:
apx subsystems reset --name {{string}} --force
echo "Remove a specific subsystem:
apx subsystems rm --name {{string}}
echo "[f]orce remove a specific subsystem:
apx subsystems rm --name {{string}} --force
echo "apx
echo "Package management utility with support for multiple sources, allowing you to install packages in subsystems.
echo "More information: <https://github.com/Vanilla-OS/apx>.
echo "View documentation for managing package managers:
tldr apx pkgmanagers
echo "View documentation for managing stacks:
tldr apx stacks
echo "View documentation for managing subsystems:
tldr apx subsystems
echo "arch-chroot
echo "Enhanced chroot command to help in the Arch Linux installation process.
echo "More information: <https://man.archlinux.org/man/arch-chroot.8>.
echo "Start an interactive shell (bash, by default) in a new root directory:
arch-chroot {{path/to/new/root}}
echo "Specify the user (other than the current user) to run the shell as:
arch-chroot -u {{user}} {{path/to/new/root}}
echo "Run a custom command (instead of the default bash) in the new root directory:
arch-chroot {{path/to/new/root}} {{command}} {{command_arguments}}
echo "Specify the shell, other than the default bash` (in this case, the `zsh package should have been installed in the target system):
arch-chroot {{path/to/new/root}} {{zsh}}
echo "archey
echo "Simple tool for stylishly displaying system information.
echo "More information: <https://lclarkmichalek.github.io/archey3/>.
echo "Show system information:
archey
echo "archinstall
echo "Guided Arch Linux installer with a twist.
echo "More information: <https://archinstall.readthedocs.io>.
echo "Start the interactive installer:
archinstall
echo "Start a preset installer:
archinstall {{minimal|unattended}}
echo "archivemount
echo "Mount an archive for access as a filesystem.
echo "More information: <https://manned.org/archivemount>.
echo "Mount an archive to a specific mountpoint:
archivemount {{path/to/archive}} {{path/to/mount_point}}
echo "archlinux-java
echo "Switch between installed Java environments.
echo "More information: <https://wiki.archlinux.org/title/Java#Switching_between_JVM>.
echo "List installed Java environments:
archlinux-java status
echo "Return the short name of the current default Java environment:
archlinux-java get
echo "Set the default Java environment:
archlinux-java set {{java_environment}}
echo "Unset the default Java environment:
archlinux-java unset
echo "Fix an invalid/broken default Java environment configuration:
archlinux-java fix
echo "arecord
echo "Sound recorder for ALSA soundcard driver.
echo "More information: <https://manned.org/arecord>.
echo "Record a snippet in "CD" quality (finish with Ctrl-C when done):
arecord -vv --format=cd {{path/to/file.wav}}
echo "Record a snippet in "CD" quality, with a fixed duration of 10 seconds:
arecord -vv --format=cd --duration={{10}} {{path/to/file.wav}}
echo "Record a snippet and save it as an MP3 (finish with Ctrl-C when done):
arecord -vv --format=cd --file-type raw | lame -r - {{path/to/file.mp3}}
echo "List all sound cards and digital audio devices:
arecord --list-devices
echo "Allow interactive interface (e.g. use space-bar or enter to play or pause):
arecord --interactive
echo "arithmetic
echo "Quiz on simple arithmetic problems.
echo "More information: <https://manpages.debian.org/latest/bsdgames/arithmetic.6.en.html>.
echo "Start an arithmetic quiz:
arithmetic
echo "Specify one or more arithmetic [o]peration symbols to get problems on them:
arithmetic -o {{+|-|x|/}}
echo "Specify a range. Addition and multiplication problems would feature numbers between 0 and range, inclusive. Subtraction and division problems would have required result and number to be operated on, between 0 and range:
arithmetic -r {{7}}
echo "ark
echo "KDE's archiving tool.
echo "More information: <https://docs.kde.org/stable5/en/ark/ark/>.
echo "Extract a specific archive into the current directory:
ark --batch {{path/to/archive}}
echo "Extract an archive into a specific directory:
ark --batch --destination {{path/to/directory}} {{path/to/archive}}
echo "Create an archive if it does not exist and add specific files to it:
ark --add-to {{path/to/archive}} {{path/to/file1 path/to/file2 ...}}
echo "arpaname
echo "Provides corresponding ARPA name for IP addresses.
echo "More information: <https://manned.org/arpaname>.
echo "Translate IP addresses (IPv4 and IPv6) to the corresponding ARPA name:
arpaname {{ip_address}}
echo "arpspoof
echo "Forge ARP replies to intercept packets.
echo "More information: <https://monkey.org/~dugsong/dsniff>.
echo "Poison all hosts to intercept packets on [i]nterface for the host:
sudo arpspoof -i {{wlan0}} {{host_ip}}
echo "Poison [t]arget to intercept packets on [i]nterface for the host:
sudo arpspoof -i {{wlan0}} -t {{target_ip}} {{host_ip}}
echo "Poison both [t]arget and host to intercept packets on [i]nterface for the host:
sudo arpspoof -i {{wlan0}} -r -t {{target_ip}} {{host_ip}}
echo "as
echo "Portable GNU assembler.
echo "Primarily intended to assemble output from gcc` to be used by `ld.
echo "More information: <https://manned.org/as>.
echo "Assemble a file, writing the output to a.out:
as {{file.s}}
echo "Assemble the output to a given file:
as {{file.s}} -o {{out.o}}
echo "Generate output faster by skipping whitespace and comment preprocessing. (Should only be used for trusted compilers):
as -f {{file.s}}
echo "Include a given path to the list of directories to search for files specified in .include directives:
as -I {{path/to/directory}} {{file.s}}
echo "ascii
echo "Show ASCII character aliases.
echo "More information: <http://www.catb.org/~esr/ascii/>.
echo "Show ASCII aliases of a character:
ascii {{a}}
echo "Show ASCII aliases in short, script-friendly mode:
ascii -t {{a}}
echo "Show ASCII aliases of multiple characters:
ascii -s {{tldr}}
echo "Show ASCII table in decimal:
ascii -d
echo "Show ASCII table in hexadecimal:
ascii -x
echo "Show ASCII table in octal:
ascii -o
echo "Show ASCII table in binary:
ascii -b
echo "Show options summary and complete ASCII table:
ascii
echo "asciiart
echo "Convert images to ASCII.
echo "More information: <https://github.com/nodanaonlyzuul/asciiart>.
echo "Read an image from a file and print in ASCII:
asciiart {{path/to/image.jpg}}
echo "Read an image from a URL and print in ASCII:
asciiart {{www.example.com/image.jpg}}
echo "Choose the output width (default is 100):
asciiart --width {{50}} {{path/to/image.jpg}}
echo "Colorize the ASCII output:
asciiart --color {{path/to/image.jpg}}
echo "Choose the output format (default format is text):
asciiart --format {{text|html}} {{path/to/image.jpg}}
echo "Invert the character map:
asciiart --invert-chars {{path/to/image.jpg}}
echo "aspell
echo "Interactive spell checker.
echo "More information: <http://aspell.net/>.
echo "Spell check a single file:
aspell check {{path/to/file}}
echo "List misspelled words from stdin:
cat {{path/to/file}} | aspell list
echo "Show available dictionary languages:
aspell dicts
echo "Run aspell with a different language (takes two-letter ISO 639 language code):
aspell --lang={{cs}}
echo "List misspelled words from stdin and ignore words from personal word list:
cat {{path/to/file}} | aspell --personal={{personal-word-list.pws}} list
echo "asterisk
echo "Telephone and exchange (phone) server.
echo "Used for running the server itself, and managing an already running instance.
echo "More information: <https://wiki.asterisk.org/wiki/display/AST/Home>.
echo "[R]econnect to a running server, and turn on logging 3 levels of [v]erbosity:
asterisk -r -vvv
echo "[R]econnect to a running server, run a single command, and return:
asterisk -r -x "{{command}}"
echo "Show chan_SIP clients (phones):
asterisk -r -x "sip show peers"
echo "Show active calls and channels:
asterisk -r -x "core show channels"
echo "Show voicemail mailboxes:
asterisk -r -x "voicemail show users"
echo "Terminate a channel:
asterisk -r -x "hangup request {{channel_ID}}"
echo "Reload chan_SIP configuration:
asterisk -r -x "sip reload"
echo "at
echo "Executes commands at a specified time.
echo "More information: <https://man.archlinux.org/man/at.1>.
echo "Open an at` prompt to create a new set of scheduled commands, press `Ctrl + D to save and exit:
at {{hh:mm}}
echo "Execute the commands and email the result using a local mailing program such as Sendmail:
at {{hh:mm}} -m
echo "Execute a script at the given time:
at {{hh:mm}} -f {{path/to/file}}
echo "Display a system notification at 11pm on February 18th:
echo "notify-send '{{Wake up!}}'" | at {{11pm}} {{Feb 18}}
echo "atool
echo "Manage archives of various formats.
echo "More information: <https://www.nongnu.org/atool/>.
echo "List files in a zip archive:
atool --list {{path/to/archive.zip}}
echo "Unpack a tar.gz archive into a new subdirectory (or current directory if it contains only one file):
atool --extract {{path/to/archive.tar.gz}}
echo "Create a new 7zip archive with two files:
atool --add {{path/to/archive.7z}} {{path/to/file1 path/to/file2 ...}}
echo "Extract all zip and rar archives in the current directory:
atool --each --extract {{*.zip *.rar}}
# echo "atop
echo "Linux system and process monitor.
# echo "More information: <https://manned.org/atop>.
echo "Start:
# atop
echo "Start and display memory consumption for each process:
# atop -m
echo "Start and display disk information:
# atop -d
echo "Start and display background process information:
# atop -c
echo "Start and display thread-specific resource utilization information:
# atop -y
echo "Start and display the number of processes for each user:
# atop -au
echo "Display help about interactive commands:
?
echo "aura
echo "The Aura Package Manager: A secure, multilingual package manager for Arch Linux and the AUR.
echo "More information: <https://github.com/fosskers/aura>.
echo "Search for packages from the official repositories and AUR:
aura --aursync --both --search {{keyword|regular_expression}}
echo "Install a package from the AUR:
aura --aursync {{package}}
echo "Update all AUR packages in a verbose mode and remove all make dependencies:
aura --aursync --diff --sysupgrade --delmakedeps --unsuppress
echo "Install a package from the official repositories:
aura --sync {{package}}
echo "Synchronize and update all packages from the official repositories:
aura --sync --refresh --sysupgrade
echo "Downgrade a package using the package cache:
aura --downgrade {{package}}
echo "Remove a package and its dependencies:
aura --remove --recursive --unneeded {{package}}
echo "Remove orphan packages (installed as dependencies but not required by any package):
aura --orphans --abandon
echo "auracle
echo "Command-line tool used to interact with Arch Linux's User Repository, commonly referred to as the AUR.
echo "More information: <https://github.com/falconindy/auracle>.
echo "Display AUR packages that match a regular expression:
auracle search '{{regular_expression}}'
echo "Display package information for a space-separated list of AUR packages:
auracle info {{package1}} {{package2}}
echo "Display the PKGBUILD file (build information) for a space-separated list of AUR packages:
auracle show {{package1}} {{package2}}
echo "Display updates for installed AUR packages:
auracle outdated
echo "aurman
echo "An Arch Linux utility to build and install packages from the Arch User Repository.
echo "See also pacman.
echo "More information: <https://github.com/polygamma/aurman>.
echo "Synchronize and update all packages:
aurman --sync --refresh --sysupgrade
echo "Synchronize and update all packages without show changes of PKGBUILD files:
aurman --sync --refresh --sysupgrade --noedit
echo "Install a new package:
aurman --sync {{package}}
echo "Install a new package without show changes of PKGBUILD files:
aurman --sync --noedit {{package}}
echo "Install a new package without prompting:
aurman --sync --noedit --noconfirm {{package}}
echo "Search the package database for a keyword from the official repositories and AUR:
aurman --sync --search {{keyword}}
echo "Remove a package and its dependencies:
aurman --remove --recursive --nosave {{package}}
echo "Clear the package cache (use two --clean flags to clean all packages):
aurman --sync --clean
echo "aurvote
echo "Vote for packages in the Arch User Repository.
echo "To be able to vote, the file ~/.config/aurvote must exist and contain your AUR credentials.
echo "More information: <https://github.com/archlinuxfr/aurvote>.
echo "Interactively create the file ~/.config/aurvote containing your AUR username and password:
aurvote --configure
echo "Vote for one or more AUR packages:
aurvote {{package1 package2 ...}}
echo "Unvote one or more AUR packages:
aurvote --unvote {{package1 package2 ...}}
echo "Check if one or more AUR packages have already been voted:
aurvote --check {{package1 package2 ...}}
echo "Display help:
aurvote --help
echo "ausyscall
echo "Program that allows mapping syscall names and numbers.
echo "More information: <https://manned.org/ausyscall>.
echo "Display syscall number of a specific system call:
ausyscall {{search_pattern}}
echo "Display name of a specific system call number:
ausyscall {{system_call_number}}
echo "Display all system calls for a specific architecture:
ausyscall {{architecture}} --dump
echo "authconfig
echo "Configure system authentication resources.
echo "More information: <https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system-level_authentication_guide/authconfig-install>.
echo "Display the current configuration (or dry run):
authconfig --test
echo "Configure the server to use a different password hashing algorithm:
authconfig --update --passalgo={{algorithm}}
echo "Enable LDAP authentication:
authconfig --update --enableldapauth
echo "Disable LDAP authentication:
authconfig --update --disableldapauth
echo "Enable Network Information Service (NIS):
authconfig --update --enablenis
echo "Enable Kerberos:
authconfig --update --enablekrb5
echo "Enable Winbind (Active Directory) authentication:
authconfig --update --enablewinbindauth
echo "Enable local authorization:
authconfig --update --enablelocauthorize
echo "auto-cpufreq
echo "Automatic CPU speed & power optimizer.
echo "More information: <https://github.com/AdnanHodzic/auto-cpufreq>.
echo "Run auto-cpufreq in a specific mode:
sudo auto-cpufreq --{{monitor|live|update|remove|stats|force=governor}}
# echo "autopkgtest
echo "Run tests on Debian packages.
# echo "More information: <https://wiki.debian.org/ContinuousIntegration/autopkgtest>.
echo "Build the package in the current directory and run all tests directly on the system:
# autopkgtest -- {{null}}
echo "Run a specific test for the package in the current directory:
# autopkgtest --test-name={{test_name}} -- {{null}}
echo "Download and build a specific package with apt-get, then run all tests:
# autopkgtest {{package}} -- {{null}}
echo "Test the package in the current directory using a new root directory:
# autopkgtest -- {{chroot}} {{path/to/new/root}}
echo "Test the package in the current directory without rebuilding it:
# autopkgtest --no-built-binaries -- {{null}}
echo "autorandr
echo "Automatically change screen layout.
echo "More information: <https://github.com/phillipberndt/autorandr>.
echo "Save the current screen layout:
autorandr --save {{profile_name}}
echo "Show the saved profiles:
autorandr
echo "Load the first detected profile:
autorandr --change
echo "Load a specific profile:
autorandr --load {{profile_name}}
echo "Set the default profile:
autorandr --default {{profile_name}}
echo "avahi-browse
echo "Displays services and hosts exposed on the local network via mDNS/DNS-SD.
echo "Avahi is compatible with Bonjour (Zeroconf) found in Apple devices.
echo "More information: <https://www.avahi.org/>.
echo "List services available on the local network along with their addresses and ports, ignoring ones on the local machine:
avahi-browse --all --resolve --ignore-local
echo "Quickly list services in the local network in SSV format for scripts:
avahi-browse --all --terminate --parsable
echo "List domains in the neighbourhood:
avahi-browse --browse-domains
echo "Limit the search to a particular domain:
avahi-browse --all --domain={{domain}}
echo "avahi-resolve
echo "Translate between host names and IP Addresses.
echo "More information: <https://www.avahi.org/>.
echo "Resolve a local service to its IPv4:
avahi-resolve -4 --name {{service.local}}
echo "Resolve an IP to a hostname, verbosely:
avahi-resolve --verbose --address {{IP}}
echo "avifenc
echo "AV1 Image File Format (AVIF) encoder.
echo "More information: <https://aomediacodec.github.io/av1-avif/>.
echo "Convert a specific PNG image to AVIF:
avifenc {{path/to/input.png}} {{path/to/output.avif}}
echo "Encode with a specific speed (6=default, 0=slowest and 10=fastest):
avifenc --speed {{2}} {{path/to/input.png}} {{path/to/output.avif}}
echo "backlight_control
echo "Control a linux machine's backlight using percentage values.
echo "More information: <https://github.com/Hendrikto/backlight_control>.
echo "Increase/decrease the backlight by a specific percent count:
backlight_control {{+|-}}{{5}}
echo "Set the backlight strength to a specific percent count:
backlight_control {{90}}
echo "Print the help:
backlight_control
echo "balooctl
echo "File indexing and searching framework for KDE Plasma.
echo "More information: <https://wiki.archlinux.org/index.php/Baloo>.
echo "Display help:
balooctl
echo "Display the status of the indexer:
balooctl status
echo "Enable/Disable the file indexer:
balooctl {{enable|disable}}
echo "Clean the index database:
balooctl purge
echo "Suspend the file indexer:
balooctl suspend
echo "Resume the file indexer:
balooctl resume
echo "Display the disk space used by Baloo:
balooctl indexSize
echo "Check for any unindexed files and index them:
balooctl check
echo "batcat
echo "This command is an alias of bat.
echo "More information: <https://github.com/sharkdp/bat>.
echo "View documentation for the original command:
tldr bat
# echo "battop
echo "An interactive viewer for the batteries installed in your notebook.
# echo "More information: <https://github.com/svartalf/rust-battop>.
echo "Display battery information:
# battop
echo "Change battery information measurement [u]nit (default: human):
# battop -u {{human|si}}
echo "bcachefs
echo "Manage bcachefs filesystems/devices.
echo "More information: <https://bcachefs.org/bcachefs-principles-of-operation.pdf>.
echo "Format a partition with bcachefs:
sudo bcachefs format {{path/to/partition}}
echo "Mount a bcachefs filesystem:
sudo bcachefs mount {{path/to/partition}} {{path/to/mountpoint}}
echo "Create a RAID 0 filesystem where an SSD acts as a cache and an HDD acts as a long-term storage:
sudo bcachefs format --label=ssd.ssd1 {{path/to/ssd/partition}} --label=hdd.hdd1 {{path/to/hdd/partition}} --replicas=1 --foreground_target=ssd --promote_target=ssd --background_target=hdd
echo "Mount a multidevice filesystem:
sudo bcachefs mount {{path/to/partition1}}:{{path/to/partition2}} {{path/to/mountpoint}}
echo "Display disk usage:
bcachefs fs usage --human-readable {{path/to/mountpoint}}
echo "Display help:
bcachefs
echo "bchunk
echo "Convert CD images to a set of .iso` and `.cdr tracks.
echo "More information: <http://he.fi/bchunk>.
echo "Convert binary CD into a standard iso9960 image file:
bchunk {{path/to/image.bin}} {{path/to/image.cue}} {{path/to/output}}
echo "Convert with verbose mode:
bchunk -v {{path/to/image.bin}} {{path/to/image.cue}} {{path/to/output}}
echo "Output audio files in WAV format:
bchunk -w {{path/to/image.bin}} {{path/to/image.cue}} {{path/to/output}}
echo "beep
echo "A utility to beep the PC speaker.
echo "More information: <https://github.com/spkr-beep/beep>.
echo "Play a beep:
beep
echo "Play a beep that repeats:
beep -r {{repetitions}}
echo "Play a beep at a specified frequency (Hz) and duration (milliseconds):
beep -f {{frequency}} -l {{duration}}
echo "Play each new frequency and duration as a distinct beep:
beep -f {{frequency}} -l {{duration}} -n -f {{frequency}} -l {{duration}}
echo "Play the C major scale:
beep -f {{262}} -n -f {{294}} -n -f {{330}} -n -f {{349}} -n -f {{392}} -n -f {{440}} -n -f {{494}} -n -f {{523}}
echo "betterdiscordctl
echo "A program for managing BetterDiscord on Linux.
echo "More information: <https://github.com/bb010g/betterdiscordctl#manual>.
echo "Install BetterDiscord on Discord Stable:
sudo betterdiscordctl install
echo "Install BetterDiscord on Discord Canary:
sudo betterdiscordctl --d-flavors canary install
echo "Install BetterDiscord on Discord PTB:
sudo betterdiscordctl --d-flavors ptb install
echo "Install BetterDiscord on Discord installed with Flatpak:
sudo betterdiscordctl --d-install flatpak install
echo "Install BetterDiscord on Discord installed with Snap:
sudo betterdiscordctl --d-install snap install
echo "betterlockscreen
echo "Simple, minimal lock screen.
echo "More information: <https://github.com/pavanjadhaw/betterlockscreen>.
echo "Lock the screen:
betterlockscreen --lock
echo "Change the lock screen background:
betterlockscreen -u {{path/to/image.png}}
echo "Lock the screen, showing some custom text:
betterlockscreen -l pixel -t "{{custom lock screen text}}"
echo "Lock the screen, with a custom monitor off timeout in seconds:
betterlockscreen --off {{5}} -l
echo "bitwise
echo "Multi base interactive calculator supporting dynamic base conversion and bit manipulation.
echo "More information: <https://github.com/mellowcandle/bitwise>.
echo "Run using interactive mode:
bitwise
echo "Convert from decimal:
bitwise {{12345}}
echo "Convert from hexadecimal:
bitwise {{0x563d}}
echo "Convert a C-style calculation:
bitwise "{{0x123 + 0x20 - 30 / 50}}"
echo "blastn
echo "Nucleotide-Nucleotide BLAST.
echo "More information: <https://www.ncbi.nlm.nih.gov/books/NBK279684/table/appendices.T.blastn_application_options/>.
echo "Align two or more sequences using megablast (default), with the e-value threshold of 1e-9, pairwise output format (default):
blastn -query {{query.fa}} -subject {{subject.fa}} -evalue {{1e-9}}
echo "Align two or more sequences using blastn:
blastn -task blastn -query {{query.fa}} -subject {{subject.fa}}
echo "Align two or more sequences, custom tabular output format, output to file:
blastn -query {{query.fa}} -subject {{subject.fa}} -outfmt {{'6 qseqid qlen qstart qend sseqid slen sstart send bitscore evalue pident'}} -out {{output.tsv}}
echo "Search nucleotide databases using a nucleotide query, 16 threads (CPUs) to use in the BLAST search, with a maximum number of 10 aligned sequences to keep:
blastn -query {{query.fa}} -db {{path/to/blast_db}} -num_threads {{16}} -max_target_seqs {{10}}
echo "Search the remote non-redundant nucleotide database using a nucleotide query:
blastn -query {{query.fa}} -db {{nt}} -remote
echo "Display help (use -help for detailed help):
blastn -h
echo "blastp
echo "Protein-Protein BLAST.
echo "More information: <https://www.ncbi.nlm.nih.gov/books/NBK279684/table/appendices.T.blastp_application_options/>.
echo "Align two or more sequences using blastp, with the e-value threshold of 1e-9, pairwise output format, output to screen:
blastp -query {{query.fa}} -subject {{subject.fa}} -evalue {{1e-9}}
echo "Align two or more sequences using blastp-fast:
blastp -task blastp-fast -query {{query.fa}} -subject {{subject.fa}}
echo "Align two or more sequences, custom tabular output format, output to file:
blastp -query {{query.fa}} -subject {{subject.fa}} -outfmt '{{6 qseqid qlen qstart qend sseqid slen sstart send bitscore evalue pident}}' -out {{output.tsv}}
echo "Search protein databases using a protein query, 16 threads to use in the BLAST search, with a maximum number of 10 aligned sequences to keep:
blastp -query {{query.fa}} -db {{blast_database_name}} -num_threads {{16}} -max_target_seqs {{10}}
echo "Search the remote non-redundant protein database using a protein query:
blastp -query {{query.fa}} -db {{nr}} -remote
echo "Display help (use -help for detailed help):
blastp -h
echo "blight
echo "Utility for changing the display brightness.
echo "More information: <https://github.com/gutjuri/blight>.
echo "Set display brightness to 50%:
blight set {{50}} -r
echo "Show current display brightness:
blight show
echo "Print maximum display brightness:
blight max
echo "Increase display brightness in %:
blight inc {{number}} -r
echo "Decrease display brightness with internal units:
blight dec {{number}}
echo "blkdiscard
echo "Discards device sectors on storage devices. Useful for SSDs.
echo "More information: <https://manned.org/blkdiscard>.
echo "Discard all sectors on a device, removing all data:
blkdiscard /dev/{{device}}
echo "Securely discard all blocks on a device, removing all data:
blkdiscard --secure /dev/{{device}}
echo "Discard the first 100 MB of a device:
blkdiscard --length {{100MB}} /dev/{{device}}
echo "blkid
echo "Lists all recognized partitions and their Universally Unique Identifier (UUID).
echo "More information: <https://manned.org/blkid>.
echo "List all partitions:
sudo blkid
echo "List all partitions in a table, including current mountpoints:
sudo blkid -o list
echo "bluetoothctl
echo "Manage Bluetooth devices.
echo "More information: <https://bitbucket.org/serkanp/bluetoothctl>.
echo "Enter the bluetoothctl shell:
bluetoothctl
echo "List all known devices:
bluetoothctl devices
echo "Power the Bluetooth controller on or off:
bluetoothctl power {{on|off}}
echo "Pair with a device:
bluetoothctl pair {{mac_address}}
echo "Remove a device:
bluetoothctl remove {{mac_address}}
echo "Connect to a paired device:
bluetoothctl connect {{mac_address}}
echo "Disconnect from a paired device:
bluetoothctl disconnect {{mac_address}}
echo "Display help:
bluetoothctl help
echo "bluetoothd
echo "Daemon to manage bluetooth devices.
echo "More information: <https://manned.org/bluetoothd>.
echo "Start the daemon:
bluetoothd
echo "Start the daemon, logging to stdout:
bluetoothd --nodetach
echo "Start the daemon with a specific configuration file (defaults to /etc/bluetooth/main.conf):
bluetoothd --configfile {{path/to/file}}
echo "Start the daemon with verbose output to stderr:
bluetoothd --debug
echo "Start the daemon with verbose output coming from specific files in the bluetoothd or plugins source:
bluetoothd --debug={{path/to/file1}}:{{path/to/file2}}:{{path/to/file3}}
echo "blurlock
echo "A simple wrapper around the i3 screen locker i3lock, which blurs the screen.
echo "See also: i3lock.
echo "More information: <https://gitlab.manjaro.org/packages/community/i3/i3exit/-/blob/master/blurlock>.
echo "Lock the screen to a blurred screenshot of the current screen:
blurlock
echo "Lock the screen and disable the unlock indicator (removes feedback on keypress):
blurlock --no-unlock-indicator
echo "Lock the screen and don't hide the mouse pointer:
blurlock --pointer {{default}}
echo "Lock the screen and show the number of failed login attempts:
blurlock --show-failed-attempts
echo "bmon
echo "Monitor bandwidth and capture network related statistics.
echo "More information: <https://github.com/tgraf/bmon>.
echo "Display the list of all the interfaces:
bmon -a
echo "Display data transfer rates in bits per second:
bmon -b
echo "Set policy to define which network interface(s) is/are displayed:
bmon -p {{interface_1,interface_2,interface_3}}
echo "Set interval (in seconds) in which rate per counter is calculated:
bmon -R {{2.0}}
echo "boltctl
echo "Control thunderbolt devices.
echo "More information: <https://manned.org/boltctl>.
echo "List connected (and authorized) devices:
boltctl
echo "List connected devices, including unauthorized ones:
boltctl list
echo "Authorize a device temporarily:
boltctl authorize {{device_uuid}}
echo "Authorize and remember a device:
boltctl enroll {{device_uuid}}
echo "Revoke a previously authorized device:
boltctl forget {{device_uuid}}
echo "Show more information about a device:
boltctl info {{device_uuid}}
echo "bootctl
echo "Control EFI firmware boot settings and manage boot loader.
echo "More information: <https://manned.org/bootctl>.
echo "Show information about the system firmware and the bootloaders:
bootctl status
echo "Show all available bootloader entries:
bootctl list
echo "Set a flag to boot into the system firmware on the next boot (similar to sudo systemctl reboot --firmware-setup):
sudo bootctl reboot-to-firmware true
echo "Specify the path to the EFI system partition (defaults to /efi/`, `/boot/` or `/boot/efi):
bootctl --esp-path={{/path/to/efi_system_partition/}}
echo "Install systemd-boot into the EFI system partition:
sudo bootctl install
echo "Remove all installed versions of systemd-boot from the EFI system partition:
sudo bootctl remove
echo "bpftool
echo "Tool for inspection and simple manipulation of eBPF programs and maps.
echo "Some subcommands such as bpftool prog have their own usage documentation.
echo "More information: <https://manned.org/bpftool>.
echo "List information about loaded eBPF programs:
bpftool prog list
echo "List eBPF program attachments in the kernel networking subsystem:
bpftool net list
echo "List all active links:
bpftool link list
echo "List all raw_tracepoint`, `tracepoint`, `kprobe attachments in the system:
bpftool perf list
echo "List BPF Type Format (BTF) data:
bpftool btf list
echo "List information about loaded maps:
bpftool map list
echo "Probe a network device "eth0" for supported eBPF features:
bpftool feature probe dev {{eth0}}
echo "Run commands in batch mode from a file:
bpftool batch file {{myfile}}
echo "bpftrace
echo "High-level tracing language for Linux eBPF.
echo "More information: <https://github.com/iovisor/bpftrace>.
echo "Display bpftrace version:
bpftrace -V
echo "List all available probes:
sudo bpftrace -l
echo "Run a one-liner program (e.g. syscall count by program):
sudo bpftrace -e '{{tracepoint:raw_syscalls:sys_enter { @[comm] = count(); }}}'
echo "Run a program from a file:
sudo bpftrace {{path/to/file}}
echo "Trace a program by PID:
sudo bpftrace -e '{{tracepoint:raw_syscalls:sys_enter /pid == 123/ { @[comm] = count(); }}}'
echo "Do a dry run and display the output in eBPF format:
sudo bpftrace -d -e '{{one_line_program}}'
echo "br
echo "Navigate directory trees interactively.
echo "See also: broot.
echo "More information: <https://github.com/Canop/broot>.
echo "Start and navigate the current directory tree interactively:
br
echo "Start displaying the size of files and directories:
br --sizes
echo "Start displaying permissions:
br --permissions
echo "Start displaying directories only:
br --only-folders
echo "Start displaying hidden files and directories:
br --hidden
echo "braa
echo "Ultra-fast mass SNMP scanner allowing multiple hosts simultaneously.
echo "More information: <https://github.com/mteg/braa>.
echo "Walk the SNMP tree of host with public string querying all OIDs under .1.3.6:
braa public@{{ip}}:{{.1.3.6.*}}
echo "Query the whole subnet ip_range` for `system.sysLocation.0:
braa public@{{ip_range}}:{{.1.3.6.1.2.1.1.6.0}}
echo "Attempt to set the value of system.sysLocation.0 to a specific workgroup:
braa private@{{ip}}:{{.1.3.6.1.2.1.1.6.0}}=s'{{workgroup}}'
echo "brctl
echo "Ethernet bridge administration.
echo "More information: <https://manned.org/brctl>.
echo "Show a list with information about currently existing Ethernet bridges:
sudo brctl show
echo "Create a new Ethernet bridge interface:
sudo brctl add {{bridge_name}}
echo "Delete an existing Ethernet bridge interface:
sudo brctl del {{bridge_name}}
echo "Add an interface to an existing bridge:
sudo brctl addif {{bridge_name}} {{interface_name}}
echo "Remove an interface from an existing bridge:
sudo brctl delif {{bridge_name}} {{interface_name}}
echo "brightnessctl
echo "Utility for reading and controlling device brightness for GNU/Linux operating systems.
echo "More information: <https://github.com/Hummer12007/brightnessctl>.
echo "List devices with changeable brightness:
brightnessctl --list
echo "Print the current brightness of the display backlight:
brightnessctl get
echo "Set the brightness of the display backlight to a specified percentage within range:
brightnessctl set {{50%}}
echo "Increase brightness by a specified increment:
brightnessctl set {{+10%}}
echo "Decrease brightness by a specified decrement:
brightnessctl set {{10%-}}
echo "broot
echo "Navigate directory trees interactively.
echo "See also: br.
echo "More information: <https://github.com/Canop/broot>.
echo "Install or reinstall the br shell function:
broot --install
echo "bspc
echo "A tool to control bspwm.
echo "More information: <https://github.com/baskerville/bspwm>.
# echo "Define two virtual desktop:
# bspc monitor --reset-desktops {{1}} {{2}}
# echo "Focus the given desktop:
# bspc desktop --focus {{number}}
echo "Close the windows rooted at the selected node:
bspc node --close
# echo "Send the selected node to the given desktop:
# bspc node --to-desktop {{number}}
echo "Toggle full screen mode for the selected node:
bspc node --state ~fullscreen
echo "bspwm
echo "A tiling window manager based on binary space partitioning.
echo "More information: <https://github.com/baskerville/bspwm>.
echo "Start bspwm (note that a pre-existing window manager must not be open when this command is run):
bspwm -c {{path/to/config}}
echo "View documentation for bspc:
tldr bspc
echo "btrbk
echo "A tool for creating snapshots and remote backups of btrfs subvolumes.
echo "More information: <https://digint.ch/btrbk/doc/readme.html>.
echo "Print statistics about configured subvolumes and snapshots:
sudo btrbk stats
echo "List configured subvolumes and snapshots:
sudo btrbk list
echo "Print what would happen in a run without making the displayed changes:
sudo btrbk --verbose dryrun
echo "Run backup routines verbosely, show progress bar:
sudo btrbk --progress --verbose run
echo "Only create snapshots for configured subvolumes:
sudo btrbk snapshot
echo "btrfs balance
echo "Balance block groups on a btrfs filesystem.
echo "More information: <https://btrfs.readthedocs.io/en/latest/btrfs-balance.html>.
echo "Show the status of a running or paused balance operation:
sudo btrfs balance status {{path/to/btrfs_filesystem}}
echo "Balance all block groups (slow; rewrites all blocks in filesystem):
sudo btrfs balance start {{path/to/btrfs_filesystem}}
echo "Balance data block groups which are less than 15% utilized, running the operation in the background:
sudo btrfs balance start --bg -dusage={{15}} {{path/to/btrfs_filesystem}}
echo "Balance a max of 10 metadata chunks with less than 20% utilization and at least 1 chunk on a given device devid` (see `btrfs filesystem show):
sudo btrfs balance start -musage={{20}},limit={{10}},devid={{devid}} {{path/to/btrfs_filesystem}}
echo "Convert data blocks to the raid6 and metadata to raid1c3 (see mkfs.btrfs(8) for profiles):
sudo btrfs balance start -dconvert={{raid6}} -mconvert={{raid1c3}} {{path/to/btrfs_filesystem}}
echo "Convert data blocks to raid1, skipping already converted chunks (e.g. after a previous cancelled conversion operation):
sudo btrfs balance start -dconvert={{raid1}},soft {{path/to/btrfs_filesystem}}
echo "Cancel, pause, or resume a running or paused balance operation:
sudo btrfs balance {{cancel|pause|resume}} {{path/to/btrfs_filesystem}}
echo "btrfs check
echo "Check or repair a btrfs filesystem.
echo "More information: <https://btrfs.readthedocs.io/en/latest/btrfs-check.html>.
echo "Check a btrfs filesystem:
sudo btrfs check {{path/to/partition}}
echo "Check and repair a btrfs filesystem (dangerous):
sudo btrfs check --repair {{path/to/partition}}
echo "Show the progress of the check:
sudo btrfs check --progress {{path/to/partition}}
echo "Verify the checksum of each data block (if the filesystem is good):
sudo btrfs check --check-data-csum {{path/to/partition}}
echo "Use the n`-th superblock (`n can be 0, 1 or 2):
sudo btrfs check --super {{n}} {{path/to/partition}}
echo "Rebuild the checksum tree:
sudo btrfs check --repair --init-csum-tree {{path/to/partition}}
echo "Rebuild the extent tree:
sudo btrfs check --repair --init-extent-tree {{path/to/partition}}
echo "btrfs device
echo "Manage devices in a btrfs filesystem.
echo "More information: <https://btrfs.readthedocs.io/en/latest/btrfs-device.html>.
echo "Add one or more devices to a btrfs filesystem:
sudo btrfs device add {{path/to/block_device1}} [{{path/to/block_device2}}] {{path/to/btrfs_filesystem}}
echo "Remove a device from a btrfs filesystem:
sudo btrfs device remove {{path/to/device|device_id}} [{{...}}]
echo "Display error statistics:
sudo btrfs device stats {{path/to/btrfs_filesystem}}
echo "Scan all disks and inform the kernel of all detected btrfs filesystems:
sudo btrfs device scan --all-devices
echo "Display detailed per-disk allocation statistics:
sudo btrfs device usage {{path/to/btrfs_filesystem}}
echo "btrfs filesystem
echo "Manage btrfs filesystems.
echo "More information: <https://btrfs.readthedocs.io/en/latest/btrfs-filesystem.html>.
echo "Show filesystem usage (optionally run as root to show detailed information):
btrfs filesystem usage {{path/to/btrfs_mount}}
echo "Show usage by individual devices:
sudo btrfs filesystem show {{path/to/btrfs_mount}}
echo "Defragment a single file on a btrfs filesystem (avoid while a deduplication agent is running):
sudo btrfs filesystem defragment -v {{path/to/file}}
echo "Defragment a directory recursively (does not cross subvolume boundaries):
sudo btrfs filesystem defragment -v -r {{path/to/directory}}
echo "Force syncing unwritten data blocks to disk(s):
sudo btrfs filesystem sync {{path/to/btrfs_mount}}
echo "Summarize disk usage for the files in a directory recursively:
sudo btrfs filesystem du --summarize {{path/to/directory}}
echo "btrfs inspect-internal
echo "Query internal information of a btrfs filesystem.
echo "More information: <https://btrfs.readthedocs.io/en/latest/btrfs-inspect-internal.html>.
echo "Print superblock's information:
sudo btrfs inspect-internal dump-super {{path/to/partition}}
echo "Print superblock's and all of its copies' information:
sudo btrfs inspect-internal dump-super --all {{path/to/partition}}
echo "Print filesystem's metadata information:
sudo btrfs inspect-internal dump-tree {{path/to/partition}}
echo "Print list of files in inode n-th:
sudo btrfs inspect-internal inode-resolve {{n}} {{path/to/btrfs_mount}}
echo "Print list of files at a given logical address:
sudo btrfs inspect-internal logical-resolve {{logical_address}} {{path/to/btrfs_mount}}
echo "Print stats of root, extent, csum and fs trees:
sudo btrfs inspect-internal tree-stats {{path/to/partition}}
echo "btrfs property
echo "Get, set, or list properties for a given btrfs filesystem object (files, directories, subvolumes, filesystems, or devices).
echo "More information: <https://btrfs.readthedocs.io/en/latest/btrfs-property.html>.
echo "List available properties (and descriptions) for the given btrfs object:
sudo btrfs property list {{path/to/btrfs_object}}
echo "Get all properties for the given btrfs object:
sudo btrfs property get {{path/to/btrfs_object}}
echo "Get the label property for the given btrfs filesystem or device:
sudo btrfs property get {{path/to/btrfs_filesystem}} label
echo "Get all object type-specific properties for the given btrfs filesystem or device:
sudo btrfs property get -t {{subvol|filesystem|inode|device}} {{path/to/btrfs_filesystem}}
echo "Set the compression property for a given btrfs inode (either a file or directory):
sudo btrfs property set {{path/to/btrfs_inode}} compression {{zstd|zlib|lzo|none}}
echo "btrfs rescue
echo "Try to recover a damaged btrfs filesystem.
echo "More information: <https://btrfs.readthedocs.io/en/latest/btrfs-rescue.html>.
echo "Rebuild the filesystem metadata tree (very slow):
sudo btrfs rescue chunk-recover {{path/to/partition}}
echo "Fix device size alignment related problems (e.g. unable to mount the filesystem with super total bytes mismatch):
sudo btrfs rescue fix-device-size {{path/to/partition}}
echo "Recover a corrupted superblock from correct copies (recover the root of filesystem tree):
sudo btrfs rescue super-recover {{path/to/partition}}
echo "Recover from an interrupted transactions (fixes log replay problems):
sudo btrfs rescue zero-log {{path/to/partition}}
echo "Create a /dev/btrfs-control` control device when `mknod is not installed:
sudo btrfs rescue create-control-device
echo "btrfs restore
echo "Try to salvage files from a damaged btrfs filesystem.
echo "More information: <https://btrfs.readthedocs.io/en/latest/btrfs-restore.html>.
echo "Restore all files from a btrfs filesystem to a given directory:
sudo btrfs restore {{path/to/btrfs_device}} {{path/to/target_directory}}
echo "List (don't write) files to be restored from a btrfs filesystem:
sudo btrfs restore --dry-run {{path/to/btrfs_device}} {{path/to/target_directory}}
echo "Restore files matching a given regex ([c]ase-insensitive) files to be restored from a btrfs filesystem (all parent directories of target file(s) must match as well):
sudo btrfs restore --path-regex {{regex}} -c {{path/to/btrfs_device}} {{path/to/target_directory}}
echo "Restore files from a btrfs filesystem using a specific root tree bytenr` (see `btrfs-find-root):
sudo btrfs restore -t {{bytenr}} {{path/to/btrfs_device}} {{path/to/target_directory}}
echo "Restore files from a btrfs filesystem (along with metadata, extended attributes, and Symlinks), overwriting files in the target:
sudo btrfs restore --metadata --xattr --symlinks --overwrite {{path/to/btrfs_device}} {{path/to/target_directory}}
echo "btrfs scrub
echo "Scrub btrfs filesystems to verify data integrity.
echo "It is recommended to run a scrub once a month.
echo "More information: <https://btrfs.readthedocs.io/en/latest/btrfs-scrub.html>.
echo "Start a scrub:
sudo btrfs scrub start {{path/to/btrfs_mount}}
echo "Show the status of an ongoing or last completed scrub:
sudo btrfs scrub status {{path/to/btrfs_mount}}
echo "Cancel an ongoing scrub:
sudo btrfs scrub cancel {{path/to/btrfs_mount}}
echo "Resume a previously cancelled scrub:
sudo btrfs scrub resume {{path/to/btrfs_mount}}
echo "Start a scrub, but wait until the scrub finishes before exiting:
sudo btrfs scrub start -B {{path/to/btrfs_mount}}
echo "Start a scrub in quiet mode (does not print errors or statistics):
sudo btrfs scrub start -q {{path/to/btrfs_mount}}
echo "btrfs subvolume
echo "Manage btrfs subvolumes and snapshots.
echo "More information: <https://btrfs.readthedocs.io/en/latest/btrfs-subvolume.html>.
echo "Create a new empty subvolume:
sudo btrfs subvolume create {{path/to/new_subvolume}}
echo "List all subvolumes and snapshots in the specified filesystem:
sudo btrfs subvolume list {{path/to/btrfs_filesystem}}
echo "Delete a subvolume:
sudo btrfs subvolume delete {{path/to/subvolume}}
echo "Create a read-only snapshot of an existing subvolume:
sudo btrfs subvolume snapshot -r {{path/to/source_subvolume}} {{path/to/target}}
echo "Create a read-write snapshot of an existing subvolume:
sudo btrfs subvolume snapshot {{path/to/source_subvolume}} {{path/to/target}}
echo "Show detailed information about a subvolume:
sudo btrfs subvolume show {{path/to/subvolume}}
echo "btrfs version
echo "Display btrfs-progs version.
echo "More information: <https://btrfs.readthedocs.io/en/latest/btrfs.html>.
echo "Display btrfs-progs version:
btrfs version
echo "Display help:
btrfs version --help
echo "btrfs
echo "A filesystem based on the copy-on-write (COW) principle for Linux.
echo "Some subcommands such as btrfs device have their own usage documentation.
echo "More information: <https://btrfs.readthedocs.io/en/latest/btrfs.html>.
echo "Create subvolume:
sudo btrfs subvolume create {{path/to/subvolume}}
echo "List subvolumes:
sudo btrfs subvolume list {{path/to/mount_point}}
echo "Show space usage information:
sudo btrfs filesystem df {{path/to/mount_point}}
echo "Enable quota:
sudo btrfs quota enable {{path/to/subvolume}}
echo "Show quota:
sudo btrfs qgroup show {{path/to/subvolume}}
echo "bully
echo "Brute-force the WPS pin of a wireless access point.
echo "Necessary information must be gathered with airmon-ng` and `airodump-ng` before using `bully.
echo "More information: <https://salsa.debian.org/pkg-security-team/bully>.
echo "Crack the password:
bully --bssid "{{mac}}" --channel "{{channel}}" --bruteforce "{{interface}}"
echo "Display help:
bully --help
echo "burpsuite
echo "A GUI based application mainly used in web application penetration testing.
# echo "More information: <https://portswigger.net/burp/documentation/desktop/troubleshooting/launch-from-command-line>.
echo "Start Burp Suite:
burpsuite
echo "Start Burp Suite using the default configuration:
burpsuite --use-defaults
echo "Open a specific project file:
burpsuite --project-file={{path/to/file}}
echo "Load a specific configuration file:
burpsuite --config-file={{path/to/file}}
echo "Start without extensions:
burpsuite --disable-extensions
echo "busctl
echo "Introspect and monitor the D-Bus bus.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/busctl.html>.
echo "Show all peers on the bus, by their service names:
busctl list
echo "Show process information and credentials of a bus service, a process, or the owner of the bus (if no parameter is specified):
busctl status {{service|pid}}
echo "Dump messages being exchanged. If no service is specified, show all messages on the bus:
busctl monitor {{service1 service2 ...}}
echo "Show an object tree of one or more services (or all services if no service is specified):
busctl tree {{service1 service2 ...}}
echo "Show interfaces, methods, properties and signals of the specified object on the specified service:
busctl introspect {{service}} {{path/to/object}}
echo "Retrieve the current value of one or more object properties:
busctl get-property {{service}} {{path/to/object}} {{interface_name}} {{property_name}}
echo "Invoke a method and show the response:
busctl call {{service}} {{path/to/object}} {{interface_name}} {{method_name}}
echo "byzanz-record
echo "Record the screen.
echo "More information: <https://manned.org/byzanz-record>.
echo "Record the screen and write the recording to a file (by default, byzanz-record will only record for 10 seconds):
byzanz-record {{path/to/file.[byzanz|flv|gif|ogg|ogv|webm]}}
echo "Show information while and after recording:
byzanz-record --verbose {{path/to/file.[byzanz|flv|gif|ogg|ogv|webm]}}
echo "Record the screen for a minute:
byzanz-record --duration 60 {{path/to/file.[byzanz|flv|gif|ogg|ogv|webm]}}
echo "Delay recording for 10 seconds:
byzanz-record --delay 10 {{path/to/file.[byzanz|flv|gif|ogg|ogv|webm]}}
echo "caffeinate
# echo "Prevent desktop from sleeping.
echo "More information: <https://manned.org/caffeinate>.
# echo "Prevent desktop from sleeping (use Ctrl + C to exit):
caffeinate
echo "caffeine-indicator
# echo "Manually inhibit desktop idleness with a toggle.
echo "More information: <https://manned.org/caffeine-indicator>.
# echo "Manually inhibit desktop idleness with a toggle:
caffeine-indicator
echo "caffeine
# echo "Prevent desktop idleness in full-screen mode.
echo "More information: <https://manned.org/caffeine>.
echo "Start a caffeine server:
caffeine
echo "Display help:
caffeine --help
echo "Display version:
caffeine --version
echo "caja
# echo "Manages files and directories in MATE desktop environment.
echo "More information: <https://manned.org/caja>.
echo "Open the current user home directory:
caja
echo "Open specific directories in separate windows:
caja {{path/to/directory1 path/to/directory2 ...}}
echo "Open specific directories in tabs:
caja --tabs {{path/to/directory1 path/to/directory2 ...}}
echo "Open a directory with a specific window size:
caja --geometry={{600}}x{{400}} {{path/to/directory}}
echo "Close all windows:
caja --quit
echo "cal
echo "Prints calendar information, with the current day highlighted.
echo "More information: <https://manned.org/cal>.
echo "Display a calendar for the current month:
cal
echo "Display previous, current and next month:
cal -3
echo "Use Monday as the first day of the week:
cal --monday
echo "Display a calendar for a specific year (4 digits):
cal {{year}}
echo "Display a calendar for a specific month and year:
cal {{month}} {{year}}
echo "calcurse
echo "A text-based calendar and scheduling application for the command-line.
echo "More information: <https://calcurse.org>.
echo "Start calcurse on interactive mode:
calcurse
echo "Print the appointments and events for the current day and exit:
calcurse --appointment
echo "Remove all local calcurse items and import remote objects:
calcurse-caldav --init=keep-remote
echo "Remove all remote objects and push local calcurse items:
calcurse-caldav --init=keep-local
echo "Copy local objects to the CalDAV server and vice versa:
calcurse-caldav --init=two-way
echo "cam
echo "Frontend tool for libcamera.
echo "More information: <https://libcamera.org/docs.html>.
echo "List available cameras:
cam --list
echo "List controls of a camera:
cam --camera {{camera_index}} --list-controls
echo "Write frames to a folder:
cam --camera {{camera_index}} --capture={{frames_to_capture}} --file
echo "Display camera feed in a window:
cam --camera {{camera_index}} --capture --sdl
echo "cat
echo "Print and concatenate files.
echo "More information: <https://www.gnu.org/software/coreutils/cat>.
echo "Print the contents of a file to stdout:
cat {{path/to/file}}
echo "Concatenate several files into an output file:
cat {{path/to/file1 path/to/file2 ...}} > {{path/to/output_file}}
echo "Append several files to an output file:
cat {{path/to/file1 path/to/file2 ...}} >> {{path/to/output_file}}
echo "Copy the contents of a file into an output file in [u]nbuffered mode:
cat -u {{/dev/tty12}} > {{/dev/tty13}}
echo "Write stdin to a file:
cat - > {{path/to/file}}
echo "[n]umber all output lines:
cat -n {{path/to/file}}
echo "Display non-printable and whitespace characters (with M- prefix if non-ASCII):
cat -v -t -e {{path/to/file}}
echo "cbatticon
echo "A lightweight and fast battery icon that sits in your system tray.
echo "More information: <https://github.com/valr/cbatticon>.
echo "Show the battery icon in the system tray:
cbatticon
echo "Show the battery icon and set the update interval to 20 seconds:
cbatticon --update-interval {{20}}
echo "List available icon types:
cbatticon --list-icon-types
echo "Show the battery icon with a specific icon type:
cbatticon --icon-type {{standard|notification|symbolic}}
echo "List available power supplies:
cbatticon --list-power-supplies
echo "Show the battery icon for a specific battery:
cbatticon {{BAT0}}
echo "Show the battery icon and which command to execute when the battery level reaches the set critical level:
cbatticon --critical-level {{5}} --command-critical-level {{poweroff}}
echo "cc
echo "This command is an alias of gcc.
echo "More information: <https://gcc.gnu.org>.
echo "View documentation for the original command:
tldr gcc
echo "ceph
echo "A unified storage system.
echo "More information: <https://ceph.io>.
echo "Check cluster health status:
ceph status
echo "Check cluster usage stats:
ceph df
echo "Get the statistics for the placement groups in a cluster:
ceph pg dump --format {{plain}}
echo "Create a storage pool:
ceph osd pool create {{pool_name}} {{page_number}}
echo "Delete a storage pool:
ceph osd pool delete {{pool_name}}
echo "Rename a storage pool:
ceph osd pool rename {{current_name}} {{new_name}}
echo "Self-repair pool storage:
ceph pg repair {{pool_name}}
echo "certbot
echo "The Let's Encrypt Agent for automatically obtaining and renewing TLS certificates.
echo "Successor to letsencrypt.
echo "More information: <https://certbot.eff.org/docs/using.html>.
echo "Obtain a new certificate via webroot authorization, but do not install it automatically:
sudo certbot certonly --webroot --webroot-path {{path/to/webroot}} --domain {{subdomain.example.com}}
echo "Obtain a new certificate via nginx authorization, installing the new certificate automatically:
sudo certbot --nginx --domain {{subdomain.example.com}}
echo "Obtain a new certificate via apache authorization, installing the new certificate automatically:
sudo certbot --apache --domain {{subdomain.example.com}}
echo "Renew all Let's Encrypt certificates that expire in 30 days or less (don't forget to restart any servers that use them afterwards):
sudo certbot renew
echo "Simulate the obtaining of a new certificate, but don't actually save any new certificates to disk:
sudo certbot --webroot --webroot-path {{path/to/webroot}} --domain {{subdomain.example.com}} --dry-run
echo "Obtain an untrusted test certificate instead:
sudo certbot --webroot --webroot-path {{path/to/webroot}} --domain {{subdomain.example.com}} --test-cert
echo "cewl
echo "URL spidering tool for making a cracking wordlist from web content.
echo "More information: <https://digi.ninja/projects/cewl.php>.
echo "Create a wordlist file from the given URL up to 2 links depth:
cewl --depth {{2}} --write {{path/to/wordlist.txt}} {{url}}
echo "Output an alphanumeric wordlist from the given URL with words of minimum 5 characters:
cewl --with-numbers --min_word_length {{5}} {{url}}
echo "Output a wordlist from the given URL in debug mode including email addresses:
cewl --debug --email {{url}}
echo "Output a wordlist from the given URL using HTTP Basic or Digest authentication:
cewl --auth_type {{basic|digest}} --auth_user {{username}} --auth_pass {{password}} {{url}}
echo "Output a wordlist from the given URL through a proxy:
cewl --proxy_host {{host}} --proxy_port {{port}} {{url}}
echo "cfdisk
echo "A program for managing partition tables and partitions on a hard disk using a curses UI.
echo "More information: <https://manned.org/cfdisk>.
echo "Start the partition manipulator with a specific device:
cfdisk {{/dev/sdX}}
echo "Create a new partition table for a specific device and manage it:
cfdisk --zero {{/dev/sdX}}
echo "cgclassify
echo "Move running task(s) to given cgroups.
echo "More information: <https://manned.org/cgclassify>.
echo "Move the process with a specific PID to the control group student in the CPU hierarchy:
cgclassify -g {{cpu:student}} {{1234}}
echo "Move the process with a specific PID to control groups based on the /etc/cgrules.conf configuration file:
cgclassify {{1234}}
echo "Move the process with a specific PID to the control group student in the CPU hierarchy. Note: The daemon of the service cgred` does not change `cgroups` of the specific PID and its children (based on `/etc/cgrules.conf):
cgclassify --sticky -g {{cpu:/student}} {{1234}}
echo "cgcreate
echo "Create cgroups, used to limit, measure, and control resources used by processes.
echo "cgroups` types can be `memory`, `cpu`, `net_cls, etc.
echo "More information: <https://manned.org/cgcreate>.
echo "Create a new group:
cgcreate -g {{group_type}}:{{group_name}}
echo "Create a new group with multiple cgroup types:
cgcreate -g {{group_type1}},{{group_type2}}:{{group_name}}
echo "Create a subgroup:
mkdir /sys/fs/cgroup/{{group_type}}/{{group_name}}/{{subgroup_name}}
echo "cgexec
echo "Limit, measure, and control resources used by processes.
echo "Multiple cgroup types (aka controllers) exist, such as cpu`, `memory, etc.
echo "More information: <https://manned.org/cgexec>.
echo "Execute a process in a given cgroup with given controller:
cgexec -g {{controller}}:{{cgroup_name}} {{process_name}}
echo "cgroups
echo "Cgroups aka control groups is a Linux kernel feature for limiting, measuring, and controlling resource usage by processes.
echo "Cgroups however is not a command, but rather a collection of commands, see the relevant pages below.
echo "More information: <https://www.kernel.org/doc/Documentation/cgroup-v2.txt>.
echo "Show the tldr page for cgclassify:
tldr cgclassify
echo "Show the tldr page for cgcreate:
tldr cgcreate
echo "Show the tldr page for cgexec:
tldr cgexec
echo "chage
echo "Change user account and password expiry information.
echo "More information: <https://manned.org/chage>.
echo "List password information for the user:
chage --list {{username}}
echo "Enable password expiration in 10 days:
sudo chage --maxdays {{10}} {{username}}
echo "Disable password expiration:
sudo chage --maxdays {{-1}} {{username}}
echo "Set account expiration date:
sudo chage --expiredate {{YYYY-MM-DD}} {{username}}
echo "Force user to change password on next log in:
sudo chage --lastday {{0}} {{username}}
echo "chatgpt
echo "Shell script to use OpenAI's ChatGPT and DALL-E from the terminal.
echo "More information: <https://github.com/0xacx/chatGPT-shell-cli>.
echo "Start in chat mode:
chatgpt
echo "Give a [p]rompt to answer to:
chatgpt --prompt "{{What is the regex to match an email address?}}"
echo "Start in chat mode using a specific [m]odel (default is gpt-3.5-turbo):
chatgpt --model {{gpt-4}}
echo "Start in chat mode with an [i]nitial prompt:
chatgpt --init-prompt "{{You are Rick, from Rick and Morty. Respond to questions using his mannerism and include insulting jokes.}}"
echo "Pipe the result of a command to chatgpt as a prompt:
echo "{{How to view running processes on Ubuntu?}}" | chatgpt
echo "Generate an image using DALL-E:
chatgpt --prompt "{{image: A white cat}}"
echo "chattr
echo "Change attributes of files or directories.
echo "More information: <https://manned.org/chattr>.
echo "Make a file or directory immutable to changes and deletion, even by superuser:
chattr +i {{path/to/file_or_directory}}
echo "Make a file or directory mutable:
chattr -i {{path/to/file_or_directory}}
echo "Recursively make an entire directory and contents immutable:
chattr -R +i {{path/to/directory}}
echo "chcon
echo "Change SELinux security context of a file or files/directories.
echo "More information: <https://www.gnu.org/software/coreutils/chcon>.
echo "View security context of a file:
ls -lZ {{path/to/file}}
echo "Change the security context of a target file, using a reference file:
chcon --reference={{reference_file}} {{target_file}}
echo "Change the full SELinux security context of a file:
chcon {{user}}:{{role}}:{{type}}:{{range/level}} {{filename}}
echo "Change only the user part of SELinux security context:
chcon -u {{user}} {{filename}}
echo "Change only the role part of SELinux security context:
chcon -r {{role}} {{filename}}
echo "Change only the type part of SELinux security context:
chcon -t {{type}} {{filename}}
echo "Change only the range/level part of SELinux security context:
chcon -l {{range/level}} {{filename}}
echo "chcpu
echo "Enable/disable a system's CPUs.
echo "More information: <https://manned.org/chcpu>.
echo "Disable CPUs via a list of CPU ID numbers:
chcpu -d {{1,3}}
echo "Enable a set of CPUs via a range of CPU ID numbers:
chcpu -e {{1-10}}
echo "check-language-support
echo "Display a list of missing language packages on Ubuntu.
echo "More information: <https://manpages.ubuntu.com/manpages/latest/man1/check-language-support.html>.
echo "Display a list of missing language packages based on installed software and enabled locales:
check-language-support
echo "List packages for a specific locale:
check-language-support --language {{en}}
echo "Display installed packages as well as missing ones:
check-language-support --show-installed
echo "check-support-status
echo "Identify installed Debian packages for which support has had to be limited or prematurely ended.
echo "More information: <https://manpages.debian.org/latest/debian-security-support/check-support-status.html>.
echo "Display packages whose support is limited, has already ended or will end earlier than the distribution's end of life:
check-support-status
echo "Display only packages whose support has ended:
check-support-status --type {{ended}}
echo "Skip printing a headline:
check-support-status --no-heading
echo "checkinstall
echo "Track the local installation of a software package, and produce a binary package which can be used with a system's native package manager.
echo "More information: <http://checkinstall.izto.org>.
echo "Create and install a package with default settings:
sudo checkinstall --default
echo "Create a package but don't install it:
sudo checkinstall --install={{no}}
echo "Create a package without documentation:
sudo checkinstall --nodoc
echo "Create a package and set the name:
sudo checkinstall --pkgname {{package}}
echo "Create a package and specify where to save it:
sudo checkinstall --pakdir {{path/to/directory}}
echo "checkupdates-aur
echo "Tool to check pending updates from the Arch User Repository (AUR).
echo "More information: <https://metacpan.org/dist/OS-CheckUpdates-AUR>.
echo "List pending updates for AUR packages:
checkupdates-aur
echo "List pending updates for AUR packages in debug mode:
CHECKUPDATES_DEBUG=1 checkupdates-aur
echo "Display help:
checkupdates-aur --help
echo "checkupdates
echo "Tool to check pending updates in Arch Linux.
echo "More information: <https://man.archlinux.org/man/checkupdates.8>.
echo "List pending updates:
checkupdates
echo "List pending updates and download the packages to the pacman cache:
checkupdates --download
echo "List pending updates using a specific pacman database:
CHECKUPDATES_DB={{path/to/directory}} checkupdates
echo "Display help:
checkupdates --help
echo "chfn
echo "Update finger info for a user.
echo "More information: <https://manned.org/chfn>.
echo "Update a user's "Name" field in the output of finger:
chfn -f {{new_display_name}} {{username}}
echo "Update a user's "Office Room Number" field for the output of finger:
chfn -o {{new_office_room_number}} {{username}}
echo "Update a user's "Office Phone Number" field for the output of finger:
chfn -p {{new_office_telephone_number}} {{username}}
echo "Update a user's "Home Phone Number" field for the output of finger:
chfn -h {{new_home_telephone_number}} {{username}}
echo "chkconfig
echo "Manage the runlevel of services on CentOS 6.
echo "More information: <https://manned.org/chkconfig>.
echo "List services with runlevel:
chkconfig --list
echo "Show a service's runlevel:
chkconfig --list {{ntpd}}
echo "Enable service at boot:
chkconfig {{sshd}} on
echo "Enable service at boot for runlevels 2, 3, 4, and 5:
chkconfig --level {{2345}} {{sshd}} on
echo "Disable service at boot:
chkconfig {{ntpd}} off
echo "Disable service at boot for runlevel 3:
chkconfig --level {{3}} {{ntpd}} off
echo "chntpw
echo "A utility that can edit windows registry, reset user password, promote users to administrator by modifying the Windows SAM.
echo "Boot target machine with live cd like Kali Linux and run with elevated privileges.
echo "More information: <http://pogostick.net/~pnh/ntpasswd>.
echo "List all users in the SAM file:
chntpw -l {{path/to/sam_file}}
echo "Edit [u]ser interactively:
chntpw -u {{username}} {{path/to/sam_file}}
echo "Use chntpw [i]nteractively:
chntpw -i {{path/to/sam_file}}
echo "chpasswd
echo "Change the passwords for multiple users by using stdin.
echo "More information: <https://manned.org/chpasswd.8>.
echo "Change the password for a specific user:
printf "{{username}}:{{new_password}}" | sudo chpasswd
echo "Change the passwords for multiple users (The input text must not contain any spaces.):
printf "{{username_1}}:{{new_password_1}}\n{{username_2}}:{{new_password_2}}" | sudo chpasswd
echo "Change the password for a specific user, and specify it in encrypted form:
printf "{{username}}:{{new_encrypted_password}}" | sudo chpasswd --encrypted
echo "Change the password for a specific user, and use a specific encryption for the stored password:
printf "{{username}}:{{new_password}}" | sudo chpasswd --crypt-method {{NONE|DES|MD5|SHA256|SHA512}}
echo "chronyc
echo "Query the Chrony NTP daemon.
echo "More information: <https://chrony.tuxfamily.org/doc/4.0/chronyc.html>.
echo "Start chronyc in interactive mode:
chronyc
echo "Display tracking stats for the Chrony daemon:
chronyc tracking
echo "Print the time sources that Chrony is currently using:
chronyc sources
echo "Display stats for sources currently used by chrony daemon as a time source:
chronyc sourcestats
echo "Step the system clock immediately, bypassing any slewing:
chronyc makestep
echo "Display verbose information about each NTP source:
chronyc ntpdata
echo "chrt
echo "Manipulate the real-time attributes of a process.
echo "More information: <https://man7.org/linux/man-pages/man1/chrt.1.html>.
echo "Display attributes of a process:
chrt --pid {{PID}}
echo "Display attributes of all threads of a process:
chrt --all-tasks --pid {{PID}}
echo "Display the min/max priority values that can be used with chrt:
chrt --max
echo "Set the scheduling policy for a process:
chrt --pid {{PID}} --{{deadline|idle|batch|rr|fifo|other}}
echo "cloud-init
echo "Command line tool for managing cloud instance initialization.
echo "More information: <https://cloudinit.readthedocs.io>.
echo "Display the status of the most recent cloud-init run:
cloud-init status
echo "Wait for cloud-init to finish running and then report status:
cloud-init status --wait
# echo "List available top-level metadata keys to query:
cloud-init query --list-keys
echo "Query cached instance metadata for data:
cloud-init query {{dot_delimited_variable_path}}
echo "Clean logs and artifacts to allow cloud-init to rerun:
cloud-init clean
echo "cmus
echo "Command-line Music Player.
echo "Use arrow keys to navigate, <enter/return> to select, and numbers 1-8 switch between different views.
echo "More information: <https://cmus.github.io>.
echo "Open cmus into the specified directory (this will become your new working directory):
cmus {{path/to/directory}}
echo "Add file/directory to library:
:add {{path/to/file_or_directory}}
echo "Pause/unpause current song:
c
echo "Toggle shuffle mode on/off:
s
echo "Quit cmus:
q
echo "cockpit-bridge
echo "Relay messages and commands between the front end and server in the cockpit suite.
echo "More information: <https://cockpit-project.org/guide/latest/cockpit-bridge.1.html>.
echo "List all cockpit packages:
cockpit-bridge --packages
echo "Display help:
cockpit-bridge --help
# echo "cockpit-desktop
echo "Provides secure access to Cockpit pages in an already running session.
echo "It starts cockpit-ws` and a web browser in an isolated network space and a `cockpit-bridge in a running user session.
# echo "More information: <https://cockpit-project.org/guide/latest/cockpit-desktop.1.html>.
echo "Open a page:
# cockpit-desktop {{url}} {{SSH_host}}
echo "Open storage page:
# cockpit-desktop {{/cockpit/@localhost/storage/index.html}}
echo "cockpit-tls
echo "TLS terminating HTTP proxy to encrypt traffic between a client and cockpit-ws.
echo "More information: <https://cockpit-project.org/guide/latest/cockpit-tls.8.html>.
echo "Serve HTTP requests to a specific port instead of port 9090:
cockpit-tls --port {{port}}
echo "Display help:
cockpit-tls --help
echo "cockpit-ws
echo "Communicate between the browser application and various configuration tools and services like cockpit-bridge.
echo "More information: <https://cockpit-project.org/guide/latest/cockpit-ws.8.html>.
echo "Start with authentication via SSH at 127.0.0.1` with port `22 enabled:
cockpit-ws --local-ssh
echo "Start an HTTP server on a specific port:
cockpit-ws --port {{port}}
echo "Start and bind to a specific IP address (defaults to 0.0.0.0):
cockpit-ws --address {{ip_address}}
echo "Start without TLS:
cockpit-ws --no-tls
echo "Display help:
cockpit-ws --help
echo "collectd
echo "System statistics collection daemon.
echo "More information: <https://collectd.org/>.
echo "Show usage help, including the program version:
collectd -h
echo "Test the configuration file and then exit:
collectd -t
echo "Test plugin data collection functionality and then exit:
collectd -T
echo "Start collectd:
collectd
echo "Specify a custom configuration file location:
collectd -C {{path/to/file}}
echo "Specify a custom PID file location:
collectd -P {{path/to/file}}
echo "Don't fork into the background:
collectd -f
echo "colrm
echo "Remove columns from stdin.
echo "More information: <https://manned.org/colrm>.
echo "Remove first column of stdin:
colrm {{1 1}}
echo "Remove from 3rd column till the end of each line:
colrm {{3}}
echo "Remove from the 3rd column till the 5th column of each line:
colrm {{3 5}}
echo "compopt
echo "Print or change the completion options for a command.
echo "More information: <https://manned.org/compopt>.
echo "Print the options for the currently executing completion:
compopt
echo "Print the completion options for given command:
compopt {{command}}
echo "compose
echo "An alias to a run-mailcap's action compose.
echo "Originally run-mailcap is used to mime-type/file.
echo "More information: <https://manned.org/compose>.
echo "Compose action can be used to compose any existing file or new on default mailcap edit tool:
compose {{filename}}
echo "With run-mailcap:
run-mailcap --action=compose {{filename}}
echo "compress
echo "Compress files using the Unix compress command.
echo "More information: <https://manned.org/compress.1>.
echo "Compress specific files:
compress {{path/to/file1 path/to/file2 ...}}
echo "Compress specific files, ignore non-existent ones:
compress -f {{path/to/file1 path/to/file2 ...}}
echo "Set maximum compression bits (9-16 bits):
compress -b {{bits}}
echo "Write to stdout (no files are changed):
compress -c {{path/to/file}}
echo "Decompress files (functions like uncompress):
compress -d {{path/to/file}}
echo "Display compression percentage:
compress -v {{path/to/file}}
echo "compsize
echo "Calculate the compression ratio of a set of files on a btrfs filesystem.
echo "See also btrfs filesystem for recompressing a file by defragmenting it.
echo "More information: <https://github.com/kilobyte/compsize>.
echo "Calculate the current compression ratio for a file or directory:
sudo compsize {{path/to/file_or_directory}}
echo "Don't traverse filesystem boundaries:
sudo compsize --one-file-system {{path/to/file_or_directory}}
echo "Show raw byte counts instead of human-readable sizes:
sudo compsize --bytes {{path/to/file_or_directory}}
echo "conky
echo "Light-weight system monitor for X.
echo "More information: <https://github.com/brndnmtthws/conky>.
echo "Launch with default, built-in config:
conky
echo "Create a new default config:
conky -C > ~/.conkyrc
echo "Launch Conky with a given config file:
conky -c {{path/to/config}}
echo "Start in the background (daemonize):
conky -d
# echo "Align Conky on the desktop:
# conky -a {{top|bottom|middle}}_{{left|right|middle}}
echo "Pause for 5 seconds at startup before launching:
conky -p {{5}}
echo "conntrack
echo "Interact with the Netfilter connection tracking system.
echo "Search, list, inspect, modify, and delete connection flows.
echo "More information: <https://manned.org/conntrack>.
echo "List all currently tracked connections:
conntrack --dump
echo "Display a real-time event log of connection changes:
conntrack --event
echo "Display a real-time event log of connection changes and associated timestamps:
conntrack --event -o timestamp
echo "Display a real-time event log of connection changes for a specific IP address:
conntrack --event --orig-src {{ip_address}}
echo "Delete all flows for a specific source IP address:
conntrack --delete --orig-src {{ip_address}}
echo "coproc
echo "Bash builtin for creating interactive asynchronous subshells.
echo "More information: <https://www.gnu.org/software/bash/manual/bash.html#Coprocesses>.
echo "Run a subshell asynchronously:
coproc { {{command1; command2; ...}}; }
echo "Create a coprocess with a specific name:
coproc {{name}} { {{command1; command2; ...}}; }
echo "Write to a specific coprocess stdin:
echo "{{input}}" >&"${{{name}}[1]}"
echo "Read from a specific coprocess stdout:
read {{variable}} <&"${{{name}}[0]}"
echo "Create a coprocess which repeatedly reads stdin and runs some commands on the input:
coproc {{name}} { while read line; do {{command1; command2; ...}}; done }
echo "Create a coprocess which repeatedly reads stdin`, runs a pipeline on the input, and writes the output to `stdout:
coproc {{name}} { while read line; do echo "$line" | {{command1 | command2 | ...}} | cat /dev/fd/0; done }
echo "Create and use a coprocess running bc:
coproc BC { bc --mathlib; }; echo "1/3" >&"${BC[1]}"; read output <&"${BC[0]}"; echo "$output"
echo "coredumpctl
echo "Retrieve and process saved core dumps and metadata.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/coredumpctl.html>.
echo "List all captured core dumps:
coredumpctl list
echo "List captured core dumps for a program:
coredumpctl list {{program}}
echo "Show information about the core dumps matching a program with PID:
coredumpctl info {{PID}}
echo "Invoke debugger using the last core dump of a program:
coredumpctl debug {{program}}
echo "Extract the last core dump of a program to a file:
coredumpctl --output={{path/to/file}} dump {{program}}
echo "cp
echo "Copy files and directories.
echo "More information: <https://www.gnu.org/software/coreutils/cp>.
echo "Copy a file to another location:
cp {{path/to/source_file.ext}} {{path/to/target_file.ext}}
echo "Copy a file into another directory, keeping the filename:
cp {{path/to/source_file.ext}} {{path/to/target_parent_directory}}
echo "Recursively copy a directory's contents to another location (if the destination exists, the directory is copied inside it):
cp -r {{path/to/source_directory}} {{path/to/target_directory}}
echo "Copy a directory recursively, in verbose mode (shows files as they are copied):
cp -vr {{path/to/source_directory}} {{path/to/target_directory}}
echo "Copy multiple files at once to a directory:
cp -t {{path/to/destination_directory}} {{path/to/file1 path/to/file2 ...}}
echo "Copy text files to another location, in interactive mode (prompts user before overwriting):
cp -i {{*.txt}} {{path/to/target_directory}}
echo "Follow symbolic links before copying:
cp -L {{link}} {{path/to/target_directory}}
echo "Use the full path of source files, creating any missing intermediate directories when copying:
cp --parents {{source/path/to/file}} {{path/to/target_file}}
echo "cpufreq-aperf
echo "Calculate the average CPU frequency over a time period.
echo "Requires root privileges.
echo "More information: <https://manned.org/cpufreq-aperf>.
echo "Start calculating, defaulting to all CPU cores and 1 second refresh interval:
sudo cpufreq-aperf
echo "Start calculating for CPU 1 only:
sudo cpufreq-aperf -c {{1}}
echo "Start calculating with a 3 second refresh interval for all CPU cores:
sudo cpufreq-aperf -i {{3}}
echo "Calculate only once:
sudo cpufreq-aperf -o
echo "cpufreq-info
echo "A tool to show CPU frequency information.
echo "More information: <https://manned.org/cpufreq-info>.
echo "Show CPU frequency information for all CPUs:
cpufreq-info
echo "Show CPU frequency information for the specified CPU:
cpufreq-info -c {{cpu_number}}
echo "Show the allowed minimum and maximum CPU frequency:
cpufreq-info -l
echo "Show the current minimum and maximum CPU frequency and policy in table format:
cpufreq-info -o
echo "Show available CPU frequency policies:
cpufreq-info -g
echo "Show current CPU work frequency in a human-readable format, according to the cpufreq kernel module:
cpufreq-info -f -m
echo "Show current CPU work frequency in a human-readable format, by reading it from hardware (only available to root):
sudo cpufreq-info -w -m
echo "cpufreq-set
echo "A tool to modify CPU frequency settings.
echo "The frequency value should range between the output of command cpufreq-info -l.
echo "More information: <https://manned.org/cpufreq-set>.
echo "Set the CPU frequency policy of CPU 1 to "userspace":
sudo cpufreq-set -c {{1}} -g {{userspace}}
echo "Set the current minimum CPU frequency of CPU 1:
sudo cpufreq-set -c {{1}} --min {{min_frequency}}
echo "Set the current maximum CPU frequency of CPU 1:
sudo cpufreq-set -c {{1}} --max {{max_frequency}}
echo "Set the current work frequency of CPU 1:
sudo cpufreq-set -c {{1}} -f {{work_frequency}}
echo "cpuid
echo "Display detailed information about all CPUs.
echo "More information: <http://etallen.com/cpuid.html>.
echo "Display information for all CPUs:
cpuid
echo "Display information only for the current CPU:
cpuid -1
echo "Display raw hex information with no decoding:
cpuid -r
echo "cpulimit
echo "A tool to throttle the CPU usage of other processes.
echo "More information: <http://cpulimit.sourceforge.net/>.
echo "Limit an existing process with PID 1234 to only use 25% of the CPU:
cpulimit --pid {{1234}} --limit {{25%}}
echo "Limit an existing program by its executable name:
cpulimit --exe {{program}} --limit {{25}}
echo "Launch a given program and limit it to only use 50% of the CPU:
cpulimit --limit {{50}} -- {{program argument1 argument2 ...}}
echo "Launch a program, limit its CPU usage to 50% and run cpulimit in the background:
cpulimit --limit {{50}} --background -- {{program}}
echo "Kill its process if the program's CPU usage goes over 50%:
cpulimit --limit 50 --kill -- {{program}}
echo "Throttle both it and its child processes so that none go about 25% CPU:
cpulimit --limit {{25}} --monitor-forks -- {{program}}
echo "cpupower
echo "Tools regarding CPU power and tuning options.
echo "More information: <https://manned.org/cpupower>.
echo "List CPUs:
sudo cpupower --cpu {{all}} info
echo "Print information about all cores:
sudo cpupower --cpu {{all}} info
echo "Set all CPUs to a power-saving frequency governor:
sudo cpupower --cpu {{all}} frequency-set --governor {{powersave}}
echo "Print CPU 0's available frequency [g]overnors:
sudo cpupower --cpu {{0}} frequency-info g | grep "analyzing\|governors"
echo "Print CPU 4's frequency from the hardware, in a human-readable format:
sudo cpupower --cpu {{4}} frequency-info --hwfreq --human
echo "create_ap
echo "Create an AP (Access Point) at any channel.
echo "More information: <https://github.com/oblique/create_ap>.
echo "Create an open network with no passphrase:
create_ap {{wlan0}} {{eth0}} {{access_point_ssid}}
echo "Use a WPA + WPA2 passphrase:
create_ap {{wlan0}} {{eth0}} {{access_point_ssid}} {{passphrase}}
echo "Create an access point without Internet sharing:
create_ap -n {{wlan0}} {{access_point_ssid}} {{passphrase}}
echo "Create a bridged network with Internet sharing:
create_ap -m bridge {{wlan0}} {{eth0}} {{access_point_ssid}} {{passphrase}}
echo "Create a bridged network with Internet sharing and a pre-configured bridge interface:
create_ap -m bridge {{wlan0}} {{br0}} {{access_point_ssid}} {{passphrase}}
echo "Create an access port for Internet sharing from the same Wi-Fi interface:
create_ap {{wlan0}} {{wlan0}} {{access_point_ssid}} {{passphrase}}
echo "Choose a different Wi-Fi adapter driver:
create_ap --driver {{wifi_adapter}} {{wlan0}} {{eth0}} {{access_point_ssid}} {{passphrase}}
echo "createrepo
echo "Initializes an RPM repository in the given directory, including all XML and SQLite files.
echo "More information: <https://manned.org/createrepo>.
echo "Initialize a basic repository in a directory:
createrepo {{path/to/directory}}
echo "Initialize a repository, exclude test RPMs and display verbose logs:
createrepo -v -x {{test_*.rpm}} {{path/to/directory}}
echo "Initialize a repository, using SHA1 as the checksum algorithm, and ignoring symbolic links:
createrepo -S -s {{sha1}} {{path/to/directory}}
echo "cryptcat
echo "Cryptcat is netcat with encryption capabilities.
echo "More information: <http://cryptcat.sourceforge.net>.
echo "[l]isten on a specified [p]ort and print any data received:
cryptcat -k {{password}} -l -p {{port}}
echo "Connect to a certain port:
cryptcat -k {{password}} {{ip_address}} {{port}}
echo "Set a timeout [w]:
cryptcat -k {{password}} -w {{timeout_in_seconds}} {{ip_address}} {{port}}
echo "Scan [z] the open ports of a specified host:
cryptcat -v -z {{ip_address}} {{port}}
echo "Act as proxy and forward data from a local TCP port to the given remote host:
cryptcat -k {{password}} -l -p {{local_port}} | cryptcat -k {{password}} {{hostname}} {{remote_port}}
echo "cryptsetup
echo "Manage plain dm-crypt and LUKS (Linux Unified Key Setup) encrypted volumes.
echo "More information: <https://gitlab.com/cryptsetup/cryptsetup/>.
echo "Initialize a LUKS volume (overwrites all data on the partition):
cryptsetup luksFormat {{/dev/sda1}}
echo "Open a LUKS volume and create a decrypted mapping at /dev/mapper/target:
cryptsetup luksOpen {{/dev/sda1}} {{target}}
echo "Remove an existing mapping:
cryptsetup luksClose {{target}}
echo "Change the LUKS volume's passphrase:
cryptsetup luksChangeKey {{/dev/sda1}}
echo "csplit
echo "Split a file into pieces.
echo "This generates files named "xx00", "xx01", and so on.
echo "More information: <https://www.gnu.org/software/coreutils/csplit>.
echo "Split a file at lines 5 and 23:
csplit {{path/to/file}} {{5}} {{23}}
echo "Split a file every 5 lines (this will fail if the total number of lines is not divisible by 5):
csplit {{path/to/file}} {{5}} {*}
echo "Split a file every 5 lines, ignoring exact-division error:
csplit -k {{path/to/file}} {{5}} {*}
echo "Split a file at line 5 and use a custom prefix for the output files:
csplit {{path/to/file}} {{5}} -f {{prefix}}
echo "Split a file at a line matching a regular expression:
csplit {{path/to/file}} /{{regular_expression}}/
echo "ctr
echo "Manage containerd containers and images.
echo "More information: <https://containerd.io>.
# echo "List all containers (running and stopped):
ctr containers list
echo "List all images:
ctr images list
echo "Pull an image:
ctr images pull {{image}}
echo "Tag an image:
ctr images tag {{source_image}}:{{source_tag}} {{target_image}}:{{target_tag}}
echo "ctrlaltdel
echo "Utility to control what happens when CTRL+ALT+DEL is pressed.
echo "More information: <https://manned.org/ctrlaltdel>.
echo "Get current setting:
#ctrlaltdel
echo "Set CTRL+ALT+DEL to reboot immediately, without any preparation:
sudo ctrlaltdel hard
echo "Set CTRL+ALT+DEL to reboot "normally", giving processes a chance to exit first (send SIGINT to PID1):
sudo ctrlaltdel soft
echo "curlie
echo "A frontend to curl` that adds the ease of use of `httpie.
echo "More information: <https://github.com/rs/curlie>.
echo "Send a GET request:
curlie {{httpbin.org/get}}
echo "Send a POST request:
curlie post {{httpbin.org/post}} {{name=john}} {{age:=25}}
echo "Send a GET request with query parameters (e.g. first_param=5&second_param=true):
curlie get {{httpbin.org/get}} {{first_param==5}} {{second_param==true}}
echo "Send a GET request with a custom header:
curlie get {{httpbin.org/get}} {{header-name:header-value}}
echo "cuyo
echo "Tetris like game.
echo "More information: <https://www.karimmi.de/cuyo/>.
echo "Start a new game:
cuyo
echo "Navigate the piece horizontally:
{{A|D|Left arrow key|Right arrow key}}
echo "Turn the piece:
{{W|Up arrow key}}
echo "Hard drop the piece:
{{S|Down arrow key}}
echo "daemon
echo "Run processes into daemons.
echo "More information: <https://manned.org/man/daemon.1>.
echo "Run a command as a daemon:
daemon --name="{{name}}" {{command}}
echo "Run a command as a daemon which will restart if the command crashes:
daemon --name="{{name}}" --respawn {{command}}
echo "Run a command as a daemon which will restart if it crashes, with two attempts every 10 seconds:
daemon --name="{{name}}" --respawn --attempts=2 --delay=10 {{command}}
echo "Run a command as a daemon, writing logs to a specific file:
daemon --name="{{name}}" --errlog={{path/to/file.log}} {{command}}
echo "Kill a daemon (SIGTERM):
# daemon --name="{{name}}" --stop
echo "List daemons:
daemon --list
echo "daemonize
echo "Run a command (that does not daemonize itself) as a Unix daemon.
echo "More information: <http://software.clapper.org/daemonize/>.
echo "Run a command as a daemon:
daemonize {{command}} {{command_arguments}}
echo "Write the PID to the specified file:
daemonize -p {{path/to/pidfile}} {{command}} {{command_arguments}}
echo "Use a lock file to ensure that only one instance runs at a time:
daemonize -l {{path/to/lockfile}} {{command}} {{command_arguments}}
echo "Use the specified user account:
sudo daemonize -u {{user}} {{command}} {{command_arguments}}
echo "datamash
echo "Tool to perform basic numeric, textual and statistical operations on input textual data files.
echo "More information: <http://www.gnu.org/software/datamash/>.
echo "Get max, min, mean and median of a single column of numbers:
seq 3 | datamash max 1 min 1 mean 1 median 1
echo "Get the mean of a single column of float numbers (floats must use "," and not "."):
echo -e '1.0\n2.5\n3.1\n4.3\n5.6\n5.7' | tr '.' ',' | datamash mean 1
echo "Get the mean of a single column of numbers with a given decimal precision:
echo -e '1\n2\n3\n4\n5\n5' | datamash -R {{number_of_decimals_wanted}} mean 1
echo "Get the mean of a single column of numbers ignoring "Na" and "NaN" (literal) strings:
echo -e '1\n2\nNa\n3\nNaN' | datamash --narm mean 1
echo "dbclient
echo "Lightweight Dropbear Secure Shell client.
echo "More information: <https://manned.org/dbclient.1>.
echo "Connect to a remote host:
dbclient {{user}}@{{host}}
echo "Connect to a remote host on [p]ort 2222:
dbclient {{user}}@{{host}} -p 2222
echo "Connect to a remote host using a specific [i]dentity key in dropbear format:
dbclient -i {{path/to/key_file}} {{user}}@{{host}}
echo "Run a command on the remote host with a [t]ty allocation allowing interaction with the remote command:
dbclient {{user}}@{{host}} -t {{command}} {{argument1 argument2 ...}}
echo "Connect and forward [A]gent connections to remote host:
dbclient -A {{user}}@{{host}}
echo "dbus-daemon
echo "The D-Bus message daemon, allowing multiple programs to exchange messages.
# echo "More information: <https://www.freedesktop.org/wiki/Software/dbus/>.
echo "Run the daemon with a configuration file:
dbus-daemon --config-file {{path/to/file}}
echo "Run the daemon with the standard per-login-session message bus configuration:
dbus-daemon --session
echo "Run the daemon with the standard systemwide message bus configuration:
dbus-daemon --system
echo "Set the address to listen on and override the configuration value for it:
dbus-daemon --address {{address}}
echo "Output the process ID to stdout:
dbus-daemon --print-pid
echo "Force the message bus to write to the system log for messages:
dbus-daemon --syslog
echo "dconf read
echo "Read key values from dconf databases.
echo "See also: dconf.
echo "More information: <https://manned.org/dconf>.
echo "Print a specific key value:
dconf read {{/path/to/key}}
echo "Print a specific key [d]efault value:
dconf read -d {{/path/to/key}}
echo "dconf reset
echo "Reset key values in dconf databases.
echo "See also: dconf.
echo "More information: <https://manned.org/dconf>.
echo "Reset a specific key value:
dconf read {{/path/to/key}}
echo "Reset a specific directory:
dconf read -d {{/path/to/directory/}}
echo "dconf write
echo "Write key values in dconf databases.
echo "See also: dconf.
echo "More information: <https://manned.org/dconf>.
echo "Write a specific key value:
dconf write {{/path/to/key}} "{{value}}"
echo "Write a specific string key value:
dconf write {{/path/to/key}} "'{{string}}'"
echo "Write a specific integer key value:
dconf write {{/path/to/key}} "{{5}}"
echo "Write a specific boolean key value:
dconf write {{/path/to/key}} "{{true|false}}"
echo "Write a specific array key value:
dconf write {{/path/to/key}} "[{{'first', 'second', ...}}]"
echo "Write a specific empty array key value:
dconf write {{/path/to/key}} "@as []"
echo "dconf
echo "Manage dconf databases.
echo "See also: dconf-read`, `dconf-reset`, `dconf-write`, `gsettings.
echo "More information: <https://manned.org/dconf>.
echo "Print a specific key value:
dconf read {{/path/to/key}}
echo "Print a specific path sub-directories and sub-keys:
dconf list {{/path/to/directory/}}
echo "Write a specific key value:
dconf write {{/path/to/key}} "{{value}}"
echo "Reset a specific key value:
dconf reset {{/path/to/key}}
echo "Watch a specific key/directory for changes:
dconf watch {{/path/to/key|/path/to/directory/}}
echo "Dump a specific directory in INI file format:
dconf dump {{/path/to/directory/}}
echo "dd
echo "Convert and copy a file.
echo "More information: <https://www.gnu.org/software/coreutils/dd>.
echo "Make a bootable USB drive from an isohybrid file (such like archlinux-xxx.iso) and show the progress:
dd if={{path/to/file.iso}} of={{/dev/usb_drive}} status=progress
echo "Clone a drive to another drive with 4 MiB block, ignore error and show the progress:
dd if={{/dev/source_drive}} of={{/dev/dest_drive}} bs={{4M}} conv={{noerror}} status=progress
echo "Generate a file of 100 random bytes by using kernel random driver:
dd if=/dev/urandom of={{path/to/random_file}} bs={{100}} count={{1}}
echo "Benchmark the write performance of a disk:
dd if=/dev/zero of={{path/to/file_1GB}} bs={{1024}} count={{1000000}}
echo "Generate a system backup into an IMG file and show the progress:
dd if={{/dev/drive_device}} of={{path/to/file.img}} status=progress
echo "Restore a drive from an IMG file and show the progress:
dd if={{path/to/file.img}} of={{/dev/drive_device}} status=progress
echo "Check the progress of an ongoing dd operation (run this command from another shell):
kill -USR1 $(pgrep ^dd)
echo "ddcutil
echo "Control the settings of connected displays via DDC/CI.
echo "This command requires the kernel module i2c-dev` to be loaded. See also: `modprobe.
echo "More information: <https://www.ddcutil.com>.
echo "List all compatible displays:
ddcutil detect
echo "Change the brightness (option 0x10) of display 1 to 50%:
ddcutil --display {{1}} setvcp {{10}} {{50}}
echo "Increase the contrast (option 0x12) of display 1 by 5%:
ddcutil -d {{1}} setvcp {{12}} {{+}} {{5}}
echo "Read the settings of display 1:
ddcutil -d {{1}} getvcp {{ALL}}
echo "ddrescue
echo "Data recovery tool that reads data from damaged block devices.
echo "More information: <https://www.gnu.org/software/ddrescue/>.
echo "Take an image of a device, creating a log file:
sudo ddrescue {{/dev/sdb}} {{path/to/image.dd}} {{path/to/log.txt}}
echo "Clone Disk A to Disk B, creating a log file:
sudo ddrescue --force --no-scrape {{/dev/sdX}} {{/dev/sdY}} {{path/to/log.txt}}
echo "debchange
echo "Tool for maintenance of the debian/changelog file in a Debian source package.
echo "More information: <https://manpages.debian.org/latest/devscripts/debchange.1.en.html>.
echo "Add a new version for a non-maintainer upload to the changelog:
debchange --nmu
echo "Add a changelog entry to the current version:
debchange --append
echo "Add a changelog entry to close the bug with specified ID:
debchange --closes {{bug_id}}
echo "debman
echo "Read man pages from uninstalled packages.
echo "More information: <https://manpages.debian.org/latest/debian-goodies/debman.1.html>.
echo "Read a man page for a command that is provided by a specified package:
debman -p {{package}} {{command}}
echo "Specify a package version to download:
debman -p {{package}}={{version}} {{command}}
echo "Read a man page in a .deb file:
debman -f {{path/to/filename.deb}} {{command}}
echo "debootstrap
echo "Create a basic Debian system.
echo "More information: <https://wiki.debian.org/Debootstrap>.
echo "Create a Debian stable release system inside the debian-root directory:
sudo debootstrap stable {{path/to/debian-root/}} http://deb.debian.org/debian
echo "Create a minimal system including only required packages:
sudo debootstrap --variant=minbase stable {{path/to/debian-root/}}
echo "Create an Ubuntu 20.04 system inside the focal-root directory with a local mirror:
sudo debootstrap focal {{path/to/focal-root/}} {{file:///path/to/mirror/}}
echo "Switch to a bootstrapped system:
sudo chroot {{path/to/root}}
echo "List available releases:
ls /usr/share/debootstrap/scripts/
echo "deborphan
echo "Display orphan packages on operating systems using the APT package manager.
echo "More information: <https://manpages.debian.org/latest/deborphan/deborphan.html>.
echo "Display library packages (from the "libs" section of the package repository) which are not required by another package:
deborphan
echo "List orphan packages from the "libs" section as well as orphan packages that have a name that looks like a library name:
deborphan --guess-all
echo "Find packages which are only recommended or suggested (but not required) by another package:
deborphan --nice-mode
echo "debsecan
echo "Debian Security Analyzer, a tool to list vulnerabilities on a particular Debian installation.
echo "More information: <https://gitlab.com/fweimer/debsecan>.
echo "List vulnerable installed packages on the current host:
debsecan
echo "List vulnerable installed packages of a specific suite:
debsecan --suite {{release_code_name}}
echo "List only fixed vulnerabilities:
debsecan --suite {{release_code_name}} --only-fixed
echo "List only fixed vulnerabilities of unstable ("sid") and mail to root:
debsecan --suite {{sid}} --only-fixed --format {{report}} --mailto {{root}} --update-history
echo "Upgrade vulnerable installed packages:
sudo apt upgrade $(debsecan --only-fixed --format {{packages}})
echo "debtap
echo "Convert Debian packages into Arch Linux packages.
echo "See also: pacman-upgrade.
echo "More information: <https://github.com/helixarch/debtap>.
echo "Update debtap database (before the first run):
sudo debtap --update
echo "Convert the specified package:
debtap {{path/to/package.deb}}
echo "Convert the specified package bypassing all questions, except for editing metadata files:
debtap --quiet {{path/to/package.deb}}
echo "Generate a PKGBUILD file:
debtap --pkgbuild {{path/to/package.deb}}
echo "debugfs
echo "An interactive ext2/ext3/ext4 filesystem debugger.
echo "More information: <https://manned.org/debugfs>.
echo "Open the filesystem in read only mode:
debugfs {{/dev/sdXN}}
echo "Open the filesystem in read write mode:
debugfs -w {{/dev/sdXN}}
echo "Read commands from a specified file, execute them and then exit:
debugfs -f {{path/to/cmd_file}} {{/dev/sdXN}}
echo "View the filesystem stats in debugfs console:
stats
echo "Close the filesystem:
close -a
echo "List all available commands:
lr
echo "debuild
echo "Tool to build a Debian package from source.
echo "More information: <https://manpages.debian.org/latest/devscripts/debuild.1.en.html>.
echo "Build the package in the current directory:
debuild
echo "Build a binary package only:
debuild -b
echo "Do not run lintian after building the package:
debuild --no-lintian
echo "delpart
echo "Ask the Linux kernel to forget about a partition.
echo "More information: <https://manned.org/delpart>.
echo "Tell the kernel to forget about the first partition of /dev/sda:
sudo delpart {{/dev/sda}} {{1}}
echo "deluser
echo "Delete a user from the system.
echo "More information: <https://manpages.debian.org/latest/adduser/deluser.html>.
echo "Remove a user:
sudo deluser {{username}}
echo "Remove a user and their home directory:
sudo deluser --remove-home {{username}}
echo "Remove a user and their home, but backup their files into a .tar.gz file in the specified directory:
sudo deluser --backup-to {{path/to/backup_directory}} --remove-home {{username}}
echo "Remove a user, and all files owned by them:
sudo deluser --remove-all-files {{username}}
echo "dex
# echo "DesktopEntry Execution is a program to generate and execute DesktopEntry files of the Application type.
echo "More information: <https://github.com/jceb/dex>.
echo "Execute all programs in the autostart folders:
dex --autostart
echo "Execute all programs in the specified folders:
dex --autostart --search-paths {{path/to/directory1}}:{{path/to/directory2}}:{{path/to/directory3}}:
echo "Preview the programs would be executed in a GNOME specific autostart:
dex --autostart --environment {{GNOME}}
echo "Preview the programs would be executed in a regular autostart:
dex --autostart --dry-run
# echo "Preview the value of the DesktopEntry property Name:
# dex --property {{Name}} {{path/to/file.desktop}}
# echo "Create a DesktopEntry for a program in the current directory:
# dex --create {{path/to/file.desktop}}
# echo "Execute a single program (with Terminal=true in the desktop file) in the given terminal:
# dex --term {{terminal}} {{path/to/file.desktop}}
echo "dget
echo "Download Debian packages.
echo "More information: <https://manpages.debian.org/latest/devscripts/dget.1.en.html>.
echo "Download a binary package:
dget {{package}}
echo "Download and extract a package source from its .dsc file:
dget {{http://deb.debian.org/debian/pool/main/h/haskell-tldr/haskell-tldr_0.4.0-2.dsc}}
echo "Download a package source tarball from its .dsc file but don't extract it:
dget -d {{http://deb.debian.org/debian/pool/main/h/haskell-tldr/haskell-tldr_0.4.0-2.dsc}}
echo "diff3
echo "Compare three files line by line.
echo "More information: <https://www.gnu.org/software/diffutils/manual/html_node/Invoking-diff3.html>.
echo "Compare files:
diff3 {{path/to/file1}} {{path/to/file2}} {{path/to/file3}}
echo "Show all changes, outlining conflicts:
diff3 --show-all {{path/to/file1}} {{path/to/file2}} {{path/to/file3}}
echo "dir
echo "List directory contents using one line per file, special characters are represented by backslash escape sequences.
echo "Works as ls -C --escape.
echo "More information: <https://manned.org/dir>.
echo "List all files, including hidden files:
dir -all
echo "List files including their author (-l is required):
dir -l --author
echo "List files excluding those that match a specified blob pattern:
dir --hide={{pattern}}
echo "List subdirectories recursively:
dir --recursive
echo "Display help:
dir --help
echo "dirb
echo "Scan HTTP-based webservers for directories and files.
echo "More information: <http://dirb.sourceforge.net>.
echo "Scan a webserver using the default wordlist:
dirb {{https://example.org}}
echo "Scan a webserver using a custom wordlist:
dirb {{https://example.org}} {{path/to/wordlist.txt}}
echo "Scan a webserver non-recursively:
dirb {{https://example.org}} -r
echo "Scan a webserver using a specified user-agent and cookie for HTTP-requests:
dirb {{https://example.org}} -a {{user_agent_string}} -c {{cookie_string}}
echo "dirbuster
echo "Brute force directories and filenames on servers.
echo "More information: <https://www.kali.org/tools/dirbuster/>.
echo "Start in GUI mode:
dirbuster -u {{http://example.com}}
echo "Start in headless (no GUI) mode:
dirbuster -H -u {{http://example.com}}
echo "Set the file extension list:
dirbuster -e {{txt,html}}
echo "Enable verbose output:
dirbuster -v
echo "Set the report location:
dirbuster -r {{path/to/report.txt}}
echo "disown
echo "Allow sub-processes to live beyond the shell that they are attached to.
echo "See also the jobs command.
echo "More information: <https://www.gnu.org/software/bash/manual/bash.html#index-disown>.
echo "Disown the current job:
disown
echo "Disown a specific job:
disown %{{job_number}}
echo "Disown all jobs:
disown -a
echo "Keep job (do not disown it), but mark it so that no future SIGHUP is received on shell exit:
disown -h %{{job_number}}
echo "distrobox-create
echo "Create a Distrobox container. See also: tldr distrobox.
echo "The container created will be tightly integrated with the host, allowing sharing of the user's HOME directory, external storage, external USB devices, graphical apps (X11/Wayland), and audio.
echo "More information: <https://distrobox.it/usage/distrobox-create>.
echo "Create a Distrobox container using the Ubuntu image:
distrobox-create {{container_name}} --image {{ubuntu:latest}}
echo "Clone a Distrobox container:
distrobox-create --clone {{container_name}} {{cloned_container_name}}
echo "distrobox-enter
echo "Enter a Distrobox container. See also: tldr distrobox.
echo "Default command executed is your SHELL, but you can specify different shells or entire commands to execute. If used inside a script, an application, or a service, you can use the --headless mode to disable the tty and interactivity.
echo "More information: <https://distrobox.it/usage/distrobox-enter>.
echo "Enter a Distrobox container:
distrobox-enter {{container_name}}
echo "Enter a Distrobox container and run a command at login:
distrobox-enter {{container_name}} -- {{sh -l}}
echo "Enter a Distrobox container without instantiating a tty:
distrobox-enter --name {{container_name}} -- {{uptime -p}}
echo "distrobox-export
echo "Export app/service/binary from container to host OS. See also: tldr distrobox.
echo "More information: <https://distrobox.it/usage/distrobox-export>.
# echo "Export an app from the container to the host (the desktop entry/icon will show up in your host system's application list):
distrobox-export --app {{package}} --extra-flags "--foreground"
echo "Export a binary from the container to the host:
distrobox-export --bin {{path/to/binary}} --export-path {{path/to/binary_on_host}}
echo "Export a binary from the container to the host (i.e.$HOME/.local/bin) :
distrobox-export --bin {{path/to/binary}} --export-path {{path/to/export}}
echo "Export a service from the container to the host (--sudo will run the service as root inside the container):
distrobox-export --service {{package}} --extra-flags "--allow-newer-config" --sudo
echo "Unexport/delete an exported application:
distrobox-export --app {{package}} --delete
echo "distrobox-host-exec
echo "Execute a command on the host from inside a Distrobox container. See also: tldr distrobox.
echo "More information: <https://distrobox.it/usage/distrobox-host-exec>.
echo "Execute command on the host system from inside the Distrobox container:
distrobox-host-exec "{{command}}"
echo "Execute the ls command on the host system from inside the container:
distrobox-host-exec ls
echo "distrobox-list
echo "List all Distrobox containers. See also: tldr distrobox.
echo "Distrobox containers are listed separately from the rest of normal podman or Docker containers.
echo "More information: <https://distrobox.it/usage/distrobox-list>.
echo "List all Distrobox containers:
distrobox-list
echo "List all Distrobox containers with verbose information:
distrobox-list --verbose
echo "distrobox-rm
echo "Remove a Distrobox container. See also: tldr distrobox.
echo "More information: <https://distrobox.it/usage/distrobox-rm>.
# echo "Remove a Distrobox container (Tip: Stop the container before removing it):
distrobox-rm {{container_name}}
echo "Remove a Distrobox container forcefully:
distrobox-rm {{container_name}} --force
# echo "distrobox-stop
# echo "Stop a Distrobox container. See also: tldr distrobox.
# echo "More information: <https://distrobox.it/usage/distrobox-stop>.
# echo "Stop a Distrobox container:
# distrobox-stop {{container_name}}
# echo "Stop a Distrobox container non-interactively (without confirmation):
# distrobox-stop --name {{container_name}} --yes
echo "distrobox-upgrade
echo "Upgrade one or multiple Distrobox containers. See also: tldr distrobox.
echo "More information: <https://distrobox.it/usage/distrobox-upgrade>.
echo "Upgrade a container using the container's native package manager:
distrobox-upgrade {{container_name}}
echo "Upgrade all containers using the container's native package managers:
distrobox-upgrade --all
echo "Upgrade specific containers via the container's native package manager:
distrobox-upgrade {{container1 container2 ...}}
echo "distrobox
echo "Use any Linux distribution inside your terminal in a container. Install & use packages inside it while tightly integrating with the host OS, sharing storage (home directory) and hardware.
echo "Note: It uses Podman or Docker to create your containers.
echo "More information: <https://github.com/89luca89/distrobox>.
echo "View documentation for creating containers:
tldr distrobox-create
echo "View documentation for listing container's information:
tldr distrobox-list
echo "View documentation for entering the container:
tldr distrobox-enter
echo "View documentation for executing a command on the host from inside a container:
tldr distrobox-host-exec
echo "View documentation for exporting app/service/binary from the container to the host:
tldr distrobox-export
echo "View documentation for upgrading containers:
tldr distrobox-upgrade
# echo "View documentation for stopping the containers:
# tldr distrobox-stop
echo "View documentation for removing the containers:
tldr distrobox-rm
echo "dkms
echo "A framework that allows for dynamic building of kernel modules.
echo "More information: <https://github.com/dell/dkms>.
echo "List currently installed modules:
dkms status
echo "Rebuild all modules for the currently running kernel:
dkms autoinstall
echo "Install version 1.2.1 of the acpi_call module for the currently running kernel:
dkms install -m {{acpi_call}} -v {{1.2.1}}
echo "Remove version 1.2.1 of the acpi_call module from all kernels:
dkms remove -m {{acpi_call}} -v {{1.2.1}} --all
echo "dm-tool
echo "A tool to communicate with the display manager.
echo "More information: <https://manned.org/dm-tool>.
# echo "Show the greeter while keeping current desktop session open and waiting to be restored upon authentication by logged in user:
dm-tool switch-to-greeter
echo "Lock the current session:
dm-tool lock
echo "Switch to a specific user, showing an authentication prompt if required:
dm-tool switch-to-user {{username}} {{session}}
echo "Add a dynamic seat from within a running LightDM session:
dm-tool add-seat {{xlocal}} {{name}}={{value}}
echo "dmenu
echo "Dynamic menu.
echo "Creates a menu from a text input with each item on a new line.
echo "More information: <https://manned.org/dmenu>.
echo "Display a menu of the output of the ls command:
{{ls}} | dmenu
echo "Display a menu with custom items separated by a new line (\n):
echo -e "{{red}}\n{{green}}\n{{blue}}" | dmenu
echo "Let the user choose between multiple items and save the selected one to a file:
echo -e "{{red}}\n{{green}}\n{{blue}}" | dmenu > {{color.txt}}
echo "Launch dmenu on a specific monitor:
ls | dmenu -m {{1}}
echo "Display dmenu at the bottom of the screen:
ls | dmenu -b
echo "dmesg
echo "Write the kernel messages to stdout.
echo "More information: <https://manned.org/dmesg>.
echo "Show kernel messages:
dmesg
echo "Show kernel error messages:
dmesg --level err
echo "Show kernel messages and keep reading new ones, similar to tail -f (available in kernels 3.5.0 and newer):
dmesg -w
echo "Show how much physical memory is available on this system:
dmesg | grep -i memory
echo "Show kernel messages 1 page at a time:
dmesg | less
echo "Show kernel messages with a timestamp (available in kernels 3.5.0 and newer):
dmesg -T
echo "Show kernel messages in human-readable form (available in kernels 3.5.0 and newer):
dmesg -H
echo "Colorize output (available in kernels 3.5.0 and newer):
dmesg -L
echo "dmidecode
echo "Display the DMI (alternatively known as SMBIOS) table contents in a human-readable format.
echo "Requires root privileges.
echo "More information: <https://manned.org/dmidecode>.
echo "Show all DMI table contents:
sudo dmidecode
echo "Show the BIOS version:
sudo dmidecode -s bios-version
echo "Show the system's serial number:
sudo dmidecode -s system-serial-number
echo "Show BIOS information:
sudo dmidecode -t bios
echo "Show CPU information:
sudo dmidecode -t processor
echo "Show memory information:
sudo dmidecode -t memory
echo "dnf
echo "Package management utility for RHEL, Fedora, and CentOS (replaces yum).
echo "For equivalent commands in other package managers, see <https://wiki.archlinux.org/title/Pacman/Rosetta>.
echo "More information: <https://dnf.readthedocs.io>.
echo "Upgrade installed packages to the newest available versions:
sudo dnf upgrade
echo "Search packages via keywords:
dnf search {{keyword1 keyword2 ...}}
echo "Display details about a package:
dnf info {{package}}
echo "Install a new package (use -y to confirm all prompts automatically):
sudo dnf install {{package1 package2 ...}}
echo "Remove a package:
sudo dnf remove {{package1 package2 ...}}
echo "List installed packages:
dnf list --installed
echo "Find which packages provide a given command:
dnf provides {{command}}
echo "View all past operations:
dnf history
echo "dnsdomainname
echo "Show the system's DNS domain name.
echo "Note: The tool uses gethostname` to get the hostname of the system and then `getaddrinfo to resolve it into a canonical name.
echo "More information: <https://www.gnu.org/software/inetutils/manual/html_node/dnsdomainname-invocation.html>.
echo "Show the system's DNS domain name:
dnsdomainname
echo "dnsmap
echo "The dnsmap command scans a domain for common subdomains e.g. smtp.domain.org.
echo "More information: <https://github.com/resurrecting-open-source-projects/dnsmap>.
echo "Scan for subdomains using the internal wordlist:
dnsmap {{example.com}}
echo "Specify a list of subdomains to check for:
dnsmap {{example.com}} -w {{path/to/wordlist.txt}}
echo "Store results to a CSV file:
dnsmap {{example.com}} -c {{path/to/file.csv}}
echo "Ignore 2 IPs that are false positives (up to 5 possible):
dnsmap {{example.com}} -i {{123.45.67.89,98.76.54.32}}
echo "dnsrecon
echo "DNS enumeration tool.
echo "More information: <https://github.com/darkoperator/dnsrecon>.
echo "Scan a domain and save the results to a SQLite database:
dnsrecon --domain {{example.com}} --db {{path/to/database.sqlite}}
echo "Scan a domain, specifying the nameserver and performing a zone transfer:
dnsrecon --domain {{example.com}} --name_server {{nameserver.example.com}} --type axfr
echo "Scan a domain, using a brute-force attack and a dictionary of subdomains and hostnames:
dnsrecon --domain {{example.com}} --dictionary {{path/to/dictionary.txt}} --type brt
echo "Scan a domain, performing a reverse lookup of IP ranges from the SPF record and saving the results to a JSON file:
dnsrecon --domain {{example.com}} -s --json
echo "Scan a domain, performing a Google enumeration and saving the results to a CSV file:
dnsrecon --domain {{example.com}} -g --csv
echo "Scan a domain, performing DNS cache snooping:
dnsrecon --domain {{example.com}} --type snoop --name_server {{nameserver.example.com}} --dictionary {{path/to/dictionary.txt}}
echo "Scan a domain, performing zone walking:
dnsrecon --domain {{example.com}} --type zonewalk
echo "dnstracer
echo "The dnstracer command determines where a DNS gets its information from.
echo "More information: <https://manned.org/dnstracer>.
echo "Find out where your local DNS got the information on www.example.com:
dnstracer {{www.example.com}}
echo "Start with a [s]pecific DNS that you already know:
dnstracer -s {{dns.example.org}} {{www.example.com}}
echo "Only query IPv4 servers:
dnstracer -4 {{www.example.com}}
echo "Retry each request 5 times on failure:
dnstracer -r {{5}} {{www.example.com}}
echo "Display all steps during execution:
dnstracer -v {{www.example.com}}
echo "Display an [o]verview of all received answers after execution:
dnstracer -o {{www.example.com}}
echo "do-release-upgrade
echo "The Ubuntu release upgrader.
echo "More information: <https://ubuntu.com/server/docs/upgrade-introduction>.
echo "Upgrade to the latest release:
sudo do-release-upgrade
echo "Upgrade to the latest development release:
sudo do-release-upgrade --devel-release
echo "Upgrade to the latest proposed release:
sudo do-release-upgrade --proposed
echo "dockerd
echo "A persistent process to start and manage docker containers.
echo "More information: <https://docs.docker.com/engine/reference/commandline/dockerd/>.
echo "Run docker daemon:
dockerd
echo "Run docker daemon and config it to listen to specific sockets (UNIX and TCP):
dockerd --host unix://{{path/to/tmp.sock}} --host tcp://{{ip}}
echo "Run with specific daemon PID file:
dockerd --pidfile {{path/to/pid_file}}
echo "Run in debug mode:
dockerd --debug
echo "Run and set a specific log level:
dockerd --log-level={{debug|info|warn|error|fatal}}
echo "dolphin
echo "KDE's file manager to manage files and directories.
echo "More information: <https://apps.kde.org/dolphin/>.
echo "Launch the file manager:
dolphin
echo "Open specific directories:
dolphin {{path/to/directory1 path/to/directory2 ...}}
echo "Open with specific files or directories selected:
dolphin --select {{path/to/file_or_directory1 path/to/file_or_directory2 ...}}
echo "Open a new window:
dolphin --new-window
echo "Open specific directories in split view:
dolphin --split {{path/to/directory1}} {{path/to/directory2}}
echo "Launch the daemon (only required to use the DBus interface):
dolphin --daemon
echo "Display help:
dolphin --help
echo "dos2unix
echo "Change DOS-style line endings to Unix-style.
echo "Replaces CRLF with LF.
echo "More information: <https://manned.org/dos2unix>.
echo "Change the line endings of a file:
dos2unix {{filename}}
echo "Create a copy with Unix-style line endings:
dos2unix -n {{filename}} {{new_filename}}
echo "dphys-swapfile
echo "Manage the swap file on Debian-based Linux systems.
echo "More information: <https://manpages.debian.org/latest/dphys-swapfile/dphys-swapfile.html>.
echo "Disable the swap file:
dphys-swapfile swapoff
echo "Enable the swap file:
dphys-swapfile swapon
echo "Create a new swap file:
dphys-swapfile setup
echo "dpkg-deb
echo "Pack, unpack and provide information about Debian archives.
echo "More information: <https://manpages.debian.org/latest/dpkg/dpkg-deb.html>.
echo "Display information about a package:
dpkg-deb --info {{path/to/file.deb}}
echo "Display the package's name and version on one line:
dpkg-deb --show {{path/to/file.deb}}
echo "List the package's contents:
dpkg-deb --contents {{path/to/file.deb}}
echo "Extract package's contents into a directory:
dpkg-deb --extract {{path/to/file.deb}} {{path/to/directory}}
echo "Create a package from a specified directory:
dpkg-deb --build {{path/to/directory}}
echo "dpkg-query
echo "A tool that shows information about installed packages.
echo "More information: <https://manpages.debian.org/latest/dpkg/dpkg-query.1.html>.
echo "List all installed packages:
dpkg-query --list
echo "List installed packages matching a pattern:
dpkg-query --list '{{libc6*}}'
echo "List all files installed by a package:
dpkg-query --listfiles {{libc6}}
echo "Show information about a package:
dpkg-query --status {{libc6}}
echo "Search for packages that own files matching a pattern:
dpkg-query --search {{/etc/ld.so.conf.d}}
echo "dpkg-reconfigure
echo "Reconfigure an already installed package.
echo "More information: <https://manpages.debian.org/latest/debconf/dpkg-reconfigure.8.html>.
echo "Reconfigure one or more packages:
dpkg-reconfigure {{package1 package2 ...}}
echo "dpkg
echo "Debian package manager.
echo "Some subcommands such as dpkg deb have their own usage documentation.
echo "For equivalent commands in other package managers, see <https://wiki.archlinux.org/title/Pacman/Rosetta>.
echo "More information: <https://manpages.debian.org/latest/dpkg/dpkg.html>.
echo "Install a package:
dpkg -i {{path/to/file.deb}}
echo "Remove a package:
dpkg -r {{package}}
echo "List installed packages:
dpkg -l {{pattern}}
echo "List a package's contents:
dpkg -L {{package}}
echo "List contents of a local package file:
dpkg -c {{path/to/file.deb}}
echo "Find out which package owns a file:
dpkg -S {{path/to/file}}
echo "dracut
echo "Generate initramfs images to boot the Linux kernel.
echo "Dracut uses options from configuration files in /etc/dracut.conf`, `/etc/dracut.conf.d/*.conf` and `/usr/lib/dracut/dracut.conf.d/*.conf by default.
echo "More information: <https://github.com/dracutdevs/dracut/wiki>.
echo "Generate an initramfs image for the current kernel without overriding any options:
dracut
echo "Generate an initramfs image for the current kernel and overwrite the existing one:
dracut --force
echo "Generate an initramfs image for a specific kernel:
dracut --kver {{kernel_version}}
echo "Show a list of available modules:
dracut --list-modules
echo "drawing
# echo "Free basic raster image editor in GNOME desktop environment.
echo "More information: <https://maoschanz.github.io/drawing/>.
echo "Start Drawing:
drawing
echo "Open specific files:
drawing {{path/to/image1 path/to/image2 ...}}
echo "Open specific files in a new window:
drawing --new-window {{path/to/image1 path/to/image2 ...}}
echo "dropbearconvert
echo "Convert between Dropbear and OpenSSH private key formats.
echo "More information: <https://manned.org/dropbearconvert.1>.
echo "Convert an OpenSSH private key to the Dropbear format:
dropbearconvert openssh dropbear {{path/to/input_key}} {{path/to/output_key}}
echo "Convert a Dropbear private key to the OpenSSH format:
dropbearconvert dropbear openssh {{path/to/input_key}} {{path/to/output_key}}
echo "dropbearkey
echo "Generate SSH keys in Dropbear format.
echo "More information: <https://manned.org/dropbearkey.1>.
echo "Generate an SSH key of [t]ype ed25519 and write it to key [f]ile:
dropbearkey -t {{ed25519}} -f {{path/to/key_file}}
echo "Generate an SSH key of [t]ype ecdsa and write it to key [f]ile:
dropbearkey -t {{ecdsa}} -f {{path/to/key_file}}
echo "Generate an SSH key of [t]ype RSA with 4096-bit key [s]ize and write it to key [f]ile:
dropbearkey -t {{rsa}} -s {{4096}} -f {{path/to/key_file}}
echo "Print the private key fingerprint and public key in key [f]ile:
dropbearkey -y -f {{path/to/key_file}}
echo "dstat
echo "Versatile tool for generating system resource statistics.
echo "More information: <http://dag.wieers.com/home-made/dstat>.
echo "Display CPU, disk, net, paging and system statistics:
dstat
echo "Display statistics every 5 seconds and 4 updates only:
dstat {{5}} {{4}}
echo "Display CPU and memory statistics only:
dstat --cpu --mem
echo "List all available dstat plugins:
dstat --list
echo "Display the process using the most memory and most CPU:
# dstat --top-mem --top-cpu
echo "Display battery percentage and remaining battery time:
dstat --battery --battery-remain
echo "duc
echo "A collection of tools for indexing, inspecting and visualizing disk usage.
echo "Duc maintains a database of accumulated sizes of directories in the file system, allowing to query this database, or creating fancy graphs to show where data is.
echo "More information: <https://duc.zevv.nl/>.
echo "Index the /usr` directory, writing to the default database location `~/.duc.db:
duc index {{/usr}}
echo "List all files and directories under /usr/local, showing relative file sizes in a [g]raph:
duc ls --classify --graph {{/usr/local}}
echo "List all files and directories under /usr/local using treeview recursively:
duc ls --classify --graph --recursive {{/usr/local}}
echo "Start the graphical interface to explore the file system using sunburst graphs:
duc gui {{/usr}}
echo "Run the ncurses console interface to explore the file system:
duc ui {{/usr}}
echo "Dump database info:
duc info
echo "dumpe2fs
echo "Print the super block and blocks group information for ext2/ext3/ext4 filesystems.
echo "Unmount the partition before running this command using umount {{device}}.
echo "More information: <https://manned.org/dumpe2fs>.
echo "Display ext2, ext3 and ext4 filesystem information:
dumpe2fs {{/dev/sdXN}}
echo "Display the blocks which are reserved as bad in the filesystem:
dumpe2fs -b {{/dev/sdXN}}
echo "Force display filesystem information even with unrecognizable feature flags:
dumpe2fs -f {{/dev/sdXN}}
echo "Only display the superblock information and not any of the block group descriptor detail information:
dumpe2fs -h {{/dev/sdXN}}
echo "Print the detailed group information block numbers in hexadecimal format:
dumpe2fs -x {{/dev/sdXN}}
echo "dunstctl
echo "Control command for dunst.
echo "More information: <https://manned.org/dunstctl>.
echo "Pause notifications:
dunstctl set-paused true
echo "Un-pause notifications:
dunstctl set-paused false
echo "Close all notifications:
dunstctl close-all
echo "Display help:
dunstctl --help
echo "dunstify
echo "A notification tool that is an extension of notify-send`, but has more features based around `dunst.
echo "Accepts all options of notify-send.
echo "More information: <https://github.com/dunst-project/dunst/wiki/Guides>.
echo "Show a notification with a given title and message:
dunstify "{{Title}}" "{{Message}}"
echo "Show a notification with specified urgency:
dunstify "{{Title}}" "{{Message}}" -u {{low|normal|critical}}
echo "Specify a message ID (overwrites any previous messages with the same ID):
dunstify "{{Title}}" "{{Message}}" -r {{123}}
echo "Display help:
notify-send --help
echo "duperemove
echo "Finds duplicate filesystem extents and optionally schedule them for deduplication.
echo "An extent is small part of a file inside the filesystem.
echo "On some filesystems one extent can be referenced multiple times, when parts of the content of the files are identical.
echo "More information: <https://markfasheh.github.io/duperemove/>.
echo "Search for duplicate extents in a directory and show them:
duperemove -r {{path/to/directory}}
echo "Deduplicate duplicate extents on a Btrfs or XFS (experimental) filesystem:
duperemove -r -d {{path/to/directory}}
echo "Use a hash file to store extent hashes (less memory usage and can be reused on subsequent runs):
duperemove -r -d --hashfile={{path/to/hashfile}} {{path/to/directory}}
echo "Limit I/O threads (for hashing and dedupe stage) and CPU threads (for duplicate extent finding stage):
duperemove -r -d --hashfile={{path/to/hashfile}} --io-threads={{N}} --cpu-threads={{N}} {{path/to/directory}}
echo "e2freefrag
echo "Print the free space fragmentation information for ext2/ext3/ext4 filesystems.
echo "More information: <https://manned.org/e2freefrag>.
echo "Check how many free blocks are present as contiguous and aligned free space:
e2freefrag {{/dev/sdXN}}
echo "Specify chunk size in kilobytes to print how many free chunks are available:
e2freefrag -c {{chunk_size_in_kb}} {{/dev/sdXN}}
echo "e2fsck
echo "Check a Linux ext2/ext3/ext4 filesystem. The partition should be unmounted.
echo "More information: <https://manned.org/e2fsck>.
echo "Check filesystem, reporting any damaged blocks:
sudo e2fsck {{/dev/sdXN}}
echo "Check filesystem and automatically repair any damaged blocks:
sudo e2fsck -p {{/dev/sdXN}}
echo "Check filesystem in read only mode:
sudo e2fsck -c {{/dev/sdXN}}
echo "Perform an exhaustive, non-destructive read-write test for bad blocks and blacklist them:
sudo e2fsck -fccky {{/dev/sdXN}}
echo "e2image
echo "Save critical ext2/ext3/ext4 filesystem metadata to a file.
echo "More information: <https://manned.org/e2image>.
echo "Write metadata located on device to a specific file:
e2image {{/dev/sdXN}} {{path/to/image_file}}
echo "Print metadata located on device to stdout:
e2image {{/dev/sdXN}} -
echo "Restore the filesystem metadata back to the device:
e2image -I {{/dev/sdXN}} {{path/to/image_file}}
echo "Create a large raw sparse file with metadata at proper offsets:
e2image -r {{/dev/sdXN}} {{path/to/image_file}}
echo "Create a QCOW2 image file instead of a normal or raw image file:
e2image -Q {{/dev/sdXN}} {{path/to/image_file}}
echo "e2label
echo "Change the label on an ext2/ext3/ext4 filesystem.
echo "More information: <https://manned.org/e2label>.
echo "Change the volume label on a specific ext partition:
e2label {{/dev/sda1}} "{{label_name}}"
echo "e2undo
echo "Replay undo logs for an ext2/ext3/ext4 filesystem.
echo "This can be used to undo a failed operation by an e2fsprogs program.
echo "More information: <https://man7.org/linux/man-pages/man8/e2undo.8.html>.
echo "Display information about a specific undo file:
e2undo -h {{path/to/undo_file}} {{/dev/sdXN}}
echo "Perform a dry-run and display the candidate blocks for replaying:
e2undo -nv {{path/to/undo_file}} {{/dev/sdXN}}
echo "Perform an undo operation:
e2undo {{path/to/undo_file}} {{/dev/sdXN}}
echo "Perform an undo operation and display verbose information:
e2undo -v {{path/to/undo_file}} {{/dev/sdXN}}
echo "Write the old contents of the block to an undo file before overwriting a file system block:
e2undo -z {{path/to/file.e2undo}} {{path/to/undo_file}} {{/dev/sdXN}}
echo "e4defrag
echo "Defragment an ext4 filesystem.
echo "More information: <https://manned.org/e4defrag>.
echo "Defragment the filesystem:
e4defrag {{/dev/sdXN}}
echo "See how fragmented a filesystem is:
e4defrag -c {{/dev/sdXN}}
echo "Print errors and the fragmentation count before and after each file:
e4defrag -v {{/dev/sdXN}}
echo "ebuild
echo "A low level interface to the Gentoo Portage system.
echo "More information: <https://wiki.gentoo.org/wiki/Ebuild>.
echo "Create or update the package manifest:
ebuild {{path/to/file.ebuild}} manifest
echo "Clean the temporary build directories for the build file:
ebuild {{path/to/file.ebuild}} clean
echo "Fetch sources if they do not exist:
ebuild {{path/to/file.ebuild}} fetch
echo "Extract the sources to a temporary build directory:
ebuild {{path/to/file.ebuild}} unpack
echo "Compile the extracted sources:
ebuild {{path/to/file.ebuild}} compile
echo "Install the package to a temporary install directory:
ebuild {{path/to/file.ebuild}} install
echo "Install the temporary files to the live filesystem:
ebuild {{path/to/file.ebuild}} qmerge
echo "Fetch, unpack, compile, install and qmerge the specified ebuild file:
ebuild {{path/to/file.ebuild}} merge
echo "edit
echo "An alias to a run-mailcap's action edit.
echo "Originally run-mailcap is used to process/edit mime-type/file.
echo "More information: <https://www.computerhope.com/unix/uedit.htm>.
echo "Edit action can be used to view any file on default mailcap explorer:
edit {{filename}}
echo "With run-mailcap:
run-mailcap --action=edit {{filename}}
echo "edquota
echo "Edit quotas for a user or group. By default it operates on all filesystems with quotas.
echo "Quota information is stored permanently in the quota.user` and `quota.group files in the root of the filesystem.
echo "More information: <https://manned.org/edquota>.
echo "Edit quota of the current user:
edquota --user $(whoami)
echo "Edit quota of a specific user:
sudo edquota --user {{username}}
echo "Edit quota for a group:
sudo edquota --group {{group}}
echo "Restrict operations to a given filesystem (by default edquota operates on all filesystems with quotas):
sudo edquota --file-system {{filesystem}}
echo "Edit the default grace period:
sudo edquota -t
echo "Duplicate a quota to other users:
sudo edquota -p {{reference_user}} {{destination_user1}} {{destination_user2}}
echo "efibootmgr
echo "Manipulate the UEFI Boot Manager.
echo "More information: <https://manned.org/efibootmgr>.
echo "List the current settings then bootnums with their name:
efibootmgr
echo "List the filepaths:
efibootmgr -v
echo "Add UEFI Shell v2 as a boot option:
sudo efibootmgr -c -d {{/dev/sda1}} -l {{\EFI\tools\Shell.efi}} -L "{{UEFI Shell}}"
echo "Change the current boot order:
sudo efibootmgr -o {{0002,0008,0001,0005}}
echo "Delete a boot option:
sudo efibootmgr -b {{0008}} --delete-bootnum
echo "ego
echo "Funtoo's official system personality management tool.
echo "More information: <https://funtoo-ego.readthedocs.io/en/develop/>.
echo "Synchronize the Portage tree:
ego sync
echo "Update the bootloader configuration:
ego boot update
echo "Read a Funtoo wiki page by name:
ego doc {{wiki_page}}
echo "Print current profile:
ego profile show
echo "Enable/Disable mix-ins:
ego profile mix-in +{{gnome}} -{{kde-plasma-5}}
echo "Query Funtoo bugs, related to a specified package:
ego query bug {{package}}
echo "einfo
echo "Provides the number of records indexed in each field of a given database, the date of the last update of the database, and the available links from the database to other Entrez databases.
echo "More information: <https://www.ncbi.nlm.nih.gov/books/NBK179288/>.
echo "Print all database names:
einfo -dbs
echo "Print all information of the protein database in XML format:
einfo -db {{protein}}
echo "Print all fields of the nuccore database:
einfo -db {{nuccore}} -fields
echo "Print all links of the protein database:
einfo -db {{protein}} -links
echo "eix
echo "Utilities for searching local Gentoo packages.
echo "Update local package cache using eix-update.
echo "More information: <https://wiki.gentoo.org/wiki/Eix>.
echo "Search for a package:
eix {{query}}
echo "Search for installed packages:
eix --installed {{query}}
echo "Search in package descriptions:
eix --description "{{description}}"
echo "Search by package license:
eix --license {{license}}
echo "Exclude results from search:
eix --not --license {{license}}
echo "eject
echo "Eject cds, floppy disks and tape drives.
echo "More information: <https://manned.org/eject>.
echo "Display the default device:
eject -d
echo "Eject the default device:
eject
echo "Eject a specific device (the default order is cd-rom, scsi, floppy and tape):
eject {{/dev/cdrom}}
echo "Toggle whether a device's tray is open or closed:
eject -T {{/dev/cdrom}}
echo "Eject a cd drive:
eject -r {{/dev/cdrom}}
echo "Eject a floppy drive:
eject -f {{/mnt/floppy}}
echo "Eject a tape drive:
eject -q {{/mnt/tape}}
echo "elink
echo "Look up precomputed neighbors within a database, or find associated records in other databases.
echo "It is part of the edirect package.
echo "More information: <https://www.ncbi.nlm.nih.gov/books/NBK179288/>.
echo "Search pubmed then find related sequences:
esearch -db pubmed -query "{{selective serotonin reuptake inhibitor}}" | elink -target nuccore
echo "Search nucleotide then find related biosamples:
esearch -db nuccore -query "{{insulin [PROT] AND rodents [ORGN]}}" | elink -target biosample
echo "emerge
echo "Gentoo Linux package manager utility.
echo "For equivalent commands in other package managers, see <https://wiki.archlinux.org/title/Pacman/Rosetta>.
echo "More information: <https://wiki.gentoo.org/wiki/Portage#emerge>.
echo "Synchronize all packages:
emerge --sync
echo "Update all packages, including dependencies:
emerge -uDNav @world
echo "Resume a failed updated, skipping the failing package:
emerge --resume --skipfirst
echo "Install a new package, with confirmation:
emerge -av {{package}}
echo "Remove a package, with confirmation:
emerge -Cav {{package}}
echo "Remove orphaned packages (that were installed only as dependencies):
emerge -avc
echo "Search the package database for a keyword:
emerge -S {{keyword}}
echo "engrampa
# echo "Package files into zip/tar file in MATE desktop environment.
echo "See also: zip`, `tar.
# echo "More information: <https://github.com/mate-desktop/engrampa>.
echo "Start Engrampa:
engrampa
echo "Open specific archives:
engrampa {{path/to/archive1.tar path/to/archive2.tar ...}}
echo "Archive specific files and/or directories recursively:
engrampa --add-to={{path/to/compressed.tar}} {{path/to/file_or_directory1 path/to/file_or_directory2 ...}}
echo "Extract files and/or directories from archives to a specific path:
engrampa --extract-to={{path/to/directory}} {{path/to/archive1.tar path/to/archive2.tar ...}}
echo "enum4linux
echo "Tool for enumerating Windows and Samba information from remote systems.
echo "More information: <https://labs.portcullis.co.uk/tools/enum4linux/>.
echo "Try to enumerate using all methods:
enum4linux -a {{remote_host}}
echo "Enumerate using given login credentials:
enum4linux -u {{user_name}} -p {{password}} {{remote_host}}
echo "List usernames from a given host:
enum4linux -U {{remote_host}}
echo "List shares:
enum4linux -S {{remote_host}}
echo "Get OS information:
enum4linux -o {{remote_host}}
echo "envycontrol
# echo "GPU switching utility for Nvidia Optimus laptops.
echo "More information: <https://github.com/bayasdev/envycontrol>.
echo "Switch between different GPU modes:
sudo envycontrol -s {{nvidia|integrated|hybrid}}
echo "Specify your display manager manually:
envycontrol --dm
echo "Check current GPU mode:
sudo envycontrol --query
echo "Reset settings:
sudo envycontrol --reset
echo "Display version:
envycontrol --version
echo "Display help:
envycontrol --help
echo "eopkg
echo "Package manager for Solus.
echo "More information: <https://getsol.us/articles/package-management/basics/en/>.
echo "Install a specific package:
sudo eopkg install {{package}}
echo "Update all packages:
sudo eopkg upgrade
echo "Search for packages:
sudo eopkg search {{search_term}}
echo "equery
echo "View information about Portage packages.
echo "More information: <https://wiki.gentoo.org/wiki/Equery>.
echo "List all installed packages:
equery list '*'
echo "Search for installed packages in the Portage tree and in overlays:
equery list -po {{package1 package2 ...}}
echo "List all packages that depend on a given package:
equery depends {{package}}
echo "List all packages that a given package depends on:
equery depgraph {{package}}
echo "List all files installed by a package:
equery files --tree {{package}}
echo "esa snap
echo "Sentinel Application Platform (SNAP) for processing satellite data from the European Space Agency (ESA).
echo "More information: <http://step.esa.int/main/download/snap-download/>.
echo "Display all updates:
snap --nosplash --nogui --modules --list --refresh
echo "Display help:
snap --help
echo "esearch
echo "Perform a new Entrez search using terms in indexed fields.
echo "It is part of the edirect package.
echo "More information: <https://www.ncbi.nlm.nih.gov/books/NBK179288/>.
echo "Search the pubmed database for selective serotonin reuptake inhibitor:
esearch -db pubmed -query "{{selective serotonin reuptake inhibitor}}"
echo "Search the protein database using a query and regexp:
esearch -db {{protein}} -query {{'Escherichia*'}}
echo "Search the nucleotide database for sequences whose metadata contain insulin and rodents:
esearch -db nuccore -query "{{insulin [PROT] AND rodents [ORGN]}}"
echo "Display [h]elp:
esearch -h
echo "etckeeper
echo "Track system configuration files in Git.
echo "More information: <http://etckeeper.branchable.com/>.
echo "Set up a Git repo and perform various setup tasks (run from /etc):
sudo etckeeper init
echo "Commit all changes in /etc:
sudo etckeeper commit {{message}}
echo "Run arbitrary Git commands:
sudo etckeeper vcs {{status}}
echo "Check if there are uncommitted changes (only returns an exit code):
sudo etckeeper unclean
# echo "Destroy existing repo and stop tracking changes:
sudo etckeeper uninit
echo "ethtool
echo "Display and modify Network Interface Controller (NIC) parameters.
echo "More information: <http://man7.org/linux/man-pages/man8/ethtool.8.html>.
echo "Display the current settings for an interface:
ethtool {{eth0}}
echo "Display the driver information for an interface:
ethtool --driver {{eth0}}
echo "Display all supported features for an interface:
ethtool --show-features {{eth0}}
echo "Display the network usage statistics for an interface:
ethtool --statistics {{eth0}}
echo "Blink one or more LEDs on an interface for 10 seconds:
ethtool --identify {{eth0}} {{10}}
echo "Set the link speed, duplex mode, and parameter auto-negotiation for a given interface:
ethtool -s {{eth0}} speed {{10|100|1000}} duplex {{half|full}} autoneg {{on|off}}
echo "exec
echo "Replace the current process with another process.
echo "More information: <https://linuxcommand.org/lc3_man_pages/exech.html>.
echo "Replace with the specified command using the current environment variables:
exec {{command -with -flags}}
echo "Replace with the specified command, clearing environment variables:
exec -c {{command -with -flags}}
echo "Replace with the specified command and login using the default shell:
exec -l {{command -with -flags}}
echo "Replace with the specified command and change the process name:
exec -a {{process_name}} {{command -with -flags}}
echo "exif
echo "Show and change EXIF information in JPEG files.
echo "More information: <https://github.com/libexif/exif/>.
echo "Show all recognized EXIF information in an image:
exif {{path/to/image.jpg}}
echo "Show a table listing known EXIF tags and whether each one exists in an image:
exif --list-tags --no-fixup {{image.jpg}}
echo "Extract the image thumbnail into the file thumbnail.jpg:
exif --extract-thumbnail --output={{thumbnail.jpg}} {{image.jpg}}
echo "Show the raw contents of the "Model" tag in the given image:
exif --ifd={{0}} --tag={{Model}} --machine-readable {{image.jpg}}
echo "Change the value of the "Artist" tag to John Smith and save to new.jpg:
exif --output={{new.jpg}} --ifd={{0}} --tag="{{Artist}}" --set-value="{{John Smith}}" --no-fixup {{image.jpg}}
echo "exiqgrep
echo "Perl script offering possibilities to grep in the Exim queue output.
echo "More information: <https://www.exim.org/exim-html-current/doc/html/spec_html/ch-exim_utilities.html>.
echo "Match the sender address using a case-insensitive search:
exiqgrep -f '<{{email@somedomain.com}}>'
echo "Match the sender address and display message IDs only:
exiqgrep -i -f '<{{email@somedomain.com}}>'
echo "Match the recipient address:
exiqgrep -r '{{email@somedomain.com}}'
echo "Remove all messages matching the sender address from the queue:
exiqgrep -i -f '<{{email@somedomain.com}}>' | xargs exim -Mrm
echo "Test for bounced messages:
exiqgrep -f '^<>$'
echo "Display the count of bounced messages:
exiqgrep -c -f '^<>$'
echo "expect
echo "Script executor that interacts with other programs that require user input.
echo "More information: <https://manned.org/expect>.
echo "Execute an expect script from a file:
expect {{path/to/file}}
echo "Execute a specified expect script:
expect -c "{{commands}}"
echo "Enter an interactive REPL (use exit or Ctrl + D to exit):
expect -i
echo "export
echo "Command to mark shell variables in the current environment to be exported with any newly forked child processes.
echo "More information: <https://www.gnu.org/software/bash/manual/bash.html#index-export>.
echo "Set a new environment variable:
export {{VARIABLE}}={{value}}
echo "Remove an environment variable:
export -n {{VARIABLE}}
echo "Mark a shell function for export:
export -f {{FUNCTION_NAME}}
echo "Append something to the PATH variable:
export PATH=$PATH:{{path/to/append}}
echo "extrace
echo "Trace exec() calls.
echo "More information: <https://github.com/chneukirchen/extrace>.
echo "Trace all program executions occurring on the system:
sudo extrace
echo "Run a command and only trace descendants of this command:
sudo extrace {{command}}
echo "Print the current working directory of each process:
sudo extrace -d
echo "Resolve the full path of each executable:
sudo extrace -l
echo "Display the user running each process:
sudo extrace -u
echo "extrepo
echo "Manage external Debian repositories.
echo "It is used to manage external repositories in Debian.
echo "More information: <https://manned.org/extrepo.1p>.
echo "Search for a given package:
extrepo search {{package}}
echo "Enable the repository:
sudo extrepo enable {{repository_name}}
echo "Disable the repository:
sudo extrepo disable {{repository_name}}
echo "Update the repository:
sudo extrepo update {{repository_name}}
echo "extundelete
echo "Recover deleted files from ext3 or ext4 partitions by parsing the journal.
echo "See also date` for Unix time information and `umount for unmounting partitions.
echo "More information: <http://extundelete.sourceforge.net>.
echo "Restore all deleted files inside partition N on device X:
sudo extundelete {{/dev/sdXN}} --restore-all
echo "Restore a file from a path relative to root (Do not start the path with /):
extundelete {{/dev/sdXN}} --restore-file {{path/to/file}}
echo "Restore a directory from a path relative to root (Do not start the path with /):
extundelete {{/dev/sdXN}} --restore-directory {{path/to/directory}}
echo "Restore all files deleted after January 1st, 2020 (in Unix time):
extundelete {{/dev/sdXN}} --restore-all --after {{1577840400}}
echo "eyeD3
echo "Read and manipulate metadata of MP3 files.
echo "More information: <https://eyed3.readthedocs.io>.
echo "View information about an MP3 file:
eyeD3 {{filename.mp3}}
echo "Set the title of an MP3 file:
eyeD3 --title "{{A Title}}" {{filename.mp3}}
echo "Set the album of all the MP3 files in a directory:
eyeD3 --album "{{Album Name}}" {{*.mp3}}
echo "Set the front cover art for an MP3 file:
eyeD3 --add-image {{front_cover.jpeg}}:FRONT_COVER: {{filename.mp3}}
echo "f5fpc
echo "A proprietary commercial SSL VPN client by BIG-IP Edge.
echo "More information: <https://techdocs.f5.com/kb/en-us/products/big-ip_apm/manuals/product/apm-client-configuration-11-4-0/4.html>.
echo "Open a new VPN connection:
sudo f5fpc --start
echo "Open a new VPN connection to a specific host:
sudo f5fpc --start --host {{host.example.com}}
echo "Specify a username (user will be prompted for a password):
sudo f5fpc --start --host {{host.example.com}} --username {{user}}
echo "Show the current VPN status:
sudo f5fpc --info
echo "Shutdown the VPN connection:
# sudo f5fpc --stop
echo "fail2ban-client
echo "Configure and control fail2ban server.
echo "More information: <https://github.com/fail2ban/fail2ban>.
echo "Retrieve current status of the jail service:
fail2ban-client status {{jail}}
echo "Remove the specified IP from the jail service's ban list:
fail2ban-client set {{jail}} unbanip {{ip}}
echo "Verify fail2ban server is alive:
fail2ban-client ping
echo "faillock
echo "Display and modify authentication failure record files.
echo "More information: <https://manned.org/faillock>.
echo "List login failures of all users:
sudo faillock
echo "List login failures of the specified user:
sudo faillock --user {{user}}
echo "Reset the failure records of the specified user:
sudo faillock --user {{user}} --reset
echo "fakeroot
echo "Run a command in an environment faking root privileges for file manipulation.
echo "More information: <https://manpages.debian.org/latest/fakeroot/fakeroot.1.html>.
echo "Start the default shell as fakeroot:
fakeroot
echo "Run a command as fakeroot:
fakeroot -- {{command}} {{command_arguments}}
echo "Run a command as fakeroot and save the environment to a file on exit:
fakeroot -s {{path/to/file}} -- {{command}} {{command_arguments}}
echo "Load a fakeroot environment and run a command as fakeroot:
fakeroot -i {{path/to/file}} -- {{command}} {{command_arguments}}
echo "Run a command keeping the real ownership of files instead of pretending they are owned by root:
fakeroot --unknown-is-real -- {{command}} {{command_arguments}}
echo "Display help:
fakeroot --help
echo "faketime
echo "Fake the system time for a given command.
echo "More information: <https://manned.org/faketime>.
echo "Fake the time to this evening, before printing the result of date:
faketime '{{today 23:30}}' {{date}}
echo "Open a new bash shell, which uses yesterday as the current date:
faketime '{{yesterday}}' {{bash}}
echo "Simulate how a program would act next Friday night:
faketime '{{next Friday 1 am}}' {{path/to/program}}
echo "fallocate
echo "Reserve or deallocate disk space to files.
echo "The utility allocates space without zeroing.
echo "More information: <https://manned.org/fallocate>.
echo "Reserve a file taking up 700 MiB of disk space:
fallocate --length {{700M}} {{path/to/file}}
echo "Shrink an already allocated file by 200 MiB:
fallocate --collapse-range --length {{200M}} {{path/to/file}}
echo "Shrink 20 MB of space after 100 MiB in a file:
fallocate --collapse-range --offset {{100M}} --length {{20M}} {{path/to/file}}
echo "farge
echo "Display the color of a specific pixel on the screen in either hexadecimal or RGB formats.
echo "More information: <https://github.com/sdushantha/farge>.
echo "Display a small preview window of a pixel's color with it's hexadecimal value, and copy this value to the clipboard:
farge
echo "Copy a pixel's hexadecimal value to the clipboard without displaying a preview window:
farge --no-preview
echo "Output a pixel's hexadecimal value to stdout, and copy this value to the clipboard:
farge --stdout
echo "Output a pixel's RGB value to stdout, and copy this value to the clipboard:
farge --rgb --stdout
echo "Display a pixel's hexadecimal value as a notification which expires in 5000 milliseconds, and copy this value to the clipboard:
farge --notify --expire-time 5000
echo "fatlabel
echo "Sets or gets the label of a FAT32 partition.
echo "More information: <https://manned.org/fatlabel>.
echo "Get the label of a FAT32 partition:
fatlabel {{/dev/sda1}}
echo "Set the label of a FAT32 partition:
fatlabel {{/dev/sdc3}} "{{new_label}}"
echo "fcrackzip
echo "ZIP archive password cracking utility.
echo "More information: <https://manned.org/fcrackzip>.
echo "Brute-force a password with a length of 4 to 8 characters, and contains only alphanumeric characters (order matters):
fcrackzip --brute-force --length 4-8 --charset aA1 {{archive}}
echo "Brute-force a password in verbose mode with a length of 3 characters that only contains lowercase characters, $` and `%:
fcrackzip -v --brute-force --length 3 --charset a:$% {{archive}}
echo "Brute-force a password that contains only lowercase and special characters:
fcrackzip --brute-force --length 4 --charset a! {{archive}}
echo "Brute-force a password containing only digits, starting from the password 12345:
fcrackzip --brute-force --length 5 --charset 1 --init-password 12345 {{archive}}
echo "Crack a password using a wordlist:
fcrackzip --use-unzip --dictionary --init-password {{wordlist}} {{archive}}
echo "Benchmark cracking performance:
fcrackzip --benchmark
echo "fdisk
echo "A program for managing partition tables and partitions on a hard disk.
echo "See also: partprobe.
echo "More information: <https://manned.org/fdisk>.
echo "List partitions:
sudo fdisk -l
echo "Start the partition manipulator:
sudo fdisk {{/dev/sdX}}
echo "Once partitioning a disk, create a partition:
n
echo "Once partitioning a disk, select a partition to delete:
d
echo "Once partitioning a disk, view the partition table:
p
echo "Once partitioning a disk, write the changes made:
w
echo "Once partitioning a disk, discard the changes made:
q
echo "Once partitioning a disk, open a help menu:
m
echo "feedreader
# echo "A GUI desktop RSS client.
echo "More information: <https://jangernert.github.io/FeedReader/>.
echo "Print the count of unread articles:
feedreader --unreadCount
echo "Add a URL for a feed to follow:
feedreader --addFeed={{feed_url}}
echo "Grab a specific article using its URL:
feedreader --grabArticle={{article_url}}
echo "Download all images from a specific article:
feedreader --url={{feed_url}} --grabImages={{article_path}}
echo "Play media from a URL:
feedreader --playMedia={{article_url}}
echo "ffuf
echo "Subdomain and directory discovery tool.
echo "More information: <https://github.com/ffuf/ffuf>.
echo "Discover directories using a [w]ordlist on a target [u]rl with [c]olorized and [v]erbose output:
ffuf -w {{path/to/wordlist}} -u {{https://target/FUZZ}} -c -v
echo "Fuzz host-[H]eaders with a host file on a target website and [m]atch HTTP 200 [c]ode responses:
ffuf -w {{hosts.txt}} -u {{https://example.org}} -H "{{Host: FUZZ}}" -mc {{200}}
echo "Discover directories using a [w]ordlist on a target website with a max individual job time of 60 seconds and recursion discovery depth of 2 levels:
ffuf -w {{path/to/wordlist}} -u {{https://target/FUZZ}} -maxtime-job {{60}} -recursion -recursion-depth {{2}}
echo "Fuzz GET parameter on a target website and [f]ilter out message [s]ize response of 4242 bytes:
ffuf -w {{path/to/param_names.txt}} -u {{https://target/script.php?FUZZ=test_value}} -fs {{4242}}
echo "Fuzz POST method with POST [d]ata of password on a target website and [f]ilter out HTTP response [c]ode 401:
ffuf -w {{path/to/postdata.txt}} -X {{POST}} -d "{{username=admin\&password=FUZZ}}" -u {{https://target/login.php}} -fc {{401}}
echo "Discover subdomains using a subdomain list on a target website:
ffuf -w {{subdomains.txt}} -u {{https://website.com}} -H "{{Host: FUZZ.website.com}}"
echo "rename
echo "Rename multiple files.
echo "NOTE: this page refers to the command from the rename Debian package.
echo "More information: <https://manned.org/file-rename>.
echo "Rename files using a Perl Common Regular Expression (substitute 'foo' with 'bar' wherever found):
rename {{'s/foo/bar/'}} {{*}}
echo "Dry-run - display which renames would occur without performing them:
rename -n {{'s/foo/bar/'}} {{*}}
echo "Force renaming even if the operation would remove existing destination files:
rename -f {{'s/foo/bar/'}} {{*}}
echo "Convert filenames to lower case (use -f in case-insensitive filesystems to prevent "already exists" errors):
rename 'y/A-Z/a-z/' {{*}}
echo "Replace whitespace with underscores:
rename 's/\s+/_/g' {{*}}
echo "filefrag
echo "Report how badly fragmented a particular file might be.
echo "More information: <https://manned.org/filefrag>.
echo "Display a report for a specific file:
filefrag {{path/to/file}}
echo "Display a report for space-separated list of files:
filefrag {{path/to/file1}} {{path/to/file2}}
echo "Display a report using a 1024 byte blocksize:
filefrag -b {{path/to/file}}
echo "Sync the file before requesting the mapping:
filefrag -s {{path/to/files}}
echo "Display mapping of extended attributes:
filefrag -x {{path/to/files}}
echo "Display a report with verbose information:
filefrag -v {{path/to/files}}
echo "finch
echo "Console-based modular messaging client.
echo "More information: <https://developer.pidgin.im/wiki/Using%20Finch>.
echo "Launch finch:
finch
echo "Quit:
<Alt> + q OR <Ctrl> + c
echo "Show actions menu:
<Alt> + a
echo "Jump to n-th window:
<Alt> + {{number_key}}
echo "Close current window:
<Alt> + c
echo "Start moving a window, use arrow keys to move, press escape when done:
<Alt> + m
echo "Start resizing a window, use arrow keys to resize, press escape when done:
<Alt> + r
echo "findfs
echo "Finds a filesystem by label or UUID.
echo "More information: <https://mirrors.edge.kernel.org/pub/linux/utils/util-linux>.
echo "Search block devices by filesystem label:
findfs LABEL={{label}}
echo "Search by filesystem UUID:
findfs UUID={{uuid}}
echo "Search by partition label (GPT or MAC partition table):
findfs PARTLABEL={{partition_label}}
echo "Search by partition UUID (GPT partition table only):
findfs PARTUUID={{partition_uuid}}
echo "findmnt
echo "Find your filesystem.
echo "More information: <https://manned.org/findmnt>.
echo "List all mounted filesystems:
findmnt
echo "Search for a device:
findmnt {{/dev/sdb1}}
echo "Search for a mountpoint:
findmnt {{/}}
echo "Find filesystems in specific type:
findmnt -t {{ext4}}
echo "Find filesystems with specific label:
findmnt LABEL={{BigStorage}}
echo "firejail
echo "Securely sandboxes processes to containers using built-in Linux capabilities.
echo "More information: <https://manned.org/firejail>.
# echo "Integrate firejail with your desktop environment:
sudo firecfg
echo "Open a restricted Mozilla Firefox:
firejail {{firefox}}
echo "Start a restricted Apache server on a known interface and address:
firejail --net={{eth0}} --ip={{192.168.1.244}} {{/etc/init.d/apache2}} {{start}}
echo "List running sandboxes:
firejail --list
echo "List network activity from running sandboxes:
firejail --netstats
echo "Shutdown a running sandbox:
firejail --shutdown={{7777}}
echo "firewall-cmd
echo "The firewalld command-line client.
echo "More information: <https://firewalld.org/documentation/man-pages/firewall-cmd>.
echo "View the available firewall zones:
firewall-cmd --get-active-zones
echo "View the rules which are currently applied:
firewall-cmd --list-all
echo "Permanently move the interface into the block zone, effectively blocking all communication:
firewall-cmd --permanent --zone={{block}} --change-interface={{enp1s0}}
echo "Permanently open the port for a service in the specified zone (like port 443 when in the public zone):
firewall-cmd --permanent --zone={{public}} --add-service={{https}}
echo "Permanently close the port for a service in the specified zone (like port 80 when in the public zone):
firewall-cmd --permanent --zone={{public}} --remove-service={{http}}
echo "Permanently open two arbitrary ports in the specified zone:
firewall-cmd --permanent --zone={{public}} --add-port={{25565/tcp}} --add-port={{19132/udp}}
echo "Reload firewalld to force rule changes to take effect:
firewall-cmd --reload
echo "fixfiles
echo "Fix file SELinux security contexts.
echo "More information: <https://manned.org/fixfiles>.
echo "If specified with onboot, this fixfiles will record the current date in the /.autorelabel file, so that it can be used later to speed up labeling. If used with restore, the restore will only affect files that were modified today:
fixfiles -B
echo "[F]orce reset of context to match file_context for customizable files:
fixfiles -F
echo "Clear /tmp directory without confirmation:
fixfiles -f
echo "Use the [R]pm database to discover all files within specific packages and restore the file contexts:
fixfiles -R {{rpm_package1,rpm_package2 ...}}
echo "Run a diff on the PREVIOUS_FILECONTEXT file to the [C]urrently installed one, and restore the context of all affected files:
fixfiles -C PREVIOUS_FILECONTEXT
echo "Only act on files created after a specific date which will be passed to find --newermt command:
fixfiles -N {{YYYY-MM-DD HH:MM}}
echo "Bind [M]ount filesystems before relabeling them, this allows fixing the context of files or directories that have been mounted over:
fixfiles -M
echo "Modify [v]erbosity from progress to verbose and run restorecon` with `-v` instead of `-p:
fixfiles -v
echo "flameshot
echo "Screenshot utility with a GUI.
echo "Supports basic image editing, such as text, shapes, colors, and imgur.
echo "More information: <https://flameshot.org>.
echo "Create a fullscreen screenshot:
flameshot full
echo "Create a screenshot interactively:
flameshot gui
echo "Create a screenshot and save it to a specific path:
flameshot gui --path {{path/to/directory}}
echo "Create a screenshot interactively in a simplified mode:
flameshot launcher
echo "Create a screenshot from a specific monitor:
flameshot screen --number {{2}}
echo "Create a screenshot and print it to stdout:
flameshot gui --raw
echo "Create a screenshot and copy it to the clipboard:
flameshot gui --clipboard
echo "Create a screenshot with a specific delay in milliseconds:
flameshot full --delay {{5000}}
echo "flash
echo "Flash cards in the terminal.
echo "More information: <https://github.com/tallguyjenks/fla.sh>.
echo "Open a menu of available flashcard decks for selection:
flash
echo "Display the program version:
flash -v
echo "Display information about the flashcard system:
flash -i
echo "Display a list of available commands:
flash -h
echo "Change the previewer from default bat` to `cat:
flash -p {{cat}}
echo "flashrom
echo "Read, write, verify and erase flash chips.
echo "More information: <https://manned.org/flashrom>.
echo "Probe the chip, ensuring the wiring is correct:
flashrom --programmer {{programmer}}
echo "Read flash and save it to a file:
flashrom -p {{programmer}} --read {{path/to/file}}
echo "Write a file to the flash:
flashrom -p {{programmer}} --write {{path/to/file}}
echo "Verify the flash against a file:
flashrom -p {{programmer}} --verify {{path/to/file}}
echo "Probe the chip using Raspberry Pi:
flashrom -p {{linux_spi:dev=/dev/spidev0.0}}
echo "flatpak-builder
echo "Help build application dependencies.
echo "More information: <https://docs.flatpak.org/en/latest/flatpak-builder-command-reference.html>.
echo "Build a Flatpak and export it to a new repository:
flatpak-builder {{path/to/build_directory}} {{path/to/manifest}}
echo "Build a Flatpak and export it to the specified repository:
flatpak-builder --repo={{repository_name}} {{path/to/build_directory}} {{path/to/manifest}}
echo "Build a Flatpak and install it locally:
flatpak-builder --install {{path/to/build_directory}} {{path/to/manifest}}
echo "Build and sign a Flatpak and export it to the specified repository:
flatpak-builder --gpg-sign={{key_id}} --repo={{repository_name}} {{path/to/manifest}}
echo "Run a shell inside of an application sandbox without installing it:
flatpak-builder --run {{path/to/build_directory}} {{path/to/manifest}} {{sh}}
echo "flatpak
echo "Build, install and run flatpak applications and runtimes.
echo "More information: <https://docs.flatpak.org/en/latest/flatpak-command-reference.html#flatpak>.
echo "Run an installed application:
flatpak run {{name}}
echo "Install an application from a remote source:
flatpak install {{remote}} {{name}}
echo "List all installed applications and runtimes:
flatpak list
echo "Update all installed applications and runtimes:
flatpak update
echo "Add a remote source:
flatpak remote-add --if-not-exists {{remote_name}} {{remote_url}}
echo "Remove an installed application:
flatpak remove {{name}}
echo "Remove all unused applications:
flatpak remove --unused
echo "Show information about an installed application:
flatpak info {{name}}
echo "flock
echo "Manage locks from shell scripts.
echo "It can be used to ensure that only one process of a command is running.
echo "More information: <https://manned.org/flock>.
echo "Run a command with a file lock as soon as the lock is not required by others:
flock {{path/to/lock.lock}} --command "{{command}}"
echo "Run a command with a file lock, and exit if the lock doesn't exist:
flock {{path/to/lock.lock}} --nonblock --command "{{command}}"
echo "Run a command with a file lock, and exit with a specific error code if the lock doesn't exist:
flock {{path/to/lock.lock}} --nonblock --conflict-exit-code {{error_code}} -c "{{command}}"
echo "fluidsynth
echo "Synthesize audio from MIDI files.
echo "More information: <https://github.com/FluidSynth/fluidsynth/wiki/UserManual>.
echo "Play a MIDI file:
fluidsynth --audio-driver={{pipewire|pulseaudio}} {{path/to/soundfont.sf2}} {{path/to/file.midi}}
echo "fold
echo "Folds long lines for fixed-width output devices.
echo "More information: <https://www.gnu.org/software/coreutils/fold>.
echo "Fold lines in a fixed width:
fold --width {{width}} {{path/to/file}}
echo "Count width in bytes (the default is to count in columns):
fold --bytes --width {{width_in_bytes}} {{path/to/file}}
echo "Break the line after the rightmost blank within the width limit:
fold --spaces --width {{width}} {{path/to/file}}
echo "foreman
echo "Manage Procfile-based applications.
echo "More information: <https://manned.org/foreman>.
echo "Start an application with the Procfile in the current directory:
foreman start
echo "Start an application with a specified Procfile:
foreman start -f {{Procfile}}
echo "Start a specific application:
foreman start {{process}}
echo "Validate Procfile format:
foreman check
echo "Run one-off commands with the process's environment:
foreman run {{command}}
echo "Start all processes except the one named "worker":
foreman start -m all=1,{{worker}}=0
echo "fprintd-delete
echo "Remove fingerprints from the database.
echo "More information: <https://manned.org/fprintd-delete>.
echo "Remove all fingerprints for a specific user:
fprintd-delete {{username}}
echo "Remove a specific fingerprints for a specific user:
fprintd-delete {{username}} --finger {{left-thumb|left-index-finger|left-middle-finger|left-ring-finger|left-little-finger|right-thumb|right-index-finger|right-middle-finger|right-ring-finger|right-little-finger}}
echo "Display help:
fprintd-delete
echo "fprintd-enroll
echo "Enroll fingerprints into the database.
echo "More information: <https://manned.org/fprintd-enroll>.
echo "Enroll the right index finger for the current user:
fprintd-enroll
echo "Enroll a specific finger for the current user:
fprintd-enroll --finger {{left-thumb|left-index-finger|left-middle-finger|left-ring-finger|left-little-finger|right-thumb|right-index-finger|right-middle-finger|right-ring-finger|right-little-finger}}
echo "Enroll the right index finger for a specific user:
fprintd-enroll {{username}}
echo "Enroll a specific finger for a specific user:
fprintd-enroll --finger {{finger_name}} {{username}}
echo "Display help:
fprintd-enroll --help
echo "fprintd-list
echo "List enrolled fingerprints.
echo "More information: <https://manned.org/fprintd-list>.
echo "List enrolled fingerprints for a specific user:
fprintd-list {{username}}
echo "List enrolled fingerprints for a space-separated list of users:
fprintd-list {{username1 username2 ...}}
echo "Display help:
fprintd-list
echo "fprintd-verify
echo "Verify fingerprints against the database.
echo "More information: <https://manned.org/fprintd-verify>.
echo "Verify all stored fingerprints for the current user:
fprintd-verify
echo "Verify a specific fingerprint for the current user:
fprintd-verify --finger {{left-thumb|left-index-finger|left-middle-finger|left-ring-finger|left-little-finger|right-thumb|right-index-finger|right-middle-finger|right-ring-finger|right-little-finger}}
echo "Verify fingerprints for a specific user:
fprint-verify {{username}}
echo "Verify a specific fingerprint for a specific user:
fprintd-verify --finger {{finger_name}} {{username}}
echo "Fail the process if a fingerprint doesn't match with ones stored in the database for the current user:
fprint-verify --g-fatal-warnings
echo "Display help:
fprintd-verify --help
echo "fprintd
echo "Fingerprint management daemon.
# echo "More information: <https://fprint.freedesktop.org/>.
echo "Display the man page for fprintd:
man fprintd
echo "free
echo "Display amount of free and used memory in the system.
echo "More information: <https://manned.org/free>.
echo "Display system memory:
free
echo "Display memory in Bytes/KB/MB/GB:
free -{{b|k|m|g}}
echo "Display memory in human-readable units:
free -h
echo "Refresh the output every 2 seconds:
free -s {{2}}
echo "fsck
echo "Check the integrity of a filesystem or repair it. The filesystem should be unmounted at the time the command is run.
echo "More information: <https://manned.org/fsck>.
echo "Check filesystem /dev/sdXN, reporting any damaged blocks:
sudo fsck {{/dev/sdXN}}
echo "Check filesystem /dev/sdXN, reporting any damaged blocks and interactively letting the user choose to repair each one:
sudo fsck -r {{/dev/sdXN}}
echo "Check filesystem /dev/sdXN, reporting any damaged blocks and automatically repairing them:
sudo fsck -a {{/dev/sdXN}}
echo "fscrypt
echo "Go tool for managing Linux filesystem encryption.
echo "More information: <https://github.com/google/fscrypt>.
echo "Prepare the root filesystem for use with fscrypt:
fscrypt setup
echo "Enable filesystem encryption for a directory:
fscrypt encrypt {{path/to/directory}}
echo "Unlock an encrypted directory:
fscrypt unlock {{path/to/encrypted_directory}}
echo "Lock an encrypted directory:
fscrypt lock {{path/to/encrypted_directory}}
echo "fstrim
echo "Discard unused blocks on a mounted filesystem.
echo "Only supported by flash memory devices such as SSDs and microSD cards.
echo "More information: <https://manned.org/fstrim>.
echo "Trim unused blocks on all mounted partitions that support it:
sudo fstrim --all
echo "Trim unused blocks on a specified partition:
sudo fstrim {{/}}
echo "Display statistics after trimming:
sudo fstrim --verbose {{/}}
echo "fuser
echo "Display process IDs currently using files or sockets.
echo "More information: <https://manned.org/fuser>.
echo "Find which processes are accessing a file or directory:
fuser {{path/to/file_or_directory}}
echo "Show more fields (USER`, `PID`, `ACCESS` and `COMMAND):
fuser --verbose {{path/to/file_or_directory}}
echo "Identify processes using a TCP socket:
fuser --namespace tcp {{port}}
echo "Kill all processes accessing a file or directory (sends the SIGKILL signal):
fuser --kill {{path/to/file_or_directory}}
echo "Find which processes are accessing the filesystem containing a specific file or directory:
fuser --mount {{path/to/file_or_directory}}
echo "Kill all processes with a TCP connection on a specific port:
fuser --kill {{port}}/tcp
echo "fwupdmgr
echo "A tool for updating device firmware, including UEFI, using fwupd.
echo "More information: <https://fwupd.org/>.
echo "Display all devices detected by fwupd:
fwupdmgr get-devices
echo "Download the latest firmware metadata from LVFS:
fwupdmgr refresh
echo "List the updates available for devices on your system:
fwupdmgr get-updates
echo "Install firmware updates:
fwupdmgr update
echo "gbp
echo "A system to integrate the Debian package build system with Git.
echo "More information: <http://honk.sigxcpu.org/projects/git-buildpackage/manual-html/gbp.html>.
echo "Convert an existing Debian package to gbp:
gbp import-dsc {{path/to/package.dsc}}
echo "Build the package in the current directory using the default builder (debuild):
gbp buildpackage -jauto -us -uc
echo "Build a package in a pbuilder environment for Debian Bullseye:
DIST={{bullseye}} ARCH={{amd64}} gbp buildpackage -jauto -us -uc --git-builder={{git-pbuilder}}
echo "Specify a package to be a source-only upload in the .changes file (see <https://wiki.debian.org/SourceOnlyUpload>):
gbp buildpackage -jauto -us -uc --changes-options={{-S}}
echo "Import a new upstream release:
gbp import-orig --pristine-tar {{path/to/package.tar.gz}}
echo "gcov
echo "Code coverage analysis and profiling tool that discovers untested parts of a program.
echo "Also displays a copy of source code annotated with execution frequencies of code segments.
echo "More information: <https://gcc.gnu.org/onlinedocs/gcc/Invoking-Gcov.html>.
echo "Generate a coverage report named file.cpp.gcov:
gcov {{path/to/file.cpp}}
echo "Write individual execution counts for every basic block:
gcov --all-blocks {{path/to/file.cpp}}
echo "Write branch frequencies to the output file and print summary information to stdout as a percentage:
gcov --branch-probabilities {{path/to/file.cpp}}
echo "Write branch frequencies as the number of branches taken, rather than the percentage:
gcov --branch-counts {{path/to/file.cpp}}
echo "Do not create a gcov output file:
gcov --no-output {{path/to/file.cpp}}
echo "Write file level as well as function level summaries:
gcov --function-summaries {{path/to/file.cpp}}
echo "gdebi
echo "Simple tool to install .deb files.
echo "More information: <https://www.commandlinux.com/man-page/man1/gdebi.1.html>.
echo "Install local .deb packages resolving and installing its dependencies:
gdebi {{path/to/package.deb}}
echo "Display the program version:
gdebi --version
echo "Do not show progress information:
gdebi {{path/to/package.deb}} --quiet
echo "Set an APT configuration option:
gdebi {{path/to/package.deb}} --option={{APT_OPTS}}
echo "Use alternative root dir:
gdebi {{path/to/package.deb}} --root={{path/to/root_dir}}
echo "gedit
# echo "Text editor of the GNOME Desktop project.
echo "More information: <https://help.gnome.org/users/gedit/stable/>.
echo "Open a text file:
gedit {{path/to/file}}
echo "Open multiple text files:
gedit {{file1 file2 ...}}
echo "Open a text file with a specific encoding:
gedit --encoding={{UTF-8}} {{path/to/file}}
echo "Display a list of supported encodings:
gedit --list-encodings
echo "genfstab
echo "Arch Linux install script to generate output suitable for addition to an fstab file.
echo "More information: <https://man.archlinux.org/man/extra/arch-install-scripts/genfstab.8>.
echo "Display an fstab compatible output based on a volume label:
genfstab -L {{path/to/mount_point}}
echo "Display an fstab compatible output based on a volume UUID:
genfstab -U {{path/to/mount_point}}
echo "A usual way to generate an fstab file, requires root permissions:
genfstab -U {{/mnt}} >> {{/mnt/etc/fstab}}
echo "Append a volume into an fstab file to mount it automatically:
genfstab -U {{path/to/mount_point}} | sudo tee -a /etc/fstab
echo "genid
echo "Generate IDs, such as snowflakes, UUIDs, and a new GAID.
echo "More information: <https://github.com/bleonard252/genid>.
echo "Generate a UUIDv4:
genid uuid
echo "Generate a UUIDv5 using a namespace UUID and a specific name:
genid uuidv5 {{ce598faa-8dd0-49ee-8525-9e24fff71dca}} {{name}}
echo "Generate a Discord Snowflake, without a trailing newline (useful in shell scripts):
genid --script snowflake
echo "Generate a Generic Anonymous ID with a specific "real ID":
genid gaid {{real_id}}
echo "Generate a Snowflake with the epoch set to a specific date:
genid snowflake --epoch={{unix_epoch_time}}
echo "genie
echo "Set up and use a "bottle" namespace to run systemd under WSL (Windows Subsystem for Linux).
echo "To run these from Windows rather than an already-running distribution, precede them with wsl.
echo "More information: <https://github.com/arkane-systems/genie>.
echo "Initialize the bottle (run once, at start):
genie -i
echo "Run a login shell inside the bottle:
genie -s
echo "Run a specified command inside the bottle:
genie -c {{command}}
echo "genisoimage
echo "Pre-mastering program to generate ISO9660/Joliet/HFS hybrid filesystems.
echo "More information: <https://manpages.debian.org/latest/genisoimage/genisoimage.1.en.html>.
echo "Create an ISO image from the given source directory:
genisoimage -o {{myimage.iso}} {{path/to/source_directory}}
echo "Create an ISO image with files larger than 2GiB by reporting a smaller apparent size for ISO9660 filesystems:
genisoimage -o -allow-limited-size {{myimage.iso}} {{path/to/source_directory}}
echo "genkernel
echo "Gentoo Linux utility to compile and install kernels.
echo "More information: <https://wiki.gentoo.org/wiki/Genkernel>.
echo "Automatically compile and install a generic kernel:
sudo genkernel all
echo "Build and install the bzImage|initramfs|kernel|ramdisk only:
sudo genkernel {{bzImage|initramfs|kernel|ramdisk}}
echo "Apply changes to the kernel configuration before compiling and installing:
sudo genkernel --menuconfig all
echo "Generate a kernel with a custom name:
sudo genkernel --kernname={{custom_name}} all
echo "Use a kernel source outside the default directory /usr/src/linux:
sudo genkernel --kerneldir={{path/to/directory}} all
echo "getcap
echo "Command to display the name and capabilities of each specified file.
echo "More information: <https://manned.org/getcap>.
echo "Get capabilities for the given files:
getcap {{path/to/file1 path/to/file2 ...}}
echo "Get capabilities for all the files recursively under the given directories:
getcap -r {{path/to/directory1 path/to/directory2 ...}}
echo "Displays all searched entries even if no capabilities are set:
getcap -v {{path/to/file1 path/to/file2 ...}}
echo "getconf
echo "Get configuration values from your Linux system.
echo "More information: <https://manned.org/getconf.1>.
echo "List [a]ll configuration values available:
getconf -a
echo "List the configuration values for a specific directory:
getconf -a {{path/to/directory}}
echo "Check if your linux system is a 32-bit or 64-bit:
getconf LONG_BIT
echo "Check how many processes the current user can run at once:
getconf CHILD_MAX
echo "List every configuration value and then find patterns with the grep command (i.e every value with MAX in it):
getconf -a | grep MAX
echo "getent
echo "Get entries from Name Service Switch libraries.
echo "More information: <https://manned.org/getent>.
echo "Get list of all groups:
getent group
echo "See the members of a group:
getent group {{group_name}}
echo "Get list of all services:
getent services
echo "Find a username by UID:
getent passwd 1000
echo "Perform a reverse DNS lookup:
getent hosts {{host}}
echo "getfacl
echo "Get file access control lists.
echo "More information: <https://manned.org/getfacl>.
echo "Display the file access control list:
getfacl {{path/to/file_or_directory}}
echo "Display the file access control list with numeric user and group IDs:
getfacl -n {{path/to/file_or_directory}}
echo "Display the file access control list with tabular output format:
getfacl -t {{path/to/file_or_directory}}
# echo "getopt
echo "Parse command-line arguments.
# echo "More information: <https://www.gnu.org/software/libc/manual/html_node/Getopt.html>.
echo "Parse optional verbose`/`version flags with shorthands:
# getopt --options vV --longoptions verbose,version -- --version --verbose
echo "Add a --file` option with a required argument with shorthand `-f:
# getopt --options f: --longoptions file: -- --file=somefile
echo "Add a --verbose` option with an optional argument with shorthand `-v`, and pass a non-option parameter `arg:
# getopt --options v:: --longoptions verbose:: -- --verbose arg
echo "Accept a -r` and `--verbose` flag, a `--accept` option with an optional argument and add a `--target with a required argument option with shorthands:
# getopt --options rv::s::t: --longoptions verbose,source::,target: -- -v --target target
echo "gnome-calculator
# echo "The official calculator for the GNOME desktop environment.
echo "More information: <https://wiki.gnome.org/Apps/Calculator>.
echo "Launch the GNOME Calculator GUI:
gnome-calculator
# echo "Solve the specified equation without launching the desktop application:
gnome-calculator --solve {{2^5 * 2 + 5}}
echo "Display the version:
gnome-calculator --version
echo "gnome-extensions
echo "Manage gnome extensions from the terminal.
echo "More information: <https://wiki.gnome.org/Projects/GnomeShell/Extensions>.
echo "Display the version:
gnome-extensions version
echo "List all the installed extensions:
gnome-extensions list
echo "Display information about a specific extension:
gnome-extensions info "{{extension_id}}"
echo "Display help for a subcommand (like list):
gnome-extensions help {{subcommand}}
echo "Enable a specific extension:
gnome-extensions enable "{{extension_id}}"
echo "Disable a specific extension:
gnome-extension disable "{{extension_id}}"
echo "Uninstall a specific extension:
gnome-extension uninstall "{{extension_id}}"
echo "gnome-screenshot
echo "Capture the screen, a window, or a user-defined area and save the image to a file.
echo "More information: <https://manned.org/gnome-screenshot>.
echo "Take a screenshot and save it to the default location, normally ~/Pictures:
gnome-screenshot
echo "Take a screenshot and save it to the named file location:
gnome-screenshot --file {{path/to/file}}
echo "Take a screenshot and save it to the clipboard:
gnome-screenshot --clipboard
echo "Take a screenshot after the specified number of seconds:
gnome-screenshot --delay {{5}}
echo "Launch the GNOME Screenshot GUI:
gnome-screenshot --interactive
echo "Take a screenshot of the current window and save it to the specified file location:
gnome-screenshot --window --file {{path/to/file}}
echo "Take a screenshot after the specified number of seconds and save it to the clipboard:
gnome-screenshot --delay {{10}} --clipboard
echo "Display the version:
gnome-screenshot --version
echo "gnome-software
echo "Add and remove applications and update your system.
echo "More information: <https://apps.gnome.org/app/org.gnome.Software/>.
echo "Launch the GNOME Software GUI if it's not already running:
gnome-software
echo "Launch the GNOME Software GUI if it's not open, and navigate to the specified page:
gnome-software --mode {{updates|updated|installed|overview}}
echo "Launch the GNOME Software GUI if it's not open and view the details of the specified package:
gnome-software --details {{package}}
echo "Display the version:
gnome-software --version
echo "gnome-terminal
echo "The GNOME Terminal emulator.
echo "More information: <https://help.gnome.org/users/gnome-terminal/stable/>.
echo "Open a new GNOME terminal window:
gnome-terminal
echo "Run a specific command in a new terminal window:
gnome-terminal -- {{command}}
echo "Open a new tab in the last opened window instead:
gnome-terminal --tab
echo "Set the title of the new tab:
gnome-terminal --tab --title "{{title}}"
echo "goaccess
echo "An open source real-time web log analyzer.
echo "More information: <https://goaccess.io>.
echo "Analyze one or more log files in interactive mode:
goaccess {{path/to/logfile1 path/to/file2 ...}}
echo "Use a specific log-format (or pre-defined formats like "combined"):
goaccess {{path/to/logfile}} --log-format={{format}}
echo "Analyze a log from stdin:
tail -f {{path/to/logfile}} | goaccess -
echo "Analyze a log and write it to an HTML file in real-time:
goaccess {{path/to/logfile}} --output {{path/to/file.html}} --real-time-html
echo "goldeneye.py
echo "A HTTP DoS test tool.
echo "More information: <https://github.com/jseidl/GoldenEye>.
echo "Test a specific website:
./goldeneye.py {{url}}
echo "Test a specific website with 100 user agents and 200 concurrent sockets:
./goldeneye.py {{url}} --useragents 100 --sockets 200
echo "Test a specific website without verifying the SSL certificate:
./goldeneye.py {{url}} --nosslcheck
echo "Test a specific website in debug mode:
./goldeneye.py {{url}} --debug
echo "Display help:
./goldeneye.py --help
echo "goobook
echo "Access Google contacts from mutt or the command-line.
echo "More information: <https://manned.org/goobook>.
echo "Allow goobook to access Google contacts using OAuth2:
goobook authenticate
echo "Dump all contacts to XML (stdout):
goobook dump_contacts
echo "gpasswd
echo "Administer /etc/group` and `/etc/gshadow.
echo "More information: <https://manned.org/gpasswd>.
echo "Define group administrators:
sudo gpasswd -A {{user1,user2}} {{group}}
echo "Set the list of group members:
sudo gpasswd -M {{user1,user2}} {{group}}
echo "Create a password for the named group:
gpasswd {{group}}
echo "Add a user to the named group:
gpasswd -a {{user}} {{group}}
echo "Remove a user from the named group:
gpasswd -d {{user}} {{group}}
echo "grim
echo "Grab images (Screenshots) from a Wayland compositor.
echo "More information: <https://sr.ht/~emersion/grim>.
echo "Screenshot all outputs:
grim
echo "Screenshot a specific output:
grim -o {{path/to/output_file}}
echo "Screenshot a specific region:
grim -g "{{<x_position>,<y_position> <width>x<height>}}"
echo "Select a specific region and screenshot it, (using slurp):
grim -g "{{$(slurp)}}"
echo "Use a custom filename:
grim "{{path/to/file.png}}"
echo "Screenshot and copy to clipboard:
grim - | {{clipboard_manager}}
echo "groupadd
echo "Add user groups to the system.
echo "See also: groups`, `groupdel`, `groupmod.
echo "More information: <https://manned.org/groupadd>.
echo "Create a new group:
sudo groupadd {{group_name}}
echo "Create a new system group:
sudo groupadd --system {{group_name}}
echo "Create a new group with the specific groupid:
sudo groupadd --gid {{id}} {{group_name}}
echo "groupdel
echo "Delete existing user groups from the system.
echo "See also: groups`, `groupadd`, `groupmod.
echo "More information: <https://manned.org/groupdel>.
echo "Delete an existing group:
sudo groupdel {{group_name}}
echo "groupmod
echo "Modify existing user groups in the system.
echo "See also: groups`, `groupadd`, `groupdel.
echo "More information: <https://manned.org/groupmod>.
echo "Change the group name:
sudo groupmod --new-name {{new_group}} {{group_name}}
echo "Change the group id:
sudo groupmod --gid {{new_id}} {{group_name}}
echo "grub-bios-setup
echo "Set up a device to use GRUB with a BIOS configuration.
echo "You should use grub-install` instead of `grub-bios-setup in most cases.
echo "More information: <https://manned.org/grub-bios-setup.8>.
echo "Set up a device to boot with GRUB:
grub-bios-setup {{/dev/sdX}}
echo "Install even if problems are detected:
grub-bios-setup --force {{/dev/sdX}}
echo "Install GRUB in a specific directory:
grub-bios-setup --directory={{/boot/grub}} {{/dev/sdX}}
echo "grub-editenv
echo "Edit GRUB environment variables.
echo "More information: <https://www.gnu.org/software/grub/manual/grub/grub.html>.
echo "Set a default boot entry (Assuming the boot entry already exists):
grub-editenv /boot/grub/grubenv set default={{Ubuntu}}
echo "Display the current value of the timeout variable:
grub-editenv /boot/grub/grubenv list timeout
echo "Reset the saved_entry variable to the default:
grub-editenv /boot/grub/grubenv unset saved_entry
echo "Append "quiet splash" to the kernel command line:
grub-editenv /boot/grub/grubenv list kernel_cmdline
echo "grub-file
echo "Check if a file is of a specific bootable image type.
echo "More information: <https://manned.org/grub-file>.
echo "Check if a file is an ARM EFI image:
grub-file --is-arm-efi {{path/to/file}}
echo "Check if a file is an i386 EFI image:
grub-file --is-i386-efi {{path/to/file}}
echo "Check if a file is an x86_64 EFI image:
grub-file --is-x86_64-efi {{path/to/file}}
echo "Check if a file is an ARM image (Linux kernel):
grub-file --is-arm-linux {{path/to/file}}
echo "Check if a file is an x86 image (Linux kernel):
grub-file --is-x86-linux {{path/to/file}}
echo "Check if a file is an x86_64 XNU image (Mac OS X kernel):
grub-file --is-x86_64-xnu {{path/to/file}}
echo "grub-install
echo "Install GRUB to a device.
echo "More information: <https://www.gnu.org/software/grub/manual/grub/html_node/Installing-GRUB-using-grub_002dinstall.html>.
echo "Install GRUB on a BIOS system:
grub-install --target={{i386-pc}} {{path/to/device}}
echo "Install GRUB on an UEFI system:
grub-install --target={{x86_64-efi}} --efi-directory={{path/to/efi_directory}} --bootloader-id={{GRUB}}
echo "Install GRUB pre-loading specific modules:
grub-install --target={{x86_64-efi}} --efi-directory={{path/to/efi_directory}} --modules="{{part_gpt part_msdos}}"
echo "grub-mkconfig
echo "Generate a GRUB configuration file.
echo "More information: <https://www.gnu.org/software/grub/manual/grub/html_node/Invoking-grub_002dmkconfig.html>.
echo "Do a dry run and print the configuration to stdout:
sudo grub-mkconfig
echo "Generate the configuration file:
sudo grub-mkconfig --output={{/boot/grub/grub.cfg}}
echo "Print the help page:
grub-mkconfig --help
echo "grub-reboot
echo "Set the default boot entry for GRUB, for the next boot only.
echo "More information: <https://manned.org/grub-reboot>.
echo "Set the default boot entry to an entry number, name or identifier for the next boot:
sudo grub-reboot {{entry_number}}
echo "Set the default boot entry to an entry number, name or identifier for an alternative boot directory for the next boot:
sudo grub-reboot --boot-directory {{/path/to/boot_directory}} {{entry_number}}
echo "grub-script-check
echo "The program grub-script-check takes a GRUB script file and checks it for syntax errors.
echo "It may take a path as a non-option argument. If none is supplied, it will read from stdin.
echo "More information: <https://www.gnu.org/software/grub/manual/grub/html_node/Invoking-grub_002dscript_002dcheck.html>.
echo "Check a specific script file for syntax errors:
grub-script-check {{path/to/grub_config_file}}
echo "Display each line of input after reading it:
grub-script-check --verbose
echo "Display version:
grub-script-check --version
echo "Display help:
grub-script-check --help
echo "grub-set-default
echo "Set the default boot entry for GRUB.
echo "More information: <https://manned.org/grub-set-default>.
echo "Set the default boot entry to an entry number, name or identifier:
sudo grub-set-default {{entry_number}}
echo "Set the default boot entry to an entry number, name or identifier for an alternative boot directory:
sudo grub-set-default --boot-directory {{/path/to/boot_directory}} {{entry_number}}
echo "gs
echo "GhostScript is a PDF and PostScript interpreter.
echo "More information: <https://manned.org/gs>.
echo "To view a file:
gs -dQUIET -dBATCH {{file.pdf}}
echo "Reduce PDF file size to 150 dpi images for reading on a e-book device:
gs -dNOPAUSE -dQUIET -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/ebook -sOutputFile={{output.pdf}} {{input.pdf}}
echo "Convert PDF file (pages 1 through 3) to an image with 150 dpi resolution:
gs -dQUIET -dBATCH -dNOPAUSE -sDEVICE=jpeg -r150 -dFirstPage={{1}} -dLastPage={{3}} -sOutputFile={{output_%d.jpg}} {{input.pdf}}
echo "Extract pages from a PDF file:
gs -dQUIET -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile={{output.pdf}} {{input.pdf}}
echo "Merge PDF files:
gs -dQUIET -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile={{output.pdf}} {{input1.pdf}} {{input2.pdf}}
echo "Convert from PostScript file to PDF file:
gs -dQUIET -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile={{output.pdf}} {{input.ps}}
echo "gsettings
echo "Query and modify dconf settings with schema validation.
# echo "More information: <https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_the_desktop_environment_in_rhel_8/configuring-gnome-at-low-level_using-the-desktop-environment-in-rhel-8#using-gsettings-command_configuring-gnome-at-low-level>.
echo "Set the value of a key. Fails if the key doesn't exist or the value is out of range:
gsettings set {{org.example.schema}} {{example-key}} {{value}}
echo "Print the value of a key or the schema-provided default if the key has not been set in dconf:
gsettings get {{org.example.schema}} {{example-key}}
echo "Unset a key, so that its schema default value will be used:
gsettings reset {{org.example.schema}} {{example-key}}
echo "Display all (non-relocatable) schemas, keys, and values:
gsettings list-recursively
echo "Display all keys and values (default if not set) from one schema:
gsettings list-recursively {{org.example.schema}}
echo "Display schema-allowed values for a key (helpful with enum keys):
gsettings range {{org.example.schema}} {{example-key}}
echo "Display the human-readable description of a key:
gsettings describe {{org.example.schema}} {{example-key}}
echo "guake
echo "A drop-down terminal for GNOME.
echo "More information: <https://github.com/Guake/guake>.
echo "Toggle Guake visibility:
F12
echo "Toggle fullscreen mode:
F11
echo "Open a new tab:
<Ctrl> + <Shift> +T
echo "Close the terminal:
<Super> + X
echo "Go to the previous tab:
<Ctrl> + <PageUp>
echo "Search the selected text in the browser:
<Shift> + <Ctrl> + L
echo "guix package
echo "Install, upgrade and remove Guix packages, or rollback to previous configurations.
echo "More information: <https://guix.gnu.org/manual/html_node/Invoking-guix-package.html>.
echo "Install a new package:
guix package -i {{package}}
echo "Remove a package:
guix package -r {{package}}
echo "Search the package database for a regular expression:
guix package -s "{{search_pattern}}"
echo "List installed packages:
guix package -I
echo "List generations:
guix package -l
echo "Roll back to the previous generation:
guix package --roll-back
echo "gummy
echo "Screen brightness/temperature manager for Linux/X11.
echo "More information: <https://github.com/Fushko/gummy>.
echo "Set the screen temperature to 3000K:
gummy --temperature {{3000}}
echo "Set the screen backlight to 50%:
gummy --backlight {{50}}
echo "Set the screen pixel brightness to 45%:
gummy --brightness {{45}}
echo "Increase current screen pixel brightness by 10%:
gummy --brightness {{+10}}
echo "Decrease current screen pixel brightness by 10%:
gummy --brightness {{-10}}
echo "Set the temperature and pixel brightness for the second screen:
gummy --screen {{1}} --temperature {{3800}} --brightness {{65}}
echo "gzexe
echo "Compress executable files while keeping them executable.
echo "Back up the original file, appending ~ to its name and create a shell script that uncompresses and executes the binary inside it.
echo "More information: <https://manned.org/gzexe.1>.
echo "Compress an executable file in-place:
gzexe {{path/to/executable}}
echo "Decompress a compressed executable in-place (i.e. convert the shell script back to an uncompressed binary):
gzexe -d {{path/to/compressed_executable}}
echo "halt
echo "Halt the system.
echo "More information: <https://manned.org/halt.8>.
echo "Halt the system:
halt
echo "Power off the system (same as poweroff):
halt --poweroff
echo "Reboot the system (same as reboot):
halt --reboot
echo "Halt immediately without contacting the system manager:
halt --force
echo "Write the wtmp shutdown entry without halting the system:
halt --wtmp-only
echo "handlr
echo "Manage your default applications.
echo "More information: <https://github.com/chmln/handlr>.
echo "Open a URL in the default application:
handlr open {{https://example.com}}
echo "Open a PDF in the default PDF viewer:
handlr open {{path/to/file.pdf}}
echo "Set imv as the default application for PNG files:
# handlr set {{.png}} {{imv.desktop}}
echo "Set MPV as the default application for all audio files:
# handlr set {{'audio/*'}} {{mpv.desktop}}
echo "List all default apps:
handlr list
echo "Print the default application for PNG files:
handlr get {{.png}}
echo "hardinfo
echo "Show hardware information in GUI window.
echo "More information: <https://github.com/lpereira/hardinfo>.
echo "Start hardinfo:
hardinfo
echo "Print report to stdout:
hardinfo -r
echo "Save report to HTML file:
hardinfo -r -f html > hardinfo.html
echo "hcitool
echo "Monitor, configure connections, and send special commands to Bluetooth devices.
echo "More information: <https://manned.org/hcitool>.
echo "Scan for Bluetooth devices:
hcitool scan
echo "Output the name of a device, returning its MAC address:
hcitool name {{bdaddr}}
echo "Fetch information about a remote Bluetooth device:
hcitool info {{bdaddr}}
echo "Check the link quality to a Bluetooth device:
hcitool lq {{bdaddr}}
echo "Modify the transmit power level:
hcitool tpl {{bdaddr}} {{0|1}}
echo "Display the link policy:
hcitool lp
echo "Request authentication with a specific device:
hcitool auth {{bdaddr}}
echo "Display local devices:
hcitool dev
echo "hdparm
echo "Get and set SATA and IDE hard drive parameters.
echo "More information: <https://manned.org/hdparm>.
echo "Request the identification info of a given device:
sudo hdparm -I /dev/{{device}}
echo "Get the Advanced Power Management level:
sudo hdparm -B /dev/{{device}}
echo "Set the Advanced Power Management value (values 1-127 permit spin-down, and values 128-254 do not):
sudo hdparm -B {{1}} /dev/{{device}}
echo "Display the device's current power mode status:
sudo hdparm -C /dev/{{device}}
echo "Force a drive to immediately enter standby mode (usually causes a drive to spin down):
sudo hdparm -y /dev/{{device}}
echo "Put the drive into idle (low-power) mode, also setting its standby timeout:
sudo hdparm -S {{standby_timeout}} {{device}}
echo "Test the read speed of a specific device:
sudo hdparm -tT {{device}}
echo "head
echo "Output the first part of files.
echo "More information: <https://www.gnu.org/software/coreutils/head>.
echo "Output the first few lines of a file:
head --lines {{count}} {{path/to/file}}
echo "Output the first few bytes of a file:
head --bytes {{count}} {{path/to/file}}
echo "Output everything but the last few lines of a file:
head --lines -{{count}} {{path/to/file}}
echo "Output everything but the last few bytes of a file:
head --bytes -{{count}} {{path/to/file}}
echo "help
echo "Display information about Bash builtin commands.
echo "More information: <https://www.gnu.org/software/bash/manual/bash.html#index-help>.
echo "Display the full list of builtin commands:
help
echo "Print instructions on how to use the while loop construct:
help while
echo "Print instructions on how to use the for loop construct:
help for
echo "Print instructions on how to use [[ ]] for conditional commands:
help [[ ]]
echo "Print instruction on how to use (( )) to evaluate arithmetic expressions:
help \( \)
echo "Print instructions on how to use the cd command:
help cd
echo "hlint
echo "Tool for suggesting improvements to Haskell code.
echo "More information: <http://hackage.haskell.org/package/hlint>.
echo "Display suggestions for a given file:
hlint {{path/to/file}} options
echo "Check all Haskell files and generate a report:
hlint {{path/to/directory}} --report
echo "Automatically apply most suggestions:
hlint {{path/to/file}} --refactor
echo "Display additional options:
hlint {{path/to/file}} --refactor-options
echo "Generate a settings file ignoring all outstanding hints:
hlint {{path/to/file}} --default > {{.hlint.yaml}}
echo "homectl
echo "Create, remove, change or inspect home directories using the systemd-homed service.
echo "More information: <https://manned.org/homectl>.
echo "List user accounts and their associated home directories:
homectl list
echo "Create a user account and their associated home directory:
sudo homectl create {{username}}
echo "Remove a specific user and the associated home directory:
sudo homectl remove {{username}}
echo "Change the password for a specific user:
sudo homectl passwd {{username}}
echo "Run a shell or a command with access to a specific home directory:
sudo homectl with {{username}} -- {{command}} {{command_arguments}}
echo "Lock or unlock a specific home directory:
sudo homectl {{lock|unlock}} {{username}}
echo "Change the disk space assigned to a specific home directory to 100 GiB:
sudo homectl resize {{username}} {{100G}}
echo "Display help:
homectl --help
echo "homeshick
echo "Synchronize Git dotfiles.
echo "More information: <https://github.com/andsens/homeshick/wiki>.
echo "Create a new castle:
homeshick generate {{castle_name}}
echo "Add a file to your castle:
homeshick track {{castle_name}} {{path/to/file}}
echo "Go to a castle:
homeshick cd {{castle_name}}
echo "Clone a castle:
homeshick clone {{github_username}}/{{repository_name}}
echo "Symlink all files from a castle:
homeshick link {{castle_name}}
echo "hostnamectl
echo "Get or set the hostname of the computer.
echo "More information: <https://manned.org/hostnamectl>.
echo "Get the hostname of the computer:
hostnamectl
echo "Set the hostname of the computer:
sudo hostnamectl set-hostname "{{hostname}}"
echo "Set a pretty hostname for the computer:
sudo hostnamectl set-hostname --static "{{hostname.example.com}}" && sudo hostnamectl set-hostname --pretty "{{hostname}}"
echo "Reset hostname to its default value:
sudo hostnamectl set-hostname --pretty ""
echo "htpdate
echo "Synchronize local date and time via HTTP headers from web servers.
echo "More information: <http://www.vervest.org/htp/>.
echo "Synchronize date and time:
sudo htpdate {{host}}
echo "Perform simulation of synchronization, without any action:
htpdate -q {{host}}
echo "Compensate the systematic clock drift:
sudo htpdate -x {{host}}
echo "Set time immediate after the synchronization:
sudo htpdate -s {{host}}
echo "http-prompt
echo "An interactive command-line HTTP client featuring autocomplete and syntax highlighting.
echo "More information: <https://github.com/httpie/http-prompt>.
echo "Launch a session targeting the default URL of <http://localhost:8000> or the previous session:
http-prompt
echo "Launch a session with a given URL:
http-prompt {{http://example.com}}
echo "Launch a session with some initial options:
http-prompt {{localhost:8000/api}} --auth {{username:password}}
echo "http_load
echo "An HTTP benchmarking tool.
echo "Runs multiple HTTP fetches in parallel to test the throughput of a web server.
echo "More information: <http://www.acme.com/software/http_load/>.
echo "Emulate 20 requests based on a given URL list file per second for 60 seconds:
http_load -rate {{20}} -seconds {{60}} {{path/to/urls.txt}}
echo "Emulate 5 concurrent requests based on a given URL list file for 60 seconds:
http_load -parallel {{5}} -seconds {{60}} {{path/to/urls.txt}}
echo "Emulate 1000 requests at 20 requests per second, based on a given URL list file:
http_load -rate {{20}} -fetches {{1000}} {{path/to/urls.txt}}
echo "Emulate 1000 requests at 5 concurrent requests at a time, based on a given URL list file:
http_load -parallel {{5}} -fetches {{1000}} {{path/to/urls.txt}}
echo "httpie
echo "A user friendly HTTP tool.
echo "More information: <https://github.com/httpie/httpie>.
echo "Send a GET request (default method with no request data):
http {{https://example.com}}
echo "Send a POST request (default method with request data):
http {{https://example.com}} {{hello=World}}
echo "Send a POST request with redirected input:
http {{https://example.com}} < {{file.json}}
echo "Send a PUT request with a given JSON body:
http PUT {{https://example.com/todos/7}} {{hello=world}}
echo "Send a DELETE request with a given request header:
http DELETE {{https://example.com/todos/7}} {{API-Key:foo}}
echo "Show the whole HTTP exchange (both request and response):
http -v {{https://example.com}}
echo "Download a file:
http --download {{https://example.com}}
echo "Follow redirects and show intermediary requests and responses:
http --follow --all {{https://example.com}}
echo "hwclock
echo "Used for reading or changing the hardware clock. Usually requires root.
echo "More information: <https://manned.org/hwclock>.
echo "Display the current time as reported by the hardware clock:
hwclock
echo "Write the current software clock time to the hardware clock (sometimes used during system setup):
hwclock --systohc
echo "Write the current hardware clock time to the software clock:
hwclock --hctosys
echo "hwinfo
echo "Probe for the hardware present in the system.
echo "More information: <https://manpages.opensuse.org/hwinfo/hwinfo.8.en.html>.
echo "Get graphics card information:
hwinfo --gfxcard
echo "Get network device information:
hwinfo --network
echo "List disks and CD-ROM drives, abbreviating the output:
hwinfo --short --disk --cdrom
echo "Write all hardware information to a file:
hwinfo --all --log {{path/to/file}}
echo "Display help:
hwinfo --help
echo "i3-scrot
echo "Wrapper script around the screenshot utility scrot for the i3 window manager.
echo "The default save location is ~/Pictures` and can be changed in `~/.config/i3-scrot.conf.
echo "More information: <https://gitlab.manjaro.org/packages/community/i3/i3-scrot>.
echo "Capture a screenshot of the whole screen and save it to the default directory:
i3-scrot
echo "Capture a screenshot of the active window:
i3-scrot --window
echo "Capture a screenshot of a specific rectangular selection:
i3-scrot --select
echo "Capture a screenshot of the whole screen and copy it to the clipboard:
i3-scrot --desk-to-clipboard
echo "Capture a screenshot of the active window and copy it to the clipboard:
i3-scrot --window-to-clipboard
echo "Capture a screenshot of a specific selection and copy it to the clipboard:
i3-scrot --select-to-clipboard
echo "Capture a screenshot of the active window after a delay of 5 seconds:
i3-scrot --window {{5}}
echo "i3
echo "A dynamic tiling window manager.
echo "More information: <https://i3wm.org/docs/userguide.html>.
echo "Start i3 (Note that a pre-existing window manager must not be open when this command is run):
i3
echo "Open a new terminal window:
<Super> + <Return>
echo "Create a new workspace:
<Super> + <Shift> + {{number}}
echo "Switch to workspace number n:
<Super> + {{n}}
echo "Open new window horizontally:
<Super> + h
echo "Open new window vertically:
<Super> + v
echo "Open application (type out application name after executing command):
<Super> + D
echo "i3exit
echo "Exit the i3 window manager.
echo "More information: <https://gitlab.manjaro.org/packages/community/i3/i3exit>.
echo "Log out of i3:
i3exit logout
echo "Lock i3:
i3exit lock
echo "Shut down the system:
i3exit shutdown
echo "Suspend the system:
i3exit suspend
echo "Switch to the login screen to log in as a different user:
i3exit switch_user
echo "Hibernate the system:
i3exit hibernate
echo "Reboot the system:
i3exit reboot
echo "i3lock
echo "Simple screen locker built for the i3 window manager.
echo "More information: <https://i3wm.org/i3lock>.
echo "Lock the screen showing a white background:
i3lock
echo "Lock the screen with a simple color background (rrggbb format):
i3lock --color {{0000ff}}
echo "Lock the screen to a PNG background:
i3lock --image {{path/to/file.png}}
echo "Lock the screen and disable the unlock indicator (removes feedback on keypress):
i3lock --no-unlock-indicator
echo "Lock the screen and don't hide the mouse pointer:
i3lock --pointer {{default}}
echo "Lock the screen to a PNG background tiled over all monitors:
i3lock --image {{path/to/file.png}} --tiling
echo "Lock the screen and show the number of failed login attempts:
i3lock --show-failed-attempts
echo "i3status
echo "Status line for the i3 window manager.
echo "This command is usually called from the i3 configuration file.
echo "More information: <https://i3wm.org/i3status/manpage.html>.
echo "Print the status line to stdout periodically, using the default configuration:
i3status
echo "Print the status line to stdout periodically, using a specific configuration:
i3status -c {{path/to/i3status.conf}}
echo "Display the i3status version and help:
i3status -h
echo "i7z
echo "An Intel CPU (only i3, i5 and i7) realtime reporting tool.
echo "More information: <https://manned.org/i7z>.
echo "Start i7z (needs to be run in superuser mode):
sudo i7z
echo "ico
echo "Displays an animation of a polyhedron.
echo "More information: <https://manned.org/ico.1>.
echo "Display the wireframe of an icosahedron that changes its position every 0.1 seconds:
ico -sleep {{0.1}}
echo "Display a solid icosahedron with red faces on a blue background:
ico -faces -noedges -colors {{red}} -bg {{blue}}
echo "Display the wireframe of a cube with size 100x100 that moves by +1+2 per frame:
ico -obj {{cube}} -size {{100x100}} -delta {{+1+2}}
echo "Display the inverted wireframe of an icosahedron with line width 10 using 5 threads:
ico -i -lw {{10}} -threads {{5}}
echo "id3v2
echo "Manages id3v2 tags, converts and lists id3v1.
echo "More information: <https://manned.org/id3v2.1>.
echo "List all genres:
id3v2 ‐‐list‐genres
echo "List all tags of specific files:
id3v2 --list-tags {{path/to/file1 path/to/file2 ...}}
echo "Delete all id3v2` or `id3v1 tags of specific files:
id3v2 {{--delete‐v2|--delete‐v1}} {{path/to/file1 path/to/file2 ...}}
echo "Display help:
id3v2 --help
echo "Display version:
id3v2 ‐‐version
echo "ifdown
echo "Disable network interfaces.
echo "More information: <https://manned.org/ifdown>.
echo "Disable interface eth0:
ifdown {{eth0}}
echo "Disable all interfaces which are enabled:
ifdown -a
echo "ifmetric
echo "An IPv4 route metrics manipulation tool.
echo "More information: <https://0pointer.de/lennart/projects/ifmetric/>.
echo "Set the priority of the specified network interface (a higher number indicates lower priority):
sudo ifmetric {{interface}} {{value}}
echo "Reset the priority of the specified network interface:
sudo ifmetric {{interface}} {{0}}
# echo "iftop
echo "Show bandwidth usage on an interface by host.
# echo "More information: <https://manned.org/iftop>.
echo "Show the bandwidth usage:
# sudo iftop
echo "Show the bandwidth usage of a given interface:
# sudo iftop -i {{interface}}
echo "Show the bandwidth usage with port information:
# sudo iftop -P
echo "Do not show bar graphs of traffic:
# sudo iftop -b
echo "Do not look up hostnames:
# sudo iftop -n
echo "Get help about interactive commands:
?
echo "ifup
echo "Tool used to enable network interfaces.
echo "More information: <https://manpages.debian.org/latest/ifupdown/ifup.8.html>.
echo "Enable interface eth0:
ifup {{eth0}}
echo "Enable all the interfaces defined with "auto" in /etc/network/interfaces:
ifup -a
echo "ikaros
echo "Vanilla OS Tool for managing drivers for your device.
echo "More information: <https://github.com/Vanilla-OS/Ikaros>.
echo "Interactively install drivers for your device:
ikaros install {{device}}
echo "Automatically install the recommended drivers for your device:
ikaros auto-install {{device}}
echo "List devices:
ikaros list-devices
echo "img2txt
echo "Convert images to colour ASCII characters and output them to text-based coloured files.
echo "More information: <https://manned.org/img2txt>.
echo "Set output column count to a specific value:
img2txt --width={{10}}
echo "Set output line count to a specific value:
img2txt --height={{5}}
echo "Set output font width to a specific value:
img2txt --font-width={{12}}
echo "Set output font height to a specific value:
img2txt --font-height={{14}}
echo "Set image brightness to a specific value:
img2txt --brightness={{2}}
echo "imgp
echo "Command-line image resizer and rotator for JPEG and PNG images.
echo "More information: <https://github.com/jarun/imgp>.
echo "Convert single images and/or whole directories containing valid image formats:
imgp -x {{1366x1000}} {{path/to/directory}} {{path/to/file}}
echo "Scale an image by 75% and overwrite the source image to a target resolution:
imgp -x {{75}} -w {{path/to/file}}
echo "Rotate an image clockwise by 90 degrees:
imgp -o {{90}} {{path/to/file}}
echo "inotifywait
echo "Waits for changes to one or more files.
echo "More information: <https://manned.org/inotifywait>.
echo "Watch a specific file for events, exiting after the first one:
inotifywait {{path/to/file}}
echo "Continuously watch a specific file for events without exiting:
inotifywait --monitor {{path/to/file}}
echo "Watch a directory recursively for events:
inotifywait --monitor --recursive {{path/to/directory}}
echo "Watch a directory for changes, excluding files, whose names match a regular expression:
inotifywait --monitor --recursive --exclude "{{regular_expression}}" {{path/to/directory}}
echo "Watch a file for changes, exiting when no event occurs for 30 seconds:
inotifywait --monitor --timeout {{30}} {{path/to/file}}
echo "Only watch a file for file modification events:
inotifywait --event {{modify}} {{path/to/file}}
echo "Watch a file printing only events, and no status messages:
inotifywait --quiet {{path/to/file}}
echo "Run a command when a file is accessed:
inotifywait --event {{access}} {{path/to/file}} && {{command}}
echo "insmod
echo "Dynamically load modules into the Linux Kernel.
echo "More information: <https://manned.org/insmod>.
echo "Insert a kernel module into the Linux kernel:
insmod {{path/to/module.ko}}
echo "instaloader
echo "Download pictures, videos, captions, and other metadata from Instagram.
echo "Note: You will need to provide Instagram login information if you want high-quality media downloads.
echo "More information: <https://instaloader.github.io>.
echo "Download a profile:
instaloader {{profile_name}}
echo "Download highlights:
instaloader --highlights {{profile_name}}
echo "Download posts with geotags (if available), suppressing any user interaction:
instaloader --quiet --geotags {{profile_name}}
echo "Specify a user agent for HTTP requests:
instaloader --user-agent {{user_agent}} {{profile_name}}
echo "Specify login info and download posts (useful for private profiles):
instaloader --login {{username}} --password {{password}} {{profile_name}}
echo "Skip a target if the first downloaded file has been found (useful for updating Instagram archives):
instaloader --fast-update {{profile_name}}
echo "Download stories and IGTV videos (login required):
instaloader --login {{username}} --password {{password}} --stories --igtv {{profile_name}}
echo "Download all types of posts (login required):
instaloader --login {{username}} --password {{password}} --stories --igtv --highlights {{profile_name}}
echo "inxi
echo "Print a summary of system information and resources for debugging purposes.
echo "More information: <https://manned.org/inxi>.
echo "Print a summary of CPU, memory, hard drive and kernel information:
inxi
echo "Print a full description of CPU, memory, disk, network and process information:
inxi -Fz
echo "Print information about the distribution's repository:
inxi -r
echo "iostat
echo "Report statistics for devices and partitions.
echo "More information: <https://manned.org/iostat>.
echo "Display a report of CPU and disk statistics since system startup:
iostat
echo "Display a report of CPU and disk statistics with units converted to megabytes:
iostat -m
echo "Display CPU statistics:
iostat -c
echo "Display disk statistics with disk names (including LVM):
iostat -N
echo "Display extended disk statistics with disk names for device "sda":
iostat -xN {{sda}}
echo "Display incremental reports of CPU and disk statistics every 2 seconds:
iostat {{2}}
echo "ip address
echo "IP Address management subcommand.
echo "More information: <https://manned.org/ip-address>.
echo "List network interfaces and their associated IP addresses:
ip address
echo "Filter to show only active network interfaces:
ip address show up
echo "Display information about a specific network interface:
ip address show dev {{eth0}}
echo "Add an IP address to a network interface:
ip address add {{ip_address}} dev {{eth0}}
echo "Remove an IP address from a network interface:
ip address delete {{ip_address}} dev {{eth0}}
echo "Delete all IP addresses in a given scope from a network interface:
ip address flush dev {{eth0}} scope {{global|host|link}}
echo "ip link
echo "Manage network interfaces.
echo "More information: <https://man7.org/linux/man-pages/man8/ip-link.8.html>.
echo "Show information about all network interfaces:
ip link
echo "Show information about a specific network interface:
ip link show {{ethN}}
echo "Bring a network interface up or down:
ip link set {{ethN}} {{up|down}}
echo "Give a meaningful name to a network interface:
ip link set {{ethN}} alias "{{LAN Interface}}"
echo "Change the MAC address of a network interface:
ip link set {{ethN}} address {{ff:ff:ff:ff:ff:ff}}
echo "Change the MTU size for a network interface to use jumbo frames:
ip link set {{ethN}} mtu {{9000}}
echo "ip neighbour
echo "Neighbour/ARP tables management IP subcommand.
echo "More information: <https://manned.org/ip-neighbour.8>.
echo "Display the neighbour/ARP table entries:
ip neighbour
echo "Remove entries in the neighbour table on device eth0:
sudo ip neighbour flush dev {{eth0}}
echo "Perform a neighbour lookup and return a neighbour entry:
ip neighbour get {{lookup_ip}} dev {{eth0}}
echo "Add or delete an ARP entry for the neighbour IP address to eth0:
sudo ip neighbour {{add|del}} {{ip_address}} lladdr {{mac_address}} dev {{eth0}} nud reachable
echo "Change or replace an ARP entry for the neighbour IP address to eth0:
sudo ip neighbour {{change|replace}} {{ip_address}} lladdr {{new_mac_address}} dev {{eth0}}
echo "ip route list
echo "This command is an alias of ip route show.
echo "View documentation for the original command:
tldr ip-route-show
echo "ip route show
echo "Display subcommand for IP Routing table management.
echo "More information: <https://manned.org/ip-route>.
echo "Display the routing table:
ip route show
echo "Display the main routing table (same as first example):
ip route show {{main|254}}
echo "Display the local routing table:
ip route show table {{local|255}}
echo "Display all routing tables:
ip route show table {{all|unspec|0}}
echo "List routes from a given device only:
ip route show dev {{eth0}}
echo "List routes within a given scope:
ip route show scope link
echo "Display the routing cache:
ip route show cache
echo "Display only IPv6 or IPv4 routes:
ip {{-6|-4}} route show
echo "ip route
echo "IP Routing table management subcommand.
echo "More information: <https://manned.org/ip-route>.
echo "Display the routing table:
ip route {{show|list}}
echo "Add a default route using gateway forwarding:
sudo ip route add default via {{gateway_ip}}
echo "Add a default route using eth0:
sudo ip route add default dev {{eth0}}
echo "Add a static route:
sudo ip route add {{destination_ip}} via {{gateway_ip}} dev {{eth0}}
echo "Delete a static route:
sudo ip route del {{destination_ip}} dev {{eth0}}
echo "Change or replace a static route:
sudo ip route {{change|replace}} {{destination_ip}} via {{gateway_ip}} dev {{eth0}}
echo "Show which route will be used by the kernel to reach an IP address:
ip route get {{destination_ip}}
echo "ip rule
echo "IP routing policy database management.
echo "More information: <https://man7.org/linux/man-pages/man8/ip-rule.8.html>.
echo "Display the routing policy:
ip rule {{show|list}}
echo "Add a new rule based on packet source addresses:
sudo ip rule add from {{192.168.178.2/32}}
echo "Add a new rule based on packet destination addresses:
sudo ip rule add to {{192.168.178.2/32}}
echo "Delete a rule based on packet source addresses:
sudo ip rule delete from {{192.168.178.2/32}}
echo "Delete a rule based on packet destination addresses:
sudo ip rule delete to {{192.168.178.2/32}}
echo "Flush all deleted rules:
ip rule flush
echo "Save all rules to a file:
ip rule save > {{path/to/ip_rules.dat}}
echo "Restore all rules from a file:
ip rule restore < {{path/to/ip_rules.dat}}
echo "ip
echo "Show/manipulate routing, devices, policy routing and tunnels.
echo "Some subcommands such as ip address have their own usage documentation.
echo "More information: <https://www.man7.org/linux/man-pages/man8/ip.8.html>.
echo "List interfaces with detailed info:
ip address
echo "List interfaces with brief network layer info:
ip -brief address
echo "List interfaces with brief link layer info:
ip -brief link
echo "Display the routing table:
ip route
echo "Show neighbors (ARP table):
ip neighbour
echo "Make an interface up/down:
ip link set {{interface}} {{up|down}}
echo "Add/Delete an IP address to an interface:
ip addr add/del {{ip}}/{{mask}} dev {{interface}}
echo "Add a default route:
ip route add default via {{ip}} dev {{interface}}
echo "ip6tables-restore
echo "This command is an alias of iptables-restore for the IPv6 firewall.
echo "View documentation for the original command:
tldr iptables-restore
echo "ip6tables-save
echo "This command is an alias of iptables-save for the IPv6 firewall.
echo "View documentation for the original command:
tldr iptables-save
echo "ip6tables
echo "This command is an alias of iptables for the IPv6 firewall.
echo "View documentation for the original command:
tldr iptables
echo "ipcalc
echo "Perform simple operations and calculations on IP addresses and networks.
echo "More information: <https://manned.org/ipcalc>.
echo "Show information about an address or network with a given subnet mask:
ipcalc {{1.2.3.4}} {{255.255.255.0}}
echo "Show information about an address or network in CIDR notation:
ipcalc {{1.2.3.4}}/{{24}}
echo "Show the broadcast address of an address or network:
ipcalc -b {{1.2.3.4}}/{{30}}
echo "Show the network address of provided IP address and netmask:
ipcalc -n {{1.2.3.4}}/{{24}}
echo "Display geographic information about a given IP address:
ipcalc -g {{1.2.3.4}}
echo "ipcmk
echo "Create IPC (Inter-process Communication) resources.
echo "More information: <https://manned.org/ipcmk>.
echo "Create a shared memory segment:
ipcmk --shmem {{segment_size_in_bytes}}
echo "Create a semaphore:
ipcmk --semaphore {{element_size}}
echo "Create a message queue:
ipcmk --queue
echo "Create a shared memory segment with specific permissions (default is 0644):
ipcmk --shmem {{segment_size_in_bytes}} {{octal_permissions}}
echo "ipcrm
echo "Delete IPC (Inter-process Communication) resources.
echo "More information: <https://manned.org/ipcrm>.
echo "Delete a shared memory segment by ID:
ipcrm --shmem-id {{shmem_id}}
echo "Delete a shared memory segment by key:
ipcrm --shmem-key {{shmem_key}}
echo "Delete an IPC queue by ID:
ipcrm --queue-id {{ipc_queue_id}}
echo "Delete an IPC queue by key:
ipcrm --queue-key {{ipc_queue_key}}
echo "Delete a semaphore by ID:
ipcrm --semaphore-id {{semaphore_id}}
echo "Delete a semaphore by key:
ipcrm --semaphore-key {{semaphore_key}}
echo "Delete all IPC resources:
ipcrm --all
echo "ipset
echo "A tool to create IP sets for firewall rules.
echo "More information: <https://manned.org/ipset>.
echo "Create an empty IP set which will contain IP addresses:
ipset create {{set_name}} hash:ip
echo "Destroy a specific IP set:
ipset destroy {{set_name}}
echo "Add an IP address to a specific set:
ipset add {{set_name}} {{192.168.1.25}}
echo "Delete a specific IP address from a set:
ipset del {{set_name}} {{192.168.1.25}}
echo "Save an IP set:
ipset save {{set_name}} > {{path/to/ip_set}}
echo "iptables-restore
echo "Restore the iptables IPv4 configuration.
echo "Use ip6tables-restore to do the same for IPv6.
echo "More information: <https://manned.org/iptables-restore>.
echo "Restore the iptables configuration from a file:
sudo iptables-restore {{path/to/file}}
echo "iptables-save
echo "Save the iptables IPv4 configuration.
echo "Use ip6tables-save to to the same for IPv6.
echo "More information: <https://manned.org/iptables-save>.
echo "Print the iptables configuration:
sudo iptables-save
echo "Print the iptables configuration of a specific [t]able:
sudo iptables-save --table {{table}}
echo "Save the iptables configuration to a [f]ile:
sudo iptables-save --file {{path/to/file}}
echo "iptables
echo "Configure tables, chains and rules of the Linux kernel IPv4 firewall.
echo "Use ip6tables` to set rules for IPv6 traffic. See also: `iptables-save`, `iptables-restore.
echo "More information: <https://manned.org/iptables>.
echo "View chains, rules, packet/byte counters and line numbers for the filter table:
sudo iptables --verbose --numeric --list --line-numbers
echo "Set chain [P]olicy rule:
sudo iptables --policy {{chain}} {{rule}}
echo "[A]ppend rule to chain policy for IP:
sudo iptables --append {{chain}} --source {{ip}} --jump {{rule}}
echo "[A]ppend rule to chain policy for IP considering [p]rotocol and port:
sudo iptables --append {{chain}} --source {{ip}} --protocol {{tcp|udp|icmp|...}} --dport {{port}} --jump {{rule}}
echo "Add a NAT rule to translate all traffic from the 192.168.0.0/24 subnet to the host's public IP:
sudo iptables --table {{nat}} --append {{POSTROUTING}} --source {{192.168.0.0/24}} --jump {{MASQUERADE}}
echo "[D]elete chain rule:
sudo iptables --delete {{chain}} {{rule_line_number}}
echo "isoinfo
echo "Utility programs for dumping and verifying ISO disk images.
echo "More information: <https://manned.org/isoinfo>.
echo "List all the files included in an ISO image:
isoinfo -f -i {{path/to/image.iso}}
echo "E[x]tract a specific file from an ISO image and send it out stdout:
isoinfo -i {{path/to/image.iso}} -x {{/PATH/TO/FILE/INSIDE/ISO.EXT}}
echo "Show header information for an ISO disk image:
isoinfo -d -i {{path/to/image.iso}}
echo "isosize
echo "Display the size of an ISO file.
echo "More information: <https://manned.org/isosize>.
echo "Display the size of an ISO file:
isosize {{path/to/file.iso}}
echo "Display the block count and block size of an ISO file:
isosize --sectors {{path/to/file.iso}}
echo "Display the size of an ISO file divided by a given number (only usable when --sectors is not given):
isosize --divisor={{number}} {{path/to/file.iso}}
echo "ispell
echo "Interactive spell checking.
echo "More information: <https://www.cs.hmc.edu/~geoff/ispell-man.html>.
echo "Start an interactive session:
ispell
echo "Check for typos in the specified file and interactively apply suggestions:
ispell {{path/to/file}}
echo "Display the version:
ispell -v
echo "iw
echo "Show and manipulate wireless devices.
echo "More information: <https://manned.org/iw>.
echo "Scan for available wireless networks:
iw dev {{wlp}} scan
echo "Join an open wireless network:
iw dev {{wlp}} connect {{SSID}}
echo "Close the current connection:
iw dev {{wlp}} disconnect
echo "Show information about the current connection:
iw dev {{wlp}} link
echo "iwconfig
echo "Configure and show the parameters of a wireless network interface.
echo "More information: <https://manned.org/iwconfig>.
echo "Show the parameters and statistics of all the interfaces:
iwconfig
echo "Show the parameters and statistics of the specified interface:
iwconfig {{interface}}
echo "Set the ESSID (network name) of the specified interface (e.g. eth0 or wlp2s0):
iwconfig {{interface}} {{new_network_name}}
echo "Set the operating mode of the specified interface:
iwconfig {{interface}} mode {{ad hoc|Managed|Master|Repeater|Secondary|Monitor|Auto}}
echo "iwctl
echo "Control the iwd network supplicant.
echo "More information: <https://iwd.wiki.kernel.org/gettingstarted>.
echo "Start the interactive mode, in this mode you can enter the commands directly, with autocompletion:
iwctl
echo "Call general help:
iwctl --help
echo "Display your Wi-Fi stations:
iwctl station list
echo "Start looking for networks with a station:
iwctl station {{station}} scan
echo "Display the networks found by a station:
iwctl station {{station}} get-networks
echo "Connect to a network with a station, if credentials are needed they will be asked:
iwctl station {{station}} connect {{network_name}}
echo "jobs
echo "Shell builtin for viewing information about processes spawned by the current shell.
echo "Options other than -l` and `-p` are exclusive to `bash.
echo "More information: <https://manned.org/jobs>.
echo "View jobs spawned by the current shell:
jobs
echo "List jobs and their process IDs:
jobs -l
echo "Display information about jobs with changed status:
jobs -n
echo "Display only process IDs:
jobs -p
echo "Display running processes:
jobs -r
# echo "Display stopped processes:
jobs -s
echo "journalctl
echo "Query the systemd journal.
echo "More information: <https://manned.org/journalctl>.
echo "Show all messages with priority level 3 (errors) from this [b]oot:
journalctl -b --priority={{3}}
echo "Delete journal logs which are older than 2 days:
journalctl --vacuum-time={{2d}}
echo "[f]ollow new messages (like tail -f for traditional syslog):
journalctl -f
echo "Show all messages by a specific [u]nit:
journalctl -u {{unit}}
echo "Show logs for a given unit since the last time it started:
journalctl _SYSTEMD_INVOCATION_ID=$(systemctl show --value --property=InvocationID {{unit}})
echo "Filter messages within a time range (either timestamp or placeholders like "yesterday"):
journalctl --since {{now|today|yesterday|tomorrow}} --until {{YYYY-MM-DD HH:MM:SS}}
echo "Show all messages by a specific process:
journalctl _PID={{pid}}
echo "Show all messages by a specific executable:
journalctl {{path/to/executable}}
echo "jpegtran
echo "Perform lossless transformation of JPEG files.
echo "More information: <https://manned.org/jpegtran>.
echo "Mirror an image horizontally or vertically:
jpegtran -flip {{horizontal|vertical}} {{path/to/image.jpg}} > {{path/to/output.jpg}}
echo "Rotate an image 90, 180 or 270 degrees clockwise:
jpegtran -rotate {{90|180|270}} {{path/to/image.jpg}} > {{path/to/output.jpg}}
echo "Transpose the image across the upper-left to lower right axis:
jpegtran -transpose {{path/to/image.jpg}} > {{path/to/output.jpg}}
echo "Transverse the image across the upper right to lower left axis:
jpegtran -transverse {{path/to/image.jpg}} > {{path/to/output.jpg}}
echo "Convert the image to grayscale:
jpegtran -grayscale {{path/to/image.jpg}} > {{path/to/output.jpg}}
echo "Crop the image to a rectangular region of width W` and height `H from the upper-left corner, saving the output to a specific file:
jpegtran -crop {{W}}x{{H}} -outfile {{path/to/output.jpg}} {{path/to/image.jpg}}
echo "Crop the image to a rectangular region of width W` and height `H`, starting at point `X` and `Y from the upper-left corner:
jpegtran -crop {{W}}x{{H}}+{{X}}+{{Y}} {{path/to/image.jpg}} > {{path/to/output.jpg}}
echo "just
echo "A V8 JavaScript runtime for Linux.
echo "More information: <https://github.com/just-js/just>.
echo "Start a REPL (interactive shell):
just
echo "Run a JavaScript file:
just {{path/to/file.js}}
echo "Evaluate JavaScript code by passing it as an argument:
just eval "{{code}}"
echo "Initialize a new project in a directory of the same name:
just init {{project_name}}
echo "Build a JavaScript application into an executable:
just build {{path/to/file.js}} --static
echo "kde-inhibit
# echo "Inhibit various desktop functions while a command runs.
echo "More information: <https://invent.kde.org/plasma/kde-cli-tools/-/blob/master/kdeinhibit/main.cpp>.
echo "Inhibit power management:
kde-inhibit --power {{command}} {{command_arguments}}
echo "Inhibit screen saver:
kde-inhibit --screenSaver {{command}} {{command_arguments}}
echo "Launch VLC, and inhibit color correction (night mode) while it's running:
kde-inhibit --colorCorrect {{vlc}}
echo "kdesrc-build
echo "Easily build KDE components from its source repositories.
echo "More information: <https://invent.kde.org/sdk/kdesrc-build>.
echo "Initialize kdesrc-build:
kdesrc-build --initial-setup
echo "Compile a KDE component and its dependencies from source:
kdesrc-build {{component_name}}
echo "Compile a component without updating its local code and without compiling its dependencies:
kdesrc-build --no-src --no-include-dependencies {{component_name}}
echo "Refresh the build directories before compiling:
kdesrc-build --refresh-build {{component_name}}
echo "Resume compilation from a specific dependency:
kdesrc-build --resume-from={{dependency_component}} {{component_name}}
echo "Print full compilation info:
kdesrc-build --debug {{component_name}}
echo "kdesrc-run
echo "Run KDE components that have been built with kdesrc-build.
echo "More information: <https://invent.kde.org/sdk/kdesrc-build>.
echo "Run a component:
kdesrc-run {{component_name}}
echo "kdialog
echo "Show KDE dialog boxes from within shell scripts.
echo "More information: <https://develop.kde.org/deploy/kdialog/>.
echo "Open a dialog box displaying a specific message:
kdialog --msgbox "{{message}}" "{{optional_detailed_message}}"
echo "Open a question dialog with a yes` and `no` button, returning `0` and `1, respectively:
kdialog --yesno "{{message}}"
echo "Open a warning dialog with a yes`, `no`, and `cancel` button, returning `0`, `1`, or `2 respectively:
kdialog --warningyesnocancel "{{message}}"
echo "Open an input dialog box and print the input to stdout` when `OK is pressed:
kdialog --inputbox "{{message}}" "{{optional_default_text}}"
echo "Open a dialog to prompt for a specific password and print it to stdout:
kdialog --password "{{message}}"
echo "Open a dialog containing a specific dropdown menu and print the selected item to stdout:
kdialog --combobx "{{message}}" "{{item1}}" "{{item2}}" "{{...}}"
echo "Open a file chooser dialog and print the selected file's path to stdout:
# kdialog --getopenfilename
echo "Open a progressbar dialog and print a DBUS reference for communication to stdout:
kdialog --progressbar "{{message}}"
echo "kdocker
echo "Easily dock applications to the system tray.
echo "More information: <https://github.com/user-none/KDocker>.
echo "Display a cursor to send a window to the system tray when pressing the left mouse button (press any other mouse button to cancel):
kdocker
echo "Open an application and send it to the system tray:
kdocker {{application}}
echo "Send focused window to the system tray:
kdocker -f
echo "Display a cursor to send a window to the system tray with a custom icon when pressing the left mouse button:
kdocker -i {{/path/to/icon}}
echo "Open an application, send it to the system tray and if focus is lost, minimize it:
kdocker -l {{application}}
echo "Print version:
kdocker --version
echo "kernel-install
echo "Add and remove kernel and initrd images to and from /boot.
echo "More information: <https://manned.org/kernel-install.8>.
echo "Add kernel and initramfs images to bootloader partition:
sudo kernel-install add {{kernel-version}} {{kernel-image}} {{path/to/initrd-file ...}}
echo "Remove kernel from the bootloader partition:
sudo kernel-install remove {{kernel-version}}
echo "Show various paths and parameters that have been configured or auto-detected:
sudo kernel-install inspect {{kernel-image}}
echo "kexec
echo "Directly reboot into a new kernel.
echo "More information: <https://manned.org/kexec>.
echo "Load a new kernel:
kexec -l {{path/to/kernel}} --initrd={{path/to/initrd}} --command-line={{arguments}}
echo "Load a new kernel with current boot parameters:
kexec -l {{path/to/kernel}} --initrd={{path/to/initrd}} --reuse-cmdline
echo "Execute a currently loaded kernel:
kexec -e
echo "Unload current kexec target kernel:
kexec -u
echo "keyctl
echo "Manipulate the Linux kernel keyring.
echo "More information: <https://manned.org/keyctl>.
echo "List keys in a specific keyring:
keyctl list {{target_keyring}}
echo "List current keys in the user default session:
keyctl list {{@us}}
echo "Store a key in a specific keyring:
keyctl add {{type_keyring}} {{key_name}} {{key_value}} {{target_keyring}}
echo "Store a key with its value from stdin:
echo -n {{key_value}} | keyctl padd {{type_keyring}} {{key_name}} {{target_keyring}}
echo "Put a timeout on a key:
keyctl timeout {{key_name}} {{timeout_in_seconds}}
echo "Read a key and format it as a hex-dump if not printable:
keyctl read {{key_name}}
echo "Read a key and format as-is:
keyctl pipe {{key_name}}
echo "Revoke a key and prevent any further action on it:
keyctl revoke {{key_name}}
echo "killall
echo "Send kill signal to all instances of a process by name (must be exact name).
echo "All signals except SIGKILL and SIGSTOP can be intercepted by the process, allowing a clean exit.
echo "More information: <https://manned.org/killall>.
echo "Terminate a process using the default SIGTERM (terminate) signal:
killall {{process_name}}
echo "List available signal names (to be used without the 'SIG' prefix):
killall --list
echo "Interactively ask for confirmation before termination:
killall -i {{process_name}}
echo "Terminate a process using the SIGINT (interrupt) signal, which is the same signal sent by pressing Ctrl + C:
killall -INT {{process_name}}
echo "Force kill a process:
killall -KILL {{process_name}}
echo "kjv
# echo "The word of God available right on your desktop.
echo "More information: <https://github.com/bontibon/kjv>.
echo "Display books:
kjv -l
echo "Open a specific book:
kjv {{Genesis}}
echo "Open a specific chapter of a book:
kjv {{Genesis}} {{2}}
echo "Open a specific verse of a specific chapter of a book:
kjv {{John}} {{3}}:{{16}}
echo "Open a specific range of verses of a book's chapter:
kjv {{Proverbs}} {{3}}:{{1-6}}
echo "Display a specific range of verses of a book from different chapters:
kjv {{Matthew}} {{1}}:{{7}}-{{2}}:{{6}}
echo "Display all verses that match a pattern:
kjv /{{Plagues}}
echo "Display all verses that match a pattern in a specific book:
kjv {{1Jn}}/{{antichrist}}
echo "konsave
echo "Save and apply your Linux customizations with just one command.
echo "More information: <https://github.com/Prayag2/konsave>.
echo "Save the current configuration as a profile:
konsave --save {{profile_name}}
echo "Apply a profile:
konsave --apply {{profile_name}}
echo "Save the current configuration as a profile, overwriting existing profiles if they exist with the same name:
konsave -s {{profile_name}} --force
echo "List all profiles:
konsave --list
echo "Remove a profile:
konsave --remove {{profile_name}}
echo "Export a profile as a .knsv to the home directory:
konsave --export-profile {{profile_name}}
echo "Import a .knsv profile:
konsave --import-profile {{path/to/profile_name.knsv}}
echo "konsole
echo "KDE's terminal emulator.
echo "More information: <https://docs.kde.org/stable5/en/konsole/konsole/command-line-options.html>.
echo "Open the terminal in a specific directory:
konsole --workdir {{path/to/directory}}
echo "[e]xecute a specific command and don't close the window after it exits:
konsole --noclose -e "{{command}}"
echo "Open a new tab:
konsole --new-tab
echo "Open the terminal in the background and bring to the front when Ctrl+Shift+F12 is pressed:
konsole --background-mode
echo "kpackagetool5
echo "KPackage Manager: Install, list, remove Plasma packages.
echo "More information: <https://techbase.kde.org/Development/Tutorials/Plasma5/QML2/GettingStarted#Kpackagetool5>.
echo "List all known package types that can be installed:
kpackagetool5 --list-types
echo "Install the package from a directory:
kpackagetool5 --type {{package_type}} --install {{path/to/directory}}
echo "Update installed package from a directory:
kpackagetool5 --type {{package_type}} --upgrade {{path/to/directory}}
echo "List installed plasmoids (--global for all users):
kpackagetool5 --type Plasma/Applet --list --global
echo "Remove a plasmoid by name:
kpackagetool5 --type Plasma/Applet --remove "{{name}}"
echo "kpartx
echo "Create device maps from partition tables.
echo "More information: <https://manned.org/kpartx>.
echo "Add partition mappings:
kpartx -a {{whole_disk.img}}
echo "Delete partition mappings:
kpartx -d {{whole_disk.img}}
echo "List partition mappings:
kpartx -l {{whole_disk.img}}
echo "kreadconfig5
echo "Read KConfig entries for KDE Plasma.
echo "More information: <https://userbase.kde.org/KDE_System_Administration/Configuration_Files>.
echo "Read a key from the global configuration:
kreadconfig5 --group {{group_name}} --key {{key_name}}
echo "Read a key from a specific configuration file:
kwriteconfig5 --file {{path/to/file}} --group {{group_name}} --key {{key_name}}
echo "Check if systemd is used to start the Plasma session:
kreadconfig5 --file {{startkderc}} --group {{General}} --key {{systemdBoot}}
echo "kscreen-console
echo "Command-line tool to query KScreen's status.
echo "More information: <https://manned.org/kscreen-console>.
echo "Show all outputs and configuration files to attach to a bug report:
kscreen-console bug
echo "Show paths to KScreen configuration files:
kscreen-console config
echo "Show KScreen output information and configuration:
kscreen-console outputs
echo "Monitor for changes:
kscreen-console monitor
echo "Show the current KScreen configuration as JSON:
kscreen-console json
echo "Display help:
kscreen-console --help
echo "Display help including Qt specific command-line options:
kscreen-console --help-all
echo "kscreen-doctor
echo "Change and manipulate the screen setup.
echo "More information: <https://invent.kde.org/plasma/libkscreen>.
echo "Show display output information:
kscreen-doctor --outputs
echo "Set the rotation of a display output with an ID of 1 to the right:
kscreen-doctor {{output.1.rotation.right}}
echo "Set the scale of a display output with an ID of HDMI-2 to 2 (200%):
kscreen-doctor {{output.HDMI-2.scale.2}}
# echo "ksvgtopng5
echo "Convert SVG files to PNG format.
# echo "More information: <https://invent.kde.org/plasma/kde-cli-tools/-/blob/master/ksvgtopng/ksvgtopng.cpp>.
echo "Convert an SVG file (should be an absolute path) to PNG:
# ksvgtopng5 {{width}} {{height}} {{path/to/file.svg}} {{output_filename.png}}
echo "kwrite
# echo "Text editor of the KDE Desktop project.
echo "See also kate.
echo "More information: <https://apps.kde.org/kwrite/>.
echo "Open a text file:
kwrite {{path/to/file}}
echo "Open multiple text files:
kwrite {{file1 file2 ...}}
echo "Open a text file with a specific encoding:
kwrite --encoding={{UTF-8}} {{path/to/file}}
echo "Open a text file and navigate to a specific line and column:
kwrite --line {{line_number}} --column {{column_number}} {{path/to/file}}
echo "kwriteconfig5
echo "Write KConfig entries for KDE Plasma.
echo "More information: <https://userbase.kde.org/KDE_System_Administration/Configuration_Files>.
echo "Display help:
kwriteconfig5 --help
echo "Set a global configuration key:
kwriteconfig5 --group {{group_name}} --key {{key}} {{value}}
echo "Set a key in a specific configuration file:
kwriteconfig5 --file {{path/to/file}} --group {{group_name}} --key {{key}} {{value}}
echo "Delete a key:
kwriteconfig5 --group {{group_name}} --key {{key}} --delete
echo "Use systemd to start the Plasma session when available:
kwriteconfig5 --file {{startkderc}} --group {{General}} --key {{systemdBoot}} {{true}}
echo "Hide the title bar when a window is maximized (like Ubuntu):
kwriteconfig5 --file {{~/.config/kwinrc}} --group {{Windows}} --key {{BorderlessMaximizedWindows}} {{true}}
echo "Configure KRunner to open with the Meta (Command/Windows) global hotkey:
# kwriteconfig5 --file {{~/.config/kwinrc}} --group {{ModifierOnlyShortcuts}} --key {{Meta}} {{"org.kde.kglobalaccel,/component/krunner_desktop,org.kde.kglobalaccel.Component,invokeShortcut,_launch"}}
# echo "laptop-detect
# echo "Attempt to determine if the script is running on a laptop or desktop.
# echo "More information: <https://gitlab.com/debiants/laptop-detect>.
# echo "Return an exit status of 0 if the current device is likely a laptop, else returns 1:
# laptop-detect
echo "Print the type of device that the current system is detected as:
# laptop-detect --verbose
echo "Display the version:
# laptop-detect --version
echo "larasail
echo "Manage Laravel on Digital Ocean servers.
echo "More information: <https://github.com/thedevdojo/larasail>.
echo "Set up the server with Laravel dependencies using the default PHP version:
larasail setup
echo "Set up the server with Laravel dependencies using a specific PHP version:
larasail setup {{php71}}
echo "Add a new Laravel site:
larasail host {{domain}} {{path/to/site_directory}}
echo "Retrieve the Larasail user password:
larasail pass
echo "Retrieve the Larasail MySQL password:
larasail mysqlpass
echo "lastb
echo "Show a listing of last logged in users.
echo "More information: <https://manned.org/lastb>.
echo "Show a list of all last logged in users:
sudo lastb
echo "Show a list of all last logged in users since a given time:
sudo lastb --since {{YYYY-MM-DD}}
echo "Show a list of all last logged in users until a given time:
sudo lastb --until {{YYYY-MM-DD}}
echo "Show a list of all logged in users at a specific time:
sudo lastb --present {{hh:mm}}
echo "Show a list of all last logged in users and translate the IP into a hostname:
sudo lastb --dns
echo "lastcomm
echo "Show last commands executed.
echo "More information: <https://manpages.debian.org/latest/acct/lastcomm.1.en.html>.
echo "Print information about all the commands in the acct (record file):
lastcomm
echo "Display commands executed by a given user:
lastcomm --user {{user}}
echo "Display information about a given command executed on the system:
lastcomm --command {{command}}
echo "Display information about commands executed on a given terminal:
lastcomm --tty {{terminal_name}}
echo "lastlog
echo "Show the most recent login of all users or of a given user.
echo "More information: <https://manned.org/lastlog>.
echo "Display the most recent login of all users:
lastlog
echo "Display the lastlog record of the specified user:
lastlog --user {{username}}
echo "Display records older than 7 days:
lastlog --before 7
echo "Display records more recent than 3 days:
lastlog --time 3
echo "latte-dock
# echo "Replacement dock for Plasma desktop.
echo "More information: <https://github.com/KDE/latte-dock>.
echo "Clear QML cache:
latte-dock --clear-cache
echo "Import and load default layout on startup:
latte-dock --default-layout
echo "Load a specific layout on startup:
latte-dock --layout {{layout_name}}
echo "Import and load a specific layout:
latte-dock --import-layout {{path/to/file}}
echo "lchage
echo "Display or change user password policy.
echo "More information: <https://manned.org/lchage>.
echo "Disable password expiration for the user:
sudo lchage --date -1 {{username}}
echo "Display the password policy for the user:
sudo lchage --list {{username}}
echo "Require password change for the user a certain number of days after the last password change:
sudo lchage --maxdays {{number_of_days}} {{username}}
echo "Start warning the user a certain number of days before the password expires:
sudo lchage --warndays {{number_of_days}} {{username}}
echo "lci
echo "LOLCODE interpreter written in C.
echo "More information: <https://github.com/justinmeza/lci>.
echo "Run a LOLCODE file:
lci {{path/to/file}}
echo "Display version:
lci -v
echo "Display help:
lci -h
echo "ldapdomaindump
echo "Dump users, computers, groups, OS and membership information via LDAP to HTML, JSON and greppable output.
echo "See also ldapsearch.
echo "More information: <https://github.com/dirkjanm/ldapdomaindump>.
echo "Dump all information using the given LDAP account:
ldapdomaindump --user {{domain}}\\{{administrator}} --password {{password|ntlm_hash}} {{hostname|ip}}
echo "Dump all information, resolving computer hostnames:
ldapdomaindump --resolve --user {{domain}}\\{{administrator}} --password {{password}} {{hostname|ip}}
echo "Dump all information, resolving computer hostnames with the selected DNS server:
ldapdomaindump --resolve --dns-server {{domain_controller_ip}} --user {{domain}}\\{{administrator}} --password {{password}} {{hostname|ip}}
echo "Dump all information to the given directory without JSON output:
ldapdomaindump --no-json --outdir {{path/to/directory}} --user {{domain}}\\{{administrator}} --password {{password}} {{hostname|ip}}
echo "ldconfig
echo "Configure symlinks and cache for shared library dependencies.
echo "More information: <https://manned.org/ldconfig>.
echo "Update symlinks and rebuild the cache (usually run when a new library is installed):
sudo ldconfig
echo "Update the symlinks for a given directory:
sudo ldconfig -n {{path/to/directory}}
echo "Print the libraries in the cache and check whether a given library is present:
ldconfig -p | grep {{library_name}}
echo "ldd
echo "Display shared library dependencies of a binary.
echo "Do not use on an untrusted binary, use objdump for that instead.
echo "More information: <https://manned.org/ldd>.
echo "Display shared library dependencies of a binary:
ldd {{path/to/binary}}
echo "Display all information about dependencies:
ldd --verbose {{path/to/binary}}
echo "Display unused direct dependencies:
ldd --unused {{path/to/binary}}
echo "Report missing data objects and perform data relocations:
ldd --data-relocs {{path/to/binary}}
echo "Report missing data objects and functions, and perform relocations for both:
ldd --function-relocs {{path/to/binary}}
echo "lddd
echo "Find broken library links on the system.
echo "This tool is only available on Arch Linux.
echo "More information: <https://man.archlinux.org/man/extra/devtools/lddd.1>.
echo "Scan directories to find and list packages with broken library links that need to be rebuilt:
lddd
echo "ledctl
echo "Intel(R) Enclosure LED Control Application.
echo "More information: <https://manned.org/ledctl>.
echo "Turn on the "Locate" LED for specified device(s):
sudo ledctl locate={{/dev/sda,/dev/sdb,...}}
echo "Turn off the "Locate" LED for specified device(s):
sudo ledctl locate_off={{/dev/sda,/dev/sdb,...}}
echo "Turn off the "Status" LED and "Failure" LED for specified device(s):
sudo ledctl off={{/dev/sda,/dev/sdb,...}}
echo "Turn off the "Status" LED, "Failure" LED and "Locate" LED for specified device(s):
sudo ledctl normal={{/dev/sda,/dev/sdb,...}}
echo "legit
echo "Complementary command-line interface for Git.
echo "More information: <https://frostming.github.io/legit>.
echo "Switch to a specified branch, stashing and restoring unstaged changes:
git switch {{target_branch}}
echo "Synchronize current branch, automatically merging or rebasing, and stashing and unstashing:
git sync
echo "Publish a specified branch to the remote server:
git publish {{branch_name}}
echo "Remove a branch from the remote server:
git unpublish {{branch_name}}
echo "List all branches and their publication status:
git branches {{glob_pattern}}
echo "Remove the last commit from the history:
git undo {{--hard}}
echo "lex
echo "Lexical analyzer generator.
echo "Given the specification for a lexical analyzer, generates C code implementing it.
echo "More information: <https://manned.org/lex.1>.
echo "Generate an analyzer from a Lex file:
lex {{analyzer.l}}
echo "Specify the output file:
lex {{analyzer.l}} --outfile {{analyzer.c}}
echo "Compile a C file generated by Lex:
cc {{path/to/lex.yy.c}} --output {{executable}}
echo "lftp
echo "Sophisticated file transfer program.
echo "More information: <https://lftp.yar.ru/lftp-man.html>.
echo "Connect to an FTP server:
lftp --user {{username}} {{ftp.example.com}}
echo "Download multiple files (glob expression):
mget {{path/to/*.png}}
echo "Upload multiple files (glob expression):
mput {{path/to/*.zip}}
echo "Delete multiple files on the remote server:
mrm {{path/to/*.txt}}
echo "Rename a file on the remote server:
mv {{original_filename}} {{new_filename}}
echo "Download or update an entire directory:
mirror {{path/to/remote_dir}} {{path/to/local_output_dir}}
echo "Upload or update an entire directory:
mirror -R {{path/to/local_dir}} {{path/to/remote_output_dir}}
echo "libreoffice
echo "CLI for the powerful and free office suite LibreOffice.
echo "More information: <https://www.libreoffice.org/>.
echo "Open a space-separated list of files in read-only mode:
libreoffice --view {{path/to/file1}} {{path/to/file2}}
echo "Display the content of specific files:
libreoffice --cat {{path/to/file1}} {{path/to/file2}}
echo "Print files to a specific printer:
libreoffice --pt {{printer_name}} {{path/to/file1}} {{path/to/file2}}
echo "Convert all .doc files in current directory to PDF:
libreoffice --convert-to {{pdf}} {{*.doc}}
echo "libtool
echo "A generic library support script that hides the complexity of using shared libraries behind a consistent, portable interface.
echo "More information: <https://www.gnu.org/software/libtool/manual/libtool.html#Invoking-libtool>.
echo "Compile a source file into a libtool object:
libtool --mode=compile gcc -c {{path/to/source.c}} -o {{path/to/source.lo}}
echo "Create a library or an executable:
libtool --mode=link gcc -o {{path/to/library.lo}} {{path/to/source.lo}}
echo "Automatically set the library path so that another program can use uninstalled libtool generated programs or libraries:
libtool --mode=execute gdb {{path/to/program}}
echo "Install a shared library:
libtool --mode=install cp {{path/to/library.la}} {{path/to/installation_directory}}
echo "Complete the installation of libtool libraries on the system:
libtool --mode=finish {{path/to/installation_dir}}
echo "Delete installed libraries or executables:
libtool --mode=uninstall {{path/to/installed_library.la}}
echo "Delete uninstalled libraries or executables:
libtool --mode=clean rm {{path/to/source.lo}} {{path/to/library.la}}
echo "libtoolize
echo "A tool used in the autotools build system to prepare a package for the use of libtool.
echo "It performs various tasks, including generating necessary files and directories to integrate libtool seamlessly into a project.
echo "More information: <https://www.gnu.org/software/libtool/manual/libtool.html#Invoking-libtoolize>.
echo "Initialize a project for libtool by copying necessary files (avoiding symbolic links) and overwriting existing files if needed:
libtoolize --copy --force
echo "light
echo "Control the backlight of your screen.
echo "More information: <https://manned.org/light>.
echo "Get the current backlight value in percent:
light
echo "Set the backlight value to 50 percent:
light -S {{50}}
echo "Reduce 20 percent from the current backlight value:
light -U {{20}}
echo "Add 20 percent to the current backlight value:
light -A {{20}}
echo "line
echo "Read a single line of input.
echo "More information: <https://manned.org/line.1>.
echo "Read input:
line
echo "links
echo "Command-line web browser.
echo "More information: <http://links.twibright.com/>.
echo "Visit a website:
links {{https://example.com}}
echo "Apply restrictions for anonymous account:
links -anonymous {{https://example.com}}
echo "Enable Cookies (1 to enable):
links -enable-cookies {{0|1}} {{https://example.com}}
echo "Navigate forwards and backwards through the links on a page:
{{Up arrow key|Down arrow key}}
echo "Go forwards and backwards one page:
{{Left arrow key|Right arrow key}}
echo "Exit:
q + y
echo "lnav
echo "Advanced log file viewer to analyze logs with little to no setup.
echo "More information: <https://docs.lnav.org/en/latest/cli.html>.
echo "View logs of a program, specifying log files, directories or URLs:
lnav {{path/to/log_or_directory|url}}
echo "View logs of a specific remote host (SSH passwordless login required):
lnav {{ssh}} {{user}}@{{host1.example.com}}:{{/var/log/syslog.log}}
echo "Validate the format of log files against the configuration and report any errors:
lnav -C {{path/to/log_directory}}
echo "loadkeys
echo "Load the kernel keymap for the console.
echo "More information: <https://manned.org/loadkeys>.
echo "Load a default keymap:
loadkeys --default
echo "Load default keymap when an unusual keymap is loaded and - sign cannot be found:
loadkeys defmap
echo "Create a kernel source table:
loadkeys --mktable
echo "Create a binary keymap:
loadkeys --bkeymap
echo "Search and parse keymap without action:
loadkeys --parse
echo "Load the keymap suppressing all output:
loadkeys --quiet
echo "Load a keymap from the specified file for the console:
loadkeys --console {{/dev/ttyN}} {{/path/to/file}}
echo "Use standard names for keymaps of different locales:
loadkeys --console {{/dev/ttyN}} {{uk}}
echo "locale
echo "Get locale-specific information.
echo "More information: <https://manned.org/locale>.
echo "List all global environment variables describing the user's locale:
locale
echo "List all available locales:
locale --all-locales
echo "Display all available locales and the associated metadata:
locale --all-locales --verbose
echo "Display the current date format:
locale date_fmt
echo "localectl
echo "Control the system locale and keyboard layout settings.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/localectl.html>.
echo "Show the current settings of the system locale and keyboard mapping:
localectl
echo "List available locales:
localectl list-locales
echo "Set a system locale variable:
localectl set-locale {{LANG}}={{en_US.UTF-8}}
echo "List available keymaps:
localectl list-keymaps
echo "Set the system keyboard mapping for the console and X11:
localectl set-keymap {{us}}
echo "locate
echo "Find filenames quickly.
echo "More information: <https://manned.org/locate>.
echo "Look for pattern in the database. Note: the database is recomputed periodically (usually weekly or daily):
locate {{pattern}}
echo "Look for a file by its exact filename (a pattern containing no globbing characters is interpreted as *pattern*):
locate '*/{{filename}}'
echo "Recompute the database. You need to do it if you want to find recently added files:
sudo updatedb
echo "login
echo "Initiates a session for a user.
echo "More information: <https://manned.org/login>.
echo "Log in as a user:
login {{user}}
echo "Log in as user without authentication if user is preauthenticated:
login -f {{user}}
echo "Log in as user and preserve environment:
login -p {{user}}
echo "Log in as a user on a remote host:
login -h {{host}} {{user}}
echo "loginctl
echo "Manage the systemd login manager.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/loginctl.html>.
echo "Print all current sessions:
loginctl list-sessions
echo "Print all properties of a specific session:
loginctl show-session {{session_id}} --all
echo "Print all properties of a specific user:
loginctl show-user {{username}}
echo "Print a specific property of a user:
loginctl show-user {{username}} --property={{property_name}}
echo "Execute a loginctl operation on a remote host:
loginctl list-users -H {{hostname}}
echo "logrotate
echo "Rotates, compresses, and mails system logs.
echo "More information: <https://manned.org/logrotate>.
echo "Trigger a run manually:
logrotate {{path/to/logrotate.conf}} --force
echo "Run using a specific command to mail reports:
logrotate {{path/to/logrotate.conf}} --mail {{/usr/bin/mail_command}}
echo "Run without using a state (lock) file:
logrotate {{path/to/logrotate.conf}} --state /dev/null
echo "Run and skip the state (lock) file check:
logrotate {{path/to/logrotate.conf}} --skip-state-lock
echo "Tell logrotate to log verbose output into the log file:
logrotate {{path/to/logrotate.conf}} --log {{path/to/log_file}}
echo "logsave
echo "Save the output of a command in a logfile.
echo "More information: <https://manned.org/logsave>.
echo "Execute command with specified argument(s) and save its output to log file:
logsave {{path/to/logfile}} {{command}}
echo "Take input from stdin and save it in a log file:
logsave {{logfile}} -
echo "Append the output to a log file, instead of replacing its current contents:
logsave -a {{logfile}} {{command}}
echo "Show verbose output:
logsave -v {{logfile}} {{command}}
echo "logwatch
echo "Summarizes many different logs for common services (e.g. apache, pam_unix, sshd, etc.) in a single report.
echo "More information: <https://manned.org/logwatch>.
echo "Analyze logs for a range of dates at a certain level of detail:
logwatch --range {{yesterday|today|all|help}} --detail {{low|medium|others}}'
echo "Restrict report to only include information for a selected service:
logwatch --range {{all}} --service {{apache|pam_unix|etc}}
echo "losetup
echo "Set up and control loop devices.
echo "More information: <https://manned.org/losetup>.
echo "List loop devices with detailed info:
losetup -a
echo "Attach a file to a given loop device:
sudo losetup /dev/{{loop}} /{{path/to/file}}
echo "Attach a file to a new free loop device and scan the device for partitions:
sudo losetup --show --partscan -f /{{path/to/file}}
echo "Attach a file to a read-only loop device:
sudo losetup --read-only /dev/{{loop}} /{{path/to/file}}
echo "Detach all loop devices:
sudo losetup -D
echo "Detach a given loop device:
sudo losetup -d /dev/{{loop}}
echo "lrunzip
echo "A large file decompression program.
echo "See also: lrzip`, `lrztar`, `lrzuntar.
echo "More information: <https://manned.org/lrunzip>.
echo "Decompress a file:
lrunzip {{filename.lrz}}
echo "Decompress a file using a specific number of processor threads:
lrunzip -p {{8}} {{filename.lrz}}
echo "Decompress a file and silently overwrite files if they exist:
lrunzip -f {{filename.lrz}}
echo "Keep broken or damaged files instead of deleting them when decompressing:
lrunzip -K {{filename.lrz}}
echo "Specify output file name and/or path:
lrunzip -o {{outfilename}} {{filename.lrz}}
echo "lrzip
echo "A large file compression program.
echo "See also: lrunzip`, `lrztar`, `lrzuntar.
echo "More information: <https://manned.org/lrzip>.
echo "Compress a file with LZMA - slow compression, fast decompression:
lrzip {{path/to/file}}
echo "Compress a file with BZIP2 - good middle ground for compression/speed:
lrzip -b {{path/to/file}}
echo "Compress with ZPAQ - extreme compression, but very slow:
lrzip -z {{path/to/file}}
echo "Compress with LZO - light compression, extremely fast decompression:
lrzip -l {{path/to/file}}
echo "Compress a file and password protect/encrypt it:
lrzip -e {{path/to/file}}
echo "Override the number of processor threads to use:
lrzip -p {{8}} {{path/to/file}}
echo "lrztar
echo "A wrapper for lrzip to simplify compression of directories.
echo "See also: tar`, `lrzuntar`, `lrunzip.
echo "More information: <https://manned.org/lrztar>.
echo "Archive a directory with tar, then compress:
lrztar {{path/to/directory}}
echo "Same as above, with ZPAQ - extreme compression, but very slow:
lrztar -z {{path/to/directory}}
echo "Specify the output file:
lrztar -o {{path/to/file}} {{path/to/directory}}
echo "Override the number of processor threads to use:
lrztar -p {{8}} {{path/to/directory}}
echo "Force overwriting of existing files:
lrztar -f {{path/to/directory}}
echo "lrzuntar
echo "A wrapper for lrunzip to simplify decompression of directories.
echo "See also: lrztar`, `lrzip.
echo "More information: <https://manned.org/lrzuntar>.
echo "Decompress from a file to the current directory:
lrzuntar {{path/to/archive.tar.lrz}}
echo "Decompress from a file to the current directory using a specific number of processor threads:
lrzuntar -p {{8}} {{path/to/archive.tar.lrz}}
echo "Decompress from a file to the current directory and silently overwrite items that already exist:
lrzuntar -f {{archive.tar.lrz}}
echo "Specify the output path:
lrzuntar -O {{path/to/directory}} {{archive.tar.lrz}}
echo "Delete the compressed file after decompression:
lrzuntar -D {{path/to/archive.tar.lrz}}
echo "lsattr
echo "List file attributes on a Linux filesystem.
echo "More information: <https://manned.org/lsattr>.
echo "Display the attributes of the files in the current directory:
lsattr
echo "List the attributes of files in a particular path:
lsattr {{path}}
echo "List file attributes recursively in the current and subsequent directories:
lsattr -R
echo "Show attributes of all the files in the current directory, including hidden ones:
lsattr -a
echo "Display attributes of directories in the current directory:
lsattr -d
echo "lsb_release
echo "Provides certain LSB (Linux Standard Base) and distribution-specific information.
echo "More information: <https://manned.org/lsb_release>.
echo "Print all available information:
lsb_release -a
echo "Print a description (usually the full name) of the operating system:
lsb_release -d
echo "Print only the operating system name (ID), suppressing the field name:
lsb_release -i -s
echo "Print the release number and codename of the distribution, suppressing the field names:
lsb_release -rcs
echo "lsblk
echo "Lists information about devices.
echo "More information: <https://manned.org/lsblk>.
echo "List all storage devices in a tree-like format:
lsblk
echo "Also list empty devices:
lsblk -a
echo "Print the SIZE column in bytes rather than in a human-readable format:
lsblk -b
echo "Output info about filesystems:
lsblk -f
echo "Use ASCII characters for tree formatting:
lsblk -i
# echo "Output info about block-device topology:
lsblk -t
echo "Exclude the devices specified by the comma-separated list of major device numbers:
lsblk -e {{1,7}}
echo "Display a customized summary using a comma-separated list of columns:
lsblk --output {{NAME}},{{SERIAL}},{{MODEL}},{{TRAN}},{{TYPE}},{{SIZE}},{{FSTYPE}},{{MOUNTPOINT}}
echo "lscpu
echo "Displays information about the CPU architecture.
echo "More information: <https://manned.org/lscpu>.
echo "Display information about all CPUs:
lscpu
echo "Display information in a table:
lscpu --extended
echo "Display only information about offline CPUs in a table:
lscpu --extended --offline
echo "lshw
echo "List detailed information about hardware configurations as root user.
echo "More information: <https://manned.org/lshw>.
echo "Launch the GUI:
sudo lshw -X
echo "List all hardware in tabular format:
sudo lshw -short
echo "List all disks and storage controllers in tabular format:
sudo lshw -class disk -class storage -short
echo "Save all network interfaces to an HTML file:
sudo lshw -class network -html > {{interfaces.html}}
echo "lsinitrd
echo "Show the contents of an initramfs image.
echo "See also: dracut.
echo "More information: <https://github.com/dracutdevs/dracut/blob/master/man/lsinitrd.1.asc>.
echo "Show the contents of the initramfs image for the current kernel:
lsinitrd
echo "Show the contents of the initramfs image for the specified kernel:
lsinitrd --kver {{kernel_version}}
echo "Show the contents of the specified initramfs image:
lsinitrd {{path/to/initramfs.img}}
echo "List modules included in the initramfs image:
lsinitrd --mod
echo "Unpack the initramfs to the current directory:
lsinitrd --unpack
echo "lslocks
echo "List local system locks.
echo "More information: <https://manned.org/lslocks>.
echo "List all local system locks:
lslocks
echo "List locks with defined column headers:
lslocks --output {{PID}},{{COMMAND}},{{PATH}}
echo "List locks producing a raw output (no columns), and without column headers:
lslocks --raw --noheadings
echo "List locks by PID input:
lslocks --pid {{PID}}
echo "List locks with JSON output to stdout:
lslocks --json
echo "lslogins
echo "Show information about users on a Linux system.
echo "More information: <https://man7.org/linux/man-pages/man1/lslogins.1.html>.
echo "Display users in the system:
lslogins
echo "Display users belonging to a specific group:
lslogins --groups={{groups}}
echo "Display user accounts:
lslogins --user-accs
echo "Display last logins:
lslogins --last
echo "Display system accounts:
lslogins --system-accs
echo "Display supplementary groups:
lslogins --supp-groups
echo "lsmod
echo "Shows the status of Linux kernel modules.
echo "See also modprobe, which loads kernel modules.
echo "More information: <https://manned.org/lsmod>.
echo "List all currently loaded kernel modules:
lsmod
echo "lsns
echo "List information about all namespaces or about the specified namespace.
echo "More information: <https://man7.org/linux/man-pages/man8/lsns.8.html>.
echo "List all namespaces:
lsns
echo "List namespaces in JSON format:
lsns --json
echo "List namespaces associated with the specified process:
lsns --task {{pid}}
echo "List the specified type of namespaces only:
lsns --type {{mnt|net|ipc|user|pid|uts|cgroup|time}}
echo "List namespaces, only showing the namespace ID, type, PID, and command:
lsns --output {{NS,TYPE,PID,COMMAND}}
echo "lspci
echo "List all PCI devices.
echo "More information: <https://manned.org/lspci>.
echo "Show a brief list of devices:
lspci
echo "Display additional info:
lspci -v
echo "Display drivers and modules handling each device:
lspci -k
echo "Show a specific device:
lspci -s {{00:18.3}}
echo "Dump info in a readable form:
lspci -vm
echo "lsscsi
echo "List SCSI devices (or hosts) and their attributes.
echo "More information: <https://manned.org/lspci>.
echo "List all SCSI devices:
lsscsi
echo "List all SCSI devices with detailed attributes:
lsscsi -L
echo "List all SCSI devices with human-readable disk capacity:
lsscsi -s
echo "lsusb
echo "Display information about USB buses and devices connected to them.
echo "More information: <https://manned.org/lsusb>.
echo "List all the USB devices available:
lsusb
echo "List the USB hierarchy as a tree:
lsusb -t
echo "List verbose information about USB devices:
lsusb --verbose
echo "List detailed information about a USB device:
lsusb --verbose -s {{bus}}:{{device number}}
echo "List devices with a specified vendor and product ID only:
lsusb -d {{vendor}}:{{product}}
echo "ltrace
echo "Display dynamic library calls of a process.
echo "More information: <https://manned.org/ltrace>.
echo "Print (trace) library calls of a program binary:
ltrace ./{{program}}
echo "Count library calls. Print a handy summary at the bottom:
ltrace -c {{path/to/program}}
echo "Trace calls to malloc and free, omit those done by libc:
ltrace -e malloc+free-@libc.so* {{path/to/program}}
echo "Write to file instead of terminal:
ltrace -o {{file}} {{path/to/program}}
echo "lvcreate
echo "Create a logical volume in an existing volume group. A volume group is a collection of logical and physical volumes.
echo "See also: lvm.
echo "More information: <https://man7.org/linux/man-pages/man8/lvcreate.8.html>.
echo "Create a logical volume of 10 gigabytes in the volume group vg1:
lvcreate -L {{10G}} {{vg1}}
echo "Create a 1500 megabyte linear logical volume named mylv in the volume group vg1:
lvcreate -L {{1500}} -n {{mylv}} {{vg1}}
echo "Create a logical volume called mylv that uses 60% of the total space in volume group vg1:
lvcreate -l {{60%VG}} -n {{mylv}} {{vg1}}
echo "Create a logical volume called mylv that uses all the unallocated space in the volume group vg1:
lvcreate -l {{100%FREE}} -n {{mylv}} {{vg1}}
echo "lvdisplay
echo "Display information about Logical Volume Manager (LVM) logical volumes.
echo "See also: lvm.
echo "More information: <https://man7.org/linux/man-pages/man8/lvdisplay.8.html>.
echo "Display information about all logical volumes:
sudo lvdisplay
echo "Display information about all logical volumes in volume group vg1:
sudo lvdisplay {{vg1}}
echo "Display information about logical volume lv1 in volume group vg1:
sudo lvdisplay {{vg1/lv1}}
echo "lvextend
echo "Increase the size of a logical volume.
echo "See also: lvm.
echo "More information: <https://manned.org/lvextend.8>.
echo "Increase a volume's size to 120 GB:
lvextend --size {{120G}} {{logical_volume}}
echo "Increase a volume's size by 40 GB as well as the underlying filesystem:
lvextend --size +{{40G}} -r {{logical_volume}}
echo "Increase a volume's size to 100% of the free physical volume space:
lvextend --size +{{100}}%FREE {{logical_volume}}
echo "lvm
echo "Manage physical volumes, volume groups, and logical volumes using the Logical Volume Manager (LVM) interactive shell.
echo "More information: <https://man7.org/linux/man-pages/man8/lvm.8.html>.
echo "Start the Logical Volume Manager interactive shell:
sudo lvm
echo "List the Logical Volume Manager commands:
sudo lvm help
echo "Initialize a drive or partition to be used as a physical volume:
sudo lvm pvcreate {{/dev/sdXY}}
echo "Display information about physical volumes:
sudo lvm pvdisplay
echo "Create a volume group called vg1 from the physical volume on /dev/sdXY:
sudo lvm vgcreate {{vg1}} {{/dev/sdXY}}
echo "Display information about volume groups:
sudo lvm vgdisplay
echo "Create a logical volume with size 10G from volume group vg1:
sudo lvm lvcreate -L {{10G}} {{vg1}}
echo "Display information about logical volumes:
sudo lvm lvdisplay
echo "lvreduce
echo "Reduce the size of a logical volume.
echo "See also: lvm.
echo "More information: <https://man7.org/linux/man-pages/man8/lvreduce.8.html>.
echo "Reduce a volume's size to 120 GB:
lvreduce --size {{120G}} {{logical_volume}}
echo "Reduce a volume's size by 40 GB as well as the underlying filesystem:
lvreduce --size -{{40G}} -r {{logical_volume}}
echo "lvremove
echo "Remove one or more logical volumes.
echo "See also: lvm.
echo "More information: <https://man7.org/linux/man-pages/man8/lvremove.8.html>.
echo "Remove a logical volume in a volume group:
sudo lvremove {{volume_group}}/{{logical_volume}}
echo "Remove all logical volumes in a volume group:
sudo lvremove {{volume_group}}
echo "lvresize
echo "Change the size of a logical volume.
echo "See also: lvm.
echo "More information: <https://man7.org/linux/man-pages/man8/lvresize.8.html>.
echo "Change the size of a logical volume to 120 GB:
lvresize --size {{120G}} {{volume_group}}/{{logical_volume}}
echo "Extend the size of a logical volume as well as the underlying filesystem by 120 GB:
lvresize --size +{{120G}} --resizefs {{volume_group}}/{{logical_volume}}
echo "Extend the size of a logical volume to 100% of the free physical volume space:
lvresize --size {{100}}%FREE {{volume_group}}/{{logical_volume}}
echo "Reduce the size of a logical volume as well as the underlying filesystem by 120 GB:
lvresize --size -{{120G}} --resizefs {{volume_group}}/{{logical_volume}}
echo "lvs
echo "Display information about logical volumes.
echo "See also: lvm.
echo "More information: <https://man7.org/linux/man-pages/man8/lvs.8.html>.
echo "Display information about logical volumes:
lvs
echo "Display all logical volumes:
lvs -a
echo "Change default display to show more details:
lvs -v
echo "Display only specific fields:
lvs -o {{field_name_1}},{{field_name_2}}
echo "Append field to default display:
lvs -o +{{field_name}}
echo "Suppress heading line:
lvs --noheadings
echo "Use a separator to separate fields:
lvs --separator {{=}}
echo "lxc network
echo "Manage networks for LXD containers.
echo "More information: <https://linuxcontainers.org/lxd/docs/master/networks>.
echo "List all available networks:
lxc network list
echo "Show the configuration of a specific network:
lxc network show {{network_name}}
echo "Add a running instance to a specific network:
lxc network attach {{network_name}} {{container_name}}
echo "Create a new managed network:
lxc network create {{network_name}}
echo "Set a bridge interface of a specific network:
lxc network set {{network_name}} bridge.external_interfaces {{eth0}}
echo "Disable NAT for a specific network:
lxc network set {{network_name}} ipv{{4}}.nat false
echo "lxc profile
echo "Manage profiles for LXD containers.
echo "More information: <https://linuxcontainers.org/lxd/docs/master/profiles>.
echo "List all available profiles:
lxc profile list
echo "Show the configuration of a specific profile:
lxc profile show {{profile_name}}
echo "Edit a specific profile in the default editor:
lxc profile edit {{profile_name}}
echo "Edit a specific profile importing the configuration values from a file:
lxc profile edit {{profile_name}} < {{config.yaml}}
echo "Launch a new container with specific profiles:
lxc launch {{container_image}} {{container_name}} --profile {{profile1}} --profile {{profile2}}
echo "Change the profiles of a running container:
lxc profile assign {{container_name}} {{profile1,profile2}}
echo "lxc
echo "Manage Linux containers using the lxd REST API.
echo "Any container names or patterns can be prefixed with the name of a remote server.
echo "More information: <https://manned.org/lxc>.
echo "List local containers matching a string. Omit the string to list all local containers:
lxc list {{match_string}}
echo "List images matching a string. Omit the string to list all images:
lxc image list [{{remote}}:]{{match_string}}
echo "Create a new container from an image:
lxc init [{{remote}}:]{{image}} {{container}}
echo "Start a container:
lxc start [{{remote}}:]{{container}}
# echo "Stop a container:
# lxc stop [{{remote}}:]{{container}}
echo "Show detailed info about a container:
lxc info [{{remote}}:]{{container}}
echo "Take a snapshot of a container:
lxc snapshot [{{remote}}:]{{container}} {{snapshot}}
echo "Execute a specific command inside a container:
lxc exec [{{remote}}:]{{container}} {{command}}
echo "lxi
echo "Control LXI compatible instruments such as oscilloscopes.
echo "More information: <https://github.com/lxi-tools/lxi-tools>.
echo "Discover LXI devices on available networks:
lxi discover
echo "Capture a screenshot, detecting a plugin automatically:
lxi screenshot --address {{ip_address}}
echo "Capture a screenshot using a specified plugin:
lxi screenshot --address {{ip_address}} --plugin {{rigol-1000z}}
echo "Send an SCPI command to an instrument:
lxi scpi --address {{ip_address}} "{{*IDN?}}"
echo "Run a benchmark for request and response performance:
lxi benchmark --address {{ip_address}}
echo "lxterminal
echo "Terminal emulator for LXDE.
echo "More information: <https://wiki.lxde.org/en/LXTerminal>.
echo "Open an LXTerminal window:
lxterminal
echo "Open an LXTerminal window, run a command, and then exit:
lxterminal -e "{{command}}"
echo "Open an LXTerminal window with multiple tabs:
lxterminal --tabs={{tab_name1,tab_name2,...}}
echo "Open an LXTerminal window with a specific title:
lxterminal --title={{title_name}}
echo "Open an LXTerminal window with a specific working directory:
lxterminal --working-directory={{path/to/directory}}
echo "lynis
echo "System and security auditing tool.
echo "More information: <https://cisofy.com/documentation/lynis/>.
echo "Check that Lynis is up-to-date:
sudo lynis update info
echo "Run a security audit of the system:
sudo lynis audit system
echo "Run a security audit of a Dockerfile:
sudo lynis audit dockerfile {{path/to/dockerfile}}
echo "lz
echo "List all files inside a '.tar.gz' compressed archive.
echo "More information: <https://manned.org/lz.1>.
echo "List all files inside a compressed archive:
lz {{path/to/file.tar.gz}}
echo "mac2unix
echo "Change macOS-style line endings to Unix-style.
echo "Replaces CR with LF.
echo "More information: <https://waterlan.home.xs4all.nl/dos2unix.html>.
echo "Change the line endings of a file:
mac2unix {{filename}}
echo "Create a copy with Unix-style line endings:
mac2unix -n {{filename}} {{new_filename}}
echo "macchanger
echo "Command-line utility for manipulating network interface MAC addresses.
echo "More information: <https://manned.org/macchanger>.
echo "View the current and permanent MAC addresses of a interface:
macchanger --show {{interface}}
echo "Set interface to a random MAC:
macchanger --random {{interface}}
echo "Set interface to a specific MAC:
macchanger --mac {{XX:XX:XX:XX:XX:XX}} {{interface}}
echo "Reset interface to its permanent hardware MAC:
macchanger --permanent {{interface}}
echo "machinectl
echo "Control the systemd machine manager.
echo "Execute operations on virtual machines, containers and images.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/machinectl.html>.
echo "Start a machine as a service using systemd-nspawn:
sudo machinectl start {{machine_name}}
# echo "Stop a running machine:
# sudo machinectl stop {{machine_name}}
echo "Display a list of running machines:
machinectl list
echo "Open an interactive shell inside the machine:
sudo machinectl shell {{machine_name}}
echo "maim
echo "Screenshot utility.
echo "More information: <https://github.com/naelstrof/maim>.
echo "Capture a screenshot and save it to the given path:
maim {{path/to/screenshot.png}}
echo "Capture a screenshot of the selected region:
maim --select {{path/to/screenshot.png}}
echo "Capture a screenshot of the selected region and save it in the clipboard (requires xclip):
maim --select | xclip -selection clipboard -target image/png
echo "Capture a screenshot of the current active window (requires xdotool):
maim --window $(xdotool getactivewindow) {{path/to/screenshot.png}}
echo "makepkg
echo "Create a package which can be used with pacman.
echo "Uses the PKGBUILD file in the current working directory by default.
echo "More information: <https://man.archlinux.org/man/makepkg.8>.
echo "Make a package:
makepkg
echo "Make a package and install its dependencies:
makepkg --syncdeps
echo "Make a package, install its dependencies then install it to the system:
makepkg --syncdeps --install
echo "Make a package, but skip checking the source's hashes:
makepkg --skipchecksums
echo "Clean up work directories after a successful build:
makepkg --clean
echo "Verify the hashes of the sources:
makepkg --verifysource
echo "Generate and save the source information into .SRCINFO:
makepkg --printsrcinfo > .SRCINFO
echo "man
echo "Format and display manual pages.
echo "More information: <https://manned.org/man>.
echo "Display the man page for a command:
man {{command}}
echo "Display the man page for a command from section 7:
man {{7}} {{command}}
echo "List all available sections for a command:
man --whatis {{command}}
echo "Display the path searched for manpages:
man --path
echo "Display the location of a manpage rather than the manpage itself:
man --where {{command}}
echo "Display the man page using a specific locale:
man --locale={{locale}} {{command}}
echo "Search for manpages containing a search string:
man --apropos "{{search_string}}"
echo "mandb
echo "Manage the pre-formatted manual page database.
echo "More information: <https://man7.org/linux/man-pages/man8/mandb.8.html>.
echo "Purge and process manual pages:
mandb
echo "Update a single entry:
mandb --filename {{path/to/file}}
echo "Create entries from scratch instead of updating:
mandb --create
echo "Only process user databases:
mandb --user-db
echo "Do not purge obsolete entries:
mandb --no-purge
echo "Check the validity of manual pages:
mandb --test
echo "manpath
echo "Determine the search path for manual pages.
echo "More information: <https://manned.org/manpath>.
echo "Display the search path used to find man pages:
manpath
echo "Show the entire global manpath:
manpath --global
echo "mashtree
echo "Makes a fast tree from genomes.
echo "Does not make a phylogeny.
echo "More information: <https://github.com/lskatz/mashtree>.
echo "Fastest method in mashtree to create a tree from fastq and/or fasta files using multiple threads, piping into a newick file:
mashtree --numcpus {{12}} {{*.fastq.gz}} {{*.fasta}} > {{mashtree.dnd}}
echo "Most accurate method in mashtree to create a tree from fastq and/or fasta files using multiple threads, piping into a newick file:
mashtree --mindepth {{0}} --numcpus {{12}} {{*.fastq.gz}} {{*.fasta}} > {{mashtree.dnd}}
echo "Most accurate method to create a tree with confidence values (note that any options for mashtree` itself has to be on the right side of the `--):
mashtree_bootstrap.pl --reps {{100}} --numcpus {{12}} {{*.fastq.gz}} -- --min-depth {{0}} > {{mashtree.bootstrap.dnd}}
echo "mate-about
# echo "Show information about MATE desktop environment.
echo "More information: <https://manned.org/mate-about>.
echo "Print MATE version:
mate-about --version
echo "mate-calc-cmd
# echo "Calculate mathematic expressions in MATE desktop environment in terminal.
echo "More information: <https://manned.org/mate-calc-cmd>.
echo "Start an interactive calculator session:
mate-calc-cmd
echo "Calculate a specific mathematic expression:
{{2 + 5}}
echo "mate-calc
# echo "Calculate mathematic expressions in MATE desktop environment.
echo "More information: <https://manned.org/mate-calc>.
echo "Start the calculator:
mate-calc
echo "Calculate a specific mathematic expression:
mate-calc --solve {{2 + 5}}
echo "mate-screenshot
# echo "Make screenshots in MATE desktop environment.
echo "More information: <https://manned.org/mate-screenshot>.
echo "Create a fullscreen screenshot:
mate-screenshot
echo "Create an active window screenshot:
mate-screenshot --window
echo "Create a specific area screenshot:
mate-screenshot --area
echo "Create a screenshot interactively:
mate-screenshot --interactive
echo "Create a screenshot without borders:
mate-screenshot --window --remove-border
echo "Create a screenshot with a specific effect:
mate-screenshot --effect={{shadow|border|none}}
echo "Create a screenshot with a specific delay in seconds:
mate-screenshot --delay={{5}}
echo "mate-search-tool
# echo "Search files in MATE desktop environment.
echo "More information: <https://manned.org/mate-search-tool>.
echo "Search files containing a specific string in their name in a specific directory:
mate-search-tool --named={{string}} --path={{path/to/directory}}
echo "Search files without waiting a user confirmation:
mate-search-tool --start --named={{string}} --path={{path/to/directory}}
echo "Search files with name matching a specific regular expression:
mate-search-tool --start --regex={{string}} --path={{path/to/directory}}
echo "Set a sorting order in search results:
mate-search-tool --start --named={{string}} --path={{path/to/directory}} --sortby={{name|folder|size|type|date}}
echo "Set a descending sorting order:
mate-search-tool --start --named={{string}} --path={{path/to/directory}} --descending
echo "Search files owned by a specific user/group:
mate-search-tool --start --{{user|group}}={{value}} --path={{path/to/directory}}
echo "mcookie
echo "Generates random 128-bit hexadecimal numbers.
echo "More information: <https://manned.org/mcookie>.
echo "Generate a random number:
mcookie
echo "Generate a random number, using the contents of a file as a seed for the randomness:
mcookie --file {{path/to/file}}
echo "Generate a random number, using a specific number of bytes from a file as a seed for the randomness:
mcookie --file {{path/to/file}} --max-size {{number_of_bytes}}
echo "Print the details of the randomness used, such as the origin and seed for each source:
mcookie --verbose
echo "mdadm
echo "RAID management utility.
echo "More information: <https://manned.org/mdadm>.
echo "Create array:
sudo mdadm --create {{/dev/md/MyRAID}} --level {{raid_level}} --raid-devices {{number_of_disks}} {{/dev/sdXN}}
# echo "Stop array:
# sudo mdadm --stop {{/dev/md0}}
echo "Mark disk as failed:
sudo mdadm --fail {{/dev/md0}} {{/dev/sdXN}}
echo "Remove disk:
sudo mdadm --remove {{/dev/md0}} {{/dev/sdXN}}
echo "Add disk to array:
sudo mdadm --assemble {{/dev/md0}} {{/dev/sdXN}}
echo "Show RAID info:
sudo mdadm --detail {{/dev/md0}}
echo "Reset disk by deleting RAID metadata:
sudo mdadm --zero-superblock {{/dev/sdXN}}
echo "mdbook
echo "Create online books by writing Markdown files.
echo "More information: <https://rust-lang.github.io/mdBook/>.
echo "Create an mdbook project in the current directory:
mdbook init
echo "Create an mdbook project in a specific directory:
mdbook init {{path/to/directory}}
echo "Clean the directory with the generated book:
mdbook clean
echo "Serve a book at <http://localhost:3000>, auto build when file changes:
mdbook serve
echo "Watch a set of Markdown files and automatically build when a file is changed:
mdbook watch
echo "mediamtx
echo "Real-time media server and proxy.
echo "More information: <https://github.com/bluenviron/mediamtx>.
echo "Run MediaMTX:
mediamtx
echo "Run MediaMTX with a custom config location:
mediamtx {{path/to/config}}.yml
echo "Start MediaMTX as a daemon:
systemctl start mediamtx
echo "Medusa
echo "A modular and parallel login brute-forcer for a variety of protocols.
echo "More information: <https://manned.org/medusa>.
echo "Execute brute force against an FTP server using a file containing usernames and a file containing passwords:
medusa -M ftp -h host -U {{path/to/username_file}} -P {{path/to/password_file}}
echo "Execute a login attempt against an HTTP server using the username, password and user-agent specified:
medusa -M HTTP -h host -u {{username}} -p {{password}} -m USER-AGENT:"{{Agent}}"
echo "Execute a brute force against a MySQL server using a file containing usernames and a hash:
medusa -M mysql -h host -U {{path/to/username_file}} -p {{hash}} -m PASS:HASH
echo "Execute a brute force against a list of SMB servers using a username and a pwdump file:
medusa -M smbnt -H {{path/to/hosts_file}} -C {{path/to/pwdump_file}} -u {{username}} -m PASS:HASH
echo "megadl
echo "This command is an alias of megatools-dl.
echo "More information: <https://megatools.megous.com/man/megatools-dl.html>.
echo "View documentation for the original command:
tldr megatools-dl
echo "megatools-dl
echo "Download files from mega.nz.
echo "Part of the megatools suite.
echo "More information: <https://megatools.megous.com/man/megatools-dl.html>.
echo "Download files from a mega.nz link into the current directory:
megatools-dl {{https://mega.nz/...}}
echo "Download files from a mega.nz link into a specific directory:
megatools-dl --path {{path/to/directory}} {{https://mega.nz/...}}
echo "Interactively choose which files to download:
megatools-dl --choose-files {{https://mega.nz/...}}
echo "Limit the download speed in KiB/s:
megatools-dl --limit-speed {{speed}} {{https://mega.nz/...}}
echo "microcom
echo "A minimalistic terminal program, used to access remote devices via a serial, CAN or telnet connection from the console.
echo "More information: <https://manned.org/microcom>.
echo "Open a serial port using the specified baud rate:
microcom --port {{path/to/serial_port}} --speed {{baud_rate}}
echo "Establish a telnet connection to the specified host:
microcom --telnet {{hostname}}:{{port}}
echo "mimetype
echo "Automatically determine the MIME type of a file.
echo "More information: <https://manned.org/mimetype>.
echo "Print the MIME type of a given file:
mimetype {{path/to/file}}
echo "Display only the MIME type, and not the filename:
mimetype --brief {{path/to/file}}
echo "Display a description of the MIME type:
mimetype --describe {{path/to/file}}
echo "Determine the MIME type of stdin (does not check a filename):
{{command}} | mimetype --stdin
echo "Display debug information about how the MIME type was determined:
mimetype --debug {{path/to/file}}
echo "Display all the possible MIME types of a given file in confidence order:
mimetype --all {{path/to/file}}
echo "Explicitly specify the 2-letter language code of the output:
mimetype --language {{path/to/file}}
echo "minicom
echo "A program to communicate with the serial interface of a device.
echo "More information: <https://manned.org/minicom>.
echo "Open a given serial port:
sudo minicom --device {{/dev/ttyUSB0}}
echo "Open a given serial port with a given baud rate:
sudo minicom --device {{/dev/ttyUSB0}} --baudrate {{115200}}
echo "Enter the configuration menu before communicating with a given serial port:
sudo minicom --device {{/dev/ttyUSB0}} --setup
echo "mke2fs
echo "Creates a Linux filesystem inside a partition.
echo "More information: <https://manned.org/mke2fs>.
echo "Create an ext2 filesystem in partition 1 of device b (sdb1):
mkfs.ext2 {{/dev/sdb1}}
echo "Create an ext3 filesystem in partition 1 of device b (sdb1):
mkfs.ext3 {{/dev/sdb1}}
echo "Create an ext4 filesystem in partition 1 of device b (sdb1):
mkfs.ext4 {{/dev/sdb1}}
echo "mkfs.btrfs
echo "Create a btrfs filesystem.
echo "Defaults to raid1, which specifies 2 copies of a given data block spread across 2 different devices.
echo "More information: <https://btrfs.readthedocs.io/en/latest/mkfs.btrfs.html>.
echo "Create a btrfs filesystem on a single device:
sudo mkfs.btrfs --metadata single --data single {{/dev/sda}}
echo "Create a btrfs filesystem on multiple devices with raid1:
sudo mkfs.btrfs --metadata raid1 --data raid1 {{/dev/sda}} {{/dev/sdb}} {{/dev/sdN}}
echo "Set a label for the filesystem:
sudo mkfs.btrfs --label "{{label}}" {{/dev/sda}} [{{/dev/sdN}}]
echo "mkfs.cramfs
echo "Creates a ROM filesystem inside a partition.
echo "More information: <https://manned.org/mkfs.cramfs>.
echo "Create a ROM filesystem inside partition 1 on device b (sdb1):
mkfs.cramfs {{/dev/sdb1}}
echo "Create a ROM filesystem with a volume-name:
mkfs.cramfs -n {{volume_name}} {{/dev/sdb1}}
echo "mkfs.exfat
echo "Creates an exfat filesystem inside a partition.
echo "More information: <https://manned.org/mkfs.exfat>.
echo "Create an exfat filesystem inside partition 1 on device b (sdb1):
mkfs.exfat {{/dev/sdb1}}
echo "Create filesystem with a volume-name:
mkfs.exfat -n {{volume_name}} {{/dev/sdb1}}
echo "Create filesystem with a volume-id:
mkfs.exfat -i {{volume_id}} {{/dev/sdb1}}
echo "mkfs.ext4
echo "Creates an ext4 filesystem inside a partition.
echo "More information: <https://manned.org/mkfs.ext4>.
echo "Create an ext4 filesystem inside partition 1 on device b (sdb1):
sudo mkfs.ext4 {{/dev/sdb1}}
echo "Create an ext4 filesystem with a volume-label:
sudo mkfs.ext4 -L {{volume_label}} {{/dev/sdb1}}
echo "mkfs.fat
echo "Creates an MS-DOS filesystem inside a partition.
echo "More information: <https://manned.org/mkfs.fat>.
echo "Create a fat filesystem inside partition 1 on device b (sdb1):
mkfs.fat {{/dev/sdb1}}
echo "Create filesystem with a volume-name:
mkfs.fat -n {{volume_name}} {{/dev/sdb1}}
echo "Create filesystem with a volume-id:
mkfs.fat -i {{volume_id}} {{/dev/sdb1}}
echo "Use 5 instead of 2 file allocation tables:
mkfs.fat -f 5 {{/dev/sdb1}}
echo "mkfs
echo "Build a Linux filesystem on a hard disk partition.
echo "This command is deprecated in favor of filesystem specific mkfs.<type> utils.
echo "More information: <https://manned.org/mkfs>.
echo "Build a Linux ext2 filesystem on a partition:
mkfs {{path/to/partition}}
echo "Build a filesystem of a specified type:
mkfs -t {{ext4}} {{path/to/partition}}
echo "Build a filesystem of a specified type and check for bad blocks:
mkfs -c -t {{ntfs}} {{path/to/partition}}
echo "mkfs.minix
echo "Creates a Minix filesystem inside a partition.
echo "More information: <https://manned.org/mkfs.minix>.
echo "Create a Minix filesystem inside partition 1 on device b (sdb1):
mkfs.minix {{/dev/sdb1}}
echo "mkfs.ntfs
echo "Creates a NTFS filesystem inside a partition.
echo "More information: <https://manned.org/mkfs.ntfs>.
echo "Create a NTFS filesystem inside partition 1 on device b (sdb1):
mkfs.ntfs {{/dev/sdb1}}
echo "Create filesystem with a volume-label:
mkfs.ntfs -L {{volume_label}} {{/dev/sdb1}}
echo "Create filesystem with specific UUID:
mkfs.ntfs -U {{UUID}} {{/dev/sdb1}}
echo "mkfs.vfat
echo "Creates an MS-DOS filesystem inside a partition.
echo "More information: <https://manned.org/mkfs.vfat>.
echo "Create a vfat filesystem inside partition 1 on device b (sdb1):
mkfs.vfat {{/dev/sdb1}}
echo "Create filesystem with a volume-name:
mkfs.vfat -n {{volume_name}} {{/dev/sdb1}}
echo "Create filesystem with a volume-id:
mkfs.vfat -i {{volume_id}} {{/dev/sdb1}}
echo "Use 5 instead of 2 file allocation tables:
mkfs.vfat -f 5 {{/dev/sdb1}}
echo "mkhomedir_helper
echo "Create the user's home directory after creating the user.
echo "More information: <https://manned.org/mkhomedir_helper>.
echo "Create a home directory for a user based on /etc/skel with umask 022:
sudo mkhomedir_helper {{username}}
echo "Create a home directory for a user based on /etc/skel with all permissions for owner (0) and read permission for group (3):
sudo mkhomedir_helper {{username}} {{037}}
echo "Create a home directory for a user based on a custom skeleton:
sudo mkhomedir_helper {{username}} {{umask}} {{path/to/skeleton_directory}}
echo "mkinitcpio
echo "Generates initial ramdisk environments for booting the Linux kernel based on the specified preset(s).
echo "More information: <https://man.archlinux.org/man/mkinitcpio.8>.
echo "Perform a dry run (print what would be done without actually doing it):
mkinitcpio
echo "Generate a ramdisk environment based on the linux preset:
mkinitcpio --preset {{linux}}
echo "Generate a ramdisk environment based on the linux-lts preset:
mkinitcpio --preset {{linux-lts}}
echo "Generate ramdisk environments based on all existing presets (used to regenerate all the initramfs images after a change in /etc/mkinitcpio.conf):
mkinitcpio --allpresets
echo "Generate an initramfs image using an alternative configuration file:
mkinitcpio --config {{path/to/mkinitcpio.conf}} --generate {{path/to/initramfs.img}}
echo "Generate an initramfs image for a kernel other than the one currently running (the installed kernel releases can be found in /usr/lib/modules/):
mkinitcpio --kernel {{kernel_version}} --generate {{path/to/initramfs.img}}
echo "List all available hooks:
mkinitcpio --listhooks
echo "Display help for a specific hook:
mkinitcpio --hookhelp {{hook_name}}
echo "mkisofs
echo "Create ISO files from directories.
echo "Also aliased as genisoimage.
echo "More information: <https://manned.org/mkisofs>.
echo "Create an ISO from a directory:
mkisofs -o {{filename.iso}} {{path/to/source_directory}}
echo "Set the disc label when creating an ISO:
mkisofs -o {{filename.iso}} -V "{{label_name}}" {{path/to/source_directory}}
echo "mklost+found
echo "Create a lost+found directory.
echo "More information: <https://manned.org/mklost+found>.
echo "Create a lost+found directory in the current directory:
mklost+found
echo "mknod
echo "Create block or character device special files.
echo "More information: <https://www.gnu.org/software/coreutils/mknod>.
echo "Create a block device:
sudo mknod {{path/to/device_file}} b {{major_device_number}} {{minor_device_number}}
echo "Create a character device:
sudo mknod {{path/to/device_file}} c {{major_device_number}} {{minor_device_number}}
echo "Create a FIFO (queue) device:
sudo mknod {{path/to/device_file}} p
echo "Create a device file with default SELinux security context:
sudo mknod -Z {{path/to/device_file}} {{type}} {{major_device_number}} {{minor_device_number}}
echo "mkosi
echo "Tool for building modern, legacy-free Linux images.
echo "Part of systemd.
echo "More information: <https://manned.org/mkosi>.
echo "Show current build configuration to verify what would be built:
mkosi summary
echo "Build an image with default settings (if no distribution is selected, the distribution of the host system is used):
mkosi build --distribution {{fedora|debian|ubuntu|arch|opensuse|...}}
echo "Build an image and run an interactive shell in a systemd-nspawn container of the image:
mkosi shell
echo "Boot an image in a virtual machine using QEMU (only supported for disk images or CPIO images when a kernel is provided):
mkosi qemu
echo "Display help:
mkosi help
echo "mksquashfs
echo "Create or append files and directories to squashfs filesystems.
echo "More information: <https://manned.org/mksquashfs>.
echo "Create or append files and directories to a squashfs filesystem (compressed using gzip by default):
mksquashfs {{path/to/file_or_directory1 path/to/file_or_directory2 ...}} {{filesystem.squashfs}}
echo "Create or append files and directories to a squashfs filesystem, using a specific [comp]ression algorithm:
mksquashfs {{path/to/file_or_directory1 path/to/file_or_directory2 ...}} {{filesystem.squashfs}} -comp {{gzip|lzo|lz4|xz|zstd|lzma}}
echo "Create or append files and directories to a squashfs filesystem, [e]xcluding some of them:
mksquashfs {{path/to/file_or_directory1 path/to/file_or_directory2 ...}} {{filesystem.squashfs}} -e {{file|directory1 file|directory2 ...}}
echo "Create or append files and directories to a squashfs filesystem, [e]xcluding those ending with .gz:
mksquashfs {{path/to/file_or_directory1 path/to/file_or_directory2 ...}} {{filesystem.squashfs}} -wildcards -e "{{*.gz}}"
echo "Create or append files and directories to a squashfs filesystem, [e]xcluding those matching a regular expression:
mksquashfs {{path/to/file_or_directory1 path/to/file_or_directory2 ...}} {{filesystem.squashfs}} -regex -e "{{regular_expression}}"
echo "mkswap
echo "Set up a Linux swap area on a device or in a file.
echo "Note: path/to/file can either point to a regular file or a swap partition.
echo "More information: <https://manned.org/mkswap>.
echo "Set up a given swap area:
sudo mkswap {{path/to/file}}
echo "Check a partition for bad blocks before creating the swap area:
sudo mkswap -c {{path/to/file}}
echo "Specify a label for the partition (to allow swapon to use the label):
sudo mkswap -L {{label}} {{/dev/sda1}}
echo "mktemp
echo "Create a temporary file or directory.
echo "More information: <https://www.gnu.org/software/coreutils/mktemp>.
echo "Create an empty temporary file and print its absolute path:
mktemp
echo "Use a custom directory (defaults to $TMPDIR`, or `/tmp):
mktemp --tmpdir={{/path/to/tempdir}}
echo "Use a custom path template (Xs are replaced with random alphanumeric characters):
mktemp {{/tmp/example.XXXXXXXX}}
echo "Use a custom file name template:
mktemp -t {{example.XXXXXXXX}}
echo "Create an empty temporary file with the given suffix and print its absolute path:
mktemp --suffix {{.ext}}
echo "Create an empty temporary directory and print its absolute path:
mktemp --directory
echo "mlabel
echo "Set an MS-DOS volume label for FAT and VFAT filesystems.
echo "More information: <https://www.gnu.org/software/mtools/manual/mtools.html#mlabel>.
echo "Set a filesystem label:
mlabel -i /dev/{{sda}} ::"{{new_label}}"
echo "mmcli
echo "Control and monitor the ModemManager.
# echo "More information: <https://www.freedesktop.org/software/ModemManager/man/latest/mmcli.1.html>.
echo "List available modems:
mmcli --list-modems
echo "Print information about a modem:
mmcli --modem={{modem}}
echo "Enable a modem:
mmcli --modem={{modem}} --enable
echo "List SMS messages available on the modem:
sudo mmcli --modem={{modem}} --messaging-list-sms
echo "Delete a message from the modem, specifying its path:
sudo mmcli --modem={{modem}} --messaging-delete-sms={{path/to/message_file}}
echo "mmdebstrap
echo "Create a Debian chroot.
echo "Alternative to debootstrap.
echo "More information: <https://gitlab.mister-muffin.de/josch/mmdebstrap/>.
echo "Create a Debian Stable directory chroot:
sudo mmdebstrap stable {{path/to/debian-root/}}
echo "Create a Debian Bookworm tarball chroot using a mirror:
mmdebstrap bookworm {{path/to/debian-bookworm.tar}} {{http://mirror.example.org/debian}}
echo "Create a Debian Sid tarball chroot with additional packages:
mmdebstrap sid {{path/to/debian-sid.tar}} --include={{pkg1,pkg2}}
echo "mocp
echo "Music on Console (MOC) audio player.
echo "More information: <https://manned.org/mocp>.
echo "Launch the MOC terminal UI:
mocp
echo "Launch the MOC terminal UI in a specific directory:
mocp {{path/to/directory}}
echo "Start the MOC server in the background, without launching the MOC terminal UI:
mocp --server
echo "Add a specific song to the play queue while MOC is in the background:
mocp --enqueue {{path/to/audio_file}}
echo "Add songs recursively to the play queue while MOC is in the background:
mocp --append {{path/to/directory}}
echo "Clear the play queue while MOC is in the background:
mocp --clear
# echo "Play or stop the currently queued song while MOC is in the background:
# mocp --{{play|stop}}
# echo "Stop the MOC server while it's in the background:
mocp --exit
echo "modinfo
echo "Extract information about a Linux kernel module.
echo "More information: <https://manned.org/modinfo>.
echo "List all attributes of a kernel module:
modinfo {{kernel_module}}
echo "List the specified attribute only:
modinfo -F {{author|description|license|parm|filename}} {{kernel_module}}
echo "modprobe
echo "Add or remove modules from the Linux kernel.
echo "More information: <https://manned.org/modprobe>.
echo "Pretend to load a module into the kernel, but don't actually do it:
sudo modprobe --dry-run {{module_name}}
echo "Load a module into the kernel:
sudo modprobe {{module_name}}
echo "Remove a module from the kernel:
sudo modprobe --remove {{module_name}}
echo "Remove a module and those that depend on it from the kernel:
sudo modprobe --remove-dependencies {{module_name}}
echo "Show a kernel module's dependencies:
sudo modprobe --show-depends {{module_name}}
echo "module
echo "Modify a users' environment using the module command.
echo "More information: <https://lmod.readthedocs.io/en/latest/010_user.html>.
echo "Display available modules:
module avail
echo "Search for a module by name:
module avail {{module_name}}
echo "Load a module:
module load {{module_name}}
echo "Display loaded modules:
module list
echo "Unload a specific loaded module:
module unload {{module_name}}
echo "Unload all loaded modules:
module purge
echo "Specify user-created modules:
module use {{path/to/modulefiles}}
echo "mokutil
echo "Configure Secure Boot Machine Owner Keys (MOK).
echo "Some operations, such as enabling and disabling Secure Boot or enrolling keys require a reboot.
echo "More information: <https://github.com/lcp/mokutil>.
echo "Show if Secure Boot is enabled:
mokutil --sb-state
echo "Enable Secure Boot:
mokutil --enable-validation
echo "Disable Secure Boot:
mokutil --disable-validation
echo "List enrolled keys:
mokutil --list-enrolled
echo "Enroll a new key:
mokutil --import {{path/to/key.der}}
echo "List the keys to be enrolled:
mokutil --list-new
echo "Set shim verbosity:
mokutil --set-verbosity true
echo "mono
echo "Runtime for the .NET Framework.
echo "More information: <https://www.mono-project.com/docs/>.
echo "Run a .NET assembly in debug mode:
mono --debug {{path/to/program.exe}}
echo "Run a .NET assembly:
mono {{path/to/program.exe}}
echo "mons
echo "A tool to quickly manage two displays.
echo "More information: <https://github.com/Ventto/mons>.
echo "Enable only the primary monitor:
mons -o
echo "Enable only the secondary monitor:
mons -s
echo "Duplicate the primary monitor onto the secondary monitor, using the resolution of the primary monitor:
mons -d
echo "Mirror the primary monitor onto the secondary monitor, using the resolution of the secondary monitor:
mons -m
echo "mount.cifs
echo "Mount SMB (Server Message Block) or CIFS (Common Internet File System) shares.
echo "Note: you can also do the same thing by passing the -t cifs` option to `mount.
echo "More information: <https://manned.org/mount.cifs>.
echo "Connect using the specified username or $USER by default (you will be prompted for a password):
mount.cifs -o user={{username}} //{{server}}/{{share_name}} {{mountpoint}}
echo "Connect as the guest user (without a password):
mount.cifs -o guest //{{server}}/{{share_name}} {{mountpoint}}
echo "Set ownership information for the mounted directory:
mount.cifs -o uid={{user_id|username}},gid={{group_id|groupname}} //{{server}}/{{share_name}} {{mountpoint}}
echo "mount.ddi
echo "Mount Discoverable Disk Images.
echo "See tldr systemd-dissect for other commands relevant to DDIs.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/latest/systemd-dissect.html>.
echo "Mount an OS image:
mount.ddi {{path/to/image.raw}} {{/mnt/image}}
echo "mount.smb3
echo "This command is an alias of mount.cifs.
echo "Note: for SMB versions before 3 you have to use mount.cifs instead.
echo "View documentation for the original command:
tldr mount.cifs
echo "mountpoint
echo "Test if a directory is a filesystem mountpoint.
echo "More information: <https://manned.org/mountpoint>.
echo "Check if a directory is a mountpoint:
mountpoint {{path/to/directory}}
echo "Check if a directory is a mountpoint without showing any output:
mountpoint -q {{path/to/directory}}
echo "Show major/minor numbers of a mountpoint's filesystem:
mountpoint --fs-devno {{path/to/directory}}
echo "mpg123
echo "Console MPEG audio player.
echo "More information: <https://manned.org/mpg123>.
echo "Play the specified mp3 files:
mpg123 {{path/to/file1.mp3 path/to/file2.mp3 ...}}
echo "Play the mp3 from stdin:
cat {{file.mp3}} | mpg123 -
echo "Jump forward to the next song:
f
echo "Jump back to the beginning for the song:
b
# echo "Stop or replay the current file:
s
echo "Fast forward:
.
echo "Quit:
q
echo "mpicc
echo "Open MPI C wrapper compiler.
# echo "The wrappers are simply thin shells on top of a C compiler, they add the relevant compiler and linker flags to the command-line that are necessary to compile/link Open MPI programs, and then invoke the underlying C compiler to actually perform the command.
echo "More information: <https://www.mpich.org/static/docs/latest/www1/mpicc.html>.
echo "Compile a source code file into an object file:
mpicc -c {{path/to/file.c}}
echo "Link an object file and make an executable:
mpicc -o {{executable}} {{path/to/object_file.o}}
echo "Compile and link source code in a single command:
mpicc -o {{executable}} {{path/to/file.c}}
echo "mpstat
echo "Report CPU statistics.
echo "More information: <https://manned.org/mpstat>.
echo "Display CPU statistics every 2 seconds:
mpstat {{2}}
echo "Display 5 reports, one by one, at 2 second intervals:
mpstat {{2}} {{5}}
echo "Display 5 reports, one by one, from a given processor, at 2 second intervals:
mpstat -P {{0}} {{2}} {{5}}
echo "mssh
echo "GTK+ based SSH client for interacting with multiple SSH servers at once.
echo "More information: <https://manned.org/mssh>.
echo "Open a new window and connect to multiple SSH servers:
mssh {{user@host1}} {{user@host2}} {{...}}
echo "Open a new window and connect to a group of servers predefined in ~/.mssh_clusters:
mssh --alias {{alias_name}}
echo "mt
echo "Control magnetic tape drive operation (commonly LTO tape).
echo "More information: <https://manned.org/mt>.
echo "Check the status of a tape drive:
mt -f {{/dev/nstX}} status
echo "Rewind the tape to beginning:
mt -f {{/dev/nstX}} rewind
echo "Move forward a given files, then position the tape on first block of next file:
mt -f {{/dev/nstX}} fsf {{count}}
echo "Rewind the tape, then position the tape at beginning of the given file:
mt -f {{/dev/nstX}} asf {{count}}
echo "Position the tape at the end of valid data:
mt -f {{/dev/nstX}} eod
echo "Rewind the tape and unload/eject it:
mt -f {{/dev/nstX}} eject
echo "Write EOF (End-of-file) mark at the current position:
mt -f {{/dev/nstX} eof
echo "mycli
echo "A CLI for MySQL, MariaDB, and Percona with auto-completion and syntax highlighting.
echo "More information: <https://manned.org/mycli>.
echo "Connect to a database with the currently logged in user:
mycli {{database_name}}
echo "Connect to a database with the specified user:
mycli -u {{user}} {{database_name}}
echo "Connect to a database on the specified host with the specified user:
mycli -u {{user}} -h {{host}} {{database_name}}
echo "nala
echo "Package management Utility.
echo "Wrapper for the apt package manager.
echo "More information: <https://gitlab.com/volian/nala>.
echo "Install a package, or update it to the latest available version:
sudo nala install {{package}}
echo "Remove a package:
sudo nala remove {{package}}
echo "Remove a package and its configuration files:
nala purge {{package}}
echo "Search package names and descriptions using a word, regex (default) or glob:
nala search "{{pattern}}"
echo "Update the list of available packages and upgrade the system:
sudo nala upgrade
echo "Remove all unused packages and dependencies from your system:
sudo nala autoremove
echo "Fetch fast mirrors to improve download speeds:
sudo nala fetch
echo "Display the history of all transactions:
nala history
echo "namcap
echo "Check binary packages and source PKGBUILDs for common packaging mistakes.
echo "More information: <https://man.archlinux.org/man/namcap.1>.
echo "Check a specific PKGBUILD file:
namcap {{path/to/pkgbuild}}
echo "Check a specific package file:
namcap {{path/to/package.pkg.tar.zst}}
echo "Check a file, printing extra [i]nformational messages:
namcap -i {{path/to/file}}
echo "named
echo "Execute the DNS (Dynamic Name Service) server daemon that converts host names to IP addresses and vice versa.
echo "More information: <https://manned.org/named>.
echo "Read the default configuration file /etc/named.conf, read any initial data and listen for queries:
named
echo "Read a custom configuration file:
named -c {{path/to/named.conf}}
echo "Use IPv4 or IPv6 only, even if the host machine is capable of utilising other protocols:
named {{-4|-6}}
echo "Listen for queries on a specific port instead of the default port 53:
named -p {{port}}
echo "Run the server in the foreground and do not daemonize:
named -f
echo "namei
echo "Follows a pathname (which can be a symbolic link) until a terminal point is found (a file/directory/char device etc).
echo "This program is useful for finding "too many levels of symbolic links" problems.
echo "More information: <https://manned.org/namei>.
echo "Resolve the pathnames specified as the argument parameters:
namei {{path/to/a}} {{path/to/b}} {{path/to/c}}
echo "Display the results in a long-listing format:
namei --long {{path/to/a}} {{path/to/b}} {{path/to/c}}
echo "Show the mode bits of each file type in the style of ls:
namei --modes {{path/to/a}} {{path/to/b}} {{path/to/c}}
echo "Show owner and group name of each file:
namei --owners {{path/to/a}} {{path/to/b}} {{path/to/c}}
echo "Don't follow symlinks while resolving:
namei --nosymlinks {{path/to/a}} {{path/to/b}} {{path/to/c}}
echo "nautilus
# echo "Default file explorer for GNOME desktop environment.
echo "Also known as GNOME Files.
echo "More information: <https://manned.org/nautilus>.
echo "Launch Nautilus:
nautilus
echo "Launch Nautilus as root user:
sudo nautilus
echo "Launch Nautilus and display a specific directory:
nautilus {{path/to/directory}}
echo "Launch Nautilus with a specific file or directory selected:
nautilus --select {{path/to/file_or_directory}}
echo "Launch Nautilus in a separated window:
nautilus --new-window
echo "Close all Nautilus instances:
nautilus --quit
echo "Display help:
nautilus --help
echo "ncal
echo "This command is an alias of cal.
echo "More information: <https://manned.org/ncal>.
echo "View documentation for the original command:
tldr cal
echo "ncat
echo "Use the normal cat functionality over networks.
echo "More information: <https://manned.org/ncat>.
echo "Listen for input on the specified port and write it to the specified file:
ncat -l {{port}} > {{path/to/file}}
echo "Accept multiple connections and keep ncat open after they have been closed:
ncat -lk {{port}}
echo "Write output of specified file to the specified host on the specified port:
ncat {{address}} {{port}} < {{path/to/file}}
echo "ndctl
echo "Utility for managing Non-Volatile DIMMs.
echo "More information: <https://manned.org/ndctl>.
echo "Create an 'fsdax' mode namespace:
ndctl create-namespace --mode={{fsdax}}
echo "Change the mode of a namespace to 'raw':
ndctl create-namespace --reconfigure={{namespaceX.Y}} --mode={{raw}}
echo "Check a sector mode namespace for consistency, and repair if needed:
ndctl check-namespace --repair {{namespaceX.Y}}
echo "List all namespaces, regions, and buses (including disabled ones):
ndctl list --namespaces --regions --buses --idle
echo "List a specific namespace and include lots of additional information:
ndctl list -vvv --namespace={{namespaceX.Y}}
echo "Run a monitor to watch for SMART health events for NVDIMMs on the 'ACPI.NFIT' bus:
ndctl monitor --bus={{ACPI.NFIT}}
echo "Remove a namespace (when applicable) or reset it to an initial state:
ndctl destroy-namespace --force {{namespaceX.Y}}
echo "needrestart
echo "Check which daemons need to be restarted after library upgrades.
echo "More information: <https://github.com/liske/needrestart>.
echo "List outdated processes:
needrestart
echo "Interactively restart services:
sudo needrestart
echo "List outdated processes in [v]erbose or [q]uiet mode:
needrestart -{{v|q}}
echo "Check if the [k]ernel is outdated:
needrestart -k
echo "Check if the CPU microcode is outdated:
needrestart -w
echo "List outdated processes in [b]atch mode:
needrestart -b
echo "List outdated processed using a specific [c]onfiguration file:
needrestart -c {{path/to/config}}
echo "Display help:
needrestart --help
echo "nemo
# echo "Manages files and directories in Cinnamon desktop environment.
echo "More information: <https://manned.org/nemo>.
echo "Open the current user home directory:
nemo
echo "Open specific directories in separate windows:
nemo {{path/to/directory1 path/to/directory2 ...}}
echo "Open specific directories in tabs:
nemo --tabs {{path/to/directory1 path/to/directory2 ...}}
echo "Open a directory with a specific window size:
nemo --geometry={{600}}x{{400}} {{path/to/directory}}
echo "Close all windows:
nemo --quit
echo "nethogs
echo "Monitor bandwidth usage per process.
echo "More information: <https://github.com/raboof/nethogs>.
echo "Start NetHogs as root (default device is eth0):
sudo nethogs
echo "Monitor bandwidth on specific device:
sudo nethogs {{device}}
echo "Monitor bandwidth on multiple devices:
sudo nethogs {{device1}} {{device2}}
echo "Specify refresh rate:
sudo nethogs -t {{seconds}}
echo "netselect-apt
echo "Create a sources.list file for a Debian mirror with the lowest latency.
echo "More information: <https://manpages.debian.org/latest/netselect-apt/netselect-apt.html>.
echo "Create sources.list using the lowest latency server:
sudo netselect-apt
echo "Specify Debian branch, stable is used by default:
sudo netselect-apt {{testing}}
echo "Include non-free section:
sudo netselect-apt --non-free
echo "Specify a country for the mirror list lookup:
sudo netselect-apt -c {{India}}
echo "netselect
echo "Speed test for choosing a fast network server.
echo "More information: <https://github.com/apenwarr/netselect>.
echo "Choose the server with the lowest latency:
sudo netselect {{host_1}} {{host_2}}
echo "Display nameserver resolution and statistics:
sudo netselect -vv {{host_1}} {{host_2}}
echo "Define maximum TTL (time to live):
sudo netselect -m {{10}} {{host_1}} {{host_2}}
echo "Print fastest N servers among the hosts:
sudo netselect -s {{N}} {{host_1}} {{host_2}} {{host_3}}
echo "List available options:
netselect
echo "networkctl
echo "Query the status of network links.
echo "Manage the network configuration using systemd-networkd.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/networkctl.html>.
echo "Show a list of existing links and their status:
networkctl list
echo "Show an overall network status:
networkctl status
echo "Bring network devices up:
networkctl up {{interface1 interface2 ...}}
echo "Bring network devices down:
networkctl down {{interface1 interface2 ...}}
echo "Renew dynamic configurations (e.g. IP addresses received from a DHCP server):
networkctl renew {{interface1 interface2 ...}}
echo "Reload configuration files (.netdev and .network):
networkctl reload
echo "Reconfigure network interfaces (if you edited the config, you need to call networkctl reload first):
networkctl reconfigure {{interface1 interface2 ...}}
echo "newgrp
echo "Switch primary group membership.
echo "More information: <https://manned.org/newgrp>.
echo "Change user's primary group membership:
newgrp {{group_name}}
echo "Reset primary group membership to user's default group in /etc/passwd:
newgrp
echo "nft
echo "Allows configuration of tables, chains and rules provided by the Linux kernel firewall.
echo "Nftables replaces iptables.
echo "More information: <https://wiki.nftables.org/wiki-nftables/index.php/Main_Page>.
echo "View current configuration:
sudo nft list ruleset
echo "Add a new table with family "inet" and table "filter":
sudo nft add table {{inet}} {{filter}}
echo "Add a new chain to accept all inbound traffic:
sudo nft add chain {{inet}} {{filter}} {{input}} \{ type {{filter}} hook {{input}} priority {{0}} \; policy {{accept}} \}
echo "Add a new rule to accept several TCP ports:
sudo nft add rule {{inet}} {{filter}} {{input}} {{tcp}} {{dport \{ telnet, ssh, http, https \} accept}}
echo "Add a NAT rule to translate all traffic from the 192.168.0.0/24 subnet to the host's public IP:
sudo nft add rule {{nat}} {{postrouting}} ip saddr {{192.168.0.0/24}} {{masquerade}}
echo "Show rule handles:
sudo nft --handle --numeric list chain {{family}} {{table}} {{chain}}
echo "Delete a rule:
sudo nft delete rule {{inet}} {{filter}} {{input}} handle {{3}}
echo "Save current configuration:
sudo nft list ruleset > {{/etc/nftables.conf}}
echo "nitch
echo "A small and incredibly fast system fetch written fully in Nim.
echo "More information: <https://github.com/ssleert/nitch>.
echo "Display system information (hostname, kernel, uptime, etc.):
nitch
echo "Display [h]elp:
nitch --help
echo "Display [v]ersion:
nitch --version
echo "nitrogen
# echo "Desktop background browser and setter for X Window.
echo "More information: <https://github.com/l3ib/nitrogen>.
echo "View and set the wallpapers from a specific directory:
nitrogen {{path/to/directory}}
echo "Set the wallpaper with automatic size settings:
nitrogen --set-auto {{path/to/file}}
echo "Restore the previous wallpaper:
nitrogen --restore
echo "nixos-container
echo "Starts NixOS containers using Linux containers.
echo "More information: <https://nixos.org/manual/nixos/stable/#ch-containers>.
echo "List running containers:
sudo nixos-container list
echo "Create a NixOS container with a specific configuration file:
sudo nixos-container create {{container_name}} --config-file {{nix_config_file_path}}
# echo "Start, stop, terminate, or destroy a specific container:
# sudo nixos-container {{start|stop|terminate|destroy|status}} {{container_name}}
echo "Run a command in a running container:
sudo nixos-container run {{container_name}} -- {{command}} {{command_arguments}}
echo "Update a container configuration:
sudo $EDITOR /var/lib/container/{{container_name}}/etc/nixos/configuration.nix && sudo nixos-container update {{container_name}}
echo "Enter an interactive shell session on an already-running container:
sudo nixos-container root-login {{container_name}}
echo "nixos-option
echo "Inspect a NixOS configuration.
echo "More information: <https://nixos.org/manual/nixos/stable/index.html#sec-modularity>.
echo "List all subkeys of a given option key:
nixos-option {{option_key}}
echo "List current boot kernel modules:
nixos-option boot.kernelModules
echo "List authorized keys for a specific user:
nixos-option users.users.{{username}}.openssh.authorizedKeys.{{keyFiles|keys}}
echo "List all remote builders:
nixos-option nix.buildMachines
echo "List all subkeys of a given key on another NixOS configuration:
NIXOS_CONFIG={{path_to_configuration.nix}} nixos-option {{option_key}}
echo "Show recursively all values of a user:
nixos-option -r users.users.{{user}}
echo "nixos-rebuild
echo "Reconfigure a NixOS machine.
echo "More information: <https://nixos.org/nixos/manual/#sec-changing-config>.
echo "Build and switch to the new configuration, making it the boot default:
sudo nixos-rebuild switch
echo "Build and switch to the new configuration, making it the boot default and naming the boot entry:
sudo nixos-rebuild switch -p {{name}}
echo "Build and switch to the new configuration, making it the boot default and installing updates:
sudo nixos-rebuild switch --upgrade
echo "Rollback changes to the configuration, switching to the previous generation:
sudo nixos-rebuild switch --rollback
echo "Build the new configuration and make it the boot default without switching to it:
sudo nixos-rebuild boot
echo "Build and activate the new configuration, but don't make a boot entry (for testing purposes):
sudo nixos-rebuild test
echo "Build the configuration and open it in a virtual machine:
sudo nixos-rebuild build-vm
echo "nm-online
echo "Ask NetworkManager whether the network is connected.
echo "More information: <https://networkmanager.dev/docs/api/latest/nm-online.html>.
echo "Find out whether the network is connected and print the result to stdout:
nm-online
echo "Wait n seconds for a connection (30 by default):
nm-online --timeout {{n}}
echo "nmcli agent
echo "Run nmcli as a NetworkManager secret agent or polkit agent.
echo "This subcommand can also be called with nmcli a.
echo "More information: <https://networkmanager.dev/docs/api/latest/nmcli.html>.
echo "Register nmcli as a secret agent and listen for secret requests:
nmcli agent secret
echo "Register nmcli as a polkit agent and listen for authorization requests:
nmcli agent polkit
echo "Register nmcli as a secret agent and a polkit agent:
nmcli agent all
echo "nmcli connection
echo "Manage connections with NetworkManager.
echo "This subcommand can also be called with nmcli c.
echo "More information: <https://networkmanager.dev/docs/api/latest/nmcli.html>.
echo "List all NetworkManager connections (shows name, UUID, type and device):
nmcli connection
echo "Activate a connection:
nmcli connection up uuid {{uuid}}
echo "Deactivate a connection:
nmcli connection down uuid {{uuid}}
echo "Create an auto-configured dual stack connection:
nmcli connection add ifname {{interface_name}} type {{ethernet}} ipv4.method {{auto}} ipv6.method {{auto}}
echo "Create a static IPv6-only connection:
nmcli connection add ifname {{interface_name}} type {{ethernet}} ip6 {{2001:db8::2/64}} gw6 {{2001:db8::1}} ipv6.dns {{2001:db8::1}} ipv4.method {{ignore}}
echo "Create a static IPv4-only connection:
nmcli connection add ifname {{interface_name}} type {{ethernet}} ip4 {{10.0.0.7/8}} gw4 {{10.0.0.1}} ipv4.dns {{10.0.0.1}} ipv6.method {{ignore}}
echo "Create a VPN connection using OpenVPN from an OVPN file:
nmcli connection import type {{openvpn}} file {{path/to/vpn_config.ovpn}}
echo "nmcli device
echo "Manage network interfaces and establish new Wi-Fi connections using NetworkManager.
echo "This subcommand can also be called with nmcli d.
echo "More information: <https://networkmanager.dev/docs/api/latest/nmcli.html>.
echo "Print the statuses of all network interfaces:
nmcli device status
echo "Print the available Wi-Fi access points:
nmcli device wifi
echo "Connect to a Wi-Fi network with the specified SSID (you will be prompted for a password):
nmcli --ask device wifi connect {{ssid}}
echo "Print the password and QR code for the current Wi-Fi network:
nmcli device wifi show-password
echo "nmcli general
echo "Manage general settings of NetworkManager.
echo "This subcommand can also be called with nmcli g.
echo "More information: <https://networkmanager.dev/docs/api/latest/nmcli.html>.
echo "Show the general status of NetworkManager:
nmcli general
echo "Show the hostname of the current device:
nmcli general hostname
echo "Change the hostname of the current device:
sudo nmcli general hostname {{new_hostname}}
echo "Show the permissions of NetworkManager:
nmcli general permissions
echo "Show the current logging level and domains:
nmcli general logging
echo "Set the logging level and/or domains (see man NetworkManager.conf for all available domains):
nmcli general logging level {{INFO|OFF|ERR|WARN|DEBUG|TRACE}} domain {{domain_1,domain_2,...}}
echo "nmcli monitor
echo "Monitor changes to the NetworkManager connection status.
echo "This subcommand can also be called with nmcli m.
echo "More information: <https://networkmanager.dev/docs/api/latest/nmcli.html>.
echo "Start monitoring NetworkManager changes:
nmcli monitor
echo "nmcli networking
echo "Manage the networking status of NetworkManager.
echo "This subcommand can also be called with nmcli n.
echo "More information: <https://networkmanager.dev/docs/api/latest/nmcli.html>.
echo "Show the networking status of NetworkManager:
nmcli networking
echo "Enable or disable networking and all interfaces managed by NetworkManager:
nmcli networking {{on|off}}
echo "Show the last known connectivity state:
nmcli networking connectivity
echo "Show the current connectivity state:
nmcli networking connectivity check
echo "nmcli radio
echo "Show the status of radio switches or enable/disable them using NetworkManager.
echo "This subcommand can also be called with nmcli r.
echo "More information: <https://networkmanager.dev/docs/api/latest/nmcli.html>.
echo "Show status of Wi-Fi:
nmcli radio wifi
echo "Turn Wi-Fi on or off:
nmcli radio wifi {{on|off}}
echo "Show status of WWAN:
nmcli radio wwan
echo "Turn WWAN on or off:
nmcli radio wwan {{on|off}}
echo "Show status of both switches:
nmcli radio all
echo "Turn both switches on or off:
nmcli radio all {{on|off}}
echo "nmcli
echo "Manage the network configuration using NetworkManager.
echo "More information: <https://networkmanager.dev/docs/api/latest/nmcli.html>.
echo "View documentation for running nmcli as a NetworkManager secret/polkit agent:
tldr nmcli agent
echo "View documentation for managing network connections:
tldr nmcli connection
echo "View documentation for managing network interfaces and establishing new Wi-Fi connections:
tldr nmcli device
echo "View documentation for managing general settings of NetworkManager:
tldr nmcli general
echo "View documentation for NetworkManager's activity monitor:
tldr nmcli monitor
echo "View documentation for enabling/disabling and checking the status of networking:
tldr nmcli networking
echo "View documentation for managing radio switches:
tldr nmcli radio
echo "nmon
echo "A system administrator, tuner, and benchmark tool.
echo "More information: <https://manned.org/nmon>.
echo "Start nmon:
nmon
echo "Save records to file ("-s 300 -c 288" by default):
nmon -f
echo "Save records to file with a total of 240 measurements, by taking 30 seconds between each measurement:
nmon -f -s {{30}} -c {{240}}
echo "nmtui-connect
echo "This command is an alias of nmtui connect.
echo "View documentation for the original command:
tldr nmtui
echo "nmtui-edit
echo "This command is an alias of nmtui edit.
echo "View documentation for the original command:
tldr nmtui
echo "nmtui-hostname
echo "This command is an alias of nmtui hostname.
echo "View documentation for the original command:
tldr nmtui
echo "nmtui
echo "Text user interface for controlling NetworkManager.
echo "Use arrow keys to navigate, enter to select an option.
echo "More information: <https://networkmanager.dev/docs/api/latest/nmtui.html>.
echo "Open the user interface:
nmtui
echo "Show a list of available connections, with the option to activate or deactivate them:
nmtui connect
echo "Connect to a given network:
nmtui connect {{name|uuid|device|SSID}}
echo "Edit/Add/Delete a given network:
nmtui edit {{name|id}}
echo "Set the system hostname:
nmtui hostname
echo "nologin
echo "Alternative shell that prevents a user from logging in.
echo "More information: <https://manned.org/nologin.5>.
echo "Set a user's login shell to nologin to prevent the user from logging in:
chsh -s {{user}} nologin
echo "Customize message for users with the login shell of nologin:
echo "{{declined_login_message}}" > /etc/nologin.txt
echo "nordvpn
echo "Command-line interface for NordVPN.
echo "More information: <https://nordvpn.com/download/linux/>.
echo "Interactively log into a NordVPN account:
nordvpn login
echo "Display the connection status:
nordvpn status
echo "Connect to the nearest NordVPN server:
nordvpn connect
echo "List all available countries:
nordvpn countries
echo "Connect to a NordVPN server in a specific country:
nordvpn connect {{Germany}}
echo "Connect to a NordVPN server in a specific country and city:
nordvpn connect {{Germany}} {{Berlin}}
echo "Set autoconnect option:
nordvpn set autoconnect on
echo "notify-send
# echo "Uses the current desktop environment's notification system to create a notification.
echo "More information: <https://manned.org/notify-send>.
echo "Show a notification with the title "Test" and the content "This is a test":
notify-send "{{Test}}" "{{This is a test}}"
echo "Show a notification with a custom icon:
notify-send -i {{icon.png}} "{{Test}}" "{{This is a test}}"
echo "Show a notification for 5 seconds:
notify-send -t 5000 "{{Test}}" "{{This is a test}}"
echo "Show a notification with an app's icon and name:
notify-send "{{Test}}" --icon={{google-chrome}} --app-name="{{Google Chrome}}"
echo "nova
echo "The OpenStack project that provides a way to provision compute instances.
echo "More information: <https://docs.openstack.org/nova/latest/>.
echo "List VMs on current tenant:
nova list
echo "List VMs of all tenants (admin user only):
nova list --all-tenants
echo "Boot a VM on a specific host:
nova boot --nic net-id={{net_id}} --image {{image_id}} --flavor {{flavor}} --availability-zone nova:{{host_name}} {{vm_name}}
echo "Start a server:
nova start {{server}}
# echo "Stop a server:
# nova stop {{server}}
echo "Attach a network interface to a specific VM:
nova interface-attach --net-id {{net_id}} {{server}}
echo "nsenter
echo "Run a new command in a running process' namespace.
echo "Particularly useful for docker images or chroot jails.
echo "More information: <https://manned.org/nsenter>.
echo "Run a specific command using the same namespaces as an existing process:
nsenter --target {{pid}} --all {{command}} {{command_arguments}}
echo "Run a specific command in an existing process's network namespace:
nsenter --target {{pid}} --net {{command}} {{command_arguments}}
echo "Run a specific command in an existing process's PID namespace:
nsenter --target {{pid}} --pid {{command}} {{command_arguments}}
echo "Run a specific command in an existing process's IPC namespace:
nsenter --target {{pid}} --ipc {{command}} {{command_arguments}}
echo "Run a specific command in an existing process's UTS, time, and IPC namespaces:
nsenter --target {{pid}} --uts --time --ipc -- {{command}} {{command_arguments}}
echo "Run a specific command in an existing process's namespace by referencing procfs:
nsenter --pid=/proc/{{pid}}/pid/net -- {{command}} {{command_arguments}}
echo "nsnake
echo "Snake game in the terminal.
echo "More information: <https://github.com/alexdantas/nsnake/>.
echo "Start a snake game:
nsnake
echo "Navigate the snake:
{{Up|Down|Left|Right arrow key}}
echo "Pause/unpause the game:
p
echo "Quit the game:
q
echo "Display help during the game:
h
echo "nsxiv
echo "Neo Simple X Image Viewer.
echo "More information: <https://nsxiv.codeberg.page/man>.
echo "Open images:
nsxiv {{path/to/file1 path/to/file2 ...}}
echo "Open images from directories in image mode:
nsxiv {{path/to/directory1 path/to/directory2 ...}}
echo "Search directories recursively for images to view:
nsxiv -r {{path/to/directory1 path/to/directory2 ...}}
echo "Quit nsxiv:
q
echo "Switch to thumbnail mode or open selected image in image mode:
<Return>
echo "Count images forward in image mode:
n
echo "Count images backward in image mode:
p
echo "ntfsfix
echo "Fix common problems on an NTFS partition.
echo "More information: <https://manned.org/ntfsfix>.
echo "Fix a given NTFS partition:
sudo ntfsfix {{/dev/sdXN}}
echo "ntpdate
echo "Synchronize and set the date and time via NTP.
echo "More information: <http://support.ntp.org/documentation>.
echo "Synchronize and set date and time:
sudo ntpdate {{host}}
echo "Query the host without setting the time:
ntpdate -q {{host}}
echo "Use an unprivileged port in case a firewall is blocking privileged ports:
sudo ntpdate -u {{host}}
echo "Force time to be stepped using settimeofday` instead of `slewed:
sudo ntpdate -b {{host}}
echo "ntpq
echo "Query the Network Time Protocol (NTP) daemon.
echo "More information: <https://www.eecis.udel.edu/~mills/ntp/html/ntpq.html>.
echo "Start ntpq in interactive mode:
ntpq --interactive
echo "Print a list of NTP peers:
ntpq --peers
echo "Print a list of NTP peers without resolving hostnames from IP addresses:
ntpq --numeric --peers
echo "Use ntpq in debugging mode:
ntpq --debug-level
echo "Print NTP system variables values:
ntpq --command={{rv}}
echo "numactl
echo "Control NUMA policy for processes or shared memory.
echo "More information: <https://man7.org/linux/man-pages/man8/numactl.8.html>.
echo "Run a command on node 0 with memory allocated on node 0 and 1:
numactl --cpunodebind={{0}} --membind={{0,1}} -- {{command}} {{command_arguments}}
echo "Run a command on CPUs (cores) 0-4 and 8-12 of the current cpuset:
numactl --physcpubind={{+0-4,8-12}} -- {{command}} {{command_arguments}}
echo "Run a command with its memory interleaved on all CPUs:
numactl --interleave={{all}} -- {{command}} {{command_arguments}}
echo "numlockx
echo "Control the number lock key status in X11 sessions.
echo "More information: <http://www.mike-devlin.com/linux/README-numlockx.htm>.
echo "Show the current number lock status:
numlockx status
echo "Turn the number lock on:
numlockx on
echo "Turn the number lock off:
numlockx off
echo "Toggle the current state:
numlockx toggle
echo "obabel
echo "Translate chemistry-related data.
echo "More information: <https://openbabel.org/wiki/Main_Page>.
echo "Convert a .mol file to XYZ coordinates:
obabel {{path/to/file.mol}} -O {{path/to/output_file.xyz}}
echo "Convert a SMILES string to a 500x500 picture:
obabel -:"{{SMILES}} -O {{path/to/output_file.png}} -xp 500
echo "Convert a file of SMILES string to separate 3D .mol files:
obabel {{path/to/file.smi}} -O {{path/to/output_file.mol}} --gen3D -m
echo "Render multiple inputs into one picture:
obabel {{path/to/file1}} {{path/to/file2}} -O {{path/to/output_file.png}}
echo "oomctl
echo "Analyze the state stored in systemd-oomd.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/oomctl.html>.
echo "Show the current state of the cgroups and system contexts stored by systemd-oomd:
oomctl dump
echo "openfortivpn
echo "A VPN client, for Fortinet's proprietary PPP+SSL VPN solution.
echo "More information: <https://github.com/adrienverge/openfortivpn>.
echo "Connect to a VPN with a username and password:
openfortivpn --username={{username}} --password={{password}}
echo "Connect to a VPN using a specific configuration file (defaults to /etc/openfortivpn/config):
sudo openfortivpn --config={{path/to/config}}
echo "Connect to a VPN by specifying the host and port:
openfortivpn {{host}}:{{port}}
echo "Trust a given gateway by passing in its certificate's sha256 sum:
openfortivpn --trusted-cert={{sha256_sum}}
echo "openrc
echo "The OpenRC service manager.
echo "See also: rc-status`, `rc-update`, and `rc-service.
echo "More information: <https://wiki.gentoo.org/wiki/OpenRC>.
echo "Change to a specific runlevel:
sudo openrc {{runlevel_name}}
# echo "Change to a specific runlevel, but don't stop any existing services:
# sudo openrc --no-stop {{runlevel_name}}
echo "openvpn3
echo "OpenVPN 3 Linux client.
echo "More information: <https://community.openvpn.net/openvpn/wiki/OpenVPN3Linux>.
echo "Start a new VPN session:
openvpn3 session-start --config {{path/to/config.conf}}
echo "List established sessions:
openvpn3 sessions-list
echo "Disconnect the currently established session started with given configuration:
openvpn3 session-manage --config {{path/to/config.conf}} --disconnect
echo "Import VPN configuration:
openvpn3 config-import --config {{path/to/config.conf}}
echo "List imported configurations:
openvpn3 configs-list
echo "opkg
echo "A lightweight package manager used to install OpenWrt packages.
echo "More information: <https://openwrt.org/docs/guide-user/additional-software/opkg>.
echo "Install a package:
opkg install {{package}}
echo "Remove a package:
opkg remove {{package}}
echo "Update the list of available packages:
opkg update
echo "Upgrade all the installed packages:
opkg upgrade
echo "Upgrade one or more specific package(s):
opkg upgrade {{package(s)}}
echo "Display information for a specific package:
opkg info {{package}}
echo "List all the available packages:
opkg list
echo "optimus-manager
# echo "GPU switching utility for Nvidia Optimus laptops.
echo "More information: <https://github.com/Askannz/optimus-manager>.
echo "Switch between different GPU modes:
optimus-manager --switch {{nvidia|integrated|hybrid}}
echo "Clean up:
optimus-manager --cleanup
echo "ostree
echo "Version control for binary files similar to git but optimized for operating system root filesystems.
echo "OSTree is the foundation for immutable image-based operating systems such as Fedora Silverblue, Fedora IoT or Fedora CoreOS.
echo "More information: <https://ostreedev.github.io/ostree>.
echo "Initialize a repository of the files in $PWD` with metadata in `$PWD/path/to/repo:
ostree init --repo {{path/to/repo}}
echo "Create a commit (snapshot) of the files:
ostree commit --repo {{path/to/repo}} --branch {{branch_name}}
echo "Show files in commit:
ostree ls --repo {{path/to/repo}} {{commit_id}}
echo "Show metadata of commit:
ostree show --repo {{path/to/repo}} {{commit_id}}
echo "Show list of commits:
ostree log --repo {{path/to/repo}} {{branch_name}}
echo "Show repo summary:
ostree summary --repo {{path/to/repo}} --view
echo "Show available refs (branches):
ostree refs --repo {{path/to/repo}}
echo "pacaur
echo "A utility for Arch Linux to build and install packages from the Arch User Repository.
echo "More information: <https://github.com/rmarquis/pacaur>.
echo "Synchronize and update all packages (includes AUR):
pacaur -Syu
echo "Synchronize and update only AUR packages:
pacaur -Syua
echo "Install a new package (includes AUR):
pacaur -S {{package}}
echo "Remove a package and its dependencies (includes AUR packages):
pacaur -Rs {{package}}
echo "Search the package database for a keyword (includes AUR):
pacaur -Ss {{keyword}}
echo "List all currently installed packages (includes AUR packages):
pacaur -Qs
echo "paccache
echo "A pacman cache cleaning utility.
echo "More information: <https://manned.org/paccache>.
echo "Remove all but the 3 most recent package versions from the pacman cache:
paccache -r
echo "Set the number of package versions to keep:
paccache -rk {{num_versions}}
echo "Perform a dry-run and show the number of candidate packages for deletion:
paccache -d
echo "Move candidate packages to a directory instead of deleting them:
paccache -m {{path/to/directory}}
echo "pacdiff
echo "Maintenance utility for .pacorig`, `.pacnew` and `.pacsave` files created by `pacman.
echo "More information: <https://man.archlinux.org/man/pacdiff>.
echo "Review files that need maintenance in interactive mode:
pacdiff
echo "Use sudo and sudoedit to remove and merge files:
pacdiff --sudo
echo "Review files needing maintenance, creating .bak`ups of the original if you `(O)verwrite:
pacdiff --sudo --backup
echo "Use a specific editor to view and merge configuration files (default is vim -d):
DIFFPROG={{editor}} pacdiff
echo "Scan for configuration files with locate instead of using pacman database:
pacdiff --locate
echo "Display help:
pacdiff --help
echo "pacman --database
echo "Operate on the Arch Linux package database.
echo "Modify certain attributes of the installed packages.
echo "See also: pacman.
echo "More information: <https://man.archlinux.org/man/pacman.8>.
echo "Mark a package as implicitly installed:
sudo pacman --database --asdeps {{package}}
echo "Mark a package as explicitly installed:
sudo pacman --database --asexplicit {{package}}
echo "Check that all the package dependencies are installed:
pacman --database --check
echo "Check the repositories to ensure all specified dependencies are available:
pacman --database --check --check
echo "Display only error messages:
pacman --database --check --quiet
echo "Display help:
pacman --database --help
echo "pacman --deptest
echo "Check each dependency specified and return a list of dependencies that are not currently satisfied on the system.
echo "See also: pacman.
echo "More information: <https://man.archlinux.org/man/pacman.8>.
echo "Print the package names of the dependencies that aren't installed:
pacman --deptest {{package1 package2 ...}}
echo "Check if the installed package satisfies the given minimum version:
pacman --deptest "{{bash>=5}}"
echo "Check if a later version of a package is installed:
pacman --deptest "{{bash>5}}"
echo "Display help:
pacman --deptest --help
echo "pacman --files
echo "Arch Linux package manager utility.
echo "See also: pacman`, `pkgfile.
echo "More information: <https://man.archlinux.org/man/pacman.8>.
echo "Update the package database:
sudo pacman --files --refresh
echo "Find the package that owns a specific file:
pacman --files {{filename}}
echo "Find the package that owns a specific file, using a regular expression:
pacman --files --regex '{{regular_expression}}'
echo "List only the package names:
pacman --files --quiet {{filename}}
echo "List the files owned by a specific package:
pacman --files --list {{package}}
echo "Display help:
pacman --files --help
echo "pacman-key
echo "Wrapper script for GnuPG used to manage pacman's keyring.
echo "See also: pacman.
echo "More information: <https://man.archlinux.org/man/pacman-key>.
echo "Initialize the pacman keyring:
sudo pacman-key --init
echo "Add the default Arch Linux keys:
sudo pacman-key --populate {{archlinux}}
echo "List keys from the public keyring:
pacman-key --list-keys
echo "Add the specified keys:
sudo pacman-key --add {{path/to/keyfile.gpg}}
echo "Receive a key from a key server:
sudo pacman-key --recv-keys "{{uid|name|email}}"
echo "Print the fingerprint of a specific key:
pacman-key --finger "{{uid|name|email}}"
echo "Sign an imported key locally:
sudo pacman-key --lsign-key "{{uid|name|email}}"
echo "Remove a specific key:
sudo pacman-key --delete "{{uid|name|email}}"
echo "pacman-mirrors
echo "Generate a pacman mirrorlist for Manjaro Linux.
echo "Every run of pacman-mirrors requires you to synchronize your database and update your system using sudo pacman -Syyu.
echo "See also: pacman.
echo "More information: <https://wiki.manjaro.org/index.php?title=Pacman-mirrors>.
echo "Generate a mirrorlist using the default settings:
sudo pacman-mirrors --fasttrack
echo "Get the status of the current mirrors:
pacman-mirrors --status
echo "Display the current branch:
pacman-mirrors --get-branch
echo "Switch to a different branch:
sudo pacman-mirrors --api --set-branch {{stable|unstable|testing}}
echo "Generate a mirrorlist, only using mirrors in your country:
sudo pacman-mirrors --geoip
echo "pacman --query
echo "Arch Linux package manager utility.
echo "See also: pacman.
echo "More information: <https://man.archlinux.org/man/pacman.8>.
echo "List installed packages and versions:
pacman --query
echo "List only packages and versions that were explicitly installed:
pacman --query --explicit
echo "Find which package owns a file:
pacman --query --owns {{filename}}
echo "Display information about an installed package:
pacman --query --info {{package}}
echo "List files owned by a package:
pacman --query --list {{package}}
echo "List orphan packages (installed as dependencies but not required by any package):
pacman --query --unrequired --deps --quiet
echo "List installed packages not found in the repositories:
pacman --query --foreign
echo "List outdated packages:
pacman --query --upgrades
echo "pacman --remove
echo "Arch Linux package manager utility.
echo "See also: pacman.
echo "More information: <https://man.archlinux.org/man/pacman.8>.
echo "Remove a package and its dependencies:
sudo pacman --remove --recursive {{package}}
echo "Remove a package and both its dependencies and configuration files:
sudo pacman --remove --recursive --nosave {{package}}
echo "Remove a package without prompting:
sudo pacman --remove --noconfirm {{package}}
echo "Remove orphan packages (installed as dependencies but not required by any package):
sudo pacman --remove --recursive --nosave $(pacman --query --unrequired --deps --quiet)
echo "Remove a package and all packages that depend on it:
sudo pacman --remove --cascade {{package}}
echo "List packages that would be affected (does not remove any packages):
pacman --remove --print {{package}}
echo "Display help for this subcommand:
pacman --remove --help
echo "pacman --sync
echo "Arch Linux package manager utility.
echo "See also: pacman.
echo "More information: <https://man.archlinux.org/man/pacman.8>.
echo "Install a new package:
sudo pacman --sync {{package}}
echo "Synchronize and update all packages (add --downloadonly to download the packages and not update them):
sudo pacman --sync --refresh --sysupgrade
echo "Update all packages and install a new one without prompting:
sudo pacman --sync --refresh --sysupgrade --noconfirm {{package}}
echo "Search the package database for a regular expression or keyword:
pacman --sync --search "{{search_pattern}}"
echo "Display information about a package:
pacman --sync --info {{package}}
echo "Overwrite conflicting files during a package update:
sudo pacman --sync --refresh --sysupgrade --overwrite {{path/to/file}}
echo "Synchronize and update all packages, but ignore a specific package (can be used more than once):
sudo pacman --sync --refresh --sysupgrade --ignore {{package}}
echo "Remove not installed packages and unused repositories from the cache (use two --clean flags to clean all packages):
sudo pacman --sync --clean
echo "pacman --upgrade
echo "Arch Linux package manager utility.
echo "See also: pacman.
echo "More information: <https://man.archlinux.org/man/pacman.8>.
echo "Install one or more packages from files:
sudo pacman --upgrade {{path/to/package1.pkg.tar.zst}} {{path/to/package2.pkg.tar.zst}}
echo "Install a package without prompting:
sudo pacman --upgrade --noconfirm {{path/to/package.pkg.tar.zst}}
echo "Overwrite conflicting files during a package installation:
sudo pacman --upgrade --overwrite {{path/to/file}} {{path/to/package.pkg.tar.zst}}
echo "Install a package, skipping the dependency version checks:
sudo pacman --upgrade --nodeps {{path/to/package.pkg.tar.zst}}
echo "List packages that would be affected (does not install any packages):
pacman --upgrade --print {{path/to/package.pkg.tar.zst}}
echo "Display help:
pacman --upgrade --help
echo "pacman
echo "Arch Linux package manager utility.
echo "See also: pacman-database`, `pacman-deptest`, `pacman-files`, `pacman-key`, `pacman-mirrors`, `pacman-query`, `pacman-remove`, `pacman-sync`, `pacman-upgrade.
echo "For equivalent commands in other package managers, see <https://wiki.archlinux.org/title/Pacman/Rosetta>.
echo "More information: <https://man.archlinux.org/man/pacman.8>.
echo "Synchronize and update all packages:
sudo pacman -Syu
echo "Install a new package:
sudo pacman -S {{package}}
echo "Remove a package and its dependencies:
sudo pacman -Rs {{package}}
echo "Search the database for packages containing a specific file:
pacman -F "{{file_name}}"
echo "List installed packages and versions:
pacman -Q
echo "List only the explicitly installed packages and versions:
pacman -Qe
echo "List orphan packages (installed as dependencies but not actually required by any package):
pacman -Qtdq
echo "Empty the entire pacman cache:
sudo pacman -Scc
echo "pacman4console
echo "A text-based console game inspired by the original Pacman.
echo "More information: <https://github.com/YoctoForBeaglebone/pacman4console>.
echo "Start a game at Level 1:
pacman4console
echo "Start a game on a certain level (there are nine official levels):
pacman4console --level={{level_number}}
echo "Start the pacman4console level editor, saving to a specified text file:
pacman4consoleedit {{path/to/level_file}}
echo "Play a custom level:
pacman4console --level={{path/to/level_file}}
echo "pacstall
echo "An AUR package manager for Ubuntu.
echo "More information: <https://github.com/pacstall/pacstall>.
echo "Search the package database for a package name:
pacstall --search {{query}}
echo "Install a package:
pacstall --install {{package}}
echo "Remove a package:
pacstall --remove {{package}}
echo "Add a repository to the database (only GitHub and GitLab are supported):
pacstall --add-repo {{remote_repository_location}}
echo "Update pacstall's scripts:
pacstall --update
echo "Update all packages:
pacstall --upgrade
echo "Display information about a package:
pacstall --query-info {{package}}
echo "List all installed packages:
pacstall --list
echo "pacstrap
echo "Arch Linux install script to install packages to the specified new root directory.
echo "More information: <https://man.archlinux.org/man/pacstrap.8>.
echo "Install the base package, Linux kernel and firmware for common hardware:
pacstrap {{path/to/new/root}} {{base}} {{linux}} {{linux-firmware}}
echo "Install the base` package, Linux LTS kernel and `base-devel build tools:
pacstrap {{path/to/new/root}} {{base}} {{base-devel}} {{linux-lts}}
echo "Install packages without copy the host's mirrorlist to the target:
pacstrap -M {{path/to/new/root}} {{packages}}
echo "Use an alternate configuration file for Pacman:
pacstrap -C {{path/to/pacman.conf}} {{path/to/new/root}} {{packages}}
echo "Install packages using the package cache on the host instead of on the target:
pacstrap -c {{path/to/new/root}} {{packages}}
echo "Install packages without copy the host's pacman keyring to the target:
pacstrap -G {{path/to/new/root}} {{packages}}
echo "Install packages in interactive mode (prompts for confirmation):
pacstrap -i {{path/to/new/root}} {{packages}}
echo "Install packages using package files:
pacstrap -U {{path/to/new/root}} {{path/to/package1}} {{path/to/package2}}
echo "pactree
echo "Package dependency tree viewer for pacman.
echo "More information: <https://man.archlinux.org/man/pactree.8>.
echo "Print the dependency tree of a specific package:
pactree {{package}}
echo "Print what packages depend on a specific package:
pactree --reverse {{package}}
echo "Dump dependencies one per line, skipping duplicates:
pactree --unique {{package}}
echo "Include optional dependencies of a specific package and colorize the output:
pactree --optional --color {{package}}
echo "Display help:
pactree
echo "pamac
echo "A command-line utility for the GUI package manager pamac.
echo "If you can't see the AUR packages, enable it in /etc/pamac.conf or in the GUI.
echo "More information: <https://wiki.manjaro.org/index.php/Pamac>.
echo "Install a new package:
pamac install {{package_name}}
echo "Remove a package and its no longer required dependencies (orphans):
pamac remove --orphans {{package_name}}
echo "Search the package database for a package:
pamac search {{package_name}}
echo "List installed packages:
pamac list --installed
echo "Check for package updates:
pamac checkupdates
echo "Upgrade all packages:
pamac upgrade
echo "parted
echo "A partition manipulation program.
echo "See also: partprobe.
echo "More information: <https://www.gnu.org/software/parted/parted.html>.
echo "List partitions on all block devices:
sudo parted --list
echo "Start interactive mode with the specified disk selected:
sudo parted {{/dev/sdX}}
echo "Create a new partition table of the specified label-type:
sudo parted --script {{/dev/sdX}} mklabel {{aix|amiga|bsd|dvh|gpt|loop|mac|msdos|pc98|sun}}
echo "Show partition information in interactive mode:
print
echo "Select a disk in interactive mode:
select {{/dev/sdX}}
echo "Create a 16 GB partition with the specified filesystem in interactive mode:
mkpart {{primary|logical|extended}} {{btrfs|ext2|ext3|ext4|fat16|fat32|hfs|hfs+|linux-swap|ntfs|reiserfs|udf|xfs}} {{0%}} {{16G}}
echo "Resize a partition in interactive mode:
resizepart {{/dev/sdXN}} {{end_position_of_partition}}
echo "Remove a partition in interactive mode:
# rm {{/dev/sdXN}}
echo "partprobe
echo "Notify the operating system kernel of partition table changes.
echo "More information: <https://manned.org/partprobe>.
echo "Notify the operating system kernel of partition table changes:
sudo partprobe
echo "Notify the kernel of partition table changes and show a summary of devices and their partitions:
sudo partprobe --summary
echo "Show a summary of devices and their partitions but don't notify the kernel:
sudo partprobe --summary --dry-run
echo "partx
echo "Parse a partition table and tell the kernel about it.
echo "More information: <https://man7.org/linux/man-pages/man8/partx.8.html>.
echo "List the partitions on a block device or disk image:
sudo partx --list {{path/to/device_or_disk_image}}
echo "Add all the partitions found in a given block device to the kernel:
sudo partx --add --verbose {{path/to/device_or_disk_image}}
echo "Delete all the partitions found from the kernel (does not alter partitions on disk):
sudo partx --delete {{path/to/device_or_disk_image}}
echo "paru
echo "An AUR helper and pacman wrapper.
echo "More information: <https://github.com/Morganamilo/paru>.
echo "Interactively search for and install a package:
paru {{package_name_or_search_term}}
echo "Synchronize and update all packages:
paru
echo "Upgrade AUR packages:
paru -Sua
echo "Get information about a package:
paru -Si {{package}}
echo "Download PKGBUILD and other package source files from the AUR or ABS:
paru --getpkgbuild {{package}}
echo "Display the PKGBUILD file of a package:
paru --getpkgbuild --print {{package}}
echo "pasuspender
echo "Temporarily suspends pulseaudio while another command is running to allow access to alsa.
echo "More information: <https://manned.org/pasuspender>.
echo "Suspend PulseAudio while running jackd:
pasuspender -- {{jackd -d alsa --device hw:0}}
echo "pdbedit
echo "Edit the Samba user database.
echo "For simple user add/remove/password, you can also use smbpasswd.
echo "More information: <https://manned.org/pdbedit>.
echo "List all Samba users (use verbose flag to show their settings):
sudo pdbedit --list --verbose
echo "Add an existing Unix user to Samba (will prompt for password):
sudo pdbedit --user {{username}} --create
echo "Remove a Samba user:
sudo pdbedit --user {{username}} --delete
echo "Reset a Samba user's failed password counter:
sudo pdbedit --user {{username}} --bad-password-count-reset
echo "pdfcrop
echo "Detect and remove margins in each page in a PDF file.
echo "More information: <https://github.com/ho-tex/pdfcrop>.
echo "Automatically detect and remove the margin for each page in a PDF file:
pdfcrop {{path/to/input_file.pdf}} {{path/to/output_file.pdf}}
echo "Set the margins of each page to a specific value:
# pdfcrop {{path/to/input_file.pdf}} --margins '{{left}} {{top}} {{right}} {{bottom}}' {{path/to/output_file.pdf}}
# echo "Set the margins of each page to a specific value, using the same value for left, top, right and bottom:
pdfcrop {{path/to/input_file.pdf}} --margins {{300}} {{path/to/output_file.pdf}}
echo "Use a user-defined bounding box for cropping instead of automatically detecting it:
# pdfcrop {{path/to/input_file.pdf}} --bbox '{{left}} {{top}} {{right}} {{bottom}}' {{path/to/output_file.pdf}}
echo "Use different user-defined bounding boxes for odd and even pages:
# pdfcrop {{path/to/input_file.pdf}} --bbox-odd '{{left}} {{top}} {{right}} {{bottom}}' --bbox-even '{{left}} {{top}} {{right}} {{bottom}}' {{path/to/output_file.pdf}}
echo "Automatically detect margins using a lower resolution for improved performance:
pdfcrop {{path/to/input_file.pdf}} --resolution {{72}} {{path/to/output_file.pdf}}
echo "pdftohtml
echo "Convert PDF files into HTML, XML and PNG images.
echo "More information: <https://manned.org/pdftohtml>.
echo "Convert a PDF file to an HTML file:
pdftohtml {{path/to/file.pdf}} {{path/to/output_file.html}}
echo "Ignore images in the PDF file:
pdftohtml -i {{path/to/file.pdf}} {{path/to/output_file.html}}
echo "Generate a single HTML file that includes all PDF pages:
pdftohtml -s {{path/to/file.pdf}} {{path/to/output_file.html}}
echo "Convert a PDF file to an XML file:
pdftohtml -xml {{path/to/file.pdf}} {{path/to/output_file.xml}}
# echo "pdftoppm
echo "Convert PDF document pages to portable Pixmap (image formats).
# echo "More information: <https://manned.org/pdftoppm>.
echo "Specify the range of pages to convert (N-first page, M-last page):
# pdftoppm -f {{N}} -l {{M}} {{path/to/file.pdf}} {{image_name_prefix}}
echo "Convert only the first page of a PDF:
# pdftoppm -singlefile {{path/to/file.pdf}} {{image_name_prefix}}
echo "Generate a monochrome PBM file (instead of a color PPM file):
# pdftoppm -mono {{path/to/file.pdf}} {{image_name_prefix}}
echo "Generate a grayscale PGM file (instead of a color PPM file):
# pdftoppm -gray {{path/to/file.pdf}} {{image_name_prefix}}
echo "Generate a PNG file instead a PPM file:
# pdftoppm -png {{path/to/file.pdf}} {{image_name_prefix}}
echo "pdfxup
echo "N-up PDF pages.
echo "N-upping means putting multiple pages onto one page by scaling and rotating them into a grid.
echo "More information: <https://ctan.org/pkg/pdfxup>.
echo "Create a 2-up PDF:
pdfxup -o {{path/to/output.pdf}} {{path/to/input.pdf}}
echo "Create a PDF with 3 columns and 2 lines per page:
pdfxup -x {{3}} -y {{2}} -o {{path/to/output.pdf}} {{path/to/input.pdf}}
echo "Create a PDF in booklet mode (2-up, and pages are sorted to form a book when folded):
pdfxup -b -o {{path/to/output.pdf}} {{path/to/input.pdf}}
echo "perf
echo "Framework for Linux performance counter measurements.
echo "More information: <https://perf.wiki.kernel.org>.
echo "Display basic performance counter stats for a command:
perf stat {{gcc hello.c}}
echo "Display system-wide real-time performance counter profile:
# sudo perf top
echo "Run a command and record its profile into perf.data:
sudo perf record {{command}}
echo "Record the profile of an existing process into perf.data:
sudo perf record -p {{pid}}
echo "Read perf.data` (created by `perf record) and display the profile:
sudo perf report
echo "rename
echo "Rename multiple files.
echo "NOTE: this page refers to the command from the perl-rename Arch Linux package.
echo "More information: <https://manned.org/rename>.
echo "Rename files using a Perl Common Regular Expression (substitute 'foo' with 'bar' wherever found):
rename {{'s/foo/bar/'}} {{*}}
echo "Dry-run - display which renames would occur without performing them:
rename -n {{'s/foo/bar/'}} {{*}}
echo "Force renaming even if the operation would remove existing destination files:
rename -f {{'s/foo/bar/'}} {{*}}
echo "Convert filenames to lower case (use -f in case-insensitive filesystems to prevent "already exists" errors):
rename 'y/A-Z/a-z/' {{*}}
echo "Replace whitespace with underscores:
rename 's/\s+/_/g' {{*}}
echo "phar
echo "Create, update or extract PHP archives (PHAR).
echo "More information: <https://manned.org/phar>.
echo "Add space-separated files or directories to a Phar file:
phar add -f {{path/to/phar_file}} {{files_or_directories}}
echo "Display the contents of a Phar file:
phar list -f {{path/to/phar_file}}
echo "Delete the specified file or directory from a Phar file:
phar delete -f {{path/to/phar_file}} -e {{file_or_directory}}
echo "Display full usage information and available hashing/compression algorithms:
phar help
echo "Compress or uncompress files and directories in a Phar file:
phar compress -f {{path/to/phar_file}} -c {{algorithm}}
echo "Get information about a Phar file:
phar info -f {{path/to/phar_file}}
echo "Sign a Phar file with a specific hash algorithm:
phar sign -f {{path/to/phar_file}} -h {{algorithm}}
echo "Sign a Phar file with an OpenSSL private key:
phar sign -f {{path/to/phar_file}} -h openssl -y {{path/to/private_key}}
echo "photorec
echo "Deleted file recovery tool.
echo "It is recommended to write recovered files to a disk separate to the one being recovered from.
echo "More information: <https://www.cgsecurity.org/wiki/PhotoRec>.
echo "Run PhotoRec on a specific device:
sudo photorec {{/dev/sdb}}
echo "Run PhotoRec on a disk image (image.dd):
sudo photorec {{path/to/image.dd}}
echo "phpdismod
echo "Disable PHP extensions on Debian-based OSes.
echo "More information: <https://salsa.debian.org/php-team/php-defaults>.
echo "Disable the JSON extension for every SAPI of every PHP version:
sudo phpdismod {{json}}
echo "Disable the JSON extension for PHP 7.3 with the cli SAPI:
sudo phpdismod -v {{7.3}} -s {{cli}} {{json}}
echo "phpenmod
echo "Enable PHP extensions on Debian-based OSes.
echo "More information: <https://salsa.debian.org/php-team/php-defaults>.
echo "Enable the JSON extension for every SAPI of every PHP version:
sudo phpenmod {{json}}
echo "Enable the JSON extension for PHP 7.3 with the cli SAPI:
sudo phpenmod -v {{7.3}} -s {{cli}} {{json}}
echo "phpquery
echo "PHP extension manager for Debian-based OSes.
echo "More information: <https://helpmanual.io/help/phpquery/>.
echo "List available PHP versions:
sudo phpquery -V
echo "List available SAPIs for PHP 7.3:
sudo phpquery -v {{7.3}} -S
echo "List enabled extensions for PHP 7.3 with the cli SAPI:
sudo phpquery -v {{7.3}} -s {{cli}} -M
echo "Check if the JSON extension is enabled for PHP 7.3 with the apache2 SAPI:
sudo phpquery -v {{7.3}} -s {{apache2}} -m {{json}}
echo "physlock
echo "Lock all consoles and virtual terminals.
echo "More information: <http://github.com/muennich/physlock>.
echo "Lock every console (require current user or root to unlock):
physlock
echo "Mute kernel messages on console while locked:
physlock -m
echo "Disable SysRq mechanism while locked:
physlock -s
echo "Display a message before the password prompt:
physlock -p "{{Locked!}}"
echo "Fork and detach physlock (useful for suspend or hibernate scripts):
physlock -d
echo "pi
echo "Compute decimal Archimedes' constant Pi.
echo "More information: <https://manned.org/pi>.
echo "Display 100 decimal digits of Archimedes' constant Pi:
pi
echo "Display a specified number of decimal digits of Archimedes' constant Pi:
pi {{number}}
echo "Display help:
pi --help
echo "Display version:
pi --version
echo "Display recommended readings:
pi --bibliography
echo "picom
echo "Standalone compositor for Xorg.
echo "More information: <https://wiki.archlinux.org/title/picom>.
echo "Enable picom during a session:
picom &
echo "Start picom as a background process:
picom -b
echo "Use a custom configuration file:
picom --config {{path/to/config_file}}
echo "pidof
echo "Gets the ID of a process using its name.
echo "More information: <https://manned.org/pidof>.
echo "List all process IDs with given name:
pidof {{bash}}
echo "List a single process ID with given name:
pidof -s {{bash}}
echo "List process IDs including scripts with given name:
pidof -x {{script.py}}
echo "Kill all processes with given name:
kill $(pidof {{name}})
echo "pidstat
echo "Show system resource usage, including CPU, memory, IO etc.
echo "More information: <https://manned.org/pidstat>.
echo "Show CPU statistics at a 2 second interval for 10 times:
pidstat {{2}} {{10}}
echo "Show page faults and memory utilization:
pidstat -r
echo "Show input/output usage per process id:
pidstat -d
echo "Show information on a specific PID:
pidstat -p {{PID}}
echo "Show memory statistics for all processes whose command name include "fox" or "bird":
pidstat -C "{{fox|bird}}" -r -p ALL
echo "pihole
echo "Terminal interface for the Pi-hole ad-blocking DNS server.
echo "More information: <https://docs.pi-hole.net/core/pihole-command/>.
echo "Check the Pi-hole daemon's status:
pihole status
echo "Update Pi-hole and Gravity:
pihole -up
echo "Monitor detailed system status:
pihole chronometer
# echo "Start or stop the daemon:
pihole {{enable|disable}}
echo "Restart the daemon (not the server itself):
pihole restartdns
echo "Whitelist or blacklist a domain:
pihole {{whitelist|blacklist}} {{example.com}}
echo "Search the lists for a domain:
pihole query {{example.com}}
echo "Open a real-time log of connections:
pihole tail
echo "pinout
echo "View the current Raspberry Pi's GPIO pin-out information on the terminal with an ASCII diagram.
echo "More information: <https://www.raspberrypi.org/documentation/computers/os.html#gpio-pinout>.
echo "View the pinout information and GPIO header diagram for the current Raspberry Pi:
pinout
echo "Open <https://pinout.xyz/> in the default browser:
pinout -x
echo "pipewire
echo "Start the PipeWire daemon.
echo "More information: <https://docs.pipewire.org/page_man_pipewire_1.html>.
echo "Start the PipeWire daemon:
pipewire
echo "Use a different configuration file:
pipewire --config {{path/to/file.conf}}
echo "Set the verbosity level (error, warn, info, debug or trace):
pipewire -{{v|vv|...|vvvvv}}
echo "Display help:
pipewire --help
echo "pivpn
echo "Easy security-hardened OpenVPN setup and manager.
echo "Originally designed for the Raspberry Pi, but works on other Linux devices too.
echo "More information: <http://www.pivpn.io/>.
echo "Add a new client device:
sudo pivpn add
echo "List all client devices:
sudo pivpn list
echo "List currently connected devices and their statistics:
sudo pivpn clients
echo "Revoke a previously authenticated device:
sudo pivpn revoke
echo "Uninstall PiVPN:
sudo pivpn uninstall
echo "pkcon
echo "Command line client for PackageKit console program used by Discover and Gnome software and alternative to 'apt'.
echo "More information: <https://manned.org/pkcon>.
echo "Install a package:
pkcon install {{package}}
echo "Remove a package:
pkcon remove {{package}}
echo "Refresh the package cache:
pkcon refresh
echo "Update packages:
pkcon update
echo "Search for a specific package:
pkcon search {{package}}
echo "List all available packages:
pkcon get-packages
echo "pkgadd
echo "Add a package to a CRUX system.
echo "More information: <https://docs.oracle.com/cd/E88353_01/html/E72487/pkgadd-8.html>.
echo "Install a local software package:
pkgadd {{package}}
echo "Update an already installed package from a local package:
pkgadd -u {{package}}
echo "pkgctl auth
echo "Authenticate pkgctl with services like GitLab.
echo "More information: <https://man.archlinux.org/man/pkgctl-auth.1>.
echo "Authenticate pkgctl with the GitLab instance:
pkgctl auth login
echo "View authentication status:
pkgctl auth status
echo "pkgctl build
echo "Build packages inside a clean chroot.
echo "More information: <https://man.archlinux.org/man/pkgctl-build.1>.
echo "Automatically choose the right build script to build packages in a clean chroot:
pkgctl build
echo "Manually build packages in a clean chroot:
pkgctl build --arch {{architecture}} --repo {{repository}} --clean
echo "pkgctl db update
echo "Update the pacman database as final release step for packages that have been transfered and staged on <https://repos.archlinux.org>.
echo "More information: <https://man.archlinux.org/man/pkgctl-db-update.1>.
echo "Update the binary repository as final release step:
pkgctl db update
echo "pkgctl diff
echo "Compare package files using different modes.
echo "See also: pkgctl.
echo "More information: <https://man.archlinux.org/man/pkgctl-diff.1>.
echo "Compare package files in tar content [l]ist different mode (default):
pkgctl diff --list {{path/to/file|pkgname}}
echo "Compare package files in [d]iffoscope different mode:
pkgctl diff --diffoscope {{path/to/file|pkgname}}
echo "Compare package files in .PKGINFO different mode:
pkgctl diff --pkginfo {{path/to/file|pkgname}}
echo "Compare package files in .BUILDINFO different mode:
pkgctl diff --buildinfo {{path/to/file|pkgname}}
echo "pkgctl release
echo "Release step to commit, tag and upload build artifacts.
echo "More information: <https://man.archlinux.org/man/pkgctl-release.1>.
echo "Release a build artifact:
pkgctl release --repo {{repository}} --message {{commit_message}}
echo "pkgctl repo
echo "Manage Git packaging repositories and their configuration for Arch Linux.
echo "See also: pkgctl.
echo "More information: <https://man.archlinux.org/man/pkgctl-repo.1>.
echo "Clone a package repository (requires setting an SSH key in your Arch Linux GitLab account):
pkgctl repo clone {{pkgname}}
echo "Clone a package repository over HTTPS:
pkgctl repo clone --protocol=https {{pkgname}}
echo "Create a new GitLab package repository and clone it after creation (requires valid GitLab API authentication):
pkgctl repo create {{pkgbase}}
echo "Switch a package repository to a specified version:
pkgctl repo switch {{version}} {{pkgbase}}
echo "Open a package repository's website:
pkgctl repo web {{pkgbase}}
echo "pkgctl
echo "Unified command-line frontend for Arch Linux devtools.
echo "More information: <https://man.archlinux.org/man/pkgctl.1>.
echo "View documentation for authenticating pkgctl with services like GitLab:
tldr pkgctl auth
echo "View documentation for building packages inside a clean chroot:
tldr pkgctl build
echo "View documentation for updating the binary repository as final release step:
tldr pkgctl db update
echo "View documentation for comparing package files using different modes:
tldr pkgctl diff
echo "View documentation for releasing build artifacts:
tldr pkgctl release
echo "View documentation for managing Git packaging repositories and their configuration:
tldr pkgctl repo
echo "Display version:
pkgctl version
echo "pkgfile
echo "Tool for searching files from packages in the official repositories on arch-based systems.
echo "See also pacman files`, describing the usage of `pacman --files.
echo "More information: <https://man.archlinux.org/man/extra/pkgfile/pkgfile.1>.
echo "Synchronize the pkgfile database:
sudo pkgfile --update
echo "Search for a package that owns a specific file:
pkgfile {{filename}}
echo "List all files provided by a package:
pkgfile --list {{package}}
echo "List executables provided by a package:
pkgfile --list --binaries {{package}}
echo "Search for a package that owns a specific file using case-insensitive matching:
pkgfile --ignorecase {{filename}}
echo "Search for a package that owns a specific file in the bin` or `sbin directory:
pkgfile --binaries {{filename}}
echo "Search for a package that owns a specific file, displaying the package version:
pkgfile --verbose {{filename}}
echo "Search for a package that owns a specific file in a specific repository:
pkgfile --repo {{repository_name}} {{filename}}
echo "pkginfo
echo "Query the package database on a CRUX system.
echo "More information: <https://crux.nu/Main/Handbook3-6#ntoc19>.
echo "List installed packages and their versions:
pkginfo -i
echo "List files owned by a package:
pkginfo -l {{package}}
echo "List the owner(s) of files matching a pattern:
pkginfo -o {{pattern}}
echo "Print the footprint of a file:
pkginfo -f {{path/to/file}}
echo "pkgmk
echo "Make a binary package for use with pkgadd on CRUX.
echo "More information: <https://docs.oracle.com/cd/E88353_01/html/E37839/pkgmk-1.html>.
echo "Make and download a package:
pkgmk -d
echo "Install the package after making it:
pkgmk -d -i
echo "Upgrade the package after making it:
pkgmk -d -u
echo "Ignore the footprint when making a package:
pkgmk -d -if
echo "Ignore the MD5 sum when making a package:
pkgmk -d -im
echo "Update the package's footprint:
pkgmk -uf
echo "pkgrm
echo "Remove a package from a CRUX system.
echo "More information: <https://docs.oracle.com/cd/E88353_01/html/E72487/pkgrm-8.html>.
echo "Remove an installed package:
pkgrm {{package}}
echo "playerctl
echo "Control media players via MPRIS.
# echo "More information: <https://github.com/altdesktop/playerctl>.
echo "Toggle play:
playerctl play-pause
echo "Skip to the next track:
playerctl next
echo "Go back to the previous track:
playerctl previous
echo "List all players:
playerctl --list-all
echo "Send a command to a specific player:
playerctl --player {{player_name}} {{play-pause|next|previous|...}}
echo "Send a command to all players:
playerctl --all-players {{play-pause|next|previous|...}}
echo "Display metadata about the current track:
playerctl metadata --format "{{Now playing: \{\{artist\}\} - \{\{album\}\} - \{\{title\}\}}}"
echo "pluma
# echo "Edit files in MATE desktop environment.
echo "More information: <https://manned.org/pluma>.
echo "Start the editor:
pluma
echo "Open specific documents:
pluma {{path/to/file1 path/to/file2 ...}}
echo "Open documents using a specific encoding:
pluma --encoding {{WINDOWS-1252}} {{path/to/file1 path/to/file2 ...}}
echo "Print all supported encodings:
pluma --list-encodings
echo "Open document and go to a specific line:
pluma +{{10}} {{path/to/file}}
echo "pmap
echo "Report memory map of a process or processes.
echo "More information: <https://manned.org/pmap>.
echo "Print memory map for a specific process id (PID):
pmap {{pid}}
echo "Show the extended format:
pmap --extended {{pid}}
echo "Show the device format:
pmap --device {{pid}}
echo "Limit results to a memory address range specified by low` and `high:
pmap --range {{low}},{{high}}
echo "Print memory maps for multiple processes:
pmap {{pid1 pid2 ...}}
echo "pmount
echo "Mount arbitrary hotpluggable devices as a normal user.
echo "More information: <https://manned.org/pmount>.
echo "Mount a device below /media/ (using device as mount point):
pmount {{/dev/to/block/device}}
echo "Mount a device with a specific filesystem type to /media/label:
pmount --type {{filesystem}} {{/dev/to/block/device}} {{label}}
echo "Mount a CD-ROM (filesystem type ISO9660) in read-only mode:
pmount --type {{iso9660}} --read-only {{/dev/cdrom}}
echo "Mount an NTFS-formatted disk, forcing read-write access:
pmount --type {{ntfs}} --read-write {{/dev/sdX}}
echo "Display all mounted removable devices:
pmount
echo "pngcheck
echo "Forensics tool for validating the integrity of PNG based (.png`, `.jng`, `.mng) image files.
echo "Can also extract embedded images and text from a file.
echo "More information: <http://www.libpng.org/pub/png/apps/pngcheck.html>.
echo "Verify the integrity of an image file:
pngcheck {{path/to/file.png}}
echo "Check the file with [v]erbose and [c]olorized output:
pngcheck -vc {{path/to/file.png}}
echo "Display contents of [t]ext chunks and [s]earch for PNGs within a specific file:
pngcheck -ts {{path/to/file.png}}
echo "Search for, and e[x]tract embedded PNGs within a specific file:
pngcheck -x {{path/to/file.png}}
echo "po4a-gettextize
echo "Convert a file to a PO file.
echo "More information: <https://po4a.org/man/man1/po4a-gettextize.1.php>.
echo "Convert a text file to PO file:
po4a-gettextize --format {{text}} --master {{path/to/master.txt}} --po {{path/to/result.po}}
echo "Get a list of available formats:
po4a-gettextize --help-format
echo "Convert a text file along with a translated document to a PO file (-l option can be provided multiple times):
po4a-gettextize --format {{text}} --master {{path/to/master.txt}} --localized {{path/to/translated.txt}} --po {{path/to/result.po}}
echo "po4a-translate
echo "Convert a PO file back to documentation format.
echo "The provided PO file should be the translation of the POT file which was produced by po4a-gettextize.
echo "More information: <https://po4a.org/man/man1/po4a-translate.1.php>.
echo "Convert a translated PO file back to a document:
po4a-translate --format {{text}} --master {{path/to/master.doc}} --po {{path/to/result.po}} --localized {{path/to/translated.txt}}
echo "Get a list of available formats:
po4a-translate --help-format
echo "po4a-updatepo
echo "Update the translation (in PO format) of a documentation.
echo "More information: <https://po4a.org/man/man1/po4a-updatepo.1.php>.
echo "Update a PO file according to the modification of its origin file:
po4a-updatepo --format {{text}} --master {{path/to/master.txt}} --po {{path/to/result.po}}
echo "Get a list of available formats:
po4a-updatepo --help-format
echo "Update several PO files according to the modification of their origin file:
po4a-updatepo --format {{text}} --master {{path/to/master.txt}} --po {{path/to/po1.po}} --po {{path/to/po2.po}}
echo "po4a
echo "Update both PO files and translated documents.
echo "More information: <https://po4a.org/man/man1/po4a.1.php>.
echo "Update PO files and documents according to the specified config file:
po4a {{path/to/config_file}}
echo "portablectl
echo "A systemd utility for managing and deploying portable service images on Linux systems.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/portablectl.html>.
echo "List available portable service images discovered in the portable image search paths:
portablectl list
echo "Attach a portable service image to the host system:
portablectl attach {{path/to/image}}
echo "Detach a portable service image from the host system:
portablectl detach {{path/to/image|image_name}}
echo "Display details and metadata about a specified portable service image:
portablectl inspect {{path/to/image}}
echo "Check if a portable service image is attached to the host system:
portablectl is-attached {{path/to/image|image_name}}
echo "ports
echo "Update/list the ports tree on a CRUX system.
echo "More information: <https://manned.org/ports>.
echo "Update the ports tree:
ports -u
echo "List the ports in the current tree:
ports -l
echo "Check the differences between installed packages and the ports tree:
ports -d
echo "postconf
echo "Postfix configuration utility.
echo "This command displays the values of the main.cf` configuration parameters by default and warns about possible mistyped parameter names. It can also change the `main.cf configuration parameter values.
echo "More information: <https://manned.org/postconf>.
echo "Specify the directory of the main.cf configuration file instead of the default configuration directory:
postconf -c {{path/to/configuration_directory}}
echo "Edit the main.cf configuration file and update parameter settings with the "name=value" pairs:
postconf -e
echo "Print the default parameter settings of the main.cf instead of the actual settings:
postconf -d
echo "Display parameters only from the specified class. The class can be one of builtin, service, user or all:
postconf -C {{class}}
echo "List available SASL plug-in types for the Postfix SMTP server. The plug-in type is selected with the smtpd_sasl_type` configuration parameter by specifying `cyrus` or `dovecot as the name:
postconf -a
echo "List the names of all supported lookup table types. Lookup tables are specified as type:name` in configuration files where the type can be `btree`, `cdb`, `hash`, `mysql, etc:
postconf -m
echo "postfix
echo "Postfix mail transfer agent (MTA) control program.
echo "See also dovecot, a mail delivery agent (MDA) that integrates with Postfix.
echo "More information: <http://www.postfix.org>.
echo "Check the configuration:
sudo postfix check
echo "Check the status of the Postfix daemon:
sudo postfix status
echo "Start Postfix:
sudo postfix start
# echo "Gracefully stop Postfix:
# sudo postfix stop
echo "Flush the mail queue:
sudo postfix flush
echo "Reload the configuration files:
sudo postfix reload
echo "poweroff
echo "Power off the system.
echo "More information: <https://www.man7.org/linux/man-pages/man8/poweroff.8.html>.
echo "Power off the system:
poweroff
echo "Halt the system (same as halt):
poweroff --halt
echo "Reboot the system (same as reboot):
poweroff --reboot
echo "Shut down immediately without contacting the system manager:
poweroff --force --force
echo "Write the wtmp shutdown entry without shutting down the system:
poweroff --wtmp-only
echo "powerprofilesctl
echo "Make power profiles handling available over D-Bus.
# echo "More information: <https://gitlab.freedesktop.org/hadess/power-profiles-daemon/>.
echo "List available power profiles:
powerprofilesctl list
echo "Set a specific power profile:
powerprofilesctl set {{profile_name}}
echo "powerstat
echo "Measures the power consumption of a computer that has a battery power source or supports the RAPL interface.
echo "More information: <https://manned.org/powerstat>.
echo "Measure power with the default of 10 samples with an interval of 10 seconds:
powerstat
echo "Measure power with custom number of samples and interval duration:
powerstat {{interval}} {{number_of_samples}}
echo "Measure power using Intel's RAPL interface:
powerstat -R {{interval}} {{number_of_samples}}
echo "Show a histogram of the power measurements:
powerstat -H {{interval}} {{number_of_samples}}
echo "Enable all statistics gathering options:
powerstat -a {{interval}} {{number_of_samples}}
# echo "powertop
echo "Optimize battery power usage.
# echo "More information: <https://github.com/fenrus75/powertop>.
echo "Calibrate power usage measurements:
# sudo powertop --calibrate
echo "Generate HTML power usage report in the current directory:
# sudo powertop --html={{power_report.html}}
echo "Tune to optimal settings:
# sudo powertop --auto-tune
echo "Generate a report for a specified number of seconds (instead of 20 by default):
# sudo powertop --time={{5}}
echo "rename
echo "Rename multiple files.
echo "NOTE: this page refers to the command from the prename Fedora package.
echo "More information: <https://manned.org/man/prename>.
echo "Rename files using a Perl Common Regular Expression (substitute 'foo' with 'bar' wherever found):
rename {{'s/foo/bar/'}} {{*}}
echo "Dry-run - display which renames would occur without performing them:
rename -n {{'s/foo/bar/'}} {{*}}
echo "Force renaming even if the operation would remove existing destination files:
rename -f {{'s/foo/bar/'}} {{*}}
echo "Convert filenames to lower case (use -f in case-insensitive filesystems to prevent "already exists" errors):
rename 'y/A-Z/a-z/' {{*}}
echo "Replace whitespace with underscores:
rename 's/\s+/_/g' {{*}}
echo "pridecat
echo "Like cat but more colorful :).
echo "More information: <https://github.com/lunasorcery/pridecat>.
echo "Print the contents of a file in pride colors to stdout:
pridecat {{path/to/file}}
echo "Print contents of a file in trans colors:
pridecat {{path/to/file}} --{{transgender|trans}}
echo "Alternate between lesbian and bisexual pride flags:
pridecat {{path/to/file}} --lesbian --bi
echo "Print contents of a file with the background colors changed:
pridecat {{path/to/file}} -b
echo "List directory contents in pride flag colors:
ls | pridecat --{{flag}}
echo "print
echo "An alias to a run-mailcap's action print.
echo "Originally run-mailcap is used to process mime-type/file.
echo "More information: <https://manned.org/print>.
echo "Print action can be used to print any file on default run-mailcap tool:
print {{filename}}
echo "With run-mailcap:
run-mailcap --action=print {{filename}}
echo "prlimit
echo "Get or set process resource soft and hard limits.
echo "Given a process ID and one or more resources, prlimit tries to retrieve and/or modify the limits.
echo "More information: <https://manned.org/prlimit>.
echo "Display limit values for all current resources for the running parent process:
prlimit
echo "Display limit values for all current resources of a specified process:
prlimit --pid {{pid number}}
echo "Run a command with a custom number of open files limit:
prlimit --nofile={{10}} {{command}}
echo "pro
echo "Manage Ubuntu Pro services.
echo "More information: <https://manpages.ubuntu.com/manpages/latest/man1/ubuntu-advantage.1.html>.
echo "Connect your system to the Ubuntu Pro support contract:
sudo pro attach
echo "Display the status of Ubuntu Pro services:
pro status
echo "Check if the system is affected by a specific vulnerability (and apply a fix if possible):
pro fix {{CVE-number}}
echo "Display the number of unsupported packages:
pro security-status
echo "List packages that are no longer available for download:
pro security-status --unavailable
echo "List third-party packages:
pro security-status --thirdparty
echo "protontricks
echo "A simple wrapper that runs Winetricks commands for Proton enabled games.
echo "More information: <https://github.com/Matoking/protontricks>.
echo "Run the protontricks GUI:
protontricks --gui
echo "Run Winetricks for a specific game:
protontricks {{appid}} {{winetricks_args}}
echo "Run a command within a game's installation directory:
protontricks -c {{command}} {{appid}}
echo "[l]ist all installed games:
protontricks -l
echo "[s]earch for a game's App ID by name:
protontricks -s {{game_name}}
echo "Show the protontricks help message:
protontricks --help
echo "protonvpn-cli connect
echo "Connect to ProtonVPN.
echo "More information: <https://protonvpn.com/support/linux-vpn-setup/>.
echo "Connect to ProtonVPN interactively:
protonvpn-cli connect
echo "Connect to ProtonVPN using the fastest server available:
protonvpn-cli connect --fastest
echo "Connect to ProtonVPN using a specific server with a specific protocol:
protonvpn-cli connect {{server_name}} --protocol {{udp|tcp}}
echo "Connect to ProtonVPN using a random server with a specific protocol:
protonvpn-cli connect --random --protocol {{udp|tcp}}
echo "Connect to ProtonVPN using the fastest Tor-supporting server:
protonvpn-cli connect --tor
echo "Display help:
protonvpn-cli connect --help
echo "protonvpn-cli
echo "Official ProtonVPN client.
echo "See also: protonvpn-cli-connect.
echo "More information: <https://github.com/ProtonVPN/linux-cli>.
echo "Log in to the ProtonVPN account:
protonvpn-cli login {{username}}
echo "Start a kill switch upon connecting to ProtonVPN:
protonvpn-cli killswitch --on
echo "Connect to ProtonVPN interactively:
protonvpn-cli connect
echo "Display connection status:
protonvpn-cli status
echo "Block malware using ProtonVPN NetShield:
protonvpn-cli netshield --malware
echo "Disconnect from ProtonVPN:
protonvpn-cli disconnect
echo "Display the current ProtonVPN configuration:
protonvpn-cli config --list
echo "Display help for a subcommand:
protonvpn-cli {{subcommand}} --help
echo "prt-get
echo "The CRUX package manager.
echo "More information: <https://crux.nu/doc/prt-get%20-%20User%20Manual.html>.
echo "Install a package:
prt-get install {{package}}
echo "Install a package with dependency handling:
prt-get depinst {{package}}
echo "Update a package manually:
prt-get upgrade {{package}}
echo "Remove a package:
prt-get remove {{package}}
echo "Upgrade the system from the local ports tree:
prt-get sysup
echo "Search the ports tree:
prt-get search {{query}}
echo "Search for a file in a package:
prt-get fsearch {{file}}
echo "pstoedit
echo "Convert PDF files into various image formats.
echo "More information: <http://www.pstoedit.net>.
echo "Convert a PDF page to PNG or JPEG format:
pstoedit -page {{page_number}} -f magick {{path/to/file.pdf}} {{page.png|page.jpg]}}
echo "Convert multiple PDF pages to numbered images:
pstoedit -f magick {{path/to/file}} {{page%d.png|page%d.jpg}}
echo "pstree
echo "A convenient tool to show running processes as a tree.
echo "More information: <https://manned.org/pstree>.
echo "Display a tree of processes:
pstree
echo "Display a tree of processes with PIDs:
pstree -p
echo "Display all process trees rooted at processes owned by specified user:
pstree {{user}}
echo "ptx
echo "Generate a permuted index of words from one or more text files.
echo "More information: <https://www.gnu.org/software/coreutils/ptx>.
echo "Generate a permuted index where the first field of each line is an index reference:
ptx --references {{path/to/file}}
echo "Generate a permuted index with automatically generated index references:
ptx --auto-reference {{path/to/file}}
echo "Generate a permuted index with a fixed width:
ptx --width={{width_in_columns}} {{path/to/file}}
echo "Generate a permuted index with a list of filtered words:
ptx --only-file={{path/to/filter}} {{path/to/file}}
echo "Generate a permuted index with SYSV-style behaviors:
ptx --traditional {{path/to/file}}
echo "pulseaudio
echo "The PulseAudio sound system daemon and manager.
# echo "More information: <https://www.freedesktop.org/wiki/Software/PulseAudio/>.
echo "Check if PulseAudio is running (a non-zero exit code means it is not running):
pulseaudio --check
echo "Start the PulseAudio daemon in the background:
pulseaudio --start
echo "Kill the running PulseAudio daemon:
pulseaudio --kill
echo "List available modules:
pulseaudio --dump-modules
echo "Load a module into the currently running daemon with the specified arguments:
pulseaudio --load="{{module_name}} {{arguments}}"
echo "pvcreate
echo "Initialize a disk or partition for use as a physical volume.
echo "See also: lvm.
echo "More information: <https://man7.org/linux/man-pages/man8/pvcreate.8.html>.
echo "Initialize the /dev/sda1 volume for use by LVM:
pvcreate {{/dev/sda1}}
echo "Force the creation without any confirmation prompts:
pvcreate --force {{/dev/sda1}}
echo "pvdisplay
echo "Display information about Logical Volume Manager (LVM) physical volumes.
echo "See also: lvm.
echo "More information: <https://man7.org/linux/man-pages/man8/pvdisplay.8.html>.
echo "Display information about all physical volumes:
sudo pvdisplay
echo "Display information about the physical volume on drive /dev/sdXY:
sudo pvdisplay {{/dev/sdXY}}
echo "pvs
echo "Display information about physical volumes.
echo "See also: lvm.
echo "More information: <https://man7.org/linux/man-pages/man8/pvs.8.html>.
echo "Display information about physical volumes:
pvs
echo "Display non-physical volumes:
pvs -a
echo "Change default display to show more details:
pvs -v
echo "Display only specific fields:
pvs -o {{field_name_1}},{{field_name_2}}
echo "Append field to default display:
pvs -o +{{field_name}}
echo "Suppress heading line:
pvs --noheadings
echo "Use separator to separate fields:
pvs --separator {{special_character}}
echo "pw-cat
echo "Play and record audio files through pipewire.
echo "More information: <https://fedoraproject.org/wiki/QA:Testcase_PipeWire_PipeWire_CLI>.
echo "Play a WAV file over the default target:
pw-cat --playback {{path/to/file.wav}}
echo "Play a WAV file with a specified resampler quality (4 by default):
pw-cat --quality {{0..15}} --playback {{path/to/file.wav}}
echo "Record a sample recording at a volume level of 125%:
pw-cat --record --volume {{1.25}} {{path/to/file.wav}}
echo "Record a sample recording using a different sample rate:
pw-cat --record --rate {{6000}} {{path/to/file.wav}}
echo "pw-cli
echo "Manage a PipeWire instance's modules, objects, nodes, devices, links and much more.
echo "More information: <https://docs.pipewire.org/page_man_pw_cli_1.html>.
echo "Print all nodes (sinks and sources) along with their IDs:
pw-cli list-objects Node
echo "Print information about an object with a specific ID:
pw-cli info {{4}}
echo "Print all objects' information:
pw-cli info all
echo "pw-dot
echo "Create .dot files of the PipeWire graph.
echo "See also: dot, for rendering graph.
echo "More information: <https://docs.pipewire.org/page_man_pw-dot_1.html>.
echo "Generate a graph to pw.dot file:
pw-dot
echo "Specify an output file, showing all object types:
pw-dot --output {{path/to/file.dot}} --all
echo "Print .dot` graph to `stdout, showing all object properties:
pw-dot --output - --detail
echo "Generate a graph from a remote instance, showing only linked objects:
pw-dot --remote {{remote_name}} --smart
# echo "Lay the graph from left to right, instead of dot's default top to bottom:
pw-dot --lr
echo "Lay the graph using 90-degree angles in edges:
pw-dot --90
echo "pw-dump
echo "Dump PipeWire's current state as JSON, including the information on nodes, devices, modules, ports, and other objects.
echo "See also: pw-mon.
echo "More information: <https://docs.pipewire.org/page_man_pw-dump_1.html>.
echo "Print a JSON representation of the default PipeWire instance's current state:
pw-dump
echo "Dump the current state [m]onitoring changes, printing it again:
pw-dump --monitor
echo "Dump the current state of a [r]emote instance to a file:
pw-dump --remote {{remote_name}} > {{path/to/dump_file.json}}
echo "Set a [C]olor configuration:
pw-dump --color {{never|always|auto}}
echo "Display help:
pw-dump --help
echo "pw-link
echo "Manage links between ports in PipeWire.
# echo "More information: <https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Virtual-Devices>.
echo "List all audio output and input ports with their IDs:
pw-link --output --input --ids
echo "Create a link between an output and an input port:
pw-link {{output_port_name}} {{input_port_name}}
echo "Disconnect two ports:
pw-link --disconnect {{output_port_name}} {{input_port_name}}
echo "List all links with their IDs:
pw-link --links --ids
echo "Display help:
pw-link -h
echo "pw-loopback
echo "Tool for creating loopback devices in pipewire.
# echo "More information: <https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Virtual-Devices>.
echo "Create a loopback device with the default loopback behavior:
pw-loopback
echo "Create a loopback device that automatically connects to the speakers:
pw-loopback -m '{{[FL FR]}}' --capture-props='{{media.class=Audio/Sink}}'
echo "Create a loopback device that automatically connects to the microphone:
pw-loopback -m '{{[FL FR]}}' --playback-props='{{media.class=Audio/Source}}'
echo "Create a dummy loopback device that doesn't automatically connect to anything:
pw-loopback -m '{{[FL FR]}}' --capture-props='{{media.class=Audio/Sink}}' --playback-props='{{media.class=Audio/Source}}'
echo "Create a loopback device that automatically connects to the speakers and swaps the left and right channels between the sink and source:
pw-loopback --capture-props='{{media.class=Audio/Sink audio.position=[FL FR]}}' --playback-props='{{audio.position=[FR FL]}}'
echo "Create a loopback device that automatically connects to the microphone and swaps the left and right channels between the sink and source:
pw-loopback --capture-props='{{audio.position=[FR FL]}}' --playback-props='{{media.class=Audio/Source audio.position=[FL FR]}}'
echo "pw-mon
echo "Monitor objects on the PipeWire instance.
echo "More information: <https://docs.pipewire.org/page_man_pw-mon_1.html>.
echo "Monitor the default PipeWire instance:
pw-mon
echo "Monitor a specific remote instance:
pw-mon --remote={{remote_name}}
echo "Monitor the default instance specifying a color configuration:
pw-mon --color={{never|always|auto}}
echo "Display help:
pw-mon --help
echo "pw-play
echo "Record audio files through pipewire.
echo "Shorthand for pw-cat --playback.
echo "More information: <https://fedoraproject.org/wiki/QA:Testcase_PipeWire_PipeWire_CLI>.
echo "Play a wav sound file over the default target:
pw-play {{path/to/file.wav}}
echo "Play a wav sound file at a different volume level:
pw-play --volume={{0.1}} {{path/to/file.wav}}
echo "pw-profiler
echo "Profile a local or remote instance.
echo "More information: <https://docs.pipewire.org/page_man_pw-profiler_1.html>.
echo "Profile the default instance, logging to profile.log` (`gnuplot` files and a `.html file for result visualizing are also generated):
pw-profiler
echo "Change the log output file:
pw-profiler --output {{path/to/file.log}}
echo "Profile a remote instance:
pw-profiler --remote {{remote_name}}
echo "Display help:
pw-profiler --help
echo "pw-record
echo "Record audio files through pipewire.
echo "Shorthand for pw-cat --record.
echo "More information: <https://fedoraproject.org/wiki/QA:Testcase_PipeWire_PipeWire_CLI>.
echo "Record a sample recording using the default target:
pw-record {{path/to/file.wav}}
echo "Record a sample recording at a different volume level:
pw-record --volume={{0.1}} {{path/to/file.wav}}
echo "Record a sample recording using a different sample rate:
pw-record --rate={{6000}} {{path/to/file.wav}}
echo "pwd
echo "Print name of current/working directory.
echo "More information: <https://www.gnu.org/software/coreutils/pwd>.
echo "Print the current directory:
pwd
echo "Print the current directory, and resolve all symlinks (i.e. show the "physical" path):
pwd --physical
echo "Print the current logical directory:
pwd --logical
echo "pwdx
echo "Print working directory of a process.
echo "More information: <https://manned.org/pwdx>.
echo "Print current working directory of a process:
pwdx {{process_id}}
echo "pyrit
echo "WPA/WPA2 cracking tool using computational power.
echo "More information: <https://github.com/JPaulMora/Pyrit>.
echo "Display system cracking speed:
pyrit benchmark
echo "List available cores:
pyrit list_cores
echo "Set [e]SSID:
pyrit -e "{{ESSID}}" create_essid
echo "[r]ead and analyze a specific packet capture file:
pyrit -r {{path/to/file.cap|path/to/file.pcap}} analyze
echo "Read and [i]mport passwords to the current database:
pyrit -i {{path/to/file}} {{import_unique_passwords|unique_passwords|import_passwords}}
echo "Exp[o]rt passwords from database to a specific file:
pyrit -o {{path/to/file}} export_passwords
echo "Translate passwords with Pired Master Keys:
pyrit batch
echo "[r]ead the capture file and crack the password:
pyrit -r {{path/to/file}} attack_db
echo "qjoypad
echo "Translate input from gamepads or joysticks into keyboard strokes or mouse actions.
echo "More information: <http://qjoypad.sourceforge.net/>.
echo "Start QJoyPad:
qjoypad
echo "Start QJoyPad and look for devices in a specific directory:
qjoypad --device={{path/to/directory}}
echo "Start QJoyPad but don't show a system tray icon:
qjoypad --notray
echo "Start QJoyPad and force the window manager to use a system tray icon:
qjoypad --force-tray
echo "Force a running instance of QJoyPad to update its list of devices and layouts:
qjoypad --update
echo "Load the given layout in an already running instance of QJoyPad, or start QJoyPad using the given layout:
qjoypad "{{layout}}"
echo "qm cleanup
echo "Clean up resources on QEMU/KVM Virtual Machine Manager like tap devices, VGPUs, etc.
echo "Called after a VM shuts down, crashes, etc.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Clean up resources:
qm cleanup {{vm_id}} {{clean-shutdown}} {{guest-requested}}
echo "qm clone
echo "Create a copy of virtual machine on QEMU/KVM Virtual Machine Manager.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Copy a virtual machine:
qm copy {{vm_id}} {{new_vm_id}}
echo "Copy a virtual machine using a specific name:
qm copy {{vm_id}} {{new_vm_id}} --name {{name}}
echo "Copy a virtual machine using a specific descriptionn:
qm copy {{vm_id}} {{new_vm_id}} --description {{description}}
echo "Copy a virtual machine creating a full copy of all disks:
qm copy {{vm_id}} {{new_vm_id}} --full
echo "Copy a virtual machine using a specific format for file storage (requires --full):
qm copy {{vm_id}} {{new_vm_id}} --full --format {{qcow2|raw|vmdk}}
echo "Copy a virtual machine then add it to a specific pool:
qm copy {{vm_id}} {{new_vm_id}} --pool {{pool_name}}
echo "qm cloud init
echo "Configure cloudinit settings for virtual machines managed by Proxmox Virtual Environment (PVE).
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Configure cloudinit settings for a specific user and set password for the user:
qm cloud-init {{vm_id}} -user={{user}} -password={{password}}
echo "Configure cloudinit settings for a specific user and set password for the user with a specific SSH key:
qm cloud-init {{vm_id}} -user={{user}} -password={{password}} -sshkey={{ssh_key}}
echo "Set the hostname for a specific virtual machine:
qm cloud-init {{vm_id}} -hostname={{hostname}}
echo "Configure the network interface settings for a specific virtual machine:
qm cloud-init {{vm_id}} -ipconfig {{ipconfig}}
echo "Configure a shell script to execute before cloud-ini is run on a virtual machine:
qm cloud-init {{vm_id}} -pre {{script}}
echo "qm cloudinit dump
echo "Generate cloudinit configuration files.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Generate a cloudinit file for a specific configuration type:
qm cloudinit dump {{virtual_machine_id}} {{meta|network|user}}
echo "qm config
echo "Display the virtual machine configuration with pending configuration changes applied.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Display the virtual machine configuration:
qm config {{vm_id}}
echo "Display the current configuration values instead of pending values for the virtual machine:
qm config --current {{true}} {{vm_id}}
echo "Fetch the configuration values from the given snapshot:
qm config --snapshot {{snapshot_name}} {{vm_id}}
echo "qm create
echo "Create or restore a virtual machine on QEMU/KVM Virtual Machine Manager.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Create a virtual machine:
qm create {{100}}
echo "Automatically start the machine after creation:
qm create {{100}} --start 1
echo "Specify the type of operating system on the machine:
qm create {{100}} --ostype {{win10}}
echo "Replace an existing machine (requires archiving it):
qm create {{100}} --archive {{path/to/backup_file.tar}} --force 1
echo "Specify a script that is executed automatically depending on the state of the virtual machine:
qm create {{100}} --hookscript {{path/to/script.pl}}
echo "qm delsnapshot
echo "Delete virtual machine snapshots.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Delete a snapshot:
qm delsnapshot {{vm_id}} {{snapshot_name}}
echo "Delete a snapshot from a configuration file (even if removing the disk snapshot fails):
qm delsnapshot {{vm_id}} {{snapshot_name}} --force 1
echo "qm destroy
echo "Destroy a virtual machine in QEMU/KVM Virtual Machine Manager.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Destroy a specific virtual machine:
qm destroy {{vm_id}}
echo "Destroy all disks that are not explicitly referenced in a specific virtual machine's configuration:
qm destroy {{vm_id}} --destroy-unreferenced-disks
echo "Destroy a virtual machine and remove from all locations (inventory, backup jobs, high availability managers, etc.):
qm destroy {{vm_id}} --purge
echo "Destroy a specific virtual machine ignoring locks and forcing destroy:
sudo qm destroy {{vm_id}} --skiplock
echo "qm disk import
echo "Import a disk image to a virtual machine as an unused disk.
echo "The supported image formats for qemu-img, such as raw, qcow2, qed, vdi, vmdk, and vhd must be used.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Import a VMDK/qcow2/raw disk image using a specific storage name:
qm importdisk {{vm_id}} {{path/to/disk}} {{storage_name}} --format {{qcow2|raw|vmdk}}
echo "qm disk move
echo "Move a virtual disk from one storage to another within the same Proxmox cluster.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Move a virtual disk:
qm disk move {{vm_id}} {{destination}} {{index}}
echo "Delete the previous copy of the virtual disk:
qm disk move -delete {{vm_id}} {{destination}} {{index}}
echo "qm disk resize
echo "Resize a virtual machine disk in the Proxmox Virtual Environment (PVE).
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Add n gigabytes to a virtual disk:
qm disk resize {{vm_id}} {{disk_name}} +{{n}}G
echo "qm guest cmd
echo "Execute QEMU Guest Agent commands.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Execute a specific QEMU Guest Agent command:
qm guest cmd {{virtual_machine_id}} {{fsfreeze-freeze|fsfreeze-status|fsfreeze-thaw|fstrim|get-fsinfo|...}}
echo "qm guest exec-status
echo "Print the status of the given pid started by the guest-agent on QEMU/KVM Virtual Machine Manager.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Print the status of a specific PID:
qm guest exec-status {{vm_id}} {{pid}}
echo "qm guest exec
echo "Execute a specific command via a guest agent.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Execute a specific command via a guest agent:
qm guest exec {{vm_id}} {{command}} {{argument1 argument2 ...}}
echo "Execute a specific command via a guest agent asynchronously:
qm guest exec {{vm_id}} {{argument1 argument2 ...}} --synchronous 0
echo "Execute a specific command via a guest agent with a specified timeout of 10 seconds:
qm guest exec {{vm_id}} {{argument1 argument2...}} --timeout {{10}}
echo "Execute a specific command via a guest agent and forward input from STDIN until EOF to the guest agent:
qm guest exec {{vm_id}} {{argument1 argument2 ...}} --pass-stdin 1
echo "qm guest passwd
echo "Set the password for a specific user on QEMU/KVM Virtual Machine Manager.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Set a password for a specific user in a virtual machine interactively:
qm guest passwd {{vm_id}} {{username}}
echo "Set an already hashed password for a specific user in a virtual machine interactively:
qm guest passwd {{vm_id}} {{username}} --crypted 1
echo "qm help
echo "Display help for a specific command.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Display help for a specific command:
qm help {{command}}
echo "Display help for a specific command with detailed information:
qm help {{command}} --verbose {{true|false}}
echo "qm import disk
echo "This command is an alias of qm disk import.
echo "View documentation for the original command:
tldr qm disk import
echo "qm list
echo "List all virtual machines.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "List all virtual machines:
qm list
echo "List all virtual machines with a full status about the ones which are currently running:
qm list --full 1
echo "qm listsnapshot
echo "List snapshots of virtual machines.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "List all snapshots of a specific virtual machine:
qm listsnapshot {{vm_id}}
echo "qm migrate
echo "Migrate a virtual machine.
echo "Used to create a new migration task.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Migrate a specific virtual machine:
qm migrate {{vm_id}} {{target}}
echo "Override the current I/O bandwidth limit with 10 KiB/s:
qm migrate {{vm_id}} {{target}} --bwlimit 10
echo "Allow migration of virtual machines using local devices (root only):
qm migrate {{vm_id}} {{target}} --force true
echo "Use online/live migration if a virtual machine is running:
qm migrate {{vm_id}} {{target}} --online true
echo "Enable live storage migration for local disks:
qm migrate {{vm_id}} {{target}} --with-local-disks true
echo "qm monitor
echo "Enter the QEMU Monitor interface.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Enter the QEMU Monitor interface of a specific virtual machine:
qm monitor {{vm_id}}
echo "qm move disk
echo "This command is an alias of qm disk move.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "View documentation for the original command:
tldr qm disk move
echo "qm move disk
echo "This command is an alias of qm disk move.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "View documentation for the original command:
tldr qm disk move
echo "qm mtunnel
echo "Used by qmigrate.
echo "It should not be invoked manually.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Command used by qmigrate during data migration from a VM to another host:
qm mtunnel
echo "qm pending
echo "Get the virtual machine configuration with both current and pending values.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Get the virtual machine configuration of a specific virtual machine:
qm pending {{vm_id}}
echo "qm reboot
echo "Reboot a virtual machine by shutting it down, and starting it again after applying pending changes.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Reboot a virtual machine:
qm reboot {{vm_id}}
echo "Reboot a virtual machine after wait for at most 10 seconds:
qm reboot --timeout {{10}} {{vm_id}}
echo "qm rescan
echo "Rescan all storages and update disk sizes and unused disk images of a virtual machine.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Rescan all storages and update disk sizes and unused disk images of a specific virtual machine:
qm rescan {{vm_id}}
echo "Perform a dry-run of rescan on a specific virtual machine and do not write any changes to configurations:
qm rescan --dryrun {{true}} {{vm_id}}
echo "qm reset
echo "Reset a virtual machine on QEMU/KVM Virtual Machine Manager.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Reset a virtual machine:
qm reset {{vm_id}}
echo "Reset a virtual machine and skip lock (only root can use this option):
qm reset --skiplock {{true}} {{vm_id}}
echo "qm resize
echo "This command is an alias of qm-disk-resize.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "View documentation for the original command:
tldr qm-disk-resize
echo "qm resume
echo "Resume a virtual machine.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Resume a specific virtual machine:
qm resume {{vm_id}}
echo "Resume a specific virtual machine ignoring locks (requires root):
sudo qm resume {{vm_id}} --skiplock true
echo "qm rollback
echo "Rollback the VM state to a specified snapshot.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Rollback the state of a specific VM to a specified snapshot:
qm rollback {{vm_id}} {{snap_name}}
echo "qm sendkey
echo "Send QEMU monitor encoding key event to a virtual machine.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Send the specified key event to a specific virtual machine:
qm sendkey {{vm_id}} {{key}}
echo "Allow root user to send key event and ignore locks:
qm sendkey --skiplock {{true}} {{vm_id}} {{key}}
echo "qm showcmd
echo "Show command-line which is used to start the VM (debug info).
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Show command-line for a specific virtual machine:
qm showcmd {{vm_id}}
echo "Put each option on a new line to enhance human readability:
qm showcmd --pretty {{true}} {{vm_id}}
echo "Fetch config values from a specific snapshot:
qm showcmd --snapshot {{string}} {{vm_id}}
echo "qm shutdown
echo "Shutdown a virtual machine on QEMU/KVM Virtual Machine Manager.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Shutdown a virtual machine:
qm shutdown {{VM_ID}}
echo "Shutdown a virtual machine after wait for at most 10 seconds:
qm shutdown --timeout {{10}} {{VM_ID}}
echo "Shutdown a virtual machine and do not deactivate storage volumes:
qm shutdown --keepActive {{true}} {{VM_ID}}
echo "Shutdown a virtual machine and skip lock (only root can use this option):
qm shutdown --skiplock {{true}} {{VM_ID}}
# echo "Stop and shutdown a virtual machine:
# qm shutdown --forceStop {{true}} {{VM_ID}}
echo "qm snapshot
echo "Create virtual machine snapshots.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Create a snapshot of a specific virtual machine:
qm snapshot {{vm_id}} {{snapshot_name}}
echo "Create a snapshot with a specific description:
qm snapshot {{vm_id}} {{snapshot_name}} --description {{description}}
echo "Create a snapshot including the vmstate:
qm snapshot {{vm_id}} {{snapshot_name}} --description {{description}} --vmstate 1
echo "qm start
echo "Start a virtual machine on QEMU/KVM Virtual Machine Manager.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Start a specific virtual machine:
qm start {{100}}
echo "Specify the QEMU machine type (i.e. the CPU to emulate):
qm start {{100}} --machine {{q35}}
echo "Start a specific virtual machine with a timeout in 60 seconds:
qm start {{100}} --timeout {{60}}
echo "qm status
echo "Show virtual machine status.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Display the status of a specific virtual machine:
qm status {{vm_id}}
echo "Display detailed status of a specific virtual machine:
qm status --verbose {{true}} {{vm_id}}
echo "qm suspend
echo "Suspends a virtual machine (VM) in the Proxmox Virtual Environment (PVE).
echo "Use --skiplock` and `--skiplockstorage flags with caution, as they may lead to data corruption in certain situations.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Suspend a virtual machine by id:
qm suspend {{vm_id}} {{integer}}
echo "Skip the lock check when suspending the VM:
qm suspend {{vm_id}} {{integer}} --skiplock
echo "Skip the lock check for storage when suspending the VM:
qm suspend {{vm_id}} {{integer}} --skiplockstorage
echo "qm template
echo "Create a Proxmox VM template.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Create a template out of a specific virtual machine:
qm template {{vm_id}}
echo "qm unlock
echo "Unlock a virtual machine in QEMU/KVM Virtual Machine Manager.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Unlock a specific virtual machine:
qm unlock {{vm_id}}
echo "qm vncproxy
echo "Proxy Virtual Machine VNC (Virtual network computing) traffic to stdin` or `stdout.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "Proxy a specific virtual machine:
qm vncproxy {{vm_id}}
echo "qm wait
# echo "Wait until the virtual machine is stopped.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
# echo "Wait until the virtual machine is stopped:
qm wait {{vm_id}}
# echo "Wait until the virtual machine is stopped with a 10 second timeout:
qm wait --timeout {{10}} {{vm_id}}
# echo "Send a shutdown request, then wait until the virtual machine is stopped with a 10 second timeout:
qm shutdown {{vm_id}} && qm wait --timeout {{10}} {{vm_id}}
echo "qm
echo "QEMU/KVM Virtual Machine Manager.
echo "More information: <https://pve.proxmox.com/pve-docs/qm.1.html>.
echo "List all virtual machines:
qm list
echo "Using an ISO file uploaded on the local storage, create a virtual machine with a 4 GB IDE disk on the local-lvm storage and an ID of 100:
qm create {{100}} -ide0 {{local-lvm:4}} -net0 {{e1000}} -cdrom {{local:iso/proxmox-mailgateway_2.1.iso}}
echo "Show the configuration of a virtual machine, specifying its ID:
qm config {{100}}
echo "Start a specific virtual machine:
qm start {{100}}
# echo "Send a shutdown request, then wait until the virtual machine is stopped:
qm shutdown {{100}} && qm wait {{100}}
echo "Destroy a virtual machine and remove all related resources:
qm destroy {{100}} --purge
echo "qmrestore
echo "Restore QemuServer vzdump backups.
echo "More information: <https://pve.proxmox.com/pve-docs/qmrestore.1.html>.
echo "Restore virtual machine from given backup file on the original storage:
qmrestore {{path/to/vzdump-qemu-100.vma.lzo}} {{100}}
echo "Overwrite existing virtual machine from a given backup file on the original storage:
qmrestore {{path/to/vzdump-qemu-100.vma.lzo}} {{100}} --force true
echo "Restore the virtual machine from a given backup file on specific storage:
qmrestore {{path/to/vzdump-qemu-100.vma.lzo}} {{100}} --storage {{local}}
echo "Start virtual machine immediately from the backup while restoring in the background (only on Proxmox Backup Server):
qmrestore {{path/to/vzdump-qemu-100.vma.lzo}} {{100}} --live-restore true
echo "qrcp
echo "A file transfer tool.
echo "More information: <https://github.com/claudiodangelis/qrcp>.
echo "Send a file or directories:
qrcp send {{path/to/file_or_directory path/to/file_directory ...}}
echo "Receive files:
qrcp receive
echo "Compress content before transferring:
qrcp send --zip {{path/to/file_or_directory}}
echo "Specify a [p]ort to use:
qrcp {{send|receive}} --port {{port_number}}
echo "Specify the network [i]nterface to use:
qrcp {{send|receive}} --interface interface
echo "Keep the server alive:
qrcp {{send|receive}} --keep-alive
echo "qsub
echo "Submits a script to the queue management system TORQUE.
echo "More information: <https://manned.org/qsub.1>.
echo "Submit a script with default settings (depends on TORQUE settings):
qsub {{script.sh}}
echo "Submit a script with a specified wallclock runtime limit of 1 hour, 2 minutes and 3 seconds:
qsub -l walltime={{1}}:{{2}}:{{3}} {{script.sh}}
echo "Submit a script that is executed on 2 nodes using 4 cores per node:
qsub -l nodes={{2}}:ppn={{4}} {{script.sh}}
echo "Submit a script to a specific queue. Note that different queues can have different maximum and minimum runtime limits:
qsub -q {{queue_name}} {{script.sh}}
echo "qtchooser
echo "A wrapper used to select between Qt development binary versions.
echo "More information: <https://manned.org/qtchooser>.
echo "List available Qt versions from the configuration files:
qtchooser --list-versions
echo "Print environment information:
qtchooser --print-env
echo "Run the specified tool using the specified Qt version:
qtchooser --run-tool={{tool}} --qt={{version_name}}
echo "Add a Qt version entry to be able to choose from:
qtchooser --install {{version_name}} {{path/to/qmake}}
echo "Display all available options:
qtchooser --help
echo "qtile
echo "A full-featured, hackable tiling window manager written and configured in Python.
echo "More information: <https://docs.qtile.org/en/latest/manual/commands/shell/index.html>.
echo "Start the window manager, if it is not running already (should ideally be run from .xsession or similar):
qtile start
echo "Check the configuration file for any compilation errors (default location is ~/.config/qtile/config.py):
qtile check
echo "Show current resource usage information:
# qtile top --force
echo "Open the program xterm` as a floating window on the group named `test-group:
qtile run-cmd --group {{test-group}} --float {{xterm}}
echo "Restart the window manager:
qtile cmd-obj --object cmd --function restart
echo "quotacheck
echo "Scan a filesystem for disk usage; create, check and repair quota files.
echo "It is best to run quota check with quotas turned off to prevent damage or loss to quota files.
echo "More information: <https://manned.org/quotacheck>.
echo "Check quotas on all mounted non-NFS filesystems:
sudo quotacheck --all
echo "Force check even if quotas are enabled (this can cause damage or loss to quota files):
sudo quotacheck --force {{mountpoint}}
echo "Check quotas on a given filesystem in debug mode:
sudo quotacheck --debug {{mountpoint}}
echo "Check quotas on a given filesystem, displaying the progress:
sudo quotacheck --verbose {{mountpoint}}
echo "Check user quotas:
sudo quotacheck --user {{user}} {{mountpoint}}
echo "Check group quotas:
sudo quotacheck --group {{group}} {{mountpoint}}
# echo "radeontop
echo "Show utilization of AMD GPUs.
echo "May require root privileges depending on your system.
# echo "More information: <https://github.com/clbr/radeontop>.
echo "Show the utilization of the default AMD GPU:
# radeontop
echo "Enable colored output:
# radeontop --color
echo "Select a specific GPU (the bus number is the first number in the output of lspci):
# radeontop --bus {{bus_number}}
echo "Specify the display refresh rate (higher means more GPU overhead):
# radeontop --ticks {{samples_per_second}}
echo "rankmirrors
echo "Rank a list of Pacman mirrors by connection and opening speed.
echo "Writes the new mirrorlist to stdout.
echo "More information: <https://wiki.archlinux.org/index.php/mirrors>.
echo "Rank a mirror list:
rankmirrors {{/etc/pacman.d/mirrorlist}}
# echo "Output only a given number of the top ranking servers:
rankmirrors -n {{number}} {{/etc/pacman.d/mirrorlist}}
echo "Be verbose when generating the mirrorlist:
rankmirrors -v {{/etc/pacman.d/mirrorlist}}
echo "Test only a specific URL:
rankmirrors --url {{url}}
echo "Output only the response times instead of a full mirrorlist:
rankmirrors --times {{/etc/pacman.d/mirrorlist}}
echo "raspi-config
echo "An ncurses terminal GUI to config a Raspberry Pi.
echo "More information: <https://www.raspberrypi.org/documentation/computers/configuration.html>.
echo "Start raspi-config:
sudo raspi-config
echo "raspinfo
echo "Display Raspberry Pi system information.
echo "More information: <https://github.com/raspberrypi/utils/tree/master/raspinfo>.
echo "Display system information:
raspinfo
echo "raw
echo "Bind a Unix raw character device.
echo "More information: <https://manned.org/raw.8>.
echo "Bind a raw character device to a block device:
raw /dev/raw/raw{{1}} {{/dev/block_device}}
echo "Query an existing binding instead of setting a new one:
raw /dev/raw/raw{{1}}
echo "Query all bound raw devices:
raw -qa
echo "rc-service
echo "Locate and run OpenRC services with arguments.
echo "See also openrc.
echo "More information: <https://manned.org/rc-service>.
echo "Show a service's status:
rc-service {{service_name}} status
echo "Start a service:
sudo rc-service {{service_name}} start
# echo "Stop a service:
# sudo rc-service {{service_name}} stop
echo "Restart a service:
sudo rc-service {{service_name}} restart
echo "Simulate running a service's custom command:
sudo rc-service --dry-run {{service_name}} {{command_name}}
echo "Actually run a service's custom command:
sudo rc-service {{service_name}} {{command_name}}
echo "Resolve the location of a service definition on disk:
sudo rc-service --resolve {{service_name}}
echo "rc-status
echo "Show status info about runlevels.
echo "See also openrc.
echo "More information: <https://manned.org/rc-status>.
echo "Show a summary of services and their status:
rc-status
echo "Include services in all runlevels in the summary:
rc-status --all
echo "List services that have crashed:
rc-status --crashed
echo "List manually started services:
rc-status --manual
echo "List supervised services:
rc-status --supervised
echo "Get the current runlevel:
rc-status --runlevel
echo "List all runlevels:
rc-status --list
echo "rc-update
echo "Add and remove OpenRC services to and from runlevels.
echo "See also openrc.
echo "More information: <https://manned.org/rc-update>.
echo "List all services and the runlevels they are added to:
rc-update show
echo "Add a service to a runlevel:
sudo rc-update add {{service_name}} {{runlevel}}
echo "Delete a service from a runlevel:
sudo rc-update delete {{service_name}} {{runlevel}}
echo "Delete a service from all runlevels:
sudo rc-update --all delete {{service_name}}
echo "rcp
echo "Copy files between local and remote systems.
echo "It mimics the behavior of the cp command but operates across different machines.
echo "More information: <https://www.gnu.org/software/inetutils/manual/html_node/rcp-invocation.html>.
echo "Copy a file to a remote host:
rcp {{path/to/local_file}} {{username}}@{{remotehost}}:{{/path/to/destination/}}
echo "Copy a directory recursively:
rcp -r {{path/to/local_directory}} {{username}}@{{remotehost}}:{{/path/to/destination/}}
echo "Preserve the file attributes:
rcp -p {{path/to/local_file}} {{username}}@{{remotehost}}:{{/path/to/destination/}}
echo "Force copy without a confirmation:
rcp -f {{path/to/local_file}} {{username}}@{{remotehost}}:{{/path/to/destination/}}
# echo "rdesktop
# echo "Remote Desktop Protocol client.
echo "It can be used to connect the remote computer using the RDP protocol.
# echo "More information: <https://manned.org/rdesktop>.
echo "Connect to a remote computer (default port is 3389):
# rdesktop -u {{username}} -p {{password}} {{host:port}}
echo "Simple Examples:
# rdesktop -u Administrator -p passwd123 192.168.1.111:3389
echo "Connect to a remote computer with full screen (press Ctrl + Alt + Enter to exist):
# rdesktop -u {{username}} -p {{password}} -f {{host:port}}
echo "Use the customed resolution (use the letter 'x' between the number):
# rdesktop -u {{username}} -p {{password}} -g 1366x768 {{host:port}}
echo "Connect to a remote computer using domain user:
# rdesktop -u {{username}} -p {{password}} -d {{domainname}} {{host:port}}
echo "Use the 16-bit color (speed up):
# rdesktop -u {{username}} -p {{password}} -a 16 {{host:port}}
echo "read
echo "Shell builtin for retrieving data from stdin.
echo "More information: <https://manned.org/read.1p>.
echo "Store data that you type from the keyboard:
read {{variable}}
echo "Store each of the next lines you enter as values of an array:
read -a {{array}}
echo "Specify the number of maximum characters to be read:
read -n {{character_count}} {{variable}}
echo "Use a specific character as a delimiter instead of a new line:
read -d {{new_delimiter}} {{variable}}
echo "Do not let backslash (\\) act as an escape character:
read -r {{variable}}
echo "Display a prompt before the input:
read -p "{{Enter your input here: }}" {{variable}}
echo "Do not echo typed characters (silent mode):
read -s {{variable}}
echo "Read stdin and perform an action on every line:
while read line; do echo "$line"; done
echo "readelf
echo "Displays information about ELF files.
echo "More information: <http://man7.org/linux/man-pages/man1/readelf.1.html>.
echo "Display all information about the ELF file:
readelf -all {{path/to/binary}}
echo "Display all the headers present in the ELF file:
readelf --headers {{path/to/binary}}
echo "Display the entries in symbol table section of the ELF file, if it has one:
readelf --symbols {{path/to/binary}}
echo "Display the information contained in the ELF header at the start of the file:
readelf --file-header {{path/to/binary}}
echo "readpe
echo "Displays information about PE files.
echo "More information: <https://manned.org/readpe>.
echo "Display all information about a PE file:
readpe {{path/to/executable}}
echo "Display all the headers present in a PE file:
readpe --all-headers {{path/to/executable}}
echo "Display all the sections present in a PE file:
readpe --all-sections {{path/to/executable}}
echo "Display a specific header from a PE file:
readpe --header {{dos|coff|optional}} {{path/to/executable}}
echo "List all imported functions:
readpe --imports {{path/to/executable}}
echo "List all exported functions:
readpe --exports {{path/to/executable}}
echo "reboot
echo "Reboot the system.
echo "More information: <https://manned.org/reboot.8>.
echo "Reboot the system:
reboot
echo "Power off the system (same as poweroff):
reboot --poweroff
echo "Halt (terminates all processes and shuts down the CPU) the system (same as halt):
reboot --halt
echo "Reboot immediately without contacting the system manager:
reboot --force
echo "Write the wtmp shutdown entry without rebooting the system:
reboot --wtmp-only
echo "reflector
echo "Arch script to fetch and sort mirrorlists.
echo "More information: <https://manned.org/reflector>.
echo "Get all mirrors, sort for download speed and save them:
sudo reflector --sort {{rate}} --save {{/etc/pacman.d/mirrorlist}}
echo "Only get German HTTPS mirrors:
reflector --country {{Germany}} --protocol {{https}}
echo "Only get the 10 recently sync'd mirrors:
reflector --latest {{10}}
echo "register_new_matrix_user
echo "Used to register new users with a given home server when registration has been disabled.
echo "More information: <https://manned.org/register_new_matrix_user>.
echo "Create a user interactively:
register_new_matrix_user --config {{path/to/homeserver.yaml}}
echo "Create an admin user interactively:
register_new_matrix_user --config {{path/to/homeserver.yaml}} --admin
echo "Create an admin user non-interactively (not recommended):
register_new_matrix_user --config {{path/to/homeserver.yaml}} --user {{username}} --password {{password}} --admin
echo "rename
echo "Rename multiple files.
echo "NOTE: this page refers to the command from the util-linux package.
echo "For the Perl version, see file-rename` or `perl-rename.
echo "Warning: This command has no safeguards and will overwrite files without prompting.
echo "More information: <https://manned.org/rename>.
echo "Rename files using simple substitutions (substitute 'foo' with 'bar' wherever found):
rename {{foo}} {{bar}} {{*}}
echo "Dry-run - display which renames would occur without performing them:
rename -vn {{foo}} {{bar}} {{*}}
echo "Do not overwrite existing files:
rename -o {{foo}} {{bar}} {{*}}
echo "Change file extensions:
rename {{.ext}} {{.bak}} {{*.ext}}
echo "Prepend "foo" to all filenames in the current directory:
rename {{''}} {{'foo'}} {{*}}
echo "Rename a group of increasingly numbered files zero-padding the numbers up to 3 digits:
rename {{foo}} {{foo00}} {{foo?}} && rename {{foo}} {{foo0}} {{foo??}}
echo "repo-add
echo "Package database maintenance utility which enables installation of said package via Pacman.
echo "More information: <https://man.archlinux.org/man/repo-add>.
echo "Add all package binaries in the current directory and remove the old database file:
repo-add --remove {{path/to/database.db.tar.gz}} {{*.pkg.tar.zst}}
echo "Add all package binaries in the current directory in silent mode except for warning and error messages:
repo-add --quiet {{path/to/database.db.tar.gz}} {{*.pkg.tar.zst}}
echo "Add all package binaries in the current directory without showing color:
repo-add --nocolor {{path/to/database.db.tar.gz}} {{*.pkg.tar.zst}}
echo "reportbug
echo "Bug report tool of Debian distribution.
echo "More information: <https://manpages.debian.org/latest/reportbug/reportbug.html>.
echo "Generate a bug report about a specific package, then send it by e-mail:
reportbug {{package}}
echo "Report a bug that is not about a specific package (general problem, infrastructure, etc.):
reportbug other
echo "Write the bug report to a file instead of sending it by e-mail:
reportbug -o {{filename}} {{package}}
echo "repquota
echo "Display a summary of existing file quotas for a filesystem.
echo "More information: <https://manned.org/repquota>.
echo "Report stats for all quotas in use:
sudo repquota -all
echo "Report quota stats for all users, even those who aren't using any of their quota:
sudo repquota -v {{filesystem}}
echo "Report on quotas for users only:
repquota --user {{filesystem}}
echo "Report on quotas for groups only:
sudo repquota --group {{filesystem}}
echo "Report on used quota and limits in a human-readable format:
sudo repquota --human-readable {{filesystem}}
echo "Report on all quotas for users and groups in a human-readable format:
sudo repquota -augs
echo "reptyr
echo "Move a running process to a new terminal.
echo "Best used when you forget to start a long running task in screen.
echo "More information: <https://github.com/nelhage/reptyr>.
echo "Move a running process to your current terminal:
reptyr {{pid}}
echo "reset
echo "Reinitializes the current terminal. Clears the entire terminal screen.
echo "More information: <https://manned.org/reset>.
echo "Reinitialize the current terminal:
reset
echo "Display the terminal type instead:
reset -q
echo "resize2fs
echo "Resize an ext2, ext3 or ext4 filesystem.
echo "Does not resize the underlying partition. The filesystem may have to be unmounted first, read the man page for more details.
echo "More information: <https://manned.org/resize2fs>.
echo "Automatically resize a filesystem:
resize2fs {{/dev/sdXN}}
echo "Resize the filesystem to a size of 40G, displaying a progress bar:
resize2fs -p {{/dev/sdXN}} {{40G}}
echo "Shrink the filesystem to its minimum possible size:
resize2fs -M {{/dev/sdXN}}
echo "resolvectl
echo "Resolve domain names, IPv4 and IPv6 addresses, DNS resource records, and services.
echo "Introspect and reconfigure the DNS resolver.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/resolvectl.html>.
echo "Show DNS settings:
resolvectl status
echo "Resolve the IPv4 and IPv6 addresses for one or more domains:
resolvectl query {{domain1 domain2 ...}}
echo "Retrieve the domain of a specified IP address:
resolvectl query {{ip_address}}
echo "Flush all local DNS caches:
resolvectl flush-caches
echo "Display DNS statistics (transactions, cache, and DNSSEC verdicts):
resolvectl statistics
echo "Retrieve an MX record of a domain:
resolvectl --legend={{no}} --type={{MX}} query {{domain}}
echo "Resolve an SRV record, for example _xmpp-server._tcp gmail.com:
resolvectl service _{{service}}._{{protocol}} {{name}}
echo "Retrieve a TLS key:
resolvectl tlsa tcp {{domain}}:443
echo "resolveip
echo "Resolve hostnames to their IP addresses and vice versa.
echo "More information: <https://mariadb.com/kb/en/resolveip/>.
echo "Resolve a hostname to an IP address:
resolveip {{example.org}}
echo "Resolve an IP address to a hostname:
resolveip {{1.1.1.1}}
echo "Silent mode. Produces less output:
resolveip --silent {{example.org}}
echo "restorecon
echo "Restore SELinux security context on files/directories according to persistent rules.
echo "See also: semanage-fcontext.
echo "More information: <https://manned.org/restorecon>.
echo "View the current security context of a file or directory:
ls -dlZ {{path/to/file_or_directory}}
echo "Restore the security context of a file or directory:
restorecon {{path/to/file_or_directory}}
echo "Restore the security context of a directory recursively, and show all changed labels:
restorecon -R -v {{path/to/directory}}
echo "Restore the security context of a directory recursively, using all available threads, and show progress:
restorecon -R -T {{0}} -p {{path/to/directory}}
echo "Preview the label changes that would happen without applying them:
restorecon -R -n -v {{path/to/directory}}
echo "retroarch
echo "A frontend for emulators, game engines and media players.
echo "The reference implementation of the libretro API.
echo "More information: <https://github.com/libretro/RetroArch>.
echo "Start in the menu mode:
retroarch
echo "Start in full screen mode:
retroarch --fullscreen
echo "List all compiled features:
retroarch --features
echo "Set the path of a configuration file:
retroarch --config={{path/to/config_file}}
echo "Display help:
retroarch --help
echo "Display version:
retroarch --version
echo "rexec
echo "Execute a command on a remote host.
echo "Note: Use rexec` with caution, as it transmits data in plain text. Consider secure alternatives like `ssh for encrypted communication.
echo "More information: <https://www.gnu.org/software/inetutils/manual/html_node/rexec-invocation.html>.
echo "Execute a command on a remote [h]ost:
rexec -h={{remote_host}} {{ls -l}}
echo "Specify the remote [u]sername on a remote [h]ost:
rexec -username={{username}} -h={{remote_host}} {{ps aux}}
echo "Redirect stdin` from `/dev/null on a remote [h]ost:
rexec --no-err -h={{remote_host}} {{ls -l}}
echo "Specify the remote [P]ort on a remote [h]ost:
rexec -P={{1234}} -h={{remote_host}} {{ls -l}}
echo "rfkill
echo "Enable and disable wireless devices.
echo "More information: <https://manned.org/rfkill>.
echo "List devices:
rfkill
echo "Filter by columns:
rfkill -o {{ID,TYPE,DEVICE}}
echo "Block devices by type (e.g. bluetooth, wlan):
rfkill block {{bluetooth}}
echo "Unblock devices by type (e.g. bluetooth, wlan):
rfkill unblock {{wlan}}
echo "Output in JSON format:
rfkill -J
echo "rig
echo "Utility to piece together a random first name, last name, street number and address, along with a geographically consistent (ie, they all match the same area) city, state, ZIP code, and area code.
echo "More information: <https://manned.org/rig>.
echo "Display a random name (male or female) and address:
rig
echo "Display a [m]ale (or [f]emale) random name and address:
rig -{{m|f}}
echo "Use data files from a specific directory (default is /usr/share/rig):
rig -d {{path/to/directory}}
echo "Display a specific number of identities:
rig -c {{number}}
echo "Display a specific number of female identities:
rig -f -c {{number}}
echo "ripmime
echo "Extract attachments out of a MIME encoded email package.
echo "More information: <https://pldaniels.com/ripmime>.
echo "Extract file contents in the current directory:
ripmime -i {{path/to/file}}
echo "Extract file contents in a specific directory:
ripmime -i {{path/to/file}} -d {{path/to/directory}}
echo "Extract file contents and print verbose output:
ripmime -i {{path/to/file}} -v
echo "Get detailed information about the whole decoding process:
ripmime -i {{path/to/file}} --debug
echo "rkhunter
echo "Searches for rootkits and malware.
echo "More information: <https://wiki.archlinux.org/title/Rkhunter>.
echo "Check a system for rootkits and malware:
sudo rkhunter --check
echo "Update rkhunter:
sudo rkhunter --update
echo "Print all available tests:
sudo rkhunter --list
echo "Display version:
sudo rkhunter --versioncheck
echo "Display help:
sudo rkhunter --help
echo "rlogin
echo "Log in to a remote host.
echo "More information: <https://www.gnu.org/software/inetutils/manual/html_node/rlogin-invocation.html>.
echo "Log in to a remote host:
rlogin {{remote_host}}
echo "Log in to a remote host with a specific username:
rlogin -l {{username}} {{remote_host}}
echo "rm
echo "Remove files or directories.
echo "See also: rmdir.
echo "More information: <https://www.gnu.org/software/coreutils/rm>.
echo "Remove specific files:
# rm {{path/to/file1 path/to/file2 ...}}
echo "Remove specific files ignoring nonexistent ones:
# rm --force {{path/to/file1 path/to/file2 ...}}
echo "Remove specific files interactively prompting before each removal:
# rm --interactive {{path/to/file1 path/to/file2 ...}}
echo "Remove specific files printing info about each removal:
# rm --verbose {{path/to/file1 path/to/file2 ...}}
echo "Remove specific files and directories recursively:
# rm --recursive {{path/to/file_or_directory1 path/to/file_or_directory2 ...}}
echo "rmdir
echo "Remove directories without files.
echo "See also: rm.
echo "More information: <https://www.gnu.org/software/coreutils/rmdir>.
echo "Remove specific directories:
# rmdir {{path/to/directory1 path/to/directory2 ...}}
echo "Remove specific nested directories recursively:
# rmdir --parents {{path/to/directory1 path/to/directory2 ...}}
echo "rmmod
echo "Remove modules from the Linux kernel.
echo "More information: <https://manned.org/rmmod>.
echo "Remove a module from the kernel:
sudo rmmod {{module_name}}
echo "Remove a module from the kernel and display verbose information:
sudo rmmod --verbose {{module_name}}
echo "Remove a module from the kernel and send errors to syslog instead of stderr:
sudo rmmod --syslog {{module_name}}
echo "Display help:
rmmod --help
echo "Display version:
rmmod --version
echo "rofi
echo "An application launcher and window switcher.
echo "More information: <https://github.com/davatorium/rofi>.
echo "Show the list of apps:
rofi -show drun
echo "Show the list of all commands:
rofi -show run
echo "Switch between windows:
rofi -show window
echo "Pipe a list of items to stdin` and print the selected item to `stdout:
printf "{{Choice1\nChoice2\nChoice3}}" | rofi -dmenu
echo "rolldice
echo "Roll virtual dice.
echo "More information: <https://manned.org/rolldice>.
echo "Roll a single 20 sided dice:
rolldice d{{20}}
echo "Roll two six sided dice and drop the lowest roll:
rolldice {{2}}d{{6}}s{{1}}
echo "Roll two 20 sided dice and add a modifier value:
rolldice {{2}}d{{20}}{{+5}}
echo "Roll a 20 sided dice two times:
rolldice {{2}}xd{{20}}
echo "rpcclient
echo "MS-RPC client tool (part of the samba suite).
echo "More information: <https://www.samba.org/samba/docs/current/man-html/rpcclient.1.html>.
echo "Connect to a remote host:
rpcclient --user {{domain}}\{{username}}%{{password}} {{ip}}
echo "Connect to a remote host on a domain without a password:
rpcclient --user {{username}} --workgroup {{domain}} --no-pass {{ip}}
echo "Connect to a remote host, passing the password hash:
rpcclient --user {{domain}}\{{username}} --pw-nt-hash {{ip}}
echo "Execute shell commands on a remote host:
rpcclient --user {{domain}}\{{username}}%{{password}} --command {{semicolon_separated_commands}} {{ip}}
echo "Display domain users:
rpcclient $> enumdomusers
echo "Display privileges:
rpcclient $> enumprivs
echo "Display information about a specific user:
rpcclient $> queryuser {{username|rid}}
echo "Create a new user in the domain:
rpcclient $> createdomuser {{username}}
echo "rpcinfo
echo "Makes an RPC call to an RPC server and reports what it finds.
echo "More information: <https://manned.org/rpcinfo>.
echo "Show full table of all RPC services registered on localhost:
rpcinfo
echo "Show concise table of all RPC services registered on localhost:
rpcinfo -s {{localhost}}
echo "Display table of statistics of rpcbind operations on localhost:
rpcinfo -m
echo "Display list of entries of given service name (mountd) and version number (2) on a remote nfs share:
rpcinfo -l {{remote_nfs_server_ip}} {{mountd}} {{2}}
echo "Delete the registration for version 1 of the mountd service for all transports:
rpcinfo -d {{mountd}} {{1}}
echo "rpi-eeprom-update
echo "Tool to update EEPROM and view other EEPROM information.
echo "More information: <https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#rpi-eeprom-update>.
echo "Print information about the current raspberry pi EEPROM installed:
sudo rpi-eeprom-update
echo "Update a raspberry pi EEPROM:
sudo rpi-eeprom-update -a
echo "Cancel the pending update:
sudo rpi-eeprom-update -r
echo "Display help:
rpi-eeprom-update -h
echo "rpm-ostree
echo "A hybrid image/package system.
echo "Manage ostree deployments, package layers, filesystem overlays, and boot configuration.
echo "More information: <https://coreos.github.io/rpm-ostree/administrator-handbook/>.
echo "Show rpm-ostree deployments in the order they will appear in the bootloader:
rpm-ostree status
echo "Show packages which are outdated and can be updated:
rpm-ostree upgrade --preview
echo "Prepare a new ostree deployment with upgraded packages and reboot into it:
rpm-ostree upgrade --reboot
echo "Reboot into the previous ostree deployment:
rpm-ostree rollback --reboot
echo "Install a package into a new ostree deployment and reboot into it:
rpm-ostree install {{package}} --reboot
echo "rpm
echo "RPM Package Manager.
echo "For equivalent commands in other package managers, see <https://wiki.archlinux.org/title/Pacman/Rosetta>.
echo "More information: <https://rpm.org/>.
echo "Show version of httpd package:
rpm --query {{httpd}}
echo "List versions of all matching packages:
rpm --query --all '{{mariadb*}}'
echo "Forcibly install a package regardless of currently installed versions:
rpm --upgrade {{path/to/package.rpm}} --force
echo "Identify owner of a file and show version of the package:
rpm --query --file {{/etc/postfix/main.cf}}
echo "List package-owned files:
rpm --query --list {{kernel}}
echo "Show scriptlets from an RPM file:
rpm --query --package --scripts {{package.rpm}}
echo "Show changed, missing and/or incorrectly installed files of matching packages:
rpm --verify --all '{{php-*}}'
echo "Display the changelog of a specific package:
rpm --query --changelog {{package}}
echo "rpm2cpio
echo "Convert an RPM package to a cpio archive.
echo "More information: <http://ftp.rpm.org/max-rpm/s1-rpm-miscellania-rpm2cpio.html>.
echo "Convert an RPM package to a cpio` archive and save it as `file.cpio in the current directory:
rpm2cpio {{path/to/file.rpm}}
echo "rpmbuild
echo "RPM Package Build tool.
echo "More information: <https://docs.fedoraproject.org/en-US/quick-docs/creating-rpm-packages/>.
echo "Build binary and source packages:
rpmbuild -ba {{path/to/spec_file}}
echo "Build a binary package without source package:
rpmbuild -bb {{path/to/spec_file}}
echo "Specify additional variables when building a package:
rpmbuild -bb {{path/to/spec_file}} --define "{{variable1}} {{value1}}" --define "{{variable2}} {{value2}}"
echo "rpmspec
echo "Query a RPM spec file.
echo "More information: <https://manned.org/rpmspec>.
echo "List binary packages which would be generated from a rpm spec file:
rpmspec --query {{path/to/rpm.spec}}
echo "List all options for --queryformat:
rpmspec --querytags
echo "Get summary information for single binary packages generated from a rpm spec file:
rpmspec --query --queryformat "{{%{name}: %{summary}\n}}" {{path/to/rpm.spec}}
echo "Get the source package which would be generated from a rpm spec file:
rpmspec --query --srpm {{path/to/rpm.spec}}
echo "Parse a rpm spec file to stdout:
rpmspec --parse {{path/to/rpm.spec}}
echo "rsh
echo "Execute commands on a remote host.
echo "More information: <https://www.gnu.org/software/inetutils/manual/html_node/rsh-invocation.html>.
echo "Execute a command on a remote host:
rsh {{remote_host}} {{ls -l}}
echo "Execute a command on a remote host with a specific username:
rsh {{remote_host}} -l {{username}} {{ls -l}}
echo "Redirect stdin` to `/dev/null when executing a command on a remote host:
rsh {{remote_host}} --no-err {{ls -l}}
echo "rspamc
echo "Command-line client for rspamd servers.
echo "More information: <https://manned.org/rspamc>.
echo "Train the bayesian filter to recognise an email as spam:
rspamc learn_spam {{path/to/email_file}}
echo "Train the bayesian filter to recognise an email as ham:
rspamc learn_ham {{path/to/email_file}}
echo "Generate a manual report on an email:
rspamc symbols {{path/to/email_file}}
echo "Show server statistics:
rspamc stat
echo "rtcwake
echo "Enter a system sleep state until specified wakeup time relative to your BIOS clock.
echo "More information: <https://manned.org/rtcwake>.
echo "Show whether an alarm is set or not:
sudo rtcwake -m show -v
echo "Suspend to RAM and wakeup after 10 seconds:
sudo rtcwake -m mem -s {{10}}
echo "Suspend to disk (higher power saving) and wakeup 15 minutes later:
sudo rtcwake -m disk --date +{{15}}min
echo "Freeze the system (more efficient than suspend-to-RAM but version 3.9 or newer of the Linux kernel is required) and wakeup at a given date and time:
sudo rtcwake -m freeze --date {{YYYYMMDDhhmm}}
echo "Disable a previously set alarm:
sudo rtcwake -m disable
echo "Perform a dry run to wakeup the computer at a given time. (Press Ctrl + C to abort):
sudo rtcwake -m on --date {{hh:ss}}
echo "rtorrent
echo "Download torrents.
echo "More information: <https://github.com/rakshasa/rtorrent>.
echo "Add a torrent file or magnet to be downloaded:
rtorrent {{torrent_or_magnet}}
echo "Start the download:
<Ctrl>S
echo "View details about downloading torrent:
->
echo "Close rtorrent safely:
<Ctrl>Q
echo "ruget
echo "Alternative to wget written in Rust.
echo "More information: <https://github.com/ksk001100/ruget>.
echo "Download the contents of a URL to a file:
ruget {{https://example.com/file}}
echo "Download the contents of a URL to a specified [o]utput file:
ruget --output {{file_name}} {{https://example.com/file}}
echo "run-mailcap
echo "Run MailCap Programs.
echo "Run mailcap view, see, edit, compose, print - execute programs via entries in the mailcap file (or any of its aliases) will use the given action to process each mime-type/file.
echo "More information: <https://manned.org/run-mailcap>.
echo "Individual actions/programs on run-mailcap can be invoked with action flag:
run-mailcap --action=ACTION [--option[=value]]
echo "In simple language:
run-mailcap --action=ACTION {{filename}}
echo "Turn on extra information:
run-mailcap --action=ACTION --debug {{filename}}
echo "Ignore any "copiousoutput" directive and forward output to stdout:
run-mailcap --action=ACTION --nopager {{filename}}
echo "Display the found command without actually executing it:
run-mailcap --action=ACTION --norun {{filename}}
echo "runcon
echo "Run a program in a different SELinux security context.
echo "With neither context nor command, print the current security context.
echo "More information: <https://www.gnu.org/software/coreutils/runcon>.
echo "Determine the current domain:
runcon
echo "Specify the domain to run a command in:
runcon -t {{domain}}_t {{command}}
echo "Specify the context role to run a command with:
runcon -r {{role}}_r {{command}}
echo "Specify the full context to run a command with:
runcon {{user}}_u:{{role}}_r:{{domain}}_t {{command}}
echo "runlim
echo "A tool for sampling and limiting time and memory usage of a program and its child processes using the proc file system on Linux.
echo "More information: <http://fmv.jku.at/runlim>.
echo "Print the time and memory usage of a command:
runlim {{command}} {{command_arguments}}
echo "Log statistics to a file instead of stdout:
runlim --output-file={{path/to/file}} {{command}} {{command_arguments}}
echo "Limit time to an upper bound (in seconds):
runlim --time-limit={{number}} {{command}} {{command_arguments}}
echo "Limit real-time to an upper bound (in seconds):
runlim --real-time-limit={{number}} {{command}} {{command_arguments}}
echo "Limit space to an upper bound (in MB):
runlim --space-limit={{number}} {{command}} {{command_arguments}}
echo "runuser
echo "Run commands as a specific user and group without asking for password (needs root privileges).
echo "More information: <https://manned.org/runuser>.
echo "Run command as a different user:
runuser {{user}} -c '{{command}}'
echo "Run command as a different user and group:
runuser {{user}} -g {{group}} -c '{{command}}'
echo "Start a login shell as a specific user:
runuser {{user}} -l
echo "Specify a shell for running instead of the default shell (also works for login):
runuser {{user}} -s {{/bin/sh}}
echo "Preserve the entire environment of root (only if --login is not specified):
runuser {{user}} --preserve-environment -c '{{command}}'
echo "rusnapshot
echo "BTRFS snapshotting utility written in Rust.
echo "More information: <https://github.com/Edu4rdSHL/rusnapshot>.
echo "Create a snapshot using a config file:
sudo rusnapshot --config {{path/to/config.toml}} --cr
echo "List created snapshots:
sudo rusnapshot -c {{path/to/config.toml}} --list
echo "Delete a snapshot by ID or the name of the snapshot:
sudo rusnapshot -c {{path/to/config.toml}} --del --id {{snapshot_id}}
echo "Delete all hourly snapshots:
sudo rusnapshot -c {{path/to/config.toml}} --list --keep {{0}} --clean --kind {{hourly}}
echo "Create a read-write snapshot:
sudo rusnapshot -c {{path/to/config.toml}} --cr --rw
echo "Restore a snapshot:
sudo rusnapshot -c {{path/to/config.toml}} --id {{snapshot_id}} --restore
echo "sa
echo "Summarizes accounting information. Part of the acct package.
echo "Shows commands called by users, including basic info on CPU time spent processing and I/O rates.
echo "More information: <https://manned.org/man/sa.8>.
echo "Display executable invocations per user (username not displayed):
sudo sa
echo "Display executable invocations per user, showing responsible usernames:
sudo sa --print-users
echo "List resources used recently per user:
sudo sa --user-summary
echo "sacct
echo "Display accounting data from the Slurm service.
echo "More information: <https://slurm.schedmd.com/sacct.html>.
echo "Display job id, job name, partition, account, number of allocated cpus, job state, and job exit codes for recent jobs:
sacct
echo "Display job id, job state, job exit code for recent jobs:
sacct --brief
echo "Display the allocations of a job:
sacct --jobs {{job_id}} --allocations
echo "Display elapsed time, job name, number of requested CPUs, and memory requested of a job:
sacct --jobs {{job_id}} --format=Elapsed,JobName,ReqCPUS,ReqMem
echo "Display recent jobs that occurred from one week ago up to the present day:
sacct --starttime=$(date -d "1 week ago" +'%F')
echo "Output a larger number of characters for an attribute:
sacct --format=JobID,JobName%100
echo "sacctmgr
echo "View, setup, and manage Slurm accounts.
echo "More information: <https://slurm.schedmd.com/sacctmgr.html>.
echo "Show current configuration:
sacctmgr show configuration
echo "Add a cluster to the slurm database:
sacctmgr add cluster {{cluster_name}}
echo "Add an account to the slurm database:
sacctmgr add account {{account_name}} cluster={{cluster_of_account}}
echo "Show details of user/association/cluster/account using a specific format:
sacctmgr show {{user|association|cluster|account}} format="Account%10" format="GrpTRES%30"
echo "salloc
echo "Start an interactive shell session or execute a command by allocating one or more nodes in a cluster.
echo "More information: <https://slurm.schedmd.com/salloc.html>.
echo "Start an interactive shell session on a node in the cluster:
salloc
echo "Execute the specified command synchronously on a node in the cluster:
salloc {{ls -a}}
echo "Only allocate nodes fulfilling the specified constraints:
salloc --constraint={{(amd|intel)&gpu}}
echo "sam
echo "AWS Serverless Application Model (SAM) CLI.
echo "More information: <https://github.com/awslabs/aws-sam-cli>.
echo "Initialize a serverless application:
sam init
echo "Initialize a serverless application with a specific runtime:
sam init --runtime {{python3.7}}
echo "Package a SAM application:
sam package
echo "Build your Lambda function code:
sam build
echo "Run your serverless application locally:
sam local start-api
echo "Deploy an AWS SAM application:
sam deploy
echo "sar
echo "Monitor performance of various Linux subsystems.
echo "More information: <https://manned.org/sar>.
echo "Report I/O and transfer rate issued to physical devices, one per second (press CTRL+C to quit):
sar -b {{1}}
echo "Report a total of 10 network device statistics, one per 2 seconds:
sar -n DEV {{2}} {{10}}
echo "Report CPU utilization, one per 2 seconds:
sar -u ALL {{2}}
echo "Report a total of 20 memory utilization statistics, one per second:
sar -r ALL {{1}} {{20}}
echo "Report the run queue length and load averages, one per second:
sar -q {{1}}
echo "Report paging statistics, one per 5 seconds:
sar -B {{5}}
echo "sattach
echo "Attach to a Slurm job step.
echo "More information: <https://slurm.schedmd.com/sattach.html>.
echo "Redirect the IO streams (stdout`, `stderr`, and `stdin) of a Slurm job step to the current terminal:
sattach {{jobid}}.{{stepid}}
echo "Use the current console's input as stdin to the specified task:
sattach --input-filter {{task_number}}
echo "Only redirect stdin`/`stderr of the specified task:
sattach --{{output|error}}-filter {{task_number}}
echo "sbatch
echo "Submit a batch job to the SLURM scheduler.
echo "More information: <https://manned.org/sbatch>.
echo "Submit a batch job:
sbatch {{path/to/job.sh}}
echo "Submit a batch job with a custom name:
sbatch --job-name={{myjob}} {{path/to/job.sh}}
echo "Submit a batch job with a time limit of 30 minutes:
sbatch --time={{00:30:00}} {{path/to/job.sh}}
echo "Submit a job and request multiple nodes:
sbatch --nodes={{3}} {{path/to/job.sh}}
echo "sbcast
echo "Send a file to a job's allocated nodes.
echo "This command should only be used from within a Slurm batch job.
echo "More information: <https://slurm.schedmd.com/sbcast.html>.
echo "Send a file to all nodes allocated to the current job:
sbcast {{path/to/file}} {{path/to/destination}}
echo "Autodetect shared libraries the transmitted file depends upon and transmit them as well:
sbcast --send-libs={{yes}} {{path/to/executable}} {{path/to/destination}}
echo "scancel
echo "Cancel a Slurm job.
echo "More information: <https://slurm.schedmd.com/scancel.html>.
echo "Cancel a job using its ID:
scancel {{job_id}}
echo "Cancel all jobs from a user:
scancel {{user_name}}
echo "scanimage
echo "Scan images with the Scanner Access Now Easy API.
echo "More information: <http://sane-project.org/man/scanimage.1.html>.
echo "List available scanners to ensure the target device is connected and recognized:
scanimage -L
echo "Scan an image and save it to a file:
scanimage --format={{pnm|tiff|png|jpeg}} > {{path/to/new_image}}
echo "schroot
echo "Run a command or start an interactive shell with a different root directory. More customizable than chroot.
echo "More information: <https://wiki.debian.org/Schroot>.
echo "List available chroots:
schroot --list
echo "Run a command in a specific chroot:
schroot --chroot {{chroot}} {{command}}
echo "Run a command with options in a specific chroot:
schroot --chroot {{chroot}} {{command}} -- {{command_options}}
echo "Run a command in all available chroots:
schroot --all {{command}}
echo "Start an interactive shell within a specific chroot as a specific user:
schroot --chroot {{chroot}} --user {{user}}
echo "Begin a new session (a unique session ID is returned on stdout):
schroot --begin-session --chroot {{chroot}}
echo "Connect to an existing session:
schroot --run-session --chroot {{session_id}}
echo "End an existing session:
schroot --end-session --chroot {{session_id}}
echo "scontrol
echo "View information about and modify jobs.
echo "More information: <https://slurm.schedmd.com/scontrol.html>.
echo "Show information for job:
scontrol show job {{job_id}}
echo "Suspend a comma-separated list of running jobs:
scontrol suspend {{job_id}}
echo "Resume a comma-separated list of suspended jobs:
scontrol resume {{job_id}}
echo "Hold a comma-separated list of queued jobs (Use release command to permit the jobs to be scheduled):
scontrol hold {{job_id}}
echo "Release a comma-separated list of suspended job:
scontrol release {{job_id}}
echo "screenkey
echo "A screencast tool to display keys pressed.
echo "More information: <https://www.thregr.org/~wavexx/software/screenkey/>.
echo "Display keys which are currently being pressed on the screen:
screenkey
echo "Display keys and mouse buttons which are currently being pressed on the screen:
screenkey --mouse
echo "Launch the settings menu of screenkey:
screenkey --show-settings
echo "Launch screenkey at a specific position:
# screenkey --position {{top|center|bottom|fixed}}
echo "Change the format of the key modifiers displayed on screen:
screenkey --mods-mode {{normal|emacs|mac|win|tux}}
echo "Change the appearance of screenkey:
screenkey --bg-color "{{#a1b2c3}}" --font {{Hack}} --font-color {{yellow}} --opacity {{0.8}}
echo "Drag and select a window on screen to display screenkey:
screenkey --position fixed --geometry {{$(slop -n -f '%g')}}
echo "script
echo "Record all terminal output to file.
echo "More information: <https://manned.org/script>.
echo "Record a new session to a file named typescript in the current directory:
script
echo "Record a new session to a custom filepath:
script {{path/to/session.out}}
echo "Record a new session, appending to an existing file:
script -a {{path/to/session.out}}
echo "Record timing information (data is outputted to stderr):
script -t 2> {{path/to/timingfile}}
echo "scriptreplay
echo "Replay a typescript created by the script` command to `stdout.
echo "More information: <https://manned.org/scriptreplay>.
echo "Replay a typescript at the speed it was recorded:
scriptreplay {{path/to/timing_file}} {{path/to/typescript}}
echo "Replay a typescript at double the original speed:
scriptreplay {{path/to/timingfile}} {{path/to/typescript}} 2
echo "Replay a typescript at half the original speed:
scriptreplay {{path/to/timingfile}} {{path/to/typescript}} 0.5
echo "scrontab
echo "Manage Slurm crontab files.
echo "More information: <https://slurm.schedmd.com/scrontab.html>.
echo "Install a new crontab from the specified file:
scrontab {{path/to/file}}
echo "[e]dit the crontab of the current user:
scrontab -e
echo "[e]dit the crontab of the specified user:
scrontab --user={{user_id}} -e
echo "[r]emove the current crontab:
scrontab -r
echo "Print the crontab of the current user to stdout:
scrontab -l
echo "scrot
echo "Screen capture utility.
echo "More information: <https://github.com/resurrecting-open-source-projects/scrot>.
echo "Capture a screenshot and save it to the current directory with the current date as the filename:
scrot
echo "Capture a screenshot and save it as capture.png:
scrot {{capture.png}}
echo "Capture a screenshot interactively:
scrot --select
echo "Capture a screenshot interactively without exiting on keyboard input, press ESC to exit:
scrot --select --ignorekeyboard
echo "Capture a screenshot interactively delimiting the region with a colored line:
scrot --select --line color={{x11_color|rgb_color}}
echo "Capture a screenshot from the currently focused window:
scrot --focused
echo "Display a countdown of 10 seconds before taking a screenshot:
scrot --count --delay {{10}}
echo "sdiag
echo "Show information about the execution of slurmctld.
echo "More information: <https://slurm.schedmd.com/sdiag.html>.
echo "Show all performance counters related to the execution of slurmctld:
sdiag --all
echo "Reset performance counters related to the execution of slurmctld:
sdiag --reset
echo "Specify the output format:
sdiag --all --{{json|yaml}}
echo "Specify the cluster to send commands to:
sdiag --all --cluster={{cluster_name}}
echo "sed
echo "Edit text in a scriptable manner.
echo "See also: awk`, `ed.
echo "More information: <https://www.gnu.org/software/sed/manual/sed.html>.
echo "Replace all apple` (basic regex) occurrences with `mango` (basic regex) in all input lines and print the result to `stdout:
{{command}} | sed 's/apple/mango/g'
echo "Execute a specific script [f]ile and print the result to stdout:
{{command}} | sed -f {{path/to/script.sed}}
echo "Replace all apple` (extended regex) occurrences with `APPLE` (extended regex) in all input lines and print the result to `stdout:
{{command}} | sed -E 's/(apple)/\U\1/g'
echo "Print just a first line to stdout:
{{command}} | sed -n '1p'
echo "Replace all apple` (basic regex) occurrences with `mango (basic regex) in a specific file and overwrite the original file in place:
sed -i 's/apple/mango/g' {{path/to/file}}
echo "see
echo "Alias to run-mailcap's view.
echo "An alias to a run-mailcap's action print.
echo "More information: <https://manned.org/see>.
echo "See action can be used to view any file (usually image) on default mailcap explorer:
see {{filename}}
echo "Using with run-mailcap:
run-mailcap --action=view {{filename}}
echo "select
echo "Bash builtin construct for creating menus.
echo "More information: <https://www.gnu.org/software/bash/manual/bash.html#index-select>.
echo "Create a menu out of individual words:
select {{word}} in {{apple}} {{orange}} {{pear}} {{banana}}; do echo ${{word}}; done
echo "Create a menu for picking a file or folder from the current directory:
select {{file}} in *; do echo ${{file}}; done
echo "Create a menu from a Bash array:
{{fruits}}=({{apple}} {{orange}} {{pear}} {{banana}}); select {{word}} in ${{{fruits}}[@]}; do echo ${{word}}; done
echo "semanage fcontext
echo "Manage persistent SELinux security context rules on files/directories.
echo "See also: semanage`, `restorecon.
echo "More information: <https://manned.org/semanage-fcontext>.
echo "List all file labelling rules:
sudo semanage fcontext --list
echo "List all user-defined file labelling rules without headings:
sudo semanage fcontext --list --locallist --noheading
echo "Add a user-defined rule that labels any path which matches a PCRE regex:
sudo semanage fcontext --add --type {{samba_share_t}} {{'/mnt/share(/.*)?'}}
echo "Delete a user-defined rule using its PCRE regex:
sudo semanage fcontext --delete {{'/mnt/share(/.*)?'}}
echo "Relabel a directory recursively by applying the new rules:
restorecon -R -v {{path/to/directory}}
echo "semanage
echo "SELinux Policy Management tool.
echo "More information: <https://manned.org/semanage>.
echo "Output local customizations:
semanage -S {{store}} -o {{path/to/output_file}}
echo "Take a set of commands from a specified file and load them in a single transaction:
semanage -S {{store}} -i {{path/to/input_file}}
echo "Manage booleans. Booleans allow the administrator to modify the confinement of processes based on the current configuration:
semanage boolean -S {{store}} {{--delete|--modify|--list|--noheading|--deleteall}} {{-on|-off}} -F {{boolean|boolean_file}}
echo "Manage policy modules:
semanage module -S {{store}} {{--add|--delete|--list|--modify}} {{--enable|--disable}} {{module_name}}
echo "Disable/Enable dontaudit rules in policy:
semanage dontaudit -S {{store}} {{on|off}}
echo "sensible-browser
echo "Open the default browser.
echo "More information: <https://manned.org/sensible-browser>.
echo "Open a new window of the default browser:
sensible-browser
echo "Open a URL in the default browser:
sensible-browser {{url}}
echo "sensible-editor
echo "Open the default editor.
echo "More information: <https://manned.org/sensible-editor>.
echo "Open a file in the default editor:
sensible-editor {{path/to/file}}
echo "Open a file in the default editor, with the cursor at the end of the file:
sensible-editor + {{path/to/file}}
echo "Open a file in the default editor, with the cursor at the beginning of line 10:
sensible-editor +10 {{path/to/file}}
echo "Open 3 files in vertically split editor windows at the same time:
sensible-editor -O3 {{path/to/file1 path/to/file2 path/to/file3}}
echo "sensors
echo "Report sensors information.
echo "More information: <https://manned.org/sensors>.
echo "Show the current readings of all sensor chips:
sensors
echo "Show temperatures in degrees Fahrenheit:
sensors --fahrenheit
echo "service
echo "Manage services by running init scripts.
echo "The full script path should be omitted (/etc/init.d/ is assumed).
echo "More information: <https://manned.org/service>.
echo "List the name and status of all services:
service --status-all
# echo "Start/Stop/Restart/Reload service (start/stop should always be available):
# service {{service_name}} {{start|stop|restart|reload}}
# echo "Do a full restart (runs script twice with start and stop):
service {{service_name}} --full-restart
echo "Show the current status of a service:
service {{service_name}} status
echo "setcap
echo "Set capabilities of specified file.
echo "See also: tldr getcap.
echo "More information: <https://manned.org/setcap>.
echo "Set capability cap_net_raw (to use RAW and PACKET sockets) for a given file:
setcap '{{cap_net_raw}}' {{path/to/file}}
echo "Set multiple capabilities on a file (ep behind the capability means "effective permitted"):
setcap '{{cap_dac_read_search,cap_sys_tty_config+ep}}' {{path/to/file}}
echo "Remove all capabilities from a file:
setcap -r {{path/to/file}}
echo "Verify that the specified capabilities are currently associated with the specified file:
setcap -v '{{cap_net_raw}}' {{path/to/file}}
echo "The optional -n root_uid argument can be used to set the file capability for use only in a user namespace with this root user ID owner:
setcap -n {{root_uid}} '{{cap_net_admin}}' {{path/to/file}}
echo "setfacl
echo "Set file access control lists (ACL).
echo "More information: <https://manned.org/setfacl>.
echo "Modify ACL of a file for user with read and write access:
setfacl -m u:{{username}}:rw {{file}}
echo "Modify default ACL of a file for all users:
setfacl -d -m u::rw {{file}}
echo "Remove ACL of a file for a user:
setfacl -x u:{{username}} {{file}}
echo "Remove all ACL entries of a file:
setfacl -b {{file}}
echo "setserial
echo "Read and modify serial port information.
echo "More information: <https://manned.org/setserial>.
echo "Print all information about a specific serial device:
setserial -a {{/dev/cuaN}}
echo "Print the configuration summary of a specific serial device (useful for printing during bootup process):
setserial -b {{device}}
echo "Set a specific configuration parameter to a device:
sudo setserial {{device}} {{parameter}}
echo "Print the configuration of a list of devices:
setserial -g {{device1 device2 ...}}
echo "setsid
echo "Run a program in a new session if the calling process is not a process group leader.
echo "The created session is by default not controlled by the current terminal.
echo "More information: <https://manned.org/setsid>.
echo "Run a program in a new session:
setsid {{program}}
echo "Run a program in a new session discarding the resulting output and error:
setsid {{program}} > /dev/null 2>&1
echo "Run a program creating a new process:
setsid --fork {{program}}
echo "Return the exit code of a program as the exit code of setsid when the program exits:
setsid --wait {{program}}
echo "Run a program in a new session setting the current terminal as the controlling terminal:
setsid --ctty {{program}}
echo "setxkbmap
echo "Set the keyboard using the X Keyboard Extension.
echo "More information: <https://manned.org/setxkbmap>.
echo "Set the keyboard in French AZERTY:
setxkbmap {{fr}}
echo "Set multiple keyboard layouts, their variants and switching option:
setxkbmap -layout {{us,de}} -variant {{,qwerty}} -option {{'grp:alt_caps_toggle'}}
echo "Get help:
setxkbmap -help
echo "List all layouts:
localectl list-x11-keymap-layouts
echo "List variants for the layout:
localectl list-x11-keymap-variants {{de}}
echo "List available switching options:
localectl list-x11-keymap-options | grep grp:
echo "sfill
echo "Securely overwrite the free space and inodes of the partition where the specified directory resides.
echo "More information: <https://manned.org/sfill>.
echo "Overwrite free space and inodes of a disk with 38 writes (slow but secure):
sfill {{/path/to/mounted_disk_directory}}
echo "Overwrite free space and inodes of a disk with 6 writes (fast but less secure) and show status:
sfill -l -v {{/path/to/mounted_disk_directory}}
echo "Overwrite free space and inodes of a disk with 1 write (very fast but insecure) and show status:
sfill -ll -v {{/path/to/mounted_disk_directory}}
echo "Overwrite only free space of a disk:
sfill -I {{/path/to/mounted_disk_directory}}
echo "Overwrite only free inodes of a disk:
sfill -i {{/path/to/mounted_disk_directory}}
echo "sh5util
echo "Merge HDF5 files produced by the sacct_gather_profile plugin.
echo "More information: <https://slurm.schedmd.com/sh5util.html>.
echo "Merge HDF5 files produced on each allocated node for the specified job or step:
sh5util --jobs={{job_id|job_id.step_id}}
echo "Extract one or more data series from a merged job file:
sh5util --jobs={{job_id|job_id.step_id}} --extract -i {{path/to/file.h5}} --series={{Energy|Filesystem|Network|Task}}
echo "Extract one data item from all nodes in a merged job file:
sh5util --jobs={{job_id|job_id.step_id}} --item-extract --series={{Energy|Filesystem|Network|Task}} --data={{data_item}}
echo "shar
echo "Create a shell archive.
echo "More information: <https://www.gnu.org/software/sharutils/manual/sharutils.html>.
echo "Create a shell script that when executed extracts the given files from itself:
shar --vanilla-operation {{path/to/file1 path/to/file2 ...}} > {{path/to/archive.sh}}
echo "Compress the files in the archive:
shar --compactor {{xz}} {{path/to/file1 path/to/file2 ...}} > {{path/to/archive.sh}}
echo "Treat all files as binary (i.e. uuencode everything):
shar --uuencode {{path/to/file1 path/to/file2 ...}} > {{path/to/archive.sh}}
echo "Treat all files as text (i.e. uuencode nothing):
shar --text-files {{path/to/file1 path/to/file2 ...}} > {{path/to/archive.sh}}
echo "Include a name and cut mark in the header comment of the archive:
shar --archive-name "{{My files}}" --cut-mark {{path/to/file1 path/to/file2 ...}} > {{path/to/archive.sh}}
echo "sherlock
echo "Find usernames across social networks.
echo "More information: <https://github.com/sherlock-project/sherlock>.
echo "Search for a specific username on social networks saving the results to a file:
sherlock {{username}} --output {{path/to/file}}
echo "Search for specific usernames on social networks saving the results into a directory:
sherlock {{username1 username2 ...}} --folderoutput {{path/to/directory}}
echo "Search for a specific username on social networks using the Tor network:
sherlock --tor {{username}}
echo "Make requests over Tor with a new Tor circuit after each request:
sherlock --unique-tor {{username}}
echo "Search for a specific username on social networks using a proxy:
sherlock {{username}} --proxy {{proxy_url}}
echo "Search for a specific username on social networks and open results in the default web browser:
sherlock {{username}} --browse
echo "Display help:
sherlock --help
echo "shiny-mirrors
echo "Generate a pacman mirror list for Manjaro Linux.
echo "Every run of shiny-mirrors requires you to synchronize your database and update your system using sudo pacman -Syyu.
echo "More information: <https://gitlab.com/Arisa_Snowbell/shiny-mirrors/-/blob/domina/shiny-mirrors/man/shiny-mirrors.md>.
echo "Get the status of the current mirrors:
shiny-mirrors status
echo "Generate a mirror list using the default behavior:
sudo shiny-mirrors refresh
echo "Display the current configuration file:
shiny-mirrors config show
echo "Switch to a different branch interactively:
sudo shiny-mirrors config --branch
echo "shnsplit
echo "Splits audio files according to a .cue file.
echo "More information: <http://shnutils.freeshell.org/shntool/>.
echo "Split a .wav` + `.cue file into multiple files:
shnsplit -f {{path/to/file.cue}} {{path/to/file.wav}}
echo "Show supported formats:
shnsplit -a
echo "Split a .flac file into multiple files:
shnsplit -f {{path/to/file.cue}} -o flac {{path/to/file.flac}}
echo "Split a .wav file into files of the form "track-number - album - title":
shnsplit -f {{path/to/file.cue}} {{path/to/file.wav}} -t "%n - %a - %t
echo "shntool split
echo "This command is an alias of shnsplit.
echo "View documentation for the original command:
tldr shnsplit
echo "shutdown
echo "Shutdown and reboot the system.
echo "More information: <https://manned.org/shutdown.8>.
echo "Power off ([h]alt) immediately:
shutdown -h now
echo "[r]eboot immediately:
shutdown -r now
echo "[r]eboot in 5 minutes:
shutdown -r +{{5}} &
echo "Shutdown at 1:00 pm (Uses 24[h] clock):
shutdown -h 13:00
echo "[c]ancel a pending shutdown/reboot operation:
shutdown -c
echo "sic
echo "Simple IRC client.
echo "Part of the suckless tools.
echo "More information: <https://tools.suckless.org/sic/>.
echo "Connect to the default host (irc.ofct.net) with the nickname set in the $USER environment variable:
sic
echo "Connect to a given host, using a given nickname:
sic -h {{host}} -n {{nickname}}
echo "Connect to a given host, using a given nickname and password:
sic -h {{host}} -n {{nickname}} -k {{password}}
echo "Join a channel:
:j #{{channel}}<Enter>
echo "Send a message to a channel or user:
:m #{{channel|user}}<Enter>
echo "Set default channel or user:
:s #{{channel|user}}<Enter>
echo "silentcast
echo "Silent screencast creator. Saves in .mkv and animated GIF formats.
echo "More information: <https://github.com/colinkeenan/silentcast>.
echo "Launch silentcast:
silentcast
echo "Launch silentcast on a specific display:
silentcast --display={{display}}
echo "sinfo
echo "View information about Slurm nodes and partitions.
echo "See also squeue` and `sbatch, which are also part of the Slurm workload manager.
echo "More information: <https://slurm.schedmd.com/sinfo.html>.
echo "Show a quick summary overview of the cluster:
sinfo --summarize
echo "View the detailed status of all partitions across the entire cluster:
sinfo
echo "View the detailed status of a specific partition:
sinfo --partition {{partition_name}}
echo "View information about idle nodes:
sinfo --states {{idle}}
echo "Summarise dead nodes:
sinfo --dead
echo "List dead nodes and the reasons why:
sinfo --list-reasons
echo "size
echo "Displays the sizes of sections inside binary files.
echo "More information: <https://sourceware.org/binutils/docs/binutils/size.html>.
echo "Display the size of sections in a given object or executable file:
size {{path/to/file}}
echo "Display the size of sections in a given object or executable file in [o]ctal:
size {{-o|--radix=8}} {{path/to/file}}
echo "Display the size of sections in a given object or executable file in [d]ecimal:
size {{-d|--radix=10}} {{path/to/file}}
echo "Display the size of sections in a given object or executable file in he[x]adecimal:
size {{-x|--radix=16}} {{path/to/file}}
echo "slapt-get
echo "An apt like system for Slackware package management.
echo "Package sources need to be configured in the slapt-getrc file.
echo "More information: <https://software.jaos.org>.
echo "Update the list of available packages and versions:
slapt-get --update
echo "Install a package, or update it to the latest available version:
slapt-get --install {{package}}
echo "Remove a package:
slapt-get --remove {{package}}
echo "Upgrade all installed packages to their latest available versions:
slapt-get --upgrade
echo "Locate packages by the package name, disk set, or version:
slapt-get --search {{query}}
echo "Show information about a package:
slapt-get --show {{package}}
echo "slapt-src
echo "A utility to automate building of slackbuilds.
echo "SlackBuild sources need to be configured in the slapt-srcrc file.
echo "More information: <https://github.com/jaos/slapt-src>.
echo "Update the list of available slackbuilds and versions:
slapt-src --update
echo "List all available slackbuilds:
slapt-src --list
echo "Fetch, build and install the specified slackbuild(s):
slapt-src --install {{slackbuild_name}}
echo "Locate slackbuilds by their name or description:
slapt-src --search {{search_term}}
echo "Display information about a slackbuild:
slapt-src --show {{slackbuild_name}}
echo "sleep
echo "Delay for a specified amount of time.
echo "More information: <https://www.gnu.org/software/coreutils/sleep>.
echo "Delay in seconds:
sleep {{seconds}}
echo "Delay in [m]inutes. (Other units [d]ay, [h]our, [s]econd, [inf]inity can also be used):
sleep {{minutes}}m
echo "Delay for 1 [d]ay 3 [h]ours:
sleep 1d 3h
echo "Execute a specific command after 20 [m]inutes delay:
sleep 20m && {{command}}
echo "slop
echo "Get a selection of the screen.
echo "More information: <https://github.com/naelstrof/slop>.
echo "Wait for the user to make a selection and output its geometry to stdout:
slop
echo "Double click, rather than click and drag, to draw a selection:
slop -D
echo "Highlight the selection rather than outlining it:
slop -l
echo "Specify the output format:
slop -f {{format_string}}
echo "Specify the selection rectangle's color:
slop -c {{red}},{{green}},{{blue}},{{alpha}}
echo "slurmctld
echo "Monitor all other Slurm daemons and resources, accept work (jobs), and allocate resources to those jobs.
echo "More information: <https://slurm.schedmd.com/slurmctld.html>.
echo "Clear all previous slurmctld states from its last checkpoint:
slurmctld -c
echo "Set the daemon's nice value to the specified value, typically a negative number:
slurmctld -n {{value}}
echo "Write log messages to the specified file:
slurmctld -L {{path/to/output_file}}
echo "Display help:
slurmctld -h
echo "Display version:
slurmctld -V
echo "slurmd
echo "Monitors all tasks running on the compute node, accepts tasks, launches tasks, and kills running tasks upon request.
echo "More information: <https://slurm.schedmd.com/slurmd.html>.
echo "Report node rebooted when daemon restarted (Used for testing purposes):
slurmd -b
echo "Run the daemon with the given nodename:
slurmd -N {{nodename}}
echo "Write log messages to the specified file:
slurmd -L {{path/to/output_file}}
echo "Read configuration from the specified file:
slurmd -f {{path/to/file}}
echo "Display help:
slurmd -h
echo "slurmdbd
echo "Provides a secure enterprise-wide interface to a database for Slurm.
echo "More information: <https://slurm.schedmd.com/slurmdbd.html>.
echo "Set the daemon's nice value to the specified value, typically a negative number:
slurmdbd -n {{value}}
echo "Change the working directory of slurmdbd` to the LogFile path or to `/var/tmp:
slurmdbd -s
echo "Display help:
slurmdbd -h
echo "Display version:
slurmdbd -V
echo "slurmrestd
echo "Interface to Slurm via REST API. It can be used in two modes: *Inetd Mode* & *Listen Mode*.
echo "More information: <https://slurm.schedmd.com/slurmrestd.html>.
echo "Change the group ID (and drop supplemental groups) before processing client requests:
slurmrestd --g {{group_id}} {{[host]:port | unix:/path/to/socket}}
echo "Comma-delimited list of authentication plugins to load:
slurmrestd -a {{authentication_plugins}} {{[host]:port | unix:/path/to/socket}}
echo "Read Slurm configuration from the specified file:
slurmrestd -f {{path/to/file}}
echo "Change user ID before processing client request:
slurmrestd -u {{user_id}}
echo "Display help:
slurmrestd -h
echo "Display version:
slurmrestd -V
echo "slurmstepd
echo "Slurm daemon for managing and monitoring individual job steps within a multi-step job.
echo "It should not be invoked manually.
echo "More information: <https://slurm.schedmd.com/slurmstepd.html>.
echo "Start the daemon:
slurmstepd
echo "sm
echo "Displays a short message fullscreen.
echo "More information: <https://github.com/nomeata/screen-message>.
echo "Display a message in full-screen:
sm "{{Hello World!}}"
echo "Display a message with inverted colors:
sm -i "{{Hello World!}}"
echo "Display a message with a custom foreground color:
sm -f {{blue}} "{{Hello World!}}"
echo "Display a message with a custom background color:
sm -b {{#008888}} "{{Hello World!}}"
echo "Display a message rotated 3 times (in steps of 90 degrees, counterclockwise):
sm -r {{3}} "{{Hello World!}}"
echo "Display a message using the output from another command:
{{echo "Hello World!"}} | sm -
echo "smbclient
echo "FTP-like client to access SMB/CIFS resources on servers.
echo "More information: <https://manned.org/smbclient>.
echo "Connect to a share (user will be prompted for password; exit to quit the session):
smbclient {{//server/share}}
echo "Connect with a different username:
smbclient {{//server/share}} --user {{username}}
echo "Connect with a different workgroup:
smbclient {{//server/share}} --workgroup {{domain}} --user {{username}}
echo "Connect with a username and password:
smbclient {{//server/share}} --user {{username%password}}
echo "Download a file from the server:
smbclient {{//server/share}} --directory {{path/to/directory}} --command "get {{file.txt}}"
echo "Upload a file to the server:
smbclient {{//server/share}} --directory {{path/to/directory}} --command "put {{file.txt}}"
echo "List the shares from a server anonymously:
smbclient --list={{server}} --no-pass
echo "smbget
echo "wget-like utility for downloading files from SMB servers.
echo "More information: <https://www.samba.org/samba/docs/current/man-html/smbget.1.html>.
echo "Download a file from a server:
smbget {{smb://server/share/file}}
echo "Download a share or directory recursively:
smbget --recursive {{smb://server/share}}
echo "Connect with a username and password:
smbget {{smb://server/share/file}} --user {{username%password}}
echo "Require encrypted transfers:
smbget {{smb://server/share/file}} --encrypt
echo "smbmap
echo "SMB enumeration tool.
echo "More information: <https://github.com/ShawnDEvans/smbmap>.
echo "Display SMB shares and permissions on a host, prompting for user's password or NTLM hash:
smbmap -u {{username}} --prompt -H {{ip}}
echo "Display SMB shares and permissions on a host, specifying the domain and passing the password NTLM hash:
smbmap -u {{username}} --prompt -d {{domain}} -H {{ip}}
echo "Display SMB shares and list a single level of directories and files:
smbmap -u {{username}} --prompt -H {{ip}} -r
echo "Display SMB shares and recursively list a defined number of levels of directories and files:
smbmap -u {{username}} --prompt -H {{ip}} -R --depth {{3}}
echo "Display SMB shares and recursively list directories and files, downloading the files matching a regular expression:
smbmap -u {{username}} --prompt -H {{ip}} -R -A {{pattern}}
echo "Display SMB shares and recursively list directories and files, searching for file content matching a regular expression:
smbmap -u {{username}} --prompt -H {{ip}} -R -F {{pattern}}
echo "Execute a shell command on a remote system:
smbmap -u {{username}} --prompt -H {{ip}} -x {{command}}
echo "Upload a file to a remote system:
smbmap -u {{username}} --prompt -H {{ip}} --upload {{source}} {{destination}}
echo "smbnetfs
echo "Mount SMB shares interactively.
echo "More information: <https://sourceforge.net/projects/smbnetfs/>.
echo "Make shares available at mountpoint:
smbnetfs {{mountpoint}}
echo "smbpasswd
echo "Add/remove a Samba user or change its password.
echo "Samba users must have an existing local Unix account.
echo "More information: <https://manned.org/smbpasswd.8>.
echo "Change the current user's SMB password:
smbpasswd
echo "Add a specified user to Samba and set password (user should already exist in system):
sudo smbpasswd -a {{username}}
echo "Modify an existing Samba user's password:
sudo smbpasswd {{username}}
echo "Delete a Samba user (use pdbedit instead if the Unix account has been deleted):
sudo smbpasswd -x {{username}}
echo "smem
echo "Print memory usage for programs.
echo "More information: <https://manned.org/smem>.
echo "Print memory usage for current processes:
smem
echo "Print memory usage for current processes for a every user on a system:
smem --users
echo "Print memory usage for current processes for a specified user:
smem --userfilter {{username}}
echo "Print system memory information:
smem --system
echo "snake4
echo "Snake game in the terminal.
echo "More information: <https://manpages.debian.org/latest/snake4/snake4.6.en.html>.
echo "Start a snake game:
snake4
echo "Choose level:
{{1|2|3|4|5}}
echo "Navigate the snake:
{{Up|Down|Left|Right arrow key}}
echo "Pause game:
<Spacebar>
echo "Quit game:
q
echo "Show the high scores:
snake4 --highscores
echo "snake4scores
echo "Show the high scores from the snake4 game.
echo "More information: <https://manpages.debian.org/snake4/snake4.6.en.html>.
echo "Show the highscores:
snake4scores
echo "snap
echo "Manage the "snap" self-contained software packages.
echo "Similar to what apt` is for `.deb.
echo "More information: <https://manned.org/snap>.
echo "Search for a package:
snap find {{query}}
echo "Install a package:
snap install {{package}}
echo "Update a package:
snap refresh {{package}}
echo "Update a package to another channel (track, risk, or branch):
snap refresh {{package}} --channel={{channel}}
echo "Update all packages:
snap refresh
echo "Display basic information about installed snap software:
snap list
echo "Uninstall a package:
snap remove {{package}}
echo "Check for recent snap changes in the system:
snap changes
echo "snapper
echo "Filesystem snapshot management tool.
echo "More information: <http://snapper.io/manpages/snapper.html>.
echo "List snapshot configs:
snapper list-configs
echo "Create snapper config:
snapper -c {{config}} create-config {{path/to/directory}}
echo "Create a snapshot with a description:
snapper -c {{config}} create -d "{{snapshot_description}}"
echo "List snapshots for a config:
snapper -c {{config}} list
echo "Delete a snapshot:
snapper -c {{config}} delete {{snapshot_number}}
echo "Delete a range of snapshots:
snapper -c {{config}} delete {{snapshot_X}}-{{snapshot_Y}}
echo "snmpwalk
echo "SNMP query tool.
echo "More information: <https://manned.org/snmpwalk>.
echo "Query the system information of a remote host using SNMPv1 and a community string:
snmpwalk -v1 -c {{community}} {{ip}}
echo "Query system information on a remote host by OID using SNMPv2 on a specified port:
snmpwalk -v2c -c {{community}} {{ip}}:{{port}} {{oid}}
echo "Query system information on a remote host by OID using SNMPv3 and authentication without encryption:
snmpwalk -v3 -l {{authNoPriv}} -u {{username}} -a {{MD5|SHA}} -A {{passphrase}} {{ip}} {{oid}}
echo "Query system information on a remote host by OID using SNMPv3, authentication, and encryption:
snmpwalk -v3 -l {{authPriv}} -u {{username}} -a {{MD5|SHA}} -A {{auth_passphrase}} -x {{DES|AES}} -X {{enc_passphrase}} {{ip}} {{oid}}
echo "Query system information on a remote host by OID using SNMPv3 without authentication or encryption:
snmpwalk -v3 -l {{noAuthNoPriv}} -u {{username}} {{ip}} {{oid}}
echo "spectre-meltdown-checker
echo "Spectre and Meltdown mitigation detection tool.
echo "More information: <https://manned.org/spectre-meltdown-checker>.
echo "Check the currently running kernel for Spectre or Meltdown:
sudo spectre-meltdown-checker
echo "Check the currently running kernel and show an explanation of the actions to take to mitigate a vulnerability:
sudo spectre-meltdown-checker --explain
echo "Check for specific variants (defaults to all):
sudo spectre-meltdown-checker --variant {{1|2|3|3a|4|l1tf|msbds|mfbds|mlpds|mdsum|taa|mcespc|srbds}}
echo "Display output using a specific output format:
sudo spectre-meltdown-checker --batch {{text|json|nrpe|prometheus|short}}
echo "Don't use the /sys interface even if present:
sudo spectre-meltdown-checker --no-sysfs
echo "Check a non-running kernel:
sudo spectre-meltdown-checker --kernel {{path/to/kernel_file}}
echo "speedometer
echo "Python script that shows a network traffic graph in the terminal.
echo "More information: <http://excess.org/speedometer>.
echo "Show graph for a specific interface:
speedometer -r {{eth0}} -t {{eth0}}
echo "speedread
echo "A simple terminal-based open source Spritz-alike.
echo "Shows input text as a per-word RSVP (rapid serial visual presentation) aligned on optimal reading points, which allows reading text at a much more rapid pace than usual as the eye can stay fixed on a single place.
echo "More information: <https://github.com/pasky/speedread>.
echo "Read a text file at a specific speed:
cat {{path/to/file.txt}} | speedread -wpm {{250}}
echo "Resume from a specific line:
cat {{path/to/file.txt}} | speedread -resume {{5}}
echo "Show multiple words at a time:
cat {{path/to/file.txt}} | speedread -multiword
echo "Slow down by 10% during the reading session:
[
echo "Speed up by 10% during the reading session:
]
echo "Pause, and show the last few lines as context:
<space>
echo "spi
echo "A meta package manager that handles both packages and slackbuilds.
echo "More information: <https://github.com/gapan/spi>.
echo "Update the list of available packages and slackbuilds:
spi --update
echo "Install a package or slackbuild:
spi --install {{package/slackbuild_name}}
echo "Upgrade all installed packages to the latest versions available:
spi --upgrade
echo "Locate packages or slackbuilds by package name or description:
spi {{search_terms}}
echo "Display information about a package or slackbuild:
spi --show {{package/slackbuild_name}}
echo "Purge the local package and slackbuild caches:
spi --clean
echo "sport
echo "Search and install SlackBuilds.
echo "More information: <http://slackermedia.info/handbook/doku.php?id=slackbuilds>.
echo "Pull the list of SlackBuilds to run sport for the first time:
sudo mkdir -p /usr/ports && sudo rsync -av rsync://slackbuilds.org /slackbuilds/$(awk '{print $2}' /etc/slackware-version)/ /usr/ports/
echo "Pull in any updates to the system's tree via rsync:
sudo sport rsync
echo "Search for a package by name:
sport search "{{keyword}}"
echo "Check if a package is installed:
sport check {{package}}
echo "Display README and .info files of a package:
sport cat {{package}}
echo "Install a package once the dependencies are resolved:
sudo sport install {{package}}
echo "Install a list of packages from a file (format: packages separated by spaces):
sudo sport install $(< {{path/to/list}})
echo "sprio
echo "View the factors determining a job's scheduling priority.
echo "More information: <https://slurm.schedmd.com/sprio.html>.
echo "View the factors determining the scheduling priority of all jobs:
sprio
echo "View the factors determining the specified job's scheduling priority:
sprio --jobs={{job_id_1,job_id_2,...}}
echo "Output additional information:
sprio --long
echo "View information for the jobs of specified users:
sprio --user={{user_name_1,user_name_2,...}}
echo "Print the weights for each factor determining job scheduling priority:
sprio --weights
echo "sqfscat
echo "Concatenate files from a squashfs filesystem and print them to stdout.
echo "More information: <https://manned.org/sqfscat>.
echo "Display the contents of one or more files from a squashfs filesystem:
sqfscat {{filesystem.squashfs}} {{file1 file2 ...}}
echo "sqfstar
echo "Create a squashfs filesystem from a tar archive.
echo "More information: <https://manned.org/sqfstar>.
echo "Create a squashfs filesystem (compressed using gzip by default) from an uncompressed tar archive:
sqfstar {{filesystem.squashfs}} < {{archive.tar}}
echo "Create a squashfs filesystem from a tar archive compressed with gzip, and [comp]ress the filesystem using a specific algorithm:
zcat {{archive.tar.gz}} | sqfstar -comp {{gzip|lzo|lz4|xz|zstd|lzma}} {{filesystem.squashfs}}
echo "Create a squashfs filesystem from a tar archive compressed with xz, excluding some of the files:
xzcat {{archive.tar.xz}} | sqfstar {{filesystem.squashfs}} {{file1 file2 ...}}
echo "Create a squashfs filesystem from a tar archive compressed with zstd`, excluding files ending with `.gz:
zstdcat {{archive.tar.zst}} | sqfstar {{filesystem.squashfs}} "{{*.gz}}"
echo "Create a squashfs filesystem from a tar archive compressed with lz4, excluding files matching a regular expression:
lz4cat {{archive.tar.lz4}} | sqfstar {{filesystem.squashfs}} -regex "{{regular_expression}}"
echo "squeue
echo "View the jobs queued in the SLURM scheduler.
echo "More information: <https://manned.org/squeue>.
echo "View the queue:
squeue
echo "View jobs queued by a specific user:
squeue -u {{username}}
echo "View the queue and refresh every 5 seconds:
squeue -i {{5}}
echo "View the queue with expected start times:
squeue --start
echo "sreport
echo "Generate reports on jobs, users, and clusters from accounting data.
echo "More information: <https://slurm.schedmd.com/sreport.html>.
echo "Show pipe delimited cluster utilization data:
sreport --parsable cluster utilization
echo "Show number of jobs run:
sreport job sizes printjobcount
echo "Show users with the highest CPU time use:
# sreport user topuser
echo "srun
echo "Create an interactive slurm job or connect to an existing job.
echo "More information: <https://slurm.schedmd.com/srun.html>.
echo "Submit a basic interactive job:
srun --pty /bin/bash
echo "Submit an interactive job with different attributes:
srun --ntasks-per-node={{num_cores}} --mem-per-cpu={{memory_MB}} --pty /bin/bash
echo "Connect to a worker node with a job running:
srun --jobid={{job_id}} --pty /bin/bash
echo "ss
echo "Utility to investigate sockets.
echo "More information: <https://manned.org/ss.8>.
echo "Show all TCP/UDP/RAW/UNIX sockets:
ss -a {{-t|-u|-w|-x}}
echo "Filter TCP sockets by states, only/exclude:
ss {{state/exclude}} {{bucket/big/connected/synchronized/...}}
echo "Show all TCP sockets connected to the local HTTPS port (443):
ss -t src :{{443}}
echo "Show all TCP sockets listening on the local 8080 port:
ss -lt src :{{8080}}
echo "Show all TCP sockets along with processes connected to a remote ssh port:
ss -pt dst :{{ssh}}
echo "Show all UDP sockets connected on specific source and destination ports:
ss -u 'sport == :{{source_port}} and dport == :{{destination_port}}'
echo "Show all TCP IPv4 sockets locally connected on the subnet 192.168.0.0/16:
ss -4t src {{192.168/16}}
echo "Kill IPv4 or IPv6 Socket Connection with destination IP 192.168.1.17 and destination port 8080:
ss --kill dst {{192.168.1.17}} dport = {{8080}}
echo "sshare
echo "List the shares of associations to a cluster.
echo "More information: <https://slurm.schedmd.com/sshare.html>.
echo "List Slurm share information:
sshare
echo "Control the output format:
sshare --{{parsable|parsable2|json|yaml}}
echo "Control the fields to display:
sshare --format={{format_string}}
echo "Display information for the specified users only:
sshare --users={{user_id_1,user_id_2,...}}
echo "sstat
echo "View information about running jobs.
echo "More information: <https://slurm.schedmd.com/sstat.html>.
echo "Display status information of a comma-separated list of jobs:
sstat --jobs={{job_id}}
echo "Display job ID, average CPU and average virtual memory size of a comma-separated list of jobs, with pipes as column delimiters:
sstat --parsable --jobs={{job_id}} --format={{JobID}},{{AveCPU}},{{AveVMSize}}
echo "Display list of fields available:
sstat --helpformat
echo "st
echo "A simple terminal emulator for the X Window System.
echo "More information: <https://st.suckless.org>.
echo "Open a terminal:
st
echo "Open a terminal with a specific title:
st -T {{title}}
echo "Open a terminal, execute a given command, and write the output to a file:
st -o {{path/to/file}} -e {{command argument1 argument2}}
echo "Increase/decrease the font size:
<Ctrl> + <Shift> + {{Page Up|Page Down}}
echo "Copy/paste from the clipboard:
<Ctrl> + <Shift> + {{C|V}}
echo "startx
echo "A front-end to xinit that provides a nice user interface for running a single session of the X Window System.
echo "More information: <https://x.org/releases/X11R7.5/doc/man/man1/startx.1.html>.
echo "Start an X session:
startx
echo "Start an X session with a predefined depth value:
startx -- -depth {{value}}
echo "Start an X session with a predefined dpi value:
startx -- -dpi {{value}}
echo "Override the settings in the .xinitrc file and start a new X session:
# startx /{{path/to/window_manager_or_desktop_environment}}
echo "steghide
echo "Steganography tool for JPEG, BMP, WAV and AU file formats.
echo "More information: <https://github.com/StefanoDeVuono/steghide>.
echo "Embed data in a PNG, prompting for a passphrase:
steghide embed --coverfile {{path/to/image.png}} --embedfile {{path/to/data.txt}}
echo "Extract data from a WAV audio file:
steghide extract --stegofile {{path/to/sound.wav}}
echo "Display file information, trying to detect an embedded file:
steghide info {{path/to/file.jpg}}
echo "Embed data in a JPEG image, using maximum compression:
steghide embed --coverfile {{path/to/image.jpg}} --embedfile {{path/to/data.txt}} --compress {{9}}
echo "Get the list of supported encryption algorithms and modes:
steghide encinfo
echo "Embed encrypted data in a JPEG image, e.g. with Blowfish in CBC mode:
steghide embed --coverfile {{path/to/image.jpg}} --embedfile {{path/to/data.txt}} --encryption {{blowfish|...}} {{cbc|...}}
echo "stegsnow
echo "Steganography tool for concealing and extracting messages in text files encoded as tabs and spaces.
echo "More information: <https://darkside.com.au/snow/manual.html>.
echo "Extract [m]essage from file:
stegsnow {{path/to/file.txt}}
echo "Extract [C]ompressed and [p]assword protected [m]essage from file:
stegsnow -C -p {{password}} {{path/to/file.txt}}
echo "Determine approximate [S]torage capacity with line [l]ength less than 72 for file:
stegsnow -S -l 72 {{path/to/file.txt}}
echo "Conceal [m]essage in text from file and save to result:
stegsnow -m '{{message}}' {{path/to/file.txt}} {{path/to/result.txt}}
echo "Conceal message [f]ile content [C]ompressed in text from file and save to result:
stegsnow -C -f '{{path/to/message.txt}}' {{path/to/file.txt}} {{path/to/result.txt}}
echo "Conceal [m]essage [C]ompressed and [p]assword protected in text from file and save to result:
stegsnow -C -p {{password}} -m '{{message}}' {{path/to/file.txt}} {{path/to/result.txt}}
echo "strace
echo "Troubleshooting tool for tracing system calls.
echo "More information: <https://manned.org/strace>.
echo "Start tracing a specific process by its PID:
strace -p {{pid}}
echo "Trace a process and filter output by system call:
strace -p {{pid}} -e {{system_call_name}}
echo "Count time, calls, and errors for each system call and report a summary on program exit:
strace -p {{pid}} -c
echo "Show the time spent in every system call:
strace -p {{pid}} -T
echo "Start tracing a program by executing it:
strace {{program}}
echo "Start tracing file operations of a program:
strace -e trace=file {{program}}
echo "stress
echo "A tool to stress test CPU, memory, and IO on a Linux system.
echo "More information: <https://manned.org/stress>.
echo "Spawn 4 workers to stress test CPU:
stress -c {{4}}
echo "Spawn 2 workers to stress test IO and timeout after 5 seconds:
stress -i {{2}} -t {{5}}
echo "Spawn 2 workers to stress test memory (each worker allocates 256M bytes):
stress -m {{2}} --vm-bytes {{256M}}
echo "Spawn 2 workers spinning on write()/unlink() (each worker writes 1G bytes):
stress -d {{2}} --hdd-bytes {{1GB}}
echo "strigger
echo "View or modify Slurm trigger information.
echo "Triggers are actions that are automatically run when a given event occurs on a Slurm cluster.
echo "More information: <https://slurm.schedmd.com/strigger.html>.
echo "Register a new trigger. Execute the specified program when the specified event occurs:
strigger --set --{{primary_database_failure|primary_slurmdbd_failure|primary_slurmctld_acct_buffer_full|primary_slurmctld_failure|...}} --program={{path/to/executable}}
echo "Execute the specified program when the specified job terminated:
strigger --set --jobid={{job_id}} --fini --program="{{path/to/executable}} {{argument1 argument2 ...}}"
echo "View active triggers:
strigger --get
echo "View active triggers regarding the specified job:
strigger --get --jobid={{job_id}}
echo "Clear the specified trigger:
strigger --clear {{trigger_id}}
echo "strip
echo "Discard symbols from executables or object files.
echo "More information: <https://manned.org/strip>.
echo "Replace the input file with its stripped version:
strip {{path/to/file}}
echo "Strip symbols from a file, saving the output to a specific file:
strip {{path/to/input_file}} -o {{path/to/output_file}}
echo "Strip debug symbols only:
strip --strip-debug {{path/to/file.o}}
echo "sview
echo "Start a GUI to view and modify the state of Slurm.
echo "More information: <https://slurm.schedmd.com/sview.html>.
echo "Start a GUI to view and modify the state of Slurm:
sview
echo "swaks
echo "Swiss Army Knife SMTP, the all-purpose SMTP transaction tester.
echo "More information: <https://github.com/jetmore/swaks/blob/develop/doc/base.pod>.
echo "Deliver a standard test email to user@example.com` on port 25 of `test-server.example.net:
swaks --to {{user@example.com}} --server {{test-server.example.net}}
echo "Deliver a standard test email, requiring CRAM-MD5 authentication as user me@example.com. An "X-Test" header will be added to the email body:
swaks --to {{user@example.com}} --from {{me@example.com}} --auth {{CRAM-MD5}} --auth-user {{me@example.com}} --header-X-Test "{{test_email}}"
echo "Test a virus scanner using EICAR in an attachment. Don't show the message DATA part:
swaks -t {{user@example.com}} --attach - --server {{test-server.example.com}} --suppress-data {{path/to/eicar.txt}}
echo "Test a spam scanner using GTUBE in the body of an email, routed via the MX records for example.com:
swaks --to {{user@example.com}} --body {{path/to/gtube_file}}
echo "Deliver a standard test email to user@example.com using the LMTP protocol via a UNIX domain socket file:
swaks --to {{user@example.com}} --socket {{/var/lda.sock}} --protocol {{LMTP}}
echo "swaplabel
echo "Print or change the label or UUID of a swap area.
echo "Note: path/to/file can either point to a regular file or a swap partition.
echo "More information: <https://manned.org/swaplabel>.
echo "Display the current label and UUID of a swap area:
swaplabel {{path/to/file}}
echo "Set the label of a swap area:
swaplabel --label {{new_label}} {{path/to/file}}
echo "Set the UUID of a swap area (you can generate a UUID using uuidgen):
swaplabel --uuid {{new_uuid}} {{path/to/file}}
echo "swapoff
echo "Disable devices and files for swapping.
echo "Note: path/to/file can either point to a regular file or a swap partition.
echo "More information: <https://manned.org/swapoff>.
echo "Disable a given swap area:
swapoff {{path/to/file}}
echo "Disable all swap areas in /proc/swaps:
swapoff --all
echo "Disable a swap partition by its label:
swapoff -L {{label}}
echo "swapon
echo "Enable devices and files for swapping.
echo "Note: path/to/file can either point to a regular file or a swap partition.
echo "More information: <https://manned.org/swapon>.
echo "Show swap information:
swapon
echo "Enable a given swap area:
swapon {{path/to/file}}
echo "Enable all swap areas specified in /etc/fstab` except those with the `noauto option:
swapon --all
echo "Enable a swap partition by its label:
swapon -L {{label}}
echo "swaybg
echo "Wallpaper tool for Wayland compositors.
echo "More information: <https://github.com/swaywm/swaybg/blob/master/swaybg.1.scd>.
echo "Set the wallpaper to an [i]mage:
swaybg --image {{path/to/image}}
echo "Set the wallpaper [m]ode:
swaybg --image {{path/to/image}} --mode {{stretch|fit|fill|center|tile|solid_color}}
echo "Set the wallpaper to a static [c]olor:
swaybg --color {{"#rrggbb"}}
echo "swayidle
echo "Idle management daemon for Wayland.
echo "Note: the configuration options are documented in its man page.
echo "More information: <https://github.com/swaywm/swayidle/blob/master/swayidle.1.scd>.
echo "Listen for idle activity using the configuration in $XDG_CONFIG_HOME/swayidle/config` or `$HOME/swayidle/config:
swayidle
echo "Specify an alternative path to the configuration file:
swayidle -C {{path/to/file}}
echo "swaylock
echo "Screen locking utility for Wayland compositors.
echo "More information: <https://manned.org/swaylock>.
echo "Lock the screen showing a white background:
swaylock
echo "Lock the screen with a simple color background (rrggbb format):
swaylock --color {{0000ff}}
echo "Lock the screen to a PNG background:
swaylock --image {{path/to/file.png}}
echo "Lock the screen and disable the unlock indicator (removes feedback on keypress):
swaylock --no-unlock-indicator
echo "Lock the screen and don't hide the mouse pointer:
swaylock --pointer {{default}}
echo "Lock the screen to a PNG background tiled over all monitors:
swaylock --image {{path/to/file.png}} --tiling
echo "Lock the screen and show the number of failed login attempts:
swaylock --show-failed-attempts
echo "Load configuration from a file:
swaylock --config {{path/to/config}}
echo "swupd
echo "Package management utility for Clear Linux.
echo "More information: <https://docs.01.org/clearlinux/latest/guides/clear/swupd.html>.
echo "Update to the latest version:
sudo swupd update
echo "Show current version, and check whether a newer one exists:
swupd check-update
echo "List installed bundles:
swupd bundle-list
echo "Locate the bundle where a wanted package exists:
swupd search -b {{package}}
echo "Install a new bundle:
sudo swupd bundle-add {{bundle}}
echo "Remove a bundle:
sudo swupd bundle-remove {{bundle}}
echo "Correct broken or missing files:
sudo swupd verify
echo "sxiv
echo "Simple X Image Viewer.
echo "More information: <https://github.com/muennich/sxiv>.
echo "Open an image:
sxiv {{path/to/file}}
echo "Open an image in fullscreen mode:
sxiv -f {{path/to/file}}
echo "Open a newline-separated list of images, reading filenames from stdin:
echo {{path/to/file}} | sxiv -i
echo "Open a space-separated list of images as a slideshow:
sxiv -S {{seconds}} {{path/to/file}}
echo "Open a space-separated list of images in thumbnail mode:
sxiv -t {{path/to/file}}
echo "synopkg
echo "Package management utility for Synology DiskStation Manager.
echo "More information: <https://www.synology.com/dsm>.
echo "List the names of installed packages:
synopkg list --name
echo "List packages which depend on a specific package:
synopkg list --depend-on {{package}}
# echo "Start/Stop a package:
# sudo synopkg {{start|stop}} {{package}}
echo "Print the status of a package:
synopkg status {{package}}
echo "Uninstall a package:
sudo synopkg uninstall {{package}}
echo "Check if updates are available for a package:
synopkg checkupdate {{package}}
echo "Upgrade all packages to the latest version:
sudo synopkg upgradeall
echo "Install a package from a synopkg file:
sudo synopkg install {{path/to/package.spk}}
echo "synoupgrade
echo "Upgrade Synology DiskStation Manager (DSM) - the Synology NAS operating system.
echo "More information: <https://www.synology.com/dsm>.
echo "Check if upgrades are available:
sudo synoupgrade --check
echo "Check for patches without upgrading the DSM version:
sudo synoupgrade --check-smallupdate
echo "Download the latest upgrade available (use --download-smallupdate for patches):
sudo synoupgrade --download
echo "Start the upgrade process:
sudo synoupgrade --start
echo "Upgrade to the latest version automatically:
sudo synoupgrade --auto
echo "Apply patches without upgrading the DSM version automatically:
sudo synoupgrade --auto-smallupdate
echo "Upgrade the DSM using a patch file (should be an absolute path):
sudo synoupgrade --patch {{/path/to/file.pat}}
echo "Display help:
synoupgrade
echo "sysctl
echo "List and change kernel runtime variables.
echo "More information: <https://manned.org/sysctl.8>.
echo "Show all available variables and their values:
sysctl -a
echo "Set a changeable kernel state variable:
sysctl -w {{section.tunable}}={{value}}
echo "Get currently open file handlers:
sysctl fs.file-nr
echo "Get limit for simultaneous open files:
sysctl fs.file-max
echo "Apply changes from /etc/sysctl.conf:
sysctl -p
echo "systemctl
echo "Control the systemd system and service manager.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemctl.html>.
echo "Show all running services:
systemctl status
echo "List failed units:
systemctl --failed
# echo "Start/Stop/Restart/Reload a service:
# systemctl {{start|stop|restart|reload}} {{unit}}
echo "Show the status of a unit:
systemctl status {{unit}}
echo "Enable/Disable a unit to be started on bootup:
systemctl {{enable|disable}} {{unit}}
echo "Mask/Unmask a unit to prevent enablement and manual activation:
systemctl {{mask|unmask}} {{unit}}
echo "Reload systemd, scanning for new or changed units:
systemctl daemon-reload
echo "Check if a unit is enabled:
systemctl is-enabled {{unit}}
echo "systemd-ac-power
echo "Report whether the computer is connected to an external power source.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-ac-power.html>.
echo "Silently check and return a 0 status code when running on AC power, and a non-zero code otherwise:
systemd-ac-power
echo "Additionally print yes` or `no` to `stdout:
systemd-ac-power --verbose
echo "systemd-analyze
echo "Analyze and debug system manager.
echo "Show timing details about the boot process of units (services, mount points, devices, sockets).
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-analyze.html>.
echo "List all running units, ordered by the time they took to initialize:
systemd-analyze blame
echo "Print a tree of the time-critical chain of units:
systemd-analyze critical-chain
echo "Create an SVG file showing when each system service started, highlighting the time that they spent on initialization:
systemd-analyze plot > {{path/to/file.svg}}
echo "Plot a dependency graph and convert it to an SVG file:
systemd-analyze dot | dot -T{{svg}} > {{path/to/file.svg}}
echo "Show security scores of running units:
systemd-analyze security
echo "systemd-ask-password
echo "Query the user for a system password.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-ask-password.html>.
echo "Query a system password with a specific message:
systemd-ask-password "{{message}}"
echo "Specify an identifier for the password query:
systemd-ask-password --id={{identifier}} "{{message}}"
echo "Use a kernel keyring key name as a cache for the password:
systemd-ask-password --keyname={{key_name}} "{{message}}"
echo "Set a custom timeout for the password query:
systemd-ask-password --timeout={{seconds}} "{{message}}"
echo "Force the use of an agent system and never ask on current TTY:
systemd-ask-password --no-tty "{{message}}"
echo "Store a password in the kernel keyring without displaying it:
systemd-ask-password --no-output --keyname={{key_name}} "{{message}}"
echo "systemd-cat
echo "Connect a pipeline or program's output streams with the systemd journal.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-cat.html>.
echo "Write the output of the specified command to the journal (both output streams are captured):
systemd-cat {{command}}
echo "Write the output of a pipeline to the journal (stderr stays connected to the terminal):
{{command}} | systemd-cat
echo "systemd-cgls
echo "Show the contents of the selected Linux control group hierarchy in a tree.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-cgls.html>.
echo "Display the whole control group hierarchy on your system:
systemd-cgls
echo "Display a control group tree of a specific resource controller:
systemd-cgls {{cpu|memory|io}}
echo "Display the control group hierarchy of one or more systemd units:
systemd-cgls --unit {{unit1 unit2 ...}}
# echo "systemd-cgtop
# echo "Show the top control groups of the local Linux control group hierarchy, ordered by their CPU, memory, or disk I/O load.
# echo "See also: top.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-cgtop.html>.
echo "Start an interactive view:
# systemd-cgtop
echo "Change the sort order:
# systemd-cgtop --order={{cpu|memory|path|tasks|io}}
echo "Show the CPU usage by time instead of percentage:
# systemd-cgtop --cpu=percentage
echo "Change the update interval in seconds (or one of these time units: ms`, `us`, `min):
# systemd-cgtop --delay={{interval}}
echo "Only count userspace processes (without kernel threads):
# systemd-cgtop -P
echo "systemd-confext
echo "This command is an alias of systemd-sysext.
echo "It follows the same principle as systemd-sysext`, but instead of working on `/usr` and `/opt`, `confext` will extend only `/etc.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/latest/systemd-sysext.html>.
echo "View documentation for the original command:
tldr systemd-sysext
echo "systemd-creds
echo "List, show, encrypt and decrypt service credentials.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-creds.html>.
echo "Encrypt a file and set a specific name:
systemd-creds encrypt --name={{name}} {{path/to/input_file}} {{path/to/output}}
echo "Decrypt the file again:
systemd-creds decrypt {{path/to/input_file}} {{path/to/output_file}}
echo "Encrypt text from stdin:
echo -n {{text}} | systemd-creds encrypt --name={{name}} - {{path/to/output}}
echo "Encrypt the text and append it to the service file (the credentials will be available in $CREDENTIALS_DIRECTORY):
echo -n {{text}} | systemd-creds encrypt --name={{name}} --pretty - - >> {{service}}
echo "Create a credential that is only valid until the given timestamp:
systemd-creds encrypt --not-after="{{timestamp}}" {{path/to/input_file}} {{path/to/output_file}}
echo "systemd-cryptenroll
echo "Interactively enroll or remove methods used to unlock LUKS2-encrypted devices. Uses a password to unlock the device unless otherwise specified.
echo "In order to allow a partition to be unlocked during system boot, update the /etc/crypttab file or the initramfs.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-cryptenroll.html>.
echo "Enroll a new password (similar to cryptsetup luksAddKey):
systemd-cryptenroll --password {{path/to/luks2_block_device}}
echo "Enroll a new recovery key (i.e. a randomly generated passphrase that can be used as a fallback):
systemd-cryptenroll --recovery-key {{path/to/luks2_block_device}}
echo "List available tokens, or enroll a new PKCS#11 token:
systemd-cryptenroll --pkcs11-token-uri {{list|auto|pkcs11_token_uri}} {{path/to/luks2_block_device}}
echo "List available FIDO2 devices, or enroll a new FIDO2 device (auto can be used as the device name when there is only one token plugged in):
systemd-cryptenroll --fido2-device {{list|auto|path/to/fido2_hidraw_device}} {{path/to/luks2_block_device}}
echo "Enroll a new FIDO2 device with user verification (biometrics):
systemd-cryptenroll --fido2-device {{auto|path/to/fido2_hidraw_device}} --fido2-with-user-verification yes {{path/to/luks2_block_device}}
echo "Unlock using a FIDO2 device, and enroll a new FIDO2 device:
systemd-cryptenroll --unlock-fido2-device {{path/to/fido2_hidraw_unlock_device}} --fido2-device {{path/to/fido2_hidraw_enroll_device}} {{path/to/luks2_block_device}}
echo "Enroll a TPM2 security chip (only secure-boot-policy PCR) and require an additional alphanumeric PIN:
systemd-cryptenroll --tpm2-device {{auto|path/to/tpm2_block_device}} --tpm2-with-pin yes {{path/to/luks2_block_device}}
echo "Remove all empty passwords/all passwords/all FIDO2 devices/all PKCS#11 tokens/all TPM2 security chips/all recovery keys/all methods:
systemd-cryptenroll --wipe-slot {{empty|password|fido2|pkcs#11|tpm2|recovery|all}} {{path/to/luks2_block_device}}
echo "systemd-delta
echo "Find overridden systemd-related configuration files.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-delta.html>.
echo "Show all overridden configuration files:
systemd-delta
echo "Show only files of specific types (comma-separated list):
systemd-delta --type {{masked|equivalent|redirected|overridden|extended|unchanged}}
echo "Show only files whose path starts with the specified prefix (Note: a prefix is a directory containing subdirectories with systemd configuration files):
systemd-delta {{/etc|/run|/usr/lib|...}}
echo "Further restrict the search path by adding a suffix (the prefix is optional):
systemd-delta {{prefix}}/{{tmpfiles.d|sysctl.d|systemd/system|...}}
echo "systemd-detect-virt
echo "Detect execution in a virtualized environment.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-detect-virt.html>.
echo "List detectable virtualization technologies:
systemd-detect-virt --list
echo "Detect virtualization, print the result and return a zero status code when running in a VM or a container, and a non-zero code otherwise:
systemd-detect-virt
echo "Silently check without printing anything:
systemd-detect-virt --quiet
echo "Only detect container virtualization:
systemd-detect-virt --container
echo "Only detect hardware virtualization:
systemd-detect-virt --vm
echo "systemd-dissect
echo "Introspect and interact with file system OS disk images, specifically Discoverable Disk Images (DDIs).
# echo "More information: <https://www.freedesktop.org/software/systemd/man/latest/systemd-dissect.html>.
echo "Show general image information about the OS image:
systemd-dissect {{path/to/image.raw}}
echo "Mount an OS image:
systemd-dissect --mount {{path/to/image.raw}} {{/mnt/image}}
echo "Unmount an OS image:
systemd-dissect --umount {{/mnt/image}}
echo "List files in an image:
systemd-dissect --list {{path/to/image.raw}}
echo "Attach an OS image to an automatically allocated loopback block device and print its path:
systemd-dissect --attach {{path/to/image.raw}}
echo "Detach an OS image from a loopback block device:
systemd-dissect --detach {{path/to/device}}
echo "systemd-escape
echo "Escape strings for usage in systemd unit names.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-escape.html>.
echo "Escape the given text:
systemd-escape {{text}}
echo "Reverse the escaping process:
systemd-escape --unescape {{text}}
echo "Treat the given text as a path:
systemd-escape --path {{text}}
echo "Append the given suffix to the escaped text:
systemd-escape --suffix {{suffix}} {{text}}
echo "Use a template and inject the escaped text:
systemd-escape --template {{template}} {{text}}
echo "systemd-firstboot
echo "Initialize basic system settings on or before the first boot-up of a system.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-firstboot.html>.
echo "Operate on the specified directory instead of the root directory of the host system:
sudo systemd-firstboot --root={{path/to/root_directory}}
echo "Set the system keyboard layout:
sudo systemd-firstboot --keymap={{keymap}}
echo "Set the system hostname:
sudo systemd-firstboot --hostname={{hostname}}
echo "Set the root user's password:
sudo systemd-firstboot --root-password={{password}}
echo "Prompt the user interactively for a specific basic setting:
sudo systemd-firstboot --prompt={{setting}}
echo "Force writing configuration even if the relevant files already exist:
sudo systemd-firstboot --force
echo "Remove all existing files that are configured by systemd-firstboot:
sudo systemd-firstboot --reset
echo "Remove the password of the system's root user:
sudo systemd-firstboot --delete-root-password
echo "systemd-hwdb
echo "Hardware database management tool.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-hwdb.html>.
echo "Update the binary hardware database in /etc/udev:
systemd-hwdb update
echo "Query the hardware database and print the result for a specific modalias:
systemd-hwdb query {{modalias}}
echo "Update the binary hardware database, returning a non-zero exit value on any parsing error:
systemd-hwdb --strict update
echo "Update the binary hardware database in /usr/lib/udev:
systemd-hwdb --usr update
echo "Update the binary hardware database in the specified root path:
systemd-hwdb --root={{path/to/root}} update
echo "systemd-id128
echo "Generate and print sd-128 identifiers.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-id128.html>.
echo "Generate a new random identifier:
systemd-id128 new
echo "Print the identifier of the current machine:
systemd-id128 machine-id
echo "Print the identifier of the current boot:
systemd-id128 boot-id
echo "Print the identifier of the current service invocation (this is available in systemd services):
systemd-id128 invocation-id
echo "Generate a new random identifier and print it as a UUID (five groups of digits separated by hyphens):
systemd-id128 new --uuid
echo "systemd-inhibit
echo "Prohibit the system from entering certain power states.
echo "Inhibitor locks may be used to block or delay system sleep and shutdown requests as well as automatic idle handling.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-inhibit.html>.
echo "List all active inhibition locks and the reasons for their creation:
systemd-inhibit --list
echo "Block system shutdown for a specified number of seconds with the sleep command:
systemd-inhibit --what shutdown sleep {{5}}
echo "Keep the system from sleeping or idling until the download is complete:
systemd-inhibit --what sleep:idle wget {{https://example.com/file}}
echo "Ignore lid close switch until the script exits:
systemd-inhibit --what sleep:handle-lid-switch {{path/to/script}}
echo "Ignore power button press while command is running:
systemd-inhibit --what handle-power-key {{command}}
echo "Describe who and why created the inhibitor (default: the command and its arguments for --who` and `Unknown reason` for `--why):
systemd-inhibit --who {{$USER}} --why {{reason}} --what {{operation}} {{command}}
echo "systemd-machine-id-setup
echo "Initialize the machine ID stored in /etc/machine-id at install time with a provisioned or randomly generated ID.
echo "Note: Always use sudo to execute these commands as they require elevated privileges.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-machine-id-setup.html>.
echo "Print the generated or committed machine ID:
systemd-machine-id-setup --print
echo "Specify an image policy:
systemd-machine-id-setup --image-policy={{your_policy}}
echo "Display the output as JSON:
sudo systemd-machine-id-setup --json=pretty
echo "Operate on a disk image instead of a directory tree:
systemd-machine-id-setup --image={{/path/to/image}}
echo "systemd-mount
echo "Establish and destroy transient mount or auto-mount points.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-mount.html>.
echo "Mount a file system (image or block device) at /run/media/system/LABEL where LABEL is the filesystem label or the device name if there is no label:
systemd-mount {{path/to/file_or_device}}
echo "Mount a file system (image or block device) at a specific location:
systemd-mount {{path/to/file_or_device}} {{path/to/mount_point}}
echo "Show a list of all local, known block devices with file systems that may be mounted:
systemd-mount --list
echo "Create an automount point that mounts the actual file system at the time of first access:
systemd-mount --automount=yes {{path/to/file_or_device}}
echo "Unmount one or more devices:
systemd-mount --umount {{path/to/mount_point_or_device1}} {{path/to/mount_point_or_device2}}
echo "Mount a file system (image or block device) with a specific file system type:
systemd-mount --type={{file_system_type}} {{path/to/file_or_device}} {{path/to/mount_point}}
echo "Mount a file system (image or block device) with additional mount options:
systemd-mount --options={{mount_options}} {{path/to/file_or_device}} {{path/to/mount_point}}
echo "systemd-notify
echo "Notify the service manager about start-up completion and other daemon status changes.
echo "This command is useless outside systemd service scripts.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-notify.html>.
echo "Notify systemd that the service has completed its initialization and is fully started. It should be invoked when the service is ready to accept incoming requests:
systemd-notify --booted
echo "Signal to systemd that the service is ready to handle incoming connections or perform its tasks:
systemd-notify --ready
echo "Provide a custom status message to systemd (this information is shown by systemctl status):
systemd-notify --status="{{Add custom status message here...}}"
echo "systemd-nspawn
echo "Spawn a command or OS in a lightweight container.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/latest/systemd-nspawn.html>.
echo "Run a command in a container:
systemd-nspawn --directory {{path/to/container_root}}
echo "Run a full Linux-based OS in a container:
systemd-nspawn --boot --directory {{path/to/container_root}}
echo "Run the specified command as PID 2 in the container (as opposed to PID 1) using a stub init process:
systemd-nspawn --directory {{path/to/container_root}} --as-pid2
echo "Specify the machine name and hostname:
systemd-nspawn --machine={{container_name}} --hostname={{container_host}} --directory {{path/to/container_root}}
echo "systemd-path
echo "List and query system and user paths.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-path.html>.
echo "Display a list of known paths and their current values:
systemd-path
echo "Query the specified path and display its value:
systemd-path "{{path_name}}"
echo "Suffix printed paths with suffix_string:
systemd-path --suffix {{suffix_string}}
echo "Print a short version string and then exit:
systemd-path --version
echo "systemd-repart
echo "Automatically grow and add partitions.
echo "Grows and adds partitions based on the configuration files described in repart.d.
echo "Does not automatically resize file system on partition. See systemd-growfs to extend file system.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-repart.html>.
echo "Grow the root partition (/) to all available disk space:
systemd-repart
echo "View changes without applying:
systemd-repart --dry-run=yes
echo "Grow root partition size to 10 gigabytes:
systemd-repart --size=10G --root /
echo "systemd-resolve
echo "Resolve domain names, IPV4 and IPv6 addresses, DNS resource records, and services.
echo "Note: this tool has been renamed to resolvectl` in new versions of `systemd.
echo "More information: <https://manned.org/systemd-resolve>.
echo "View documentation for resolvectl:
tldr resolvectl
echo "systemd-run
echo "Run programs in transient scope units, service units, or path-, socket-, or timer-triggered service units.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-run.html>.
echo "Start a transient service:
sudo systemd-run {{command}} {{argument1 argument2 ...}}
echo "Start a transient service under the service manager of the current user (no privileges):
systemd-run --user {{command}} {{argument1 argument2 ...}}
echo "Start a transient service with a custom unit name and description:
sudo systemd-run --unit={{name}} --description={{string}} {{command}} {{argument1 argument2 ...}}
echo "Start a transient service that does not get cleaned up after it terminates with a custom environment variable:
sudo systemd-run --remain-after-exit --set-env={{name}}={{value}} {{command}} {{argument1 argument2 ...}}
echo "Start a transient timer that periodically runs its transient service (see man systemd.time for calendar event format):
sudo systemd-run --on-calendar={{calendar_event}} {{command}} {{argument1 argument2 ...}}
echo "Share the terminal with the program (allowing interactive input/output) and make sure the execution details remain after the program exits:
systemd-run --remain-after-exit --pty {{command}}
echo "Set properties (e.g. CPUQuota, MemoryMax) of the process and wait until it exits:
systemd-run --property MemoryMax={{memory_in_bytes}} --property CPUQuota={{percentage_of_CPU_time}}% --wait {{command}}
echo "Use the program in a shell pipeline:
{{command1}} | systemd-run --pipe {{command2}} | {{command3}}
echo "systemd-socket-activate
echo "Socket activation for systemd services.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/latest/systemd-socket-activate.html>.
echo "Activate a service when a specific socket is connected:
systemd-socket-activate {{path/to/socket.service}}
echo "Activate multiple sockets for a service:
systemd-socket-activate {{path/to/socket1.service}} {{path/to/socket2.service}}
echo "Pass environment variables to the service being activated:
{{SYSTEMD_SOCKET_ACTIVATION=1}} systemd-socket-activate {{path/to/socket.service}}
echo "Activate a service along with a notification socket:
systemd-socket-activate {{path/to/socket.socket}} {{path/to/service.service}}
echo "Activate a service with a specified port:
systemd-socket-activate {{path/to/socket.service}} -l {{8080}}
echo "systemd-stdio-bridge
echo "Implement a proxy between stdin`/`stdout and a D-Bus.
echo "Note: It expects to receive an open connection via stdin`/`stdout when started, and will create a new connection to the specified bus.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/latest/systemd-stdio-bridge.html>.
echo "Forward stdin`/`stdout to the local system bus:
systemd-stdio-bridge
echo "Forward stdin`/`stdout to a specific user's D-Bus:
systemd-stdio-bridge --{{user}}
echo "Forward stdin`/`stdout to the local system bus within a specific container:
systemd-stdio-bridge --machine={{mycontainer}}
echo "Forward stdin`/`stdout to a custom D-Bus address:
systemd-stdio-bridge --bus-path=unix:path={{/custom/dbus/socket}}
echo "systemd-sysext
echo "Activate or deactivate system extension images.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-sysext.html>.
echo "List installed extension images:
systemd-sysext list
echo "Merge system extension images into /usr/` and `/opt/:
systemd-sysext merge
echo "Check the current merge status:
systemd-sysext status
echo "Unmerge all currently installed system extension images from /usr/` and `/opt/:
systemd-sysext unmerge
echo "Refresh system extension images (a combination of unmerge` and `merge):
systemd-sysext refresh
echo "systemd-sysusers
echo "Create system users and groups.
echo "If the config file is not specified, files in the sysusers.d directories are used.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-sysusers.html>.
echo "Create users and groups from a specific configuration file:
systemd-sysusers {{path/to/file}}
echo "Process configuration files and print what would be done without actually doing anything:
systemd-sysusers --dry-run {{path/to/file}}
echo "Print the contents of all config files (before each file, its name is printed as a comment):
systemd-sysusers --cat-config
echo "systemd-tmpfiles
echo "Create, delete and clean up volatile and temporary files and directories.
echo "This command is automatically invoked on boot by systemd services, and running it manually is usually not needed.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-tmpfiles.html>.
echo "Create files and directories as specified in the configuration:
systemd-tmpfiles --create
echo "Clean up files and directories with age parameters configured:
systemd-tmpfiles --clean
echo "Remove files and directories as specified in the configuration:
systemd-tmpfiles --remove
echo "Apply operations for user-specific configurations:
systemd-tmpfiles --create --user
echo "Execute lines marked for early boot:
systemd-tmpfiles --create --boot
echo "systemd-tty-ask-password-agent
echo "List or process pending systemd password requests.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/systemd-tty-ask-password-agent.html>.
echo "List all currently pending system password requests:
systemd-tty-ask-password-agent --list
echo "Continuously process password requests:
systemd-tty-ask-password-agent --watch
echo "Process all currently pending system password requests by querying the user on the calling TTY:
systemd-tty-ask-password-agent --query
echo "Forward password requests to wall instead of querying the user on the calling TTY:
systemd-tty-ask-password-agent --wall
echo "systemd-umount
echo "This command is an alias of systemd-mount --umount.
echo "View documentation for the original command:
tldr systemd-mount
echo "tac
echo "Display and concatenate files with lines in reversed order.
echo "See also: cat.
echo "More information: <https://www.gnu.org/software/coreutils/tac>.
echo "Concatenate specific files in reversed order:
tac {{path/to/file1 path/to/file2 ...}}
echo "Display stdin in reversed order:
{{cat path/to/file}} | tac
echo "Use a specific separator:
tac --separator {{,}} {{path/to/file1 path/to/file2 ...}}
echo "Use a specific regex as a separator:
tac --regex --separator {{[,;]}} {{path/to/file1 path/to/file2 ...}}
echo "Use a separator before each file:
tac --before {{path/to/file1 path/to/file2 ...}}
echo "talk
echo "A visual communication program which copies lines from your terminal to that of another user.
echo "More information: <https://www.gnu.org/software/inetutils/manual/html_node/talk-invocation.html>.
echo "Start a talk session with a user on the same machine:
talk {{username}}
echo "Start a talk session with a user on the same machine, who is logged in on tty3:
talk {{username}} {{tty3}}
echo "Start a talk session with a user on a remote machine:
talk {{username}}@{{hostname}}
echo "Clear text on both terminal screens:
<Ctrl>+D
echo "Exit the talk session:
<Ctrl>+C
echo "taskset
echo "Get or set a process' CPU affinity or start a new process with a defined CPU affinity.
echo "More information: <https://manned.org/taskset>.
echo "Get a running process' CPU affinity by PID:
taskset --pid --cpu-list {{pid}}
echo "Set a running process' CPU affinity by PID:
taskset --pid --cpu-list {{cpu_id}} {{pid}}
echo "Start a new process with affinity for a single CPU:
taskset --cpu-list {{cpu_id}} {{command}}
echo "Start a new process with affinity for multiple non-sequential CPUs:
taskset --cpu-list {{cpu_id_1}},{{cpu_id_2}},{{cpu_id_3}}
echo "Start a new process with affinity for CPUs 1 through 4:
taskset --cpu-list {{cpu_id_1}}-{{cpu_id_4}}
echo "tc
echo "Show/manipulate traffic control settings.
echo "More information: <https://manned.org/tc>.
echo "Add constant network delay to outbound packages:
tc qdisc add dev {{eth0}} root netem delay {{delay_in_milliseconds}}ms
echo "Add normal distributed network delay to outbound packages:
tc qdisc add dev {{eth0}} root netem delay {{mean_delay_ms}}ms {{delay_std_ms}}ms
echo "Add package corruption/loss/duplication to a portion of packages:
tc qdisc add dev {{eth0}} root netem {{corruption|loss|duplication}} {{effect_percentage}}%
echo "Limit bandwidth, burst rate and max latency:
tc qdisc add dev eth0 root tbf rate {{max_bandwidth_mb}}mbit burst {{max_burst_rate_kb}}kbit latency {{max_latency_before_drop_ms}}ms
echo "Show active traffic control policies:
tc qdisc show dev {{eth0}}
echo "Delete all traffic control rules:
tc qdisc del dev {{eth0}}
echo "Change traffic control rule:
tc qdisc change dev {{eth0}} root netem {{policy}} {{policy_parameters}}
echo "tcpflow
echo "Capture TCP traffic for debugging and analysis.
echo "More information: <https://manned.org/tcpflow>.
echo "Show all data on the given interface and port:
tcpflow -c -i {{eth0}} port {{80}}
echo "tcpick
echo "Packet sniffing and network traffic analysis tool.
echo "It can capture and display TCP connections and data. It can also monitor network traffic on a specific interface, host, or port.
echo "More information: <https://manned.org/tcpick.8>.
echo "Capture traffic on a specific [i]nterface, port and host::
sudo tcpick -i {{interface}} -C -h {{host}} -p {{port}}
echo "Capture traffic on port 80 (HTTP) of a specific host:
sudo tcpick -i {{eth0}} -C -h {{192.168.1.100}} -p {{80}}
echo "Display help:
tcpick --help
echo "tcpkill
echo "Kills specified in-progress TCP connections.
echo "More information: <https://manned.org/tcpkill>.
echo "Kill in-progress connections at a specified interface, host and port:
tcpkill -i {{eth1}} host {{192.95.4.27}} and port {{2266}}
echo "tcptraceroute
echo "A traceroute implementation using TCP packets.
echo "More information: <https://github.com/mct/tcptraceroute>.
echo "Trace the route to a host:
tcptraceroute {{host}}
echo "Specify the destination port and packet length in bytes:
tcptraceroute {{host}} {{destination_port}} {{packet_length}}
echo "Specify the local source port and source address:
tcptraceroute {{host}} -p {{source_port}} -s {{source_address}}
echo "Set the first and maximum TTL:
tcptraceroute {{host}} -f {{first_ttl}} -m {{max_ttl}}
echo "Specify the wait time and number of queries per hop:
tcptraceroute {{host}} -w {{wait_time}} -q {{number_of_queries}}
echo "Specify the interface:
tcptraceroute {{host}} -i {{interface}}
echo "telinit
echo "Change SysV runlevel.
echo "Since the concept SysV runlevels is obsolete the runlevel requests will be transparently translated into systemd unit activation requests.
echo "More information: <https://manned.org/telinit>.
echo "Power off the machine:
telinit 0
echo "Reboot the machine:
telinit 6
echo "Change SysV run level:
telinit {{2|3|4|5}}
echo "Change to rescue mode:
telinit 1
echo "Reload daemon configuration:
telinit q
echo "Do not send a wall message before reboot/power-off (6/0):
telinit --no-wall {{value}}
echo "terminator
echo "Arrange multiple GNOME terminals in one window.
echo "More information: <https://gnome-terminator.org/>.
echo "Start terminator window:
terminator
echo "Start with a fullscreen window:
terminator -f
echo "Split terminals horizontally:
<Ctrl> + <Shift> + O
echo "Split terminals vertically:
<Ctrl> + <Shift> + E
echo "Open new tab:
<Ctrl> + <Shift> + T
echo "termusic
echo "A terminal music player written in Rust that uses vim-like key bindings.
echo "More information: <https://github.com/tramhao/termusic>.
echo "Open termusic to a specific directory. (It can be set permanently in ~/.config/termusic/config.toml):
termusic {path/to/directory}
echo "Disable showing the album cover for a specific file:
termusic -c {path/to/music_file}
echo "View termusic's usage info:
termusic --help
echo "tftp
echo "Trivial File Transfer Protocol client.
echo "More information: <https://manned.org/tftp.1>.
echo "Connect to a TFTP server specifying its IP address and port:
tftp {{server_ip}} {{port}}
echo "Connect to a TFTP server and execute a TFTP [c]ommand:
tftp {{server_ip}} -c {{command}}
echo "Connect to a TFTP server using IPv6 and force originating port to be in [R]ange:
tftp {{server_ip}} -6 -R {{port}}:{{port}}
echo "Set the transfer mode to binary or ascii through the tftp client:
mode {{binary|ascii}}
echo "Download file from a server through the tftp client:
get {{file}}
echo "Upload file to a server through the tftp client:
put {{file}}
echo "Exit the tftp client:
quit
echo "thunar
# echo "Graphical file manager for XFCE desktop environments.
echo "More information: <https://docs.xfce.org/xfce/thunar/start>.
echo "Open a new window showing the current directory:
thunar
echo "Open the bulk rename utility:
thunar --bulk-rename
echo "Close all open thunar windows:
thunar --quit
echo "tic
echo "Compile terminfo and install for ncurses.
echo "More information: <https://pubs.opengroup.org/onlinepubs/007908799/xcurses/terminfo.html>.
echo "Compile and install terminfo for a terminal:
tic -xe {{terminal}} {{path/to/terminal.info}}
echo "Check terminfo file for errors:
tic -c {{path/to/terminal.info}}
echo "Print database locations:
tic -D
echo "timedatectl
echo "Control the system time and date.
echo "More information: <https://manned.org/timedatectl>.
echo "Check the current system clock time:
timedatectl
echo "Set the local time of the system clock directly:
timedatectl set-time "{{yyyy-MM-dd hh:mm:ss}}"
echo "List available timezones:
timedatectl list-timezones
echo "Set the system timezone:
timedatectl set-timezone {{timezone}}
echo "Enable Network Time Protocol (NTP) synchronization:
timedatectl set-ntp on
echo "Change the hardware clock time standard to localtime:
timedatectl set-local-rtc 1
echo "timeshift
echo "System restore utility.
echo "More information: <https://github.com/teejee2008/timeshift>.
echo "List snapshots:
sudo timeshift --list
echo "Create a new snapshot (if scheduled):
sudo timeshift --check
echo "Create a new snapshot (even if not scheduled):
sudo timeshift --create
echo "Restore a snapshot (selecting which snapshot to restore interactively):
sudo timeshift --restore
echo "Restore a specific snapshot:
sudo timeshift --restore --snapshot '{{snapshot}}'
echo "Delete a specific snapshot:
sudo timeshift --delete --snapshot '{{snapshot}}'
echo "tlp-stat
echo "A tool to generate TLP status reports.
echo "See also tlp.
echo "More information: <https://linrunner.de/tlp/usage/tlp-stat>.
echo "Generate status report with configuration and all active settings:
sudo tlp-stat
echo "Show battery information:
sudo tlp-stat -b
echo "Show configuration:
sudo tlp-stat -c
echo "tlp
echo "Advanced power management for Linux.
echo "See also tlp-stat.
echo "More information: <https://linrunner.de/tlp/>.
echo "Apply settings (according to the actual power source):
sudo tlp start
echo "Apply battery settings (ignoring the actual power source):
sudo tlp bat
echo "Apply AC settings (ignoring the actual power source):
sudo tlp ac
echo "tod
echo "A tiny Todoist client in Rust.
echo "It takes simple input and dumps it in your inbox or another project. Taking advantage of natural language processing to assign due dates, tags, etc.
echo "More information: <https://github.com/alanvardy/tod>.
echo "Import your projects (this is necessary to enable project prompts):
tod project import
echo "Quickly create a task with due date:
tod --quickadd {{Buy more milk today}}
echo "Create a new task (you will be prompted for content and project):
tod task create
echo "Create a task in a project:
tod task create --content "{{Write more rust}}" --project {{code}}
echo "Get the next task for a project:
tod task next
echo "Get your work schedule:
tod task list --scheduled --project {{work}}
echo "Get all tasks for work:
tod task list --project {{work}}
echo "toilet
echo "A tool to display ASCII-art fonts.
echo "More information: <http://caca.zoy.org/wiki/toilet>.
echo "Generate ASCII art for a given text:
toilet {{input_text}}
echo "Generate ASCII art using a custom font file:
toilet {{input_text}} -f {{font_filename}}
echo "Generate ASCII art using a filter:
toilet {{input_text}} --filter {{filter_name}}
echo "Show available toilet filters:
toilet --filter list
echo "tomb
echo "Manage encrypted storage directories that can be safely transported and hidden in a filesystem.
echo "More information: <https://www.dyne.org/software/tomb/>.
echo "Create a new tomb with an initial size of 100 MB:
tomb dig -s {{100}} {{encrypted_directory.tomb}}
echo "Create a new key file that can be used to lock a tomb; user will be prompted for a password for the key:
tomb forge {{encrypted_directory.tomb.key}}
echo "Forcefully create a new key, even if the tomb isn't allowing key forging (due to swap):
tomb forge {{encrypted_directory.tomb.key}} -f
echo "Initialize and lock an empty tomb using a key made with forge:
tomb lock {{encrypted_directory.tomb}} -k {{encrypted_directory.tomb.key}}
echo "Mount a tomb (by default in /media) using its key, making it usable as a regular filesystem directory:
tomb open {{encrypted_directory.tomb}} -k {{encrypted_directory.tomb.key}}
echo "Close a tomb (fails if the tomb is being used by a process):
tomb close {{encrypted_directory.tomb}}
echo "Forcefully close all open tombs, killing any applications using them:
tomb slam all
echo "List all open tombs:
tomb list
echo "toolbox create
echo "Create a new toolbox container.
echo "More information: <https://manned.org/toolbox-create.1>.
echo "Create a toolbox container for a specific distribution:
toolbox create --distro {{distribution}}
echo "Create a toolbox container for a specific release of the current distribution:
toolbox create --release {{release}}
echo "Create a toolbox container with a custom image:
toolbox create --image {{name}}
echo "Create a toolbox container from a custom Fedora image:
toolbox create --image {{registry.fedoraproject.org/fedora-toolbox:39}}
echo "Create a toolbox container using the default image for Fedora 39:
toolbox create --distro {{fedora}} --release {{f39}}
echo "toolbox enter
echo "Enter a toolbox container for interactive use.
echo "See also: toolbox run.
echo "More information: <https://manned.org/toolbox-enter.1>.
echo "Enter a toolbox container using the default image of a specific distribution:
toolbox enter --distro {{distribution}}
echo "Enter a toolbox container using the default image of a specific release of the current distribution:
toolbox enter --release {{release}}
echo "Enter a toolbox container using the default image for Fedora 39:
toolbox enter --distro {{fedora}} --release {{f39}}
echo "toolbox help
echo "Displays help information about toolbox.
echo "More information: <https://manned.org/toolbox-help.1>.
echo "Display the toolbox manual:
toolbox help
echo "Display the toolbox manual for a specific subcommand:
toolbox help {{subcommand}}
echo "toolbox init-container
echo "Initialize a running toolbox container.
echo "This command should not be executed by the user, and cannot be run on the host.
echo "More information: <https://manned.org/toolbox-init-container.1>.
echo "Initialize a running toolbox:
toolbox init-container --gid {{gid}} --home {{home}} --home-link --media-link --mnt-link --monitor-host --shell {{shell}} --uid {{uid}} --user {{user}}
echo "toolbox list
echo "List existing toolbox containers and images.
echo "More information: <https://manned.org/toolbox-list.1>.
echo "List all toolbox containers and images:
toolbox list
echo "List only toolbox containers:
toolbox list --containers
echo "List only toolbox images:
toolbox list --images
echo "toolbox rm
echo "Remove one or more toolbox containers.
echo "See also: toolbox rmi.
echo "More information: <https://manned.org/toolbox-rm.1>.
echo "Remove a toolbox container:
toolbox rm {{container_name}}
echo "Remove all toolbox containers:
toolbox rm --all
echo "Force the removal of a currently active toolbox container:
toolbox rm --force {{container_name}}
echo "toolbox rmi
echo "Remove one or more toolbox images.
echo "See also: toolbox rm.
echo "More information: <https://manned.org/toolbox-rmi.1>.
echo "Remove a toolbox image:
toolbox rmi {{image_name}}
echo "Remove all toolbox images:
toolbox rmi --all
echo "Force the removal of a toolbox image which is currently being used by a container (the container will be removed as well):
toolbox rmi --force {{image_name}}
echo "toolbox run
echo "Run a command in an existing toolbox container.
echo "See also: toolbox enter.
echo "More information: <https://manned.org/toolbox-run>.
echo "Run a command inside a specific toolbox container:
toolbox run --container {{container_name}} {{command}}
echo "Run a command inside a toolbox container for a specific release of a distribution:
toolbox run --distro {{distribution}} --release {{release}} {{command}}
echo "Run emacs` inside a `toolbox container using the default image for Fedora 39:
toolbox run --distro {{fedora}} --release {{f39}} {{emacs}}
echo "toolbox
echo "Tool for containerized command-line environments on Linux.
echo "Some subcommands such as toolbox create have their own usage documentation.
echo "More information: <https://manned.org/toolbox.1>.
echo "Run a toolbox subcommand:
toolbox {{subcommand}}
echo "Display help for a toolbox` subcommand (such as `create`, `enter`, `rm`, `rmi, etc.):
toolbox help {{subcommand}}
echo "Display help:
toolbox --help
echo "Display version:
toolbox --version
# echo "top
echo "Display dynamic real-time information about running processes.
# echo "More information: <https://manned.org/top>.
# echo "Start top:
# top
echo "Do not show any idle or zombie processes:
# top -i
echo "Show only processes owned by given user:
# top -u {{username}}
echo "Sort processes by a field:
# top -o {{field_name}}
echo "Show the individual threads of a given process:
# top -Hp {{process_id}}
echo "Show only the processes with the given PID(s), passed as a comma-separated list. (Normally you wouldn't know PIDs off hand. This example picks the PIDs from the process name):
# top -p $(pgrep -d ',' {{process_name}})
echo "Get help about interactive commands:
?
echo "trace-cmd
echo "Utility to interact with the Ftrace Linux kernel internal tracer.
echo "This utility only runs as root.
echo "More information: <https://manned.org/trace-cmd>.
echo "Display the status of tracing system:
trace-cmd stat
echo "List available tracers:
trace-cmd list -t
echo "Start tracing with a specific plugin:
trace-cmd start -p {{timerlat|osnoise|hwlat|blk|mmiotrace|function_graph|wakeup_dl|wakeup_rt|wakeup|function|nop}}
echo "View the trace output:
trace-cmd show
# echo "Stop the tracing but retain the buffers:
# trace-cmd stop
echo "Clear the trace buffers:
trace-cmd clear
# echo "Clear the trace buffers and stop tracing:
trace-cmd reset
echo "tracepath
echo "Trace the path to a network host discovering MTU along this path.
echo "More information: <https://manned.org/tracepath>.
echo "A preferred way to trace the path to a host:
tracepath -p {{33434}} {{host}}
echo "Specify the initial destination port, useful with non-standard firewall settings:
tracepath -p {{destination_port}} {{host}}
echo "Print both hostnames and numerical IP addresses:
tracepath -b {{host}}
echo "Specify a maximum TTL (number of hops):
tracepath -m {{max_hops}} {{host}}
echo "Specify the initial packet length (defaults to 65535 for IPv4 and 128000 for IPv6):
tracepath -l {{packet_length}} {{host}}
echo "Use only IPv6 addresses:
tracepath -6 {{host}}
echo "trap
echo "Automatically execute commands after receiving signals by processes or the operating system.
echo "Can be used to perform cleanups for interruptions by the user or other actions.
echo "More information: <https://manned.org/trap>.
echo "List available signals to set traps for:
trap -l
echo "List active traps for the current shell:
trap -p
echo "Set a trap to execute commands when one or more signals are detected:
trap 'echo "Caught signal {{SIGHUP}}"' {{SIGHUP}}
echo "Remove active traps:
trap - {{SIGHUP}} {{SIGINT}}
echo "trash
echo "Manage the trashcan/recycling bin.
echo "More information: <https://github.com/andreafrancia/trash-cli>.
echo "Delete a file and send it to the trash:
trash {{path/to/file}}
echo "List all files in the trash:
trash-list
echo "Interactively restore a file from the trash:
trash-restore
echo "Empty the trash:
trash-empty
echo "Permanently delete all files in the trash which are older than 10 days:
trash-empty {{10}}
echo "Remove all files in the trash, which match a specific blob pattern:
trash-rm "{{*.o}}"
echo "Remove all files with a specific original location:
trash-rm {{/path/to/file_or_directory}}
echo "trashy
echo "An alternative to rm` and `trash-cli written in Rust.
echo "More information: <https://github.com/oberblastmeister/trashy>.
echo "Move a specific file to the trash:
trash {{path/to/file}}
echo "Move specific files to the trash:
trash {{path/to/file1 path/to/file2 ...}}
echo "List items in the trash:
trash list
echo "Restore a specific file from the trash:
trash restore {{file}}
echo "Remove a specific file from the trash:
trash empty {{file}}
echo "Restore all files from the trash:
trash restore --all
echo "Remove all files from the trash:
trash empty --all
echo "trayer
echo "A lightweight GTK-2 based systray.
echo "More information: <https://github.com/sargon/trayer-srg>.
echo "Run trayer:
trayer
echo "Position trayer to a specific edge:
# trayer --edge {{left|right|top|bottom}}
echo "Provide a specific height and width of the panel (in pixels):
trayer --width {{10}} --height {{32}}
echo "Provide the width of the panel in pixels or percentages:
trayer --widthtype {{pixel|percent}} --width {{72}}
echo "Align trayer to a specific direction:
trayer --align {{left|center|right}}
echo "Provide spacing between icons (in pixels):
trayer --iconspacing {{10}}
echo "treetime
echo "TreeTime provides routines for ancestral sequence reconstruction and inference of molecular-clock phylogenies.
echo "More information: <https://treetime.readthedocs.io/en/latest/tutorials.html>.
echo "Infer ancestral sequences maximizing the joint or marginal likelihood:
treetime ancestral
echo "Analyze patterns of recurrent mutations aka homoplasies:
treetime homoplasy
echo "Estimate molecular clock parameters and reroot the tree:
treetime clock
echo "Map discrete character such as host or country to the tree:
treetime mugration
echo "trizen
echo "Arch Linux utility for building packages from the Arch User Repository (AUR).
echo "More information: <https://github.com/trizen/trizen>.
echo "Synchronize and update all AUR packages:
trizen -Syua
echo "Install a new package:
trizen -S {{package}}
echo "Remove a package and its dependencies:
trizen -Rs {{package}}
echo "Search the package database for a keyword:
trizen -Ss {{keyword}}
echo "Show information about a package:
trizen -Si {{package}}
echo "List installed packages and versions:
trizen -Qe
echo "trust
echo "Tool for operating on the trust policy store.
echo "More information: <https://manned.org/trust>.
echo "List trust policy store items:
trust list
echo "List information about specific items in the trust policy store:
trust list --filter={{blocklist|ca-anchors|certificates|trust-policy}}
echo "Store a specific trust anchor in the trust policy store:
trust anchor {{path/to/certificate.crt}}
echo "Remove a specific anchor from the trust policy store:
trust anchor --remove {{path/to/certificate.crt}}
echo "Extract trust policy from the shared trust policy store:
trust extract --format=x509-directory --filter=ca-anchors {{path/to/directory}}
echo "Display help for a subcommand:
trust {{subcommand}} --help
echo "tshark
echo "Packet analysis tool, CLI version of Wireshark.
echo "More information: <https://tshark.dev/>.
echo "Monitor everything on localhost:
tshark
echo "Only capture packets matching a specific capture filter:
tshark -f '{{udp port 53}}'
echo "Only show packets matching a specific output filter:
tshark -Y '{{http.request.method == "GET"}}'
echo "Decode a TCP port using a specific protocol (e.g. HTTP):
tshark -d tcp.port=={{8888}},{{http}}
echo "Specify the format of captured output:
tshark -T {{json|text|ps|…}}
echo "Select specific fields to output:
tshark -T {{fields|ek|json|pdml}} -e {{http.request.method}} -e {{ip.src}}
echo "Write captured packet to a file:
tshark -w {{path/to/file}}
echo "Analyze packets from a file:
tshark -r {{path/to/file.pcap}}
echo "ttyplot
echo "A realtime plotting utility for the command-line with data input from stdin.
echo "More information: <https://github.com/tenox7/ttyplot>.
echo "Plot the values 1`, `2` and `3` (`cat prevents ttyplot to exit):
{ echo {{1 2 3}}; cat } | ttyplot
echo "Set a specific title and unit:
{ echo {{1 2 3}}; cat } | ttyplot -t {{title}} -u {{unit}}
echo "Use a while loop to continuously plot random values:
{ while {{true}}; do echo {{$RANDOM}}; sleep {{1}}; done } | ttyplot
echo "Parse the output from ping and visualize it:
ping {{8.8.8.8}} | sed -u '{{s/^.*time=//g; s/ ms//g}}' | ttyplot -t "{{ping to 8.8.8.8}}" -u {{ms}}
echo "tune2fs
echo "Adjust parameters of an ext2, ext3 or ext4 filesystem.
echo "May be used on mounted filesystems.
echo "More information: <https://manned.org/tune2fs>.
echo "Set the max number of counts before a filesystem is checked to 2:
tune2fs -c {{2}} {{/dev/sdXN}}
echo "Set the filesystem label to MY_LABEL:
tune2fs -L {{'MY_LABEL'}} {{/dev/sdXN}}
echo "Enable discard and user-specified extended attributes for a filesystem:
tune2fs -o {{discard,user_xattr}} {{/dev/sdXN}}
echo "Enable journaling for a filesystem:
tune2fs -o^{{nobarrier}} {{/dev/sdXN}}
echo "tuxi
echo "Scrape Google search results and SERPs and provide instant and concise answers.
echo "More information: <https://github.com/Bugswriter/tuxi>.
echo "Make a search using Google:
tuxi {{search_terms}}
echo "Display the search results in [r]aw format (no pretty output, no colors):
tuxi -r {{search_terms}}
echo "Display only search results (silences "Did you mean?", greetings and usage):
tuxi -q {{search_terms}}
echo "Display help:
tuxi -h
echo "ubuntu-bug
echo "This command is an alias of apport-bug.
echo "More information: <https://manned.org/ubuntu-bug>.
echo "View documentation for the original command:
tldr apport-bug
echo "ubuntu-security-status
echo "Display information about security support for installed Ubuntu packages.
echo "More information: <https://git.launchpad.net/ubuntu/+source/update-manager/tree/ubuntu-security-status>.
echo "Display the number of unsupported packages:
ubuntu-security-status
echo "List packages that are no longer available for download:
ubuntu-security-status --unavailable
echo "List third-party packages:
ubuntu-security-status --thirdparty
echo "udevadm
echo "Linux udev management tool.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/udevadm>.
echo "Monitor all device events:
sudo udevadm monitor
echo "Print uevents sent out by the kernel:
sudo udevadm monitor --kernel
echo "Print device events after being processed by udev:
sudo udevadm monitor --udev
echo "List attributes of device /dev/sda:
sudo udevadm info --attribute-walk {{/dev/sda}}
echo "Reload all udev rules:
sudo udevadm control --reload-rules
echo "Trigger all udev rules to run:
sudo udevadm trigger
echo "Test an event run by simulating loading of /dev/sda:
sudo udevadm test {{/dev/sda}}
echo "udisksctl
echo "Interact with udisksd to query and manipulate storage devices.
echo "More information: <http://storaged.org/doc/udisks2-api/latest/udisksctl.1.html>.
echo "Show high-level information about disk drives and block devices:
udisksctl status
echo "Show detailed information about a device:
udisksctl info --block-device {{/dev/sdX}}
echo "Show detailed information about a device partition:
udisksctl info --block-device {{/dev/sdXN}}
echo "Mount a device partition and prints the mount point:
udisksctl mount --block-device {{/dev/sdXN}}
echo "Unmount a device partition:
udisksctl unmount --block-device {{/dev/sdXN}}
echo "Monitor the daemon for events:
udisksctl monitor
echo "ufw
echo "Uncomplicated Firewall.
echo "Frontend for iptables aiming to make configuration of a firewall easier.
echo "More information: <https://wiki.ubuntu.com/UncomplicatedFirewall>.
echo "Enable ufw:
ufw enable
echo "Disable ufw:
ufw disable
echo "Show ufw rules, along with their numbers:
ufw status numbered
echo "Allow incoming traffic on port 5432 on this host with a comment identifying the service:
ufw allow {{5432}} comment "{{Service}}"
echo "Allow only TCP traffic from 192.168.0.4 to any address on this host, on port 22:
ufw allow proto {{tcp}} from {{192.168.0.4}} to {{any}} port {{22}}
echo "Deny traffic on port 80 on this host:
ufw deny {{80}}
echo "Deny all UDP traffic to ports in range 8412:8500:
ufw deny proto {{udp}} from {{any}} to {{any}} port {{8412:8500}}
echo "Delete a particular rule. The rule number can be retrieved from the ufw status numbered command:
ufw delete {{rule_number}}
echo "ul
echo "Performs the underlining of a text.
echo "Each character in a given string must be underlined separately.
echo "More information: <https://manned.org/ul>.
echo "Display the contents of the file with underlines where applicable:
ul {{file.txt}}
echo "Display the contents of the file with underlines made of dashes -:
ul -i {{file.txt}}
echo "uname
echo "Uname prints information about the machine and operating system it is run on.
echo "More information: <https://www.gnu.org/software/coreutils/manual/html_node/uname-invocation.html>.
echo "Print all information:
uname --all
echo "Print the current kernel name:
uname --kernel-name
echo "Print the current network node host name:
uname --nodename
echo "Print the current kernel release:
uname --kernel-release
echo "Print the current kernel version:
uname --kernel-version
echo "Print the current machine hardware name:
uname --machine
echo "Print the current processor type:
uname --processor
echo "Print the current operating system name:
uname --operating-system
echo "uncompress
echo "Uncompress files compressed using the Unix compress command.
echo "More information: <https://manned.org/uncompress.1>.
echo "Uncompress specific files:
uncompress {{path/to/file1.Z path/to/file2.Z ...}}
echo "Uncompress specific files while ignoring non-existent ones:
uncompress -f {{path/to/file1.Z path/to/file2.Z ...}}
echo "Write to stdout` (no files are changed and no `.Z files are created):
uncompress -c {{path/to/file1.Z path/to/file2.Z ...}}
echo "Verbose mode (write to stderr about percentage reduction or expansion):
uncompress -v {{path/to/file1.Z path/to/file2.Z ...}}
echo "unix2dos
echo "Change Unix-style line endings to DOS-style.
echo "Replaces LF with CRLF.
echo "More information: <https://waterlan.home.xs4all.nl/dos2unix.html>.
echo "Change the line endings of a file:
unix2dos {{path/to/file}}
echo "Create a copy with DOS-style line endings:
unix2dos -n {{path/to/unix_file}} {{path/to/dos_file}}
echo "unix2mac
echo "Change Unix-style line endings to macOS-style.
echo "Replaces LF with CR.
echo "More information: <https://waterlan.home.xs4all.nl/dos2unix.html>.
echo "Change the line endings of a file:
unix2mac {{path/to/file}}
echo "Create a copy with macOS-style line endings:
unix2mac -n {{path/to/unix_file}} {{path/to/mac_file}}
echo "unset
echo "Remove shell variables or functions.
echo "More information: <https://manned.org/unset>.
echo "Remove the variable foo`, or if the variable doesn't exist, remove the function `foo:
unset {{foo}}
echo "Remove the variables foo and bar:
unset -v {{foo}} {{bar}}
echo "Remove the function my_func:
unset -f {{my_func}}
echo "unshadow
echo "Utility provided by the John the Ripper project to obtain the traditional Unix password file if the system uses shadow passwords.
echo "More information: <https://www.openwall.com/john/>.
echo "Combine the /etc/shadow` and `/etc/passwd of the current system:
sudo unshadow /etc/passwd /etc/shadow
echo "Combine two arbitrary shadow and password files:
sudo unshadow {{path/to/passwd}} {{path/to/shadow}}
echo "unshare
echo "Execute a command in new user-defined namespaces.
echo "More information: <https://www.kernel.org/doc/html/latest/userspace-api/unshare.html>.
echo "Execute a command without sharing access to connected networks:
unshare --net {{command}} {{command_arguments}}
echo "Execute a command as a child process without sharing mounts, processes, or networks:
unshare --mount --pid --net --fork {{command}} {{command_arguments}}
echo "unsquashfs
echo "Uncompress, extract and list files in squashfs filesystems.
echo "More information: <https://manned.org/unsquashfs>.
echo "Extract a squashfs filesystem to squashfs-root in the current working directory:
unsquashfs {{filesystem.squashfs}}
echo "Extract a squashfs filesystem to the specified directory:
unsquashfs -dest {{path/to/directory}} {{filesystem.squashfs}}
echo "Display the names of files as they are extracted:
unsquashfs -info {{filesystem.squashfs}}
echo "Display the names of files and their attributes as they are extracted:
unsquashfs -linfo {{filesystem.squashfs}}
echo "List files inside the squashfs filesystem (without extracting):
unsquashfs -ls {{filesystem.squashfs}}
echo "List files and their attributes inside the squashfs filesystem (without extracting):
unsquashfs -lls {{filesystem.squashfs}}
echo "unzipsfx
echo "Create a self-extracting compressed binary file by prepending self-extracting stubs on a zip file.
echo "More information: <https://manned.org/unzipsfx>.
echo "Create a self-extracting binary file of a zip archive:
cat unzipsfx {{path/to/archive.zip}} > {{filename}} && chmod 755 {{filename}}
echo "Extract a self-extracting binary in the current directory:
{{./path/to/binary)}}
echo "Test a self-extracting binary for errors:
{{./path/to/binary)}} -t
echo "Print content of a file in the self-extracting binary without extraction:
{{./path/to/binary)}} -c {{path/to/filename}}
echo "Print comments on zip archive in the self-extracting binary:
{{./path/to/binary)}} -z
echo "update-alternatives
echo "A convenient tool for maintaining symbolic links to determine default commands.
echo "More information: <https://manned.org/update-alternatives>.
echo "Add a symbolic link:
sudo update-alternatives --install {{path/to/symlink}} {{command_name}} {{path/to/command_binary}} {{priority}}
echo "Configure a symbolic link for java:
sudo update-alternatives --config {{java}}
echo "Remove a symbolic link:
sudo update-alternatives --remove {{java}} {{/opt/java/jdk1.8.0_102/bin/java}}
echo "Display information about a specified command:
update-alternatives --display {{java}}
echo "Display all commands and their current selection:
update-alternatives --get-selections
echo "update-rc.d
echo "Install and remove services which are System-V style init script links.
echo "Init scripts are in the /etc/init.d/.
echo "More information: <https://manned.org/update-rc.d>.
echo "Install a service:
update-rc.d {{mysql}} defaults
echo "Enable a service:
update-rc.d {{mysql}} enable
echo "Disable a service:
update-rc.d {{mysql}} disable
echo "Forcibly remove a service:
update-rc.d -f {{mysql}} remove
echo "updatedb
echo "Create or update the database used by locate.
echo "It is usually run daily by cron.
echo "More information: <https://manned.org/updatedb>.
echo "Refresh database content:
sudo updatedb
echo "Display file names as soon as they are found:
sudo updatedb --verbose
echo "updpkgsums
echo "Update the checksums of the sources in a PKGBUILD.
echo "Unless a preexisting hashing algorithm is used, SHA256 will be used.
echo "More information: <https://manned.org/updpkgsums>.
echo "Update the checksums in a PKGBUILD:
updpkgsums
echo "Display Version:
updpkgsums -v
echo "Display help:
updpkgsums -h
echo "upower
echo "System utility to provide power and battery information and statistics.
# echo "More information: <https://upower.freedesktop.org/docs/upower.1.html>.
echo "Display power and battery information:
upower --dump
echo "List all power devices:
upower --enumerate
echo "Watch for and print power status changes:
upower --monitor
echo "Watch for and print detailed power status changes:
upower --monitor-detail
echo "Display version:
upower --version
echo "uprecords
echo "Displays a summary of historical uptime records.
echo "More information: <https://manned.org/uprecords>.
# echo "Display a summary of the top 10 historical uptime records:
uprecords
# echo "Display the top 25 records:
uprecords -m {{25}}
echo "Display the downtime between reboots instead of the kernel version:
uprecords -d
echo "Show the most recent reboots:
uprecords -B
echo "Don't truncate information:
uprecords -w
echo "urxvt
echo "Rxvt-unicode.
echo "A customizable terminal emulator.
echo "More information: <https://manned.org/urxvt>.
echo "Open a new urxvt window:
urxvt
echo "Run in a specific directory:
urxvt -cd {{path/to/directory}}
echo "Run a command in a new urxvt window:
urxvt -e {{command}}
echo "Run a command and keep the window open:
urxvt --hold -e {{command}}
echo "Run a command within the sh shell:
urxvt -e {{sh}} -c {{command}}
echo "usbip
echo "Use USB devices remotely.
echo "More information: <https://usbip.sourceforge.net>.
echo "List all local USB devices and their bus ID's:
usbip list --local
echo "Start a usbip daemon on the server:
systemctl start usbipd
echo "Bind a USB device to usbip on the server:
sudo usbip bind --busid={{bus_id}}
echo "Load the kernel module required by usbip on the client:
sudo modprobe vhci-hcd
echo "Attach to the usbip device on the client (bus ID is the same as on the server):
sudo usbip attach -r {{ip_address}} --busid={{bus_id}}
echo "List attached devices:
usbip port
echo "Detach from a device:
sudo usbip detach --port={{port}}
echo "Unbind a device:
usbip unbind --busid={{bus_id}}
echo "useradd
echo "Create a new user.
echo "See also: users`, `userdel`, `usermod.
echo "More information: <https://manned.org/useradd>.
echo "Create a new user:
sudo useradd {{username}}
echo "Create a new user with the specified user id:
sudo useradd --uid {{id}} {{username}}
echo "Create a new user with the specified shell:
sudo useradd --shell {{path/to/shell}} {{username}}
echo "Create a new user belonging to additional groups (mind the lack of whitespace):
sudo useradd --groups {{group1,group2,...}} {{username}}
echo "Create a new user with the default home directory:
sudo useradd --create-home {{username}}
echo "Create a new user with the home directory filled by template directory files:
sudo useradd --skel {{path/to/template_directory}} --create-home {{username}}
echo "Create a new system user without the home directory:
sudo useradd --system {{username}}
echo "userdbctl
echo "Inspect users, groups and group memberships on the system.
# echo "More information: <https://www.freedesktop.org/software/systemd/man/userdbctl.html>.
echo "List all known user records:
userdbctl user
echo "Show details of a specific user:
userdbctl user {{username}}
echo "List all known groups:
userdbctl group
echo "Show details of a specific group:
userdbctl group {{groupname}}
echo "List all services currently providing user/group definitions to the system:
userdbctl services
echo "userdel
echo "Remove a user account or remove a user from a group.
echo "See also: users`, `useradd`, `usermod.
echo "More information: <https://manned.org/userdel>.
echo "Remove a user:
sudo userdel {{username}}
echo "Remove a user in other root directory:
sudo userdel --root {{path/to/other/root}} {{username}}
echo "Remove a user along with the home directory and mail spool:
sudo userdel --remove {{username}}
echo "usermod
echo "Modifies a user account.
echo "See also: users`, `useradd`, `userdel.
echo "More information: <https://manned.org/usermod>.
echo "Change a username:
sudo usermod --login {{new_username}} {{username}}
echo "Change a user id:
sudo usermod --uid {{id}} {{username}}
echo "Change a user shell:
sudo usermod --shell {{path/to/shell}} {{username}}
echo "Add a user to supplementary groups (mind the lack of whitespace):
sudo usermod --append --groups {{group1,group2,...}} {{username}}
echo "Change a user home directory:
sudo usermod --move-home --home {{path/to/new_home}} {{username}}
echo "utmpdump
echo "Dump and load btmp, utmp and wtmp accounting files.
echo "More information: <https://manned.org/utmpdump>.
echo "Dump the /var/log/wtmp` file to `stdout as plain text:
utmpdump {{/var/log/wtmp}}
echo "Load a previously dumped file into /var/log/wtmp:
utmpdump -r {{dumpfile}} > {{/var/log/wtmp}}
echo "uuid
echo "Generate and decode Universally Unique Identifiers (UUID).
echo "See also uuidgen.
echo "More information: <https://manned.org/uuid>.
echo "Generate a UUIDv1 (based on time and system's hardware address, if present):
uuid
echo "Generate a UUIDv4 (based on random data):
uuid -v {{4}}
echo "Generate multiple UUIDv4 identifiers at once:
uuid -v {{4}} -n {{number_of_uuids}}
echo "Generate a UUIDv4 and specify the output format:
uuid -v {{4}} -F {{BIN|STR|SIV}}
echo "Generate a UUIDv4 and write the output to a file:
uuid -v {{4}} -o {{path/to/file}}
echo "Generate a UUIDv5 (based on the supplied object name) with a specified namespace prefix:
uuid -v {{5}} ns:{{DNS|URL|OID|X500}} {{object_name}}
echo "Decode a given UUID:
uuid -d {{uuid}}
echo "uuidd
echo "Daemon for generating UUIDs.
echo "More information: <https://manned.org/uuidd>.
echo "Generate a random UUID:
uuidd --random
echo "Generate a bulk number of random UUIDs:
uuidd --random --uuids {{number_of_uuids}}
echo "Generate a time-based UUID, based on the current time and MAC address of the system:
uuidd --time
echo "uuidgen
echo "Generate unique identifiers (UUIDs).
echo "See also uuid.
echo "More information: <https://manned.org/uuidgen>.
echo "Create a random UUIDv4:
uuidgen --random
echo "Create a UUIDv1 based on the current time:
uuidgen --time
echo "Create a UUIDv5 of the name with a specified namespace prefix:
uuidgen --sha1 --namespace {{@dns|@url|@oid|@x500}} --name {{object_name}}
echo "uvcdynctrl
echo "A libwebcam command-line tool to manage dynamic controls in uvcvideo.
echo "More information: <https://manned.org/uvcdynctrl>.
echo "List all available cameras:
uvcdynctrl -l
echo "Specify the device to use (defaults to video0):
uvcdynctrl -d {{device_name}}
echo "List available controls:
uvcdynctrl -c
echo "Set a new control value (for negative values, use -- -value):
uvcdynctrl -s {{control_name}} {{value}}
echo "Get the current control value:
uvcdynctrl -g {{control_name}}
echo "Save the state of the current controls to a file:
uvcdynctrl -W {{filename}}
echo "Load the state of the controls from a file:
uvcdynctrl -L {{filename}}
echo "v4l2-ctl
echo "Control video devices.
echo "More information: <https://manned.org/v4l2-ctl>.
echo "List all video devices:
v4l2-ctl --list-devices
echo "List supported video formats and resolutions of default video device /dev/video0:
v4l2-ctl --list-formats-ext
echo "List supported video formats and resolutions of a specific video device:
v4l2-ctl --list-formats-ext --device {{path/to/video_device}}
echo "Get all details of a video device:
v4l2-ctl --all --device {{path/to/video_device}}
echo "Capture a JPEG photo with a specific resolution from video device:
v4l2-ctl --device {{path/to/video_device}} --set-fmt-video=width={{width}},height={{height}},pixelformat=MJPG --stream-mmap --stream-to={{path/to/output.jpg}} --stream-count=1
echo "Capture a raw video stream from video device:
v4l2-ctl --device {{path/to/video_device}} --set-fmt-video=width={{width}},height={{height}},pixelformat={{format}} --stream-mmap --stream-to={{path/to/output}} --stream-count={{number_of_frames_to_capture}}
echo "List all video device's controls and their values:
v4l2-ctl --list-ctrls --device {{path/to/video_device}}
echo "Set the value of a video device control:
v4l2-ctl --device {{path/to/video_device}} --set-ctrl={{control_name}}={{value}}
echo "vcgencmd
echo "Print system information for a Raspberry Pi.
echo "More information: <https://www.raspberrypi.org/documentation/computers/os.html#vcgencmd>.
echo "List all available commands:
vcgencmd commands
echo "Print the current CPU temperature:
vcgencmd measure_temp
echo "Print the current voltage:
vcgencmd measure_volts
echo "Print the throttled state of the system as a bit pattern:
vcgencmd get_throttled
echo "Print the bootloader config (only available on Raspberry Pi 4 models):
vcgencmd bootloader_config
echo "Display Help:
vcgencmd --help
echo "veracrypt
echo "Free and open source disk encryption software.
echo "More information: <https://www.veracrypt.fr/code/VeraCrypt/plain/doc/html/Documentation.html>.
echo "Create a new volume through a text user interface and use /dev/urandom as a source of random data:
veracrypt --text --create --random-source={{/dev/urandom}}
echo "Decrypt a volume interactively through a text user interface and mount it to a directory:
veracrypt --text {{path/to/volume}} {{path/to/mount_point}}
echo "Decrypt a partition using a keyfile and mount it to a directory:
veracrypt --keyfiles={{path/to/keyfile}} {{/dev/sdXN}} {{path/to/mount_point}}
echo "Dismount a volume on the directory it is mounted to:
veracrypt --dismount {{path/to/mounted_point}}
echo "vgchange
echo "Change the attributes of a Logical Volume Manager (LVM) volume group.
echo "See also: lvm.
echo "More information: <https://manned.org/vgchange>.
echo "Change the activation status of logical volumes in all volume groups:
sudo vgchange --activate {{y|n}}
echo "Change the activation status of logical volumes in the specified volume group (determine with vgscan):
sudo vgchange --activate {{y|n}} {{volume_group}}
echo "vgcreate
echo "Create volume groups combining multiple mass-storage devices.
echo "See also: lvm.
echo "More information: <https://man7.org/linux/man-pages/man8/vgcreate.8.html>.
echo "Create a new volume group called vg1 using the /dev/sda1 device:
vgcreate {{vg1}} {{/dev/sda1}}
echo "Create a new volume group called vg1 using multiple devices:
vgcreate {{vg1}} {{/dev/sda1}} {{/dev/sdb1}} {{/dev/sdc1}}
echo "vgdisplay
echo "Display information about Logical Volume Manager (LVM) volume groups.
echo "See also: lvm.
echo "More information: <https://man7.org/linux/man-pages/man8/vgdisplay.8.html>.
echo "Display information about all volume groups:
sudo vgdisplay
echo "Display information about volume group vg1:
sudo vgdisplay {{vg1}}
echo "vgs
echo "Display information about volume groups.
echo "See also: lvm.
echo "More information: <https://man7.org/linux/man-pages/man8/vgs.8.html>.
echo "Display information about volume groups:
vgs
echo "Display all volume groups:
vgs -a
echo "Change default display to show more details:
vgs -v
echo "Display only specific fields:
vgs -o {{field_name_1}},{{field_name_2}}
echo "Append field to default display:
vgs -o +{{field_name}}
echo "Suppress heading line:
vgs --noheadings
echo "Use separator to separate fields:
vgs --separator =
echo "vgscan
echo "Scan for volume groups on all supported Logical Volume Manager (LVM) block devices.
echo "See also: lvm` and `vgchange.
echo "More information: <https://manned.org/vgscan>.
echo "Scan for volume groups and print information about each group found:
sudo vgscan
echo "Scan for volume groups and add the special files in /dev, if they don't already exist, needed to access the logical volumes in the found groups:
sudo vgscan --mknodes
echo "viewnior
echo "Simple and elegant image viewer.
echo "More information: <https://manned.org/viewnior>.
echo "View an image:
viewnior {{path/to/image.ext}}
echo "View in fullscreen mode:
viewnior --fullscreen {{path/to/image.ext}}
echo "View fullscreen in slideshow mode:
viewnior --slideshow {{path/to/image.ext}}
echo "vigr
echo "Edit the group file.
echo "More information: <https://manned.org/vigr>.
echo "Edit the group file:
vigr
echo "Display version:
vigr --version
echo "vipw
echo "Edit the password file.
echo "More information: <https://manned.org/vipw>.
echo "Edit the password file:
vipw
echo "Display the current version of vipw:
vipw --version
echo "virt-manager
# echo "CLI launcher for virt-manager, a desktop user interface for managing KVM and Xen virtual machines and LXC containers.
echo "More information: <https://manpages.ubuntu.com/manpages/man1/virt-manager.1.html>.
echo "Launch virt-manager:
virt-manager
echo "Connect to a hypervisor:
virt-manager --connect {{hypervisor_uri}}
echo "Don't fork virt-manager process into background on startup:
virt-manager --no-fork
echo "Print debug output:
virt-manager --debug
echo "Open the "New VM" wizard:
virt-manager --show-domain-creator
echo "Show domain details window:
virt-manager --show-domain-editor {{name|id|uuid}}
echo "Show domain performance window:
virt-manager --show-domain-performance {{name|id|uuid}}
echo "Show connection details window:
virt-manager --show-host-summary
echo "virt-viewer
echo "Minimal graphical interface for a virtual machine (VM).
echo "NOTE: 'domain' refers to the name, UUID or ID for the existing VMs (See: tldr virsh).
echo "More information: <https://manned.org/virt-viewer>.
echo "Launch virt-viewer with a prompt to select running virtual machines:
virt-viewer
echo "Launch virt-viewer for a specific virtual machine by ID, UUID or name:
virt-viewer "{{domain}}"
echo "Wait for a virtual machine to start and automatically reconnect if it shutdown and restarts:
virt-viewer --reconnect --wait "{{domain}}"
echo "Connect to a specific remote virtual machine over TLS:
virt-viewer --connect "xen//{{url}}" "{{domain}}"
echo "Connect to a specific remote virtual machine over SSH:
virt-viewer --connect "qemu+ssh//{{username}}@{{url}}/system" "{{domain}}"
echo "virt-xml-validate
echo "Validate libvirt XML files against a schema.
echo "If a schema is not specified, the schema is determined by the root element in the XML file.
echo "More information: <https://libvirt.org/manpages/virt-xml-validate.html>.
echo "Validate an XML file against a specific schema:
virt-xml-validate {{path/to/file.xml}} {{schema}}
echo "Validate the domain XML against the domain schema:
virt-xml-validate {{path/to/domain.xml}} domain
echo "virt-xml
echo "Edit libvirt Domain XML files with explicit command-line options.
echo "NOTE: 'domain' refers to the name, UUID or ID for the existing VMs (See: tldr virsh).
echo "More information: <https://github.com/virt-manager/virt-manager/blob/main/man/virt-xml.rst>.
echo "List all the suboptions for a specific option:
virt-xml --{{option}}=?
echo "List all the suboptions for disk, network, and boot:
virt-xml --disk=? --network=? --boot=?
echo "Edit a value for a specific domain:
virt-xml {{domain}} --edit --{{option}} {{suboption}}={{new_value}}
echo "Change the description for a specific domain:
virt-xml {{domain}} --edit --metadata description="{{new_description}}"
echo "Enable/Disable the boot device menu for a specific domain:
virt-xml {{domain}} --edit --boot bootmenu={{on|off}}
echo "Attach host USB hub to a running VM (See: tldr lsusb):
virt-xml {{domain}} --update --add-device --hostdev {{bus}}.{{device}}
echo "vkpurge
echo "List or remove old kernel versions left behind by xbps.
echo "The version arguments support shell globs.
echo "More information: <https://man.voidlinux.org/vkpurge.8>.
echo "List all removable kernel versions (or those matching version if the argument is specified):
vkpurge list {{version}}
echo "Remove all unused kernels:
vkpurge rm all
echo "Remove kernel versions matching version:
vkpurge rm {{version}}
echo "vmstat
echo "Report information about processes, memory, paging, block IO, traps, disks and CPU activity.
echo "More information: <https://manned.org/vmstat>.
echo "Display virtual memory statistics:
vmstat
echo "Display reports every 2 seconds for 5 times:
vmstat {{2}} {{5}}
echo "vmware-checkvm
echo "Check if the current host is a VMware VM or not.
echo "More information: <https://manned.org/vmware-checkvm>.
echo "Return the current VMware software version (exit status determines whether the system is a VM or not):
vmware-checkvm
echo "Return the VMware hardware version:
vmware-checkvm -h
echo "vncserver
# echo "Launches a VNC (Virtual Network Computing) desktop.
echo "More information: <https://manned.org/vncserver.1x>.
echo "Launch a VNC Server on next available display:
vncserver
echo "Launch a VNC Server with specific screen geometry:
vncserver --geometry {{width}}x{{height}}
echo "Kill an instance of VNC Server running on a specific display:
vncserver --kill :{{display_number}}
echo "vncviewer
echo "Launches a VNC (Virtual Network Computing) client.
echo "More information: <https://manned.org/vncviewer>.
echo "Launch a VNC client which connects to a host on a given display:
vncviewer {{host}}:{{display_number}}
echo "Launch in full-screen mode:
vncviewer -FullScreen {{host}}:{{display_number}}
echo "Launch a VNC client with a specific screen geometry:
vncviewer --geometry {{width}}x{{height}} {{host}}:{{display_number}}
echo "Launch a VNC client which connects to a host on a given port:
vncviewer {{host}}::{{port}}
echo "vnstat
echo "A console-based network traffic monitor.
echo "More information: <https://manned.org/vnstat>.
echo "Display traffic summary for all interfaces:
vnstat
echo "Display traffic summary for a specific network interface:
vnstat -i {{eth0}}
echo "Display live stats for a specific network interface:
vnstat -l -i {{eth0}}
echo "Show traffic statistics on an hourly basis for the last 24 hours using a bar graph:
vnstat -hg
echo "Measure and show average traffic for 30 seconds:
vnstat -tr {{30}}
echo "vnstati
echo "PNG image output support for vnStat.
echo "More information: <https://manned.org/vnstati>.
echo "Output a summary of the last 2: months, days, and all-time:
vnstati --summary --iface {{network_interface}} --output {{path/to/output.png}}
echo "Output the 10 most traffic-intensive days of all time:
# vnstati --top10 --iface {{network_interface}} --output {{path/to/output.png}}
echo "Output monthly traffic statistics from the last 12 months:
vnstati --months --iface {{network_interface}} --output {{path/to/output.png}}
echo "Output hourly traffic statistics from the last 24 hours:
vnstati --hours --iface {{network_interface}} --output {{path/to/output.png}}
echo "vpnc
echo "A VPN client for the Cisco 3000 VPN Concentrator.
echo "More information: <https://manned.org/vpnc>.
echo "Connect with a defined configuration file:
sudo vpnc {{config_file}}
echo "Terminate the previously created connection:
sudo vpnc-disconnect
echo "vrms
echo "Report non-free packages installed on Debian-based OSes.
echo "More information: <https://debian.pages.debian.net/vrms/>.
echo "List non-free and contrib packages (and their description):
vrms
echo "Only output the package names:
vrms --sparse
echo "vso
echo "Package manager, system updater and a task automator for Vanilla OS.
echo "More information: <https://github.com/Vanilla-OS/vanilla-system-operator>.
echo "Check for system updates to the host system:
vso sys-upgrade check
echo "Upgrade the host system now:
vso sys-upgrade upgrade --now
echo "Initialize the Pico subsystem (used for package management):
vso pico-init
echo "Install applications inside the subsystem:
vso install {{package1 package2 ...}}
echo "Remove applications from the subsystem:
vso remove {{package1 package2 ...}}
echo "Enter the subsystem's shell:
vso shell
echo "Run an application from the subsystem:
vso run {{package}}
echo "Display VSO configuration:
vso config show
echo "w
echo "Display who is logged in and their processes.
echo "More information: <https://www.geeksforgeeks.org/w-command-in-linux-with-examples/>.
echo "Display information about all users who are currently logged in:
w
echo "Display information about a specific user:
w {{username}}
echo "Display information without including the header:
w --no-header
echo "Display information without including the login, JCPU and PCPU columns:
w --short
echo "wajig
echo "Simplified all-in-one-place system support tool for Debian-based systems.
echo "More information: <https://wajig.togaware.com>.
echo "Update the list of available packages and versions:
wajig update
echo "Install a package, or update it to the latest available version:
wajig install {{package}}
echo "Remove a package and its configuration files:
wajig purge {{package}}
echo "Perform an update and then a dist-upgrade:
wajig daily-upgrade
echo "Display the sizes of installed packages:
wajig sizes
echo "List the version and distribution for all installed packages:
wajig versions
echo "List versions of upgradable packages:
wajig toupgrade
echo "Display packages which have some form of dependency on the given package:
wajig dependents {{package}}
echo "wal-telegram
echo "Generates themes for Telegram based the colors generated by pywal/wal.
echo "More information: <https://github.com/guillaumeboehm/wal-telegram>.
echo "Generate with wal's palette and the current wallpaper (feh only):
wal-telegram
echo "Generate with wal's palette and a specified background image:
wal-telegram --background={{path/to/image}}
echo "Generate with wal's palette and a colored background based on the palette:
wal-telegram --tiled
echo "Apply a gaussian blur on the background image:
wal-telegram -g
echo "Specify a location for the generated theme (default is $XDG_CACHE_HOME/wal-telegram` or `~/.cache/wal-telegram):
wal-telegram --destination={{path/to/destination}}
echo "Restart the telegram app after generation:
wal-telegram --restart
echo "wall
echo "Write a message on the terminals of users currently logged in.
echo "More information: <https://manned.org/wall>.
echo "Send a message:
wall {{message}}
echo "Send a message to users that belong to a specific group:
wall --group {{group_name}} {{message}}
echo "Send a message from a file:
wall {{file}}
echo "Send a message with timeout (default 300):
wall --timeout {{seconds}} {{file}}
echo "warpd
echo "A modal keyboard driven pointer manipulation program.
echo "More information: <https://github.com/rvaiya/warpd/blob/master/man.md>.
echo "Run warpd in normal mode:
warpd --normal
echo "Run warpd in hint mode:
warpd --hint
echo "Move cursor left:
h
echo "Move cursor down:
j
echo "Move cursor up:
k
echo "Move cursor right:
l
echo "Emulate left click:
m
echo "watch
echo "Execute a command repeatedly, and monitor the output in full-screen mode.
echo "More information: <https://manned.org/watch>.
echo "Monitor files in the current directory:
watch {{ls}}
echo "Monitor disk space and highlight the changes:
watch -d {{df}}
echo "Monitor "node" processes, refreshing every 3 seconds:
watch -n {{3}} "{{ps aux | grep node}}"
# echo "Monitor disk space and if it changes, stop monitoring:
watch -g {{df}}
echo "waydroid
echo "A container-based approach to boot a full Android system on a regular GNU/Linux system like Ubuntu.
echo "More information: <https://docs.waydro.id>.
echo "Start Waydroid:
waydroid
echo "Initialize Waydroid (required on first run or after reinstalling Android):
waydroid init
echo "Install a new Android app from a file:
waydroid app install {{path/to/file.apk}}
echo "Launch an Android app by its package name:
waydroid app launch {{com.example.app}}
# echo "Start or stop the Waydroid session:
# waydroid session {{start|stop}}
echo "Manage the Waydroid container:
# waydroid container {{start|stop|restart|freeze|unfreeze}}
echo "wdctl
echo "Show the hardware watchdog status.
echo "More information: <https://manned.org/wdctl>.
echo "Display the watchdog status:
wdctl
echo "Display the watchdog status in a single line in key-value pairs:
wdctl --oneline
echo "Display only specific watchdog flags (list is driver specific):
wdctl --flags {{flag_list}}
echo "wg-quick
echo "Quickly set up WireGuard tunnels based on config files.
echo "More information: <https://www.wireguard.com/quickstart/>.
echo "Set up a VPN tunnel:
wg-quick up {{interface_name}}
echo "Delete a VPN tunnel:
wg-quick down {{interface_name}}
echo "wg
echo "Manage the configuration of WireGuard interfaces.
echo "More information: <https://www.wireguard.com/quickstart/>.
echo "Check status of currently active interfaces:
sudo wg
echo "Generate a new private key:
wg genkey
echo "Generate a public key from a private key:
wg pubkey < {{path/to/private_key}} > {{path/to/public_key}}
echo "Generate a public and private key:
wg genkey | tee {{path/to/private_key}} | wg pubkey > {{path/to/public_key}}
echo "Show the current configuration of a wireguard interface:
sudo wg showconf {{wg0}}
echo "whatis
echo "Display one-line descriptions from manual pages.
echo "More information: <https://manned.org/whatis>.
echo "Display a description from a man page:
whatis {{command}}
echo "Don't cut the description off at the end of the line:
whatis --long {{command}}
echo "Display descriptions for all commands matching a glob:
whatis --wildcard {{net*}}
echo "Search man page descriptions with a regular expression:
whatis --regex '{{wish[0-9]\.[0-9]}}'
echo "Display descriptions in a specific language:
whatis --locale={{en}} {{command}}
echo "whiptail
echo "Display text-based dialog boxes from shell scripts.
echo "More information: <https://manned.org/whiptail>.
echo "Display a simple message:
whiptail --title "{{title}}" --msgbox "{{message}}" {{height_in_chars}} {{width_in_chars}}
echo "Display a boolean choice, returning the result through the exit code:
whiptail --title "{{title}}" --yesno "{{message}}" {{height_in_chars}} {{width_in_chars}}
echo "Customise the text on the yes/no buttons:
whiptail --title "{{title}}" --yes-button "{{text}}" --no-button "{{text}}" --yesno "{{message}}" {{height_in_chars}} {{width_in_chars}}
echo "Display a text input box:
{{result_variable_name}}="$(whiptail --title "{{title}}" --inputbox "{{message}}" {{height_in_chars}} {{width_in_chars}} {{default_text}} 3>&1 1>&2 2>&3)"
echo "Display a password input box:
{{result_variable_name}}="$(whiptail --title "{{title}}" --passwordbox "{{message}}" {{height_in_chars}} {{width_in_chars}} 3>&1 1>&2 2>&3)"
echo "Display a multiple-choice menu:
{{result_variable_name}}=$(whiptail --title "{{title}}" --menu "{{message}}" {{height_in_chars}} {{width_in_chars}} {{menu_display_height}} "{{value_1}}" "{{display_text_1}}" "{{value_n}}" "{{display_text_n}}" ..... 3>&1 1>&2 2>&3)
echo "wifi-menu
echo "Interactively connect to a wireless network.
echo "More information: <https://manned.org/wifi-menu>.
echo "Set up a wireless connection interactively:
wifi-menu
echo "Interactively set up a connection to a network and obscure the password:
wifi-menu --obscure
echo "Display help:
wifi-menu --help
echo "wikit
echo "A command line program for getting Wikipedia summaries easily.
echo "More information: <https://github.com/KorySchneider/wikit>.
# echo "Show a short summary of a specific topic on Wikipedia:
# wikit {{topic}}
echo "Specify a [l]anguage (ISO 639-1 language code):
# wikit {{topic}} --lang {{language_code}}
echo "Open the full Wikipedia article in the default browser:
# wikit {{topic}} -b
echo "Open a disambiguation menu:
# wikit {{topic}} -d
echo "wine
echo "Run Windows executables on Unix-based systems.
echo "More information: <https://wiki.winehq.org/>.
echo "Run a specific program inside the wine environment:
wine {{command}}
echo "Run a specific program in background:
wine start {{command}}
echo "Install/uninstall an MSI package:
wine msiexec /{{i|x}} {{path/to/package.msi}}
echo "Run File Explorer`, `Notepad`, or `WordPad:
wine {{explorer|notepad|write}}
echo "Run Registry Editor`, `Control Panel`, or `Task Manager:
wine {{regedit|control|taskmgr}}
echo "Run the configuration tool:
wine winecfg
echo "winetricks
echo "Manage Wine virtual Windows environments.
echo "More information: <https://wiki.winehq.org/Winetricks>.
echo "Start a graphical setup at the default Wine location:
winetricks
echo "Specify a custom Wine directory to run Winetricks in:
WINEPREFIX={{path/to/wine_directory}} winetricks
echo "Install a Windows DLL or component to the default Wine directory:
winetricks {{package}}
echo "wipefs
echo "Wipe filesystem, raid, or partition-table signatures from a device.
echo "More information: <https://manned.org/wipefs>.
echo "Display signatures for specified device:
sudo wipefs {{/dev/sdX}}
echo "Wipe all available signature types for a specific device with no recursion into partitions:
sudo wipefs --all {{/dev/sdX}}
echo "Wipe all available signature types for the device and partitions using a glob pattern:
sudo wipefs --all {{/dev/sdX}}*
echo "Perform dry run:
sudo wipefs --all --no-act {{/dev/sdX}}
echo "Force wipe, even if the filesystem is mounted:
sudo wipefs --all --force {{/dev/sdX}}
echo "wl-copy
echo "Wayland clipboard manipulation tool.
echo "See also: wl-paste.
echo "More information: <https://github.com/bugaevc/wl-clipboard>.
echo "Copy the text to the clipboard:
wl-copy "{{text}}"
echo "Pipe the command (ls) output to the clipboard:
{{ls}} | wl-copy
echo "Copy for only one paste and then clear it:
wl-copy --paste-once "{{text}}"
echo "Copy an image:
wl-copy < {{path/to/image}}
echo "Clear the clipboard:
wl-copy --clear
echo "wl-paste
echo "Tool to access data stored in the clipboard for Wayland.
echo "See also: wl-copy.
echo "More information: <https://github.com/bugaevc/wl-clipboard>.
echo "Paste the contents of the clipboard:
wl-paste
echo "Write the contents of the clipboard to a file:
wl-paste > {{path/to/file}}
echo "Pipe the contents of the clipboard to a command:
wl-paste | {{command}}
echo "wmctrl
echo "CLI for X Window Manager.
echo "More information: <https://manned.org/wmctrl>.
echo "List all windows, managed by the window manager:
wmctrl -l
echo "Switch to the first window whose (partial) title matches:
wmctrl -a {{window_title}}
echo "Move a window to the current workspace, raise it and give it focus:
wmctrl -R {{window_title}}
echo "Switch to a workspace:
wmctrl -s {{workspace_number}}
echo "Select a window and toggle fullscreen:
wmctrl -r {{window_title}} -b toggle,fullscreen
echo "Select a window a move it to a workspace:
wmctrl -r {{window_title}} -t {{workspace_number}}
echo "wodim
echo "Command (aliased as cdrecord on some systems) for recording data to CDs or DVDs.
echo "Some invocations of wodim can cause destructive actions, such as erasing all the data on a disc.
echo "More information: <https://manned.org/wodim>.
echo "Display optical drives available to wodim:
wodim --devices
echo "Record ("burn") an audio-only disc:
wodim dev=/dev/{{optical_drive}} -audio {{track*.cdaudio}}
echo "Burn a file to a disc, ejecting the disc once done (some recorders require this):
wodim -eject dev=/dev/{{optical_drive}} -data {{file.iso}}
echo "Burn a file to the disc in an optical drive, potentially writing to multiple discs in succession:
wodim -tao dev=/dev/{{optical_drive}} -data {{file.iso}}
echo "woeusb
echo "Windows media creation tool.
echo "More information: <https://github.com/WoeUSB/WoeUSB>.
echo "Format a USB then create a bootable Windows installation drive:
woeusb --device {{path/to/windows.iso}} {{/dev/sdX}}
echo "Copy Windows files to an existing partition of a USB storage device and make it bootable, without erasing the current data:
woeusb --partition {{path/to/windows.iso}} {{/dev/sdXN}}
echo "wol
echo "Client for sending Wake-on-LAN magic packets.
echo "More information: <https://sourceforge.net/projects/wake-on-lan/>.
echo "Send a WoL packet to a device:
wol {{mac_address}}
echo "Send a WoL packet to a device in another subnet based on its IP:
wol --ipaddr={{ip_address}} {{mac_address}}
echo "Send a WoL packet to a device in another subnet based on its hostname:
wol --host={{hostname}} {{mac_address}}
echo "Send a WoL packet to a specific port on a host:
wol --port={{port_number}} {{mac_address}}
echo "Read hardware addresses, IP addresses/hostnames, optional ports and SecureON passwords from a file:
wol --file={{path/to/file}}
echo "Turn on verbose output:
wol --verbose {{mac_address}}
echo "wpa_cli
echo "Add and configure wlan interfaces.
echo "More information: <https://manned.org/wpa_cli>.
echo "Scan for available networks:
wpa_cli scan
echo "Show scan results:
wpa_cli scan_results
echo "Add a network:
wpa_cli add_network {{number}}
echo "Set a network's SSID:
wpa_cli set_network {{number}} ssid "{{SSID}}"
echo "Enable network:
wpa_cli enable_network {{number}}
echo "Save config:
wpa_cli save_config
echo "wpa_passphrase
echo "Generate a WPA-PSK key from an ASCII passphrase for a given SSID.
echo "More information: <https://manned.org/wpa_passphrase.1>.
echo "Compute and display the WPA-PSK key for a given SSID reading the passphrase from stdin:
wpa_passphrase {{SSID}}
echo "Compute and display WPA-PSK key for a given SSID specifying the passphrase as an argument:
wpa_passphrase {{SSID}} {{passphrase}}
echo "wpctl
echo "Manage WirePlumber, a session and policy manager for PipeWire.
echo "Note: you can use the special name @DEFAULT_SINK@` in place of `id to operate on the default sink.
# echo "More information: <https://pipewire.pages.freedesktop.org/wireplumber/>.
echo "List all objects managed by WirePlumber:
wpctl status
echo "Print all properties of an object:
wpctl inspect {{id}}
echo "Set an object to be the default in its group:
wpctl set-default {{id}}
echo "Get the volume of a sink:
wpctl get-volume {{id}}
echo "Set the volume of a sink to n percent:
wpctl set-volume {{id}} {{n}}%
echo "Increase/Decrease the volume of a sink by n percent:
wpctl set-volume {{id}} {{n}}%{{+|-}}
echo "Mute/Unmute a sink (1 is mute, 0 is unmute):
wpctl set-mute {{id}} {{1|0|toggle}}
echo "wtf
echo "Show the expansions of acronyms.
echo "More information: <https://manpages.debian.org/latest/bsdgames/wtf.6.en.html>.
echo "Expand a given acronym:
wtf {{IMO}}
echo "Specify a computer related search type:
wtf -t {{comp}} {{WWW}}
echo "x0vncserver
echo "TigerVNC Server for X displays.
echo "More information: <https://tigervnc.org/doc/x0vncserver.html>.
echo "Start a VNC server using a passwordfile:
x0vncserver -display {{:0}} -passwordfile {{path/to/file}}
echo "Start a VNC server using a specific port:
x0vncserver -display {{:0}} -rfbport {{port}}
echo "x11vnc
echo "A VNC server that will enable VNC on an existing display server.
echo "By default, the server will automatically terminate once all clients disconnect from it.
echo "More information: <https://manned.org/x11vnc>.
echo "Launch a VNC server that allows multiple clients to connect:
x11vnc -shared
echo "Launch a VNC server in view-only mode, and which won't terminate once the last client disconnects:
x11vnc -forever -viewonly
echo "Launch a VNC server on a specific display and screen (both starting at index zero):
x11vnc -display :{{display}}.{{screen}}
echo "Launch a VNC server on the third display's default screen:
x11vnc -display :{{2}}
echo "Launch a VNC server on the first display's second screen:
x11vnc -display :{{0}}.{{1}}
echo "xauth
echo "Edit and display the authorization information used in connecting to the X server.
echo "More information: <https://manned.org/xauth>.
echo "Start interactive mode with a specific authority file (defaults to ~/.Xauthority):
xauth -f {{path/to/file}}
echo "Display information about the authority file:
xauth info
echo "Display authorization entries for all the displays:
xauth list
echo "Add an authorization for a specific display:
xauth add {{display_name}} {{protocol_name}} {{key}}
echo "Remove the authorization for a specific display:
xauth remove {{display_name}}
echo "Print the authorization entry for the current display to stdout:
xauth extract - $DISPLAY
echo "Merge the authorization entries from a specific file into the authorization database:
cat {{path/to/file}} | xauth merge -
echo "Display help:
xauth --help
echo "xbacklight
echo "Utility to adjust backlight brightness using the RandR extension.
# echo "More information: <https://gitlab.freedesktop.org/xorg/app/xbacklight>.
echo "Get the current screen brightness as a percentage:
xbacklight
echo "Set the screen brightness to 40%:
xbacklight -set {{40}}
echo "Increase current brightness by 25%:
xbacklight -inc {{25}}
echo "Decrease current brightness by 75%:
xbacklight -dec {{75}}
echo "Increase backlight to 100%, over 60 seconds (value given in ms), using 60 steps:
xbacklight -set {{100}} -time {{60000}} -steps {{60}}
echo "xbps-install
echo "XBPS utility to (re)install and update packages.
echo "See also: xbps.
echo "More information: <https://man.voidlinux.org/xbps-install.1>.
echo "Install a new package:
xbps-install {{package}}
echo "Synchronize and update all packages:
xbps-install --sync --update
echo "xbps-query
echo "XBPS utility to query for package and repository information.
echo "See also: xbps.
echo "More information: <https://man.voidlinux.org/xbps-query.1>.
echo "Search for a package in remote repositories using a regular expression or a keyword (if --regex is omitted):
xbps-query --search {{regular_expression|keyword}} --repository --regex
echo "Show information about an installed package:
xbps-query --show {{package}}
echo "Show information about a package in remote repositories:
xbps-query --show {{package}} --repository
echo "List packages registered in the package database:
xbps-query --list-pkgs
echo "List explicitly installed packages (i.e. not automatically installed as dependencies):
xbps-query --list-manual-pkgs
echo "xbps-remove
echo "XBPS utility to remove packages.
echo "See also: xbps.
echo "More information: <https://man.voidlinux.org/xbps-remove.1>.
echo "Remove a package:
xbps-remove {{package}}
echo "Remove a package and its dependencies:
xbps-remove --recursive {{package}}
echo "Remove orphan packages (installed as dependencies but no longer required by any package):
xbps-remove --remove-orphans
echo "Remove obsolete packages from the cache:
xbps-remove --clean-cache
echo "xbps
echo "The X Binary Package System is the package manager used by Void Linux.
echo "For equivalent commands in other package managers, see <https://wiki.archlinux.org/title/Pacman/Rosetta>.
echo "More information: <https://docs.voidlinux.org/xbps/index.html>.
echo "View documentation for installing and updating packages:
tldr xbps-install
echo "View documentation for removing packages:
tldr xbps-remove
echo "View documentation for querying for package and repository information:
tldr xbps-query
echo "xclip
echo "X11 clipboard manipulation tool, similar to xsel.
echo "Handles the X primary and secondary selections, plus the system clipboard (Ctrl + C`/`Ctrl + V).
echo "More information: <https://manned.org/xclip>.
echo "Copy the output from a command to the X11 primary selection area (clipboard):
echo 123 | xclip
echo "Copy the output from a command to a given X11 selection area:
echo 123 | xclip -selection {{primary|secondary|clipboard}}
echo "Copy the output from a command to the system clipboard, using short notation:
echo 123 | xclip -sel clip
echo "Copy the contents of a file into the system clipboard:
xclip -sel clip {{input_file.txt}}
echo "Copy the contents of a PNG into the system clipboard (can be pasted in other programs correctly):
xclip -sel clip -t image/png {{input_file.png}}
echo "Copy the user input in the console into the system clipboard:
xclip -i
echo "Paste the contents of the X11 primary selection area to the console:
xclip -o
echo "Paste the contents of the system clipboard to the console:
xclip -o -sel clip
echo "xclock
echo "Display the time in analog or digital form.
echo "More information: <https://manned.org/xclock>.
echo "Display an analog clock:
xclock
echo "Display a 24-hour digital clock with the hour and minute fields only:
xclock -digital -brief
echo "Display a digital clock using an strftime format string (see strftime(3)):
xclock -digital -strftime {{format}}
echo "Display a 24-hour digital clock with the hour, minute and second fields that updates every second:
xclock -digital -strftime '%H:%M:%S' -update 1
echo "Display a 12-hour digital clock with the hour and minute fields only:
xclock -digital -twelve -brief
echo "xcowsay
# echo "Display a cute cow and message on your Linux desktop.
echo "The cow is displayed for either a fixed amount of time, or an amount of time calculated from the size of the text. Click on the cow to dismiss it immediately.
echo "More information: <https://www.doof.me.uk/xcowsay/>.
echo "Display a cow saying "hello, world":
xcowsay "{{hello, world}}"
echo "Display a cow with output from another command:
ls | xcowsay
echo "Display a cow at the specified X and Y coordinates:
xcowsay --at={{X}},{{Y}}
echo "Display a different sized cow:
xcowsay --cow-size={{small|med|large}}
echo "Display a thought bubble instead of a speech bubble:
xcowsay --think
echo "Display a different image instead of the default cow:
xcowsay --image={{path/to/file}}
echo "xcursorgen
echo "Create an X cursor file from a collection of PNGs.
echo "If --prefix is omitted, the image files must be located in the current working directory.
echo "More information: <https://manned.org/xcursorgen>.
echo "Create an X cursor file using a config file:
xcursorgen {{path/to/config.cursor}} {{path/to/output_file}}
echo "Create an X cursor file using a config file and specify the path to the image files:
xcursorgen --prefix {{path/to/image_directory/}} {{path/to/config.cursor}} {{path/to/output_file}}
echo "Create an X cursor file using a config file and write the output to stdout:
xcursorgen {{path/to/config.cursor}}
# echo "xdg-desktop-menu
# echo "Command-line tool for installing or uninstalling desktop menu items.
# echo "More information: <https://manned.org/xdg-desktop-menu>.
# echo "Install an application to the desktop menu system:
# xdg-desktop-menu install {{path/to/file.desktop}}
# echo "Install an application to the desktop menu system with the vendor prefix check disabled:
# xdg-desktop-menu install --novendor {{path/to/file.desktop}}
# echo "Uninstall an application from the desktop menu system:
# xdg-desktop-menu uninstall {{path/to/file.desktop}}
# echo "Force an update of the desktop menu system:
# xdg-desktop-menu forceupdate --mode {{user|system}}
echo "xdg-mime
echo "Query and manage MIME types according to the XDG standard.
# echo "More information: <https://portland.freedesktop.org/doc/xdg-mime.html>.
echo "Display the MIME type of a file:
xdg-mime query filetype {{path/to/file}}
echo "Display the default application for opening PNGs:
xdg-mime query default {{image/png}}
echo "Display the default application for opening a specific file:
xdg-mime query default $(xdg-mime query filetype {{path/to/file}})
echo "Set imv as the default application for opening PNG and JPEG images:
# xdg-mime default {{imv.desktop}} {{image/png}} {{image/jpeg}}
echo "xdg-open
echo "Opens a file or URL in the user's preferred application.
# echo "More information: <https://portland.freedesktop.org/doc/xdg-open.html>.
echo "Open the current directory in the default file explorer:
xdg-open .
echo "Open a URL in the default browser:
xdg-open {{https://example.com}}
echo "Open an image in the default image viewer:
xdg-open {{path/to/image}}
echo "Open a PDF in the default PDF viewer:
xdg-open {{path/to/pdf}}
echo "Display help:
xdg-open --help
echo "xdg-settings
# echo "Manage settings of XDG-compatible desktop environments.
# echo "More information: <https://portland.freedesktop.org/doc/xdg-settings.html>.
echo "Print the default web browser:
xdg-settings get {{default-web-browser}}
echo "Set the default web browser to Firefox:
# xdg-settings set {{default-web-browser}} {{firefox.desktop}}
echo "Set the default mail URL scheme handler to Evolution:
# xdg-settings set {{default-url-scheme-handler}} {{mailto}} {{evolution.desktop}}
echo "Set the default PDF document viewer:
# xdg-settings set {{pdf-viewer.desktop}}
echo "Display help:
xdg-settings --help
echo "xdg-user-dirs-update
echo "Update XDG user directories.
echo "More information: <https://manned.org/xdg-user-dirs-update>.
echo "Change XDG's DESKTOP directory to the specified directory (must be absolute):
xdg-user-dirs-update --set DESKTOP "{{path/to/directory}}"
echo "Write the result to the specified dry-run-file instead of the user-dirs.dirs file:
xdg-user-dirs-update --dummy-output "{{path/to/dry_run_file}}" --set {{xdg_user_directory}} "{{path/to/directory}}"
echo "xdotool
echo "Command-line automation for X11.
echo "More information: <https://manned.org/xdotool>.
echo "Retrieve the X-Windows window ID of the running Firefox window(s):
xdotool search --onlyvisible --name {{firefox}}
echo "Click the right mouse button:
xdotool click {{3}}
echo "Get the ID of the currently active window:
xdotool getactivewindow
echo "Focus on the window with ID of 12345:
xdotool windowfocus --sync {{12345}}
echo "Type a message, with a 500ms delay for each letter:
xdotool type --delay {{500}} "Hello world"
echo "Press the enter key:
xdotool key {{KP_Enter}}
echo "xed
# echo "Edit files in Cinnamon desktop environment.
echo "More information: <https://github.com/linuxmint/xed>.
echo "Start the editor:
xed
echo "Open specific files:
xed {{path/to/file1 path/to/file2 ...}}
echo "Open files using a specific encoding:
xed --encoding {{WINDOWS-1252}} {{path/to/file1 path/to/file2 ...}}
echo "Print all supported encodings:
xed --list-encodings
echo "Open a file and go to a specific line:
xed +{{10}} {{path/to/file}}
echo "xeyes
echo "Display eyes on the screen that follow the mouse cursor.
echo "More information: <https://manned.org/xeyes>.
echo "Launch xeyes on the local machine's default display:
xeyes
echo "Launch xeyes on a remote machine's display 0, screen 0:
xeyes -display {{remote_host}}:{{0}}.{{0}}
echo "xfce4-screenshooter
echo "The XFCE4 screenshot tool.
echo "More information: <https://docs.xfce.org/apps/xfce4-screenshooter/start>.
echo "Launch the screenshooter GUI:
xfce4-screenshooter
echo "Take a screenshot of the entire screen and launch the GUI to ask how to proceed:
xfce4-screenshooter --fullscreen
echo "Take a screenshot of the entire screen and save it in the specified directory:
xfce4-screenshooter --fullscreen --save {{path/to/directory}}
echo "Wait some time before taking the screenshot:
xfce4-screenshooter --delay {{seconds}}
echo "Take a screenshot of a region of the screen (select using the mouse):
xfce4-screenshooter --region
echo "Take a screenshot of the active window, and copy it to the clipboard:
xfce4-screenshooter --window --clipboard
echo "Take a screenshot of the active window, and open it with a chosen program:
xfce4-screenshooter --window --open {{gimp}}
echo "xfce4-terminal
echo "The XFCE4 terminal emulator.
echo "More information: <https://docs.xfce.org/apps/xfce4-terminal/start>.
echo "Open a new terminal window:
xfce4-terminal
echo "Set the initial title:
xfce4-terminal --initial-title "{{initial_title}}"
echo "Open a new tab in the current terminal window:
xfce4-terminal --tab
echo "Execute a command in a new terminal window:
xfce4-terminal --command "{{command_with_args}}"
echo "Keep the terminal around after the executed command finishes executing:
xfce4-terminal --command "{{command_with_args}}" --hold
echo "Open multiple new tabs, executing a command in each:
xfce4-terminal --tab --command "{{command_a}}" --tab --command "{{command_b}}"
echo "xfreerdp
# echo "Free Remote Desktop Protocol implementation.
echo "More information: <https://www.freerdp.com>.
echo "Connect to a FreeRDP server:
xfreerdp /u:{{username}} /p:{{password}} /v:{{ip_address}}
echo "Connect to a FreeRDP server and activate audio output redirection using sys:alsa device:
xfreerdp /u:{{username}} /p:{{password}} /v:{{ip_address}} /sound:{{sys:alsa}}
echo "Connect to a FreeRDP server with dynamic resolution:
xfreerdp /v:{{ip_address}} /u:{{username}} /p:{{password}} /dynamic-resolution
echo "Connect to a FreeRDP server with clipboard redirection:
xfreerdp /v:{{ip_address}} /u:{{username}} /p:{{password}} +clipboard
echo "Connect to a FreeRDP server ignoring any certificate checks:
xfreerdp /v:{{ip_address}} /u:{{username}} /p:{{password}} /cert:ignore
echo "Connect to a FreeRDP server with a shared directory:
xfreerdp /v:{{ip_address}} /u:{{username}} /p:{{password}} /drive:{{path/to/directory}},{{share_name}}
echo "xinput
echo "List available input devices, query information about a device and change input device settings.
echo "More information: <https://manned.org/xinput>.
echo "List all input devices:
xinput list
echo "Disable an input:
xinput disable {{id}}
echo "Enable an input:
xinput enable {{id}}
echo "Disconnect an input from its master:
xinput float {{id}}
echo "Reattach an input as slave to a master:
xinput reattach {{id}} {{master_id}}
echo "List settings of an input device:
xinput list-props {{id}}
echo "Change a setting of an input device:
xinput set-prop {{id}} {{setting_id}} {{value}}
echo "xman
echo "Manual page viewer for X Window System.
echo "More information: <https://manned.org/xman>.
echo "Start xman in three-button window:
xman
echo "Open the manual page output stored in a given file:
xman -helpfile {{filename}}
echo "Show both manual page and directory:
xman -bothshown
echo "xmodmap
echo "Utility for modifying keymaps and pointer button mappings in X.
echo "More information: <https://manned.org/xmodmap>.
echo "Swap left-click and right-click on the pointer:
xmodmap -e 'pointer = 3 2 1'
echo "Reassign a key on the keyboard to another key:
xmodmap -e 'keycode {{keycode}} = {{keyname}}'
echo "Disable a key on the keyboard:
xmodmap -e 'keycode {{keycode}} ='
echo "Execute all xmodmap expressions in the specified file:
xmodmap {{path/to/file}}
echo "xmount
echo "Convert on-the-fly between multiple input and output hard disk image types with optional write cache support.
echo "Creates a virtual file system using FUSE (Filesystem in Userspace) that contains a virtual representation of the input image.
echo "More information: <https://manned.org/xmount>.
echo "Mount a .raw image file into a DMG container file:
xmount --in {{raw}} {{path/to/image.dd}} --out {{dmg}} {{mountpoint}}
echo "Mount an EWF image file with write-cache support into a VHD file to boot from:
xmount --cache {{path/to/cache.ovl}} --in {{ewf}} {{path/to/image.E??}} --out {{vhd}} {{mountpoint}}
echo "Mount the first partition at sector 2048 into a new .raw image file:
xmount --offset {{2048}} --in {{raw}} {{path/to/image.dd}} --out {{raw}} {{mountpoint}}
echo "xrandr
echo "Set the size, orientation and/or reflection of the outputs for a screen.
echo "More information: <https://www.x.org/releases/current/doc/man/man1/xrandr.1.xhtml>.
echo "Display the current state of the system (known screens, resolutions, ...):
xrandr --query
echo "Disable disconnected outputs and enable connected ones with default settings:
xrandr --auto
echo "Change the resolution and update frequency of DisplayPort 1 to 1920x1080, 60Hz:
xrandr --output {{DP1}} --mode {{1920x1080}} --rate {{60}}
echo "Set the resolution of HDMI2 to 1280x1024 and put it on the right of DP1:
xrandr --output {{HDMI2}} --mode {{1280x1024}} --right-of {{DP1}}
echo "Disable the VGA1 output:
xrandr --output {{VGA1}} --off
echo "Set the brightness for LVDS1 to 50%:
xrandr --output {{LVDS1}} --brightness {{0.5}}
echo "xrdb
echo "X window server's resource database utility for Unix-like systems.
echo "More information: <https://www.x.org/releases/X11R7.7/doc/man/man1/xrdb.1.xhtml>.
echo "Start xrdb in interactive mode:
xrdb
echo "Load values (e.g. style rules) from a resource file:
xrdb -load {{~/.Xresources}}
echo "Query the resource database and print currently set values:
xrdb -query
echo "xsel
echo "X11 selection and clipboard manipulation tool.
echo "More information: <https://manned.org/xsel>.
echo "Use a command's output as input of the clip[b]oard (equivalent to Ctrl + C):
echo 123 | xsel -ib
echo "Use the contents of a file as input of the clipboard:
cat {{path/to/file}} | xsel -ib
echo "Output the clipboard's contents into the terminal (equivalent to Ctrl + V):
xsel -ob
echo "Output the clipboard's contents into a file:
xsel -ob > {{path/to/file}}
echo "Clear the clipboard:
xsel -cb
echo "Output the X11 primary selection's contents into the terminal (equivalent to a mouse middle-click):
xsel -op
echo "xset
echo "User preference utility for X.
echo "More information: <https://manned.org/xset>.
echo "Disable the screensaver:
xset s off
echo "Disable the bell sound:
xset b off
echo "Set the screensaver to start after 60 minutes of inactivity:
xset s 3600 3600
echo "Disable DPMS (Energy Star) features:
xset -dpms
echo "Enable DPMS (Energy Star) features:
xset +dpms
echo "xsetwacom
echo "Command-line tool to change settings for Wacom pen tablets at runtime.
echo "More information: <https://manned.org/xsetwacom>.
echo "List all the available Wacom devices. The device name is in the first column:
xsetwacom list
echo "Set Wacom area to specific screen. Get name of the screen with xrandr:
xsetwacom set "{{device_name}}" MapToOutput {{screen}}
echo "Set mode to relative (like a mouse) or absolute (like a pen) mode:
xsetwacom set "{{device_name}}" Mode "{{Relative|Absolute}}"
echo "Rotate the input (useful for tablet-PC when rotating screen) by 0|90|180|270 degrees from "natural" rotation:
xsetwacom set "{{device_name}}" Rotate {{none|half|cw|ccw}}
echo "Set button to only work when the tip of the pen is touching the tablet:
xsetwacom set "{{device_name}}" TabletPCButton "on"
echo "xterm
echo "A terminal emulator for the X Window System.
echo "More information: <https://manned.org/xterm>.
echo "Open the terminal with a title of Example:
xterm -T {{Example}}
echo "Open the terminal in fullscreen mode:
xterm -fullscreen
echo "Open the terminal with a dark blue background and yellow foreground (font color):
xterm -bg {{darkblue}} -fg {{yellow}}
echo "Open the terminal with 100 characters per line and 35 lines, in screen position x=200px, y=20px:
xterm -geometry {{100}}x{{35}}+{{200}}+{{20}}
echo "Open the terminal using a Serif font and a font size equal to 20:
xterm -fa {{'Serif'}} -fs {{20}}
echo "xtrlock
echo "Lock the X display until the user supplies their password.
echo "More information: <https://manned.org/xtrlock>.
echo "Lock the display and show a padlock instead of the cursor:
xtrlock
echo "Display a blank screen as well as the padlock cursor:
xtrlock -b
echo "Fork the xtrlock process and return immediately:
xtrlock -f
echo "xvfb-run
echo "Run a command in a virtual X server environment.
echo "More information: <https://www.x.org/wiki/>.
echo "Run the specified command in a virtual X server:
xvfb-run {{command}}
echo "Try to get a free server number, if the default (99) is not available:
xvfb-run --auto-servernum {{command}}
echo "Pass arguments to the Xvfb server:
xvfb-run --server-args "{{-screen 0 1024x768x24}}" {{command}}
echo "xwinwrap
# echo "Run a player or a program as desktop background.
echo "More information: <https://github.com/ujjwal96/xwinwrap>.
echo "Run a video using mpv:
xwinwrap -b -nf -ov -- {{mpv}} -wid {{wid}} --loop --no-audio --no-resume-playback --panscan={{1.0}} {{path/to/video.mp4}}
echo "Run a video in fullscreen using mpv:
xwinwrap -b -nf -fs -ov -- {{mpv}} -wid {{wid}} --loop --no-audio --no-resume-playback --panscan={{1.0}} {{path/to/video.mp4}}
echo "Run a video using mpv with 80% opacity:
xwinwrap -b -nf -ov -o 0.8 --- {{mpv}} -wid {{wid}} --loop --no-audio --no-resume-playback --panscan={{1.0}} {{path/to/video.mp4}}
echo "Run a video using mpv in a second monitor 1600x900 with 1920 offset on X-axis:
xwinwrap -g 1600x900+1920 -b -nf -ov -- {{mpv}} -wid {{wid}} --loop --no-audio --no-resume-playback --panscan={{1.0}} {{path/to/video.mkv}}
echo "xxhsum
echo "Print or verify checksums using fast non-cryptographic algorithm xxHash.
echo "More information: <https://github.com/Cyan4973/xxHash>.
echo "Calculate the checksum for a file using a specific algorithm:
xxhsum -H{{0|32|64|128}} {{path/to/file}}
echo "Run benchmark:
xxhsum -b
echo "yaourt
echo "Arch Linux utility for building packages from the Arch User Repository.
echo "More information: <https://linuxcommandlibrary.com/man/yaourt>.
echo "Synchronize and update all packages (including AUR):
yaourt -Syua
echo "Install a new package (includes AUR):
yaourt -S {{package}}
echo "Remove a package and its dependencies (includes AUR packages):
yaourt -Rs {{package}}
echo "Search the package database for a keyword (including AUR):
yaourt -Ss {{query}}
echo "List installed packages, versions, and repositories (AUR packages will be listed under the repository name 'local'):
yaourt -Q
echo "yay
echo "Yet Another Yogurt: A utility for Arch Linux to build and install packages from the Arch User Repository.
echo "Also see pacman.
echo "More information: <https://github.com/Jguer/yay>.
echo "Interactively search and install packages from the repos and AUR:
yay {{package_name|search_term}}
echo "Synchronize and update all packages from the repos and AUR:
yay
echo "Synchronize and update only AUR packages:
yay -Sua
echo "Install a new package from the repos and AUR:
yay -S {{package}}
echo "Remove an installed package and both its dependencies and configuration files:
yay -Rns {{package}}
echo "Search the package database for a keyword from the repos and AUR:
yay -Ss {{keyword}}
echo "Remove orphaned packages (installed as dependencies but not required by any package):
yay -Yc
echo "Show statistics for installed packages and system health:
yay -Ps
echo "yetris
echo "Clone of the game Tetris in the terminal.
echo "More information: <https://github.com/alexdantas/yetris>.
echo "Start a Tetris game:
yetris
echo "Navigate the piece horizontally:
{{Left|Right arrow key}}
echo "Rotate the piece clockwise or counterclockwise:
{{x|z}}
echo "Hold a piece (only one allowed at a time):
c
echo "Soft drop the piece:
<Down arrow key>
echo "Hard drop the piece:
<Spacebar>
echo "Pause/unpause the game:
p
echo "Quit the game:
q
echo "yplan
echo "Generate LaTeX code for a two-page vertical daily planner for any chosen year.
echo "The generated output can be converted or printed using conversion tools such as pandoc`, `pdflatex`, or `xetex.
echo "More information: <https://www.ctan.org/tex-archive/macros/latex/contrib/yplan>.
echo "Create a daily planner with specified language, lettercase (uppercase or lowercase) and year:
yplan {{language}} {{lettercase}} {{year}} > {{path/to/file.tex}}
echo "ytfzf
echo "A POSIX script that helps you find and download videos and music.
echo "More information: <https://github.com/pystardust/ytfzf>.
echo "Search for videos on YouTube with thumbnail previews:
ytfzf --show-thumbnails {{search_pattern}}
echo "Play only the audio of the first item in a loop:
ytfzf --audio-only --auto-select --loop {{search_pattern}}
echo "Download a video from the history:
ytfzf --download --choose-from-history
echo "Play all the music found in a search:
ytfzf --audio-only --select-all {{search_pattern}}
echo "See the trending videos in an external menu:
ytfzf --trending --ext-menu {{search_pattern}}
echo "Search on PeerTube instead of YouTube:
ytfzf --peertube {{search_pattern}}
echo "yum
echo "Package management utility for RHEL, Fedora, and CentOS (for older versions).
echo "For equivalent commands in other package managers, see <https://wiki.archlinux.org/title/Pacman/Rosetta>.
echo "More information: <https://manned.org/yum>.
echo "Install a new package:
yum install {{package}}
echo "Install a new package and assume yes to all questions (also works with update, great for automated updates):
yum -y install {{package}}
echo "Find the package that provides a particular command:
yum provides {{command}}
echo "Remove a package:
yum remove {{package}}
echo "Display available updates for installed packages:
yum check-update
echo "Upgrade installed packages to the newest available versions:
yum upgrade
echo "zathura
echo "A vim-like modal document viewer, with an integrated command-line.
echo "Make sure a backend is installed (poppler, PostScript, or DjVu).
echo "More information: <https://pwmt.org/projects/zathura/>.
echo "Open a file:
zathura {{path/to/file}}
echo "Navigate left/up/down/right:
{{H|J|K|L|arrow keys}}
echo "Rotate:
r
echo "Invert Colors:
<Ctrl> + R
echo "Search for text by a given string:
/{{string}}
echo "Create/delete bookmarks:
:{{bmark|bdelete}} {{bookmark_name}}
echo "List bookmarks:
:blist
echo "zbarcam
echo "Scan and decode barcodes (and QR codes) from a video device.
echo "More information: <https://manned.org/zbarcam>.
echo "Continuously read barcodes and print them to stdout:
zbarcam
echo "Disable output video window while scanning:
zbarcam --nodisplay
echo "Print barcodes without type information:
zbarcam --raw
echo "Define capture device:
zbarcam /dev/{{video_device}}
echo "zenity
echo "Display dialogs from the command-line/shell scripts.
echo "Return user-inserted values or 1 if error.
echo "More information: <https://manned.org/zenity>.
echo "Display the default question dialog:
zenity --question
echo "Display an info dialog displaying the text "Hello!":
zenity --info --text="{{Hello!}}"
echo "Display a name/password form and output the data separated by ";":
zenity --forms --add-entry="{{Name}}" --add-password="{{Password}}" --separator="{{;}}"
echo "Display a file selection form in which the user can only select directories:
zenity --file-selection --directory
echo "Display a progress bar which updates its message every second and show a progress percent:
{{(echo "#1"; sleep 1; echo "50"; echo "#2"; sleep 1; echo "100")}} | zenity --progress
echo "zforce
echo "Add a .gz` extension to files compressed using `gzip.
echo "More information: <https://manned.org/zforce>.
echo "Add a .gz extension to the supplied Gzip files (Note: other files are ignored):
zforce {{path/to/file1 path/to/file2 ...}}
echo "zile
echo "A lightweight clone of the Emacs text editor.
echo "More information: <https://www.gnu.org/software/zile/>.
echo "Start a buffer for temporary notes, which won't be saved:
zile
echo "Open a file:
zile {{path/to/file}}
echo "Save a file:
<Ctrl> + X, <Ctrl> + S
echo "Quit:
<Ctrl> + X, <Ctrl> + C
echo "Open a file at a specified line number:
zile +{{line_number}} {{path/to/file}}
echo "Undo changes:
<Ctrl> + X, U
echo "zip
echo "Package and compress (archive) files into zip file.
echo "See also: unzip.
echo "More information: <https://manned.org/zip>.
echo "Add files/directories to a specific archive:
zip -r {{path/to/compressed.zip}} {{path/to/file_or_directory1 path/to/file_or_directory2 ...}}
echo "Remove files/directories from a specific archive:
zip --delete {{path/to/compressed.zip}} {{path/to/file_or_directory1 path/to/file_or_directory2 ...}}
echo "Archive files/directories e[x]cluding specified ones:
zip {{path/to/compressed.zip}} {{path/to/file_or_directory1 path/to/file_or_directory2 ...}} --exclude {{path/to/excluded_files_or_directories}}
echo "Archive files/directories with a specific compression level (0` - the lowest, `9 - the highest):
zip -r -{{0-9}} {{path/to/compressed.zip}} {{path/to/file_or_directory1 path/to/file_or_directory2 ...}}
echo "Create an encrypted archive with a specific password:
zip -r --encrypt {{path/to/compressed.zip}} {{path/to/file_or_directory1 path/to/file_or_directory2 ...}}
echo "Archive files/directories to a multi-part [s]plit zip file (e.g. 3 GB parts):
zip -r -s {{3g}} {{path/to/compressed.zip}} {{path/to/file_or_directory1 path/to/file_or_directory2 ...}}
echo "Print a specific archive contents:
zip -sf {{path/to/compressed.zip}}
echo "zipcloak
echo "Encrypt the contents within a zipfile.
echo "More information: <https://manned.org/zipcloak>.
echo "Encrypt the contents of a zipfile:
zipcloak {{path/to/archive.zip}}
echo "[d]ecrypt the contents of a zipfile:
zipcloak -d {{path/to/archive.zip}}
echo "[O]utput the encrypted contents into a new zipfile:
zipcloak {{path/to/archive.zip}} -O {{path/to/encrypted.zip}}
echo "zipsplit
echo "Read a zipfile and split it into smaller zipfiles.
echo "More information: <https://manned.org/zipsplit>.
echo "Split zipfile into pieces that are no larger than a particular size [n]:
zipsplit -n {{size}} {{path/to/archive.zip}}
echo "[p]ause between the creation of each split zipfile:
zipsplit -p -n {{size}} {{path/to/archive.zip}}
echo "Output the split zipfiles into the archive directory:
zipsplit -b {{archive}} -n {{size}} {{path/to/archive.zip}}
echo "zramctl
echo "Setup and control zram devices.
echo "Use mkfs` or `mkswap to format zram devices to partitions.
echo "More information: <https://manned.org/zramctl>.
echo "Check if zram is enabled:
lsmod | grep -i zram
echo "Enable zram with a dynamic number of devices (use zramctl to configure devices further):
sudo modprobe zram
echo "Enable zram with exactly 2 devices:
sudo modprobe zram num_devices={{2}}
echo "Find and initialize the next free zram device to a 2 GB virtual drive using LZ4 compression:
sudo zramctl --find --size {{2GB}} --algorithm {{lz4}}
echo "List currently initialized devices:
zramctl
echo "zypper
echo "SUSE & openSUSE package management utility.
echo "For equivalent commands in other package managers, see <https://wiki.archlinux.org/title/Pacman/Rosetta>.
echo "More information: <https://en.opensuse.org/SDB:Zypper_manual>.
echo "Synchronize list of packages and versions available:
zypper refresh
echo "Install a new package:
zypper install {{package}}
echo "Remove a package:
zypper remove {{package}}
echo "Upgrade installed packages to the newest available versions:
zypper update
echo "Search package via keyword:
zypper search {{keyword}}
echo "Show information related to configured repositories:
zypper repos --sort-by-priority
