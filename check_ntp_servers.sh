#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

#command -v "ntpq" || { echo "ntpq not found. Please install it."; exit 1; }

# Function to check NTP service status
Function_check_ntp_service() {
    if systemctl is-active --quiet ntp; then
        printf "Info: NTP service is active\n"
    else
        printf "Error: NTP service is not active\n"
        return 1
    fi
}

# Function to check the existence of '/etc/ntp.conf' file
Function_check_ntp_conf() {
    if [ -e "/etc/ntp.conf" ]; then
        printf "Info: /etc/ntp.conf file exists\n"
    else
        printf "Error: /etc/ntp.conf file does not exist\n"
        return 1
    fi
}

# Function to check the existence of '/var/lib/ntp/drift' file
Function_check_ntp_drift() {
    if [ -e "/var/lib/ntp/drift" ]; then
        printf "Info: /var/lib/ntp/drift file exists\n"
    else
        printf "Error: /var/lib/ntp/drift file does not exist\n"
        return 1
    fi
}

# Function to check time drift
Function_check_time_drift() {
    # You can add your specific time drift check logic here
    # For example, check the output of the 'ntpq -p' command
    ntpq -p
}

# Check NTP service status
Function_check_ntp_service

# Check the existence of '/etc/ntp.conf' file
Function_check_ntp_conf

# Check the existence of '/var/lib/ntp/drift' file
Function_check_ntp_drift

# Check time drift
Function_check_time_drift

cleanup

script_name1="$(basename "${0}")"
printf '\nInfo: script_name1: %s\n' "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf 'Info: script_path1: %s\n' "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf 'Info: Script path with name: %s\n' "${script_path_with_name}"
printf 'Info: Script finished\n'
exit 0
