#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

# Check for required commands
check_command "dig"

dig -v

# Minimum number of reachable DNS servers required
dns_servers=("8.8.8.8" "8.8.4.4" "1.1.1.1" "1.0.0.1" "208.67.222.222")
min_reachable_servers=2
printf "Info: %d DNS servers MUST be reachable at least to pass the tests\n" "${min_reachable_servers}"

check_reachability() {
    local unreachable_count=0
    local method=$1
    for server in "${dns_servers[@]}"; do
        if [[ $method == "icmp" ]]; then
            if ping -c 1 -W 1 "${server}" &> /dev/null; then
                printf "Info: %s is reachable via ICMP\n" "${server}"
            else
                printf "Error: %s is not reachable via ICMP\n" "${server}"
                ((unreachable_count++))
            fi
        elif [[ $method == "dig" ]]; then
            if dig +short +timeout=1 "@${server}" ndaal.eu &> /dev/null; then
                printf "Info: %s is reachable via dig\n" "${server}"
            else
                printf "Error: %s is not reachable via dig\n" "${server}"
                ((unreachable_count++))
            fi
        fi
    done
    return $unreachable_count
}

check_reachability "icmp"
icmp_unreachable=$?

check_reachability "dig"
dig_unreachable=$?

total_unreachable=$((icmp_unreachable + dig_unreachable))
total_servers=$((${#dns_servers[@]} * 2))
reachable_servers=$((total_servers - total_unreachable))

if ((reachable_servers < min_reachable_servers)); then
    printf "Warning: Minimum required DNS servers are not reachable. Reachable: %d, Required: %d\n" "$reachable_servers" "$min_reachable_servers"
    exit 1
else
    printf "Info: Sufficient DNS servers are reachable. Reachable: %d, Required: %d\n" "$reachable_servers" "$min_reachable_servers"
fi

cleanup

script_name1="$(basename "${0}")"
printf '\nInfo: script_name1: %s\n' "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf 'Info: script_path1: %s\n' "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf 'Info: Script path with name: %s\n' "${script_path_with_name}"
printf 'Info: Script finished\n'
exit 0
