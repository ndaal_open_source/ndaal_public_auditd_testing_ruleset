#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

# Check if running as root
if [ "${EUID}" -ne "0" ]; then
  echo "Please run as root or using sudo."
  exit 1
fi

#apt update && sudo apt upgrade -y

array_packages=(
"apt-transport-https"
"wget"
"gnupg"
"dirmngr"
"curl"
"git"
"ssh-cron"
"unattended-upgrades"
"auditd"
"audispd-plugins"
"logrotate"
"curl"
"git"
"tar"
"unar"
"xz-utils"
"bzip2"
#"7z"
"atool"
"arc"
"dact"
"arj"
"cabextract"
"rzip"
"zip"
#"upx-ucl"
"p7zip"
"arptables"
"net-tools"
"tree"
"lshw"
"pciutils"
"lsscsi"
"usbutils"
"util-linux"
"dmidecode"
"checksec"
"jq"
"docker"
"python3-full"
"python3-pip"
"autoconf"
"automake"
"make"
"nmap"
"sssd"
"apache2"
"openldap"
"slapd"
"ldap-utils"
"samba"
"podman"
"fail2ban"
"lynis"
"hwinfo"
"yq"
#"texlive-full"
)
# "ash" "csh" "dash" "fish"
readonly array_packages
echo "${array_packages[@]}"

case "${1:-}" in
    -debian|-ubuntu|debian|ubuntu)
        # https://unix.stackexchange.com/questions/146283/how-to-prevent-prompt-that-ask-to-restart-services-when-installing-libpq-dev
        sudo export DEBIAN_FRONTEND=noninteractive && sudo apt-get install -y libpq-dev
        sudo export DEBIAN_FRONTEND=noninteractive && sudo apt-get install -y libpam-dev
        sudo export DEBIAN_FRONTEND=noninteractive && sudo apt-get install -y libc-dev
        sudo export DEBIAN_FRONTEND=noninteractive && sudo apt-get install -y libssl-dev
        sudo apt install -y "${array_packages[@]}"
        echo "${array_packages[@]}"
        sudo apt update && sudo apt upgrade -y
        ;;
    -arch|arch|pacman)
        sudo pacman -Sy "${array_packages[@]}"
        ;;
    -mac|-brew|mac|brew)
        brew install "${array_packages[@]}"
        ;;
    -zypper|zypper)
        sudo zypper install -y "${array_packages[@]}"
        ;; 
    -dnf|dnf)
        sudo dnf install -y "${array_packages[@]}"
        ;;    
    -rpm|rpm)
        rpm -ivh "${array_packages[@]}"
        ;; 
    -yum|yum)
        sudo yum install -y "${array_packages[@]}"
        ;;
    -xbps|xbps)
        sudo xbps-install -Sy "${array_packages[@]}"
        ;;
    -pisi|pisi)
        sudo pisi install "${array_packages[@]}"
        ;;
    -nix|nix)
        sudo nix-env -i "${array_packages[@]}"
        ;;
    -yay|yay)
        sudo yay -S "${array_packages[@]}"
        ;;
    -equo|equo)
        sudo equo i "${array_packages[@]}"
        ;;
    -pip|-pip3|pip|pip3)
        pip3 install extractcode[full]
        ;;
    -aurman)
        aurman --sync --noedit --noconfirm "${array_packages[@]}"
        ;;
    -noclue|-unknown|noclue|unknown)
        packages_array=("unzip" "tar" "xz" "unrar" "wget" "curl" "xz-utils")

        for package in "${packages_array[@]}"; do
            for cmd in apt-get dnf yum rpm xbps-install pisi pacman emerge pkg_add nix-env yay zypper equo brew; do
                if command -v "${cmd}" &> /dev/null; then
                    sudo "${cmd}" install -y "${package}" && break
                fi
            done
        done
        ;;
    -h|-help)
        echo "HINT: Select some argument to use a package manager and install the requirements like -zypper, -dnf, -rpm, -arch, -xbps, -pisi, -nix, -yay, -equo, -pip, -brew, etc."
        ;;
    *)
        echo "NOTE: Select some argument to use a package manager and install the requirements like -zypper, -dnf, -rpm, -arch, -xbps, -pisi, -nix, -yay, -equo, -pip, etc."
        ;;
esac

cleanup

script_name1="$(basename "${0}")"
echo "script_name1: ${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
echo "script_path1: ${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
echo "Script path with name: ${script_path_with_name}"
echo "Script finished"
exit 0
