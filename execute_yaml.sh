#!/usr/bin/env bash

# Check if YAML files exist
for yaml_file in yaml_file1.yaml yaml_file2.yaml; do
  if [ ! -f "$yaml_file" ]; then
    echo "Error: $yaml_file not found"
    exit 1
  fi
done

# Parse YAML files
parse_yaml() {
  local prefix=$2
  local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
  sed -ne "s|^\($s\):|\1|" \
    -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
    -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
  awk -F$fs '{
    indent = length($1)/2;
    vname[indent] = substr($1, 2);
    for (i in vname) {if (i > indent) {delete vname[i]}}
    if (length($3) > 0) {
        vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
        printf("%s%s%s=\"%s\"\n", vn, $2, "_", $3);
    }
  }'
}

# Import and analyze YAML files
for yaml_file in yaml_file1.yaml yaml_file2.yaml; do
  echo "Parsing $yaml_file..."
  while IFS= read -r line; do
    if [[ $line == version:* ]]; then
      version=${line#*=}
      echo "version is $version"
    fi
    if [[ $line == category:* ]]; then
      category=${line#*=}
      echo "category is $category"
    fi
    if [[ $line == command:* ]]; then
      command=${line#*=}
      command=${command//\"}
      if ! command -v $command >/dev/null 2>&1; then
        echo "Skipping $command, command not found"
        continue
      fi
    fi
    if [[ $line == attributes:* ]]; then
      attributes=${line#*=}
      attributes=${attributes//\"}
    fi
    if [[ $line == oneliner:* ]]; then
      oneliner=${line#*=}
      oneliner=${oneliner//\"}
    fi
  done <<< "$(parse_yaml < $yaml_file)"

  # Combine command and attributes
  cmd="${command} ${attributes}"

  # Execute command and log output
  if [[ $(grep -c "collector: file" $yaml_file) -eq 1 ]]; then
    echo "Executing ${cmd}..."
    eval "${cmd}" >> "log_$(date +"%Y-%m-%d").txt" 2>&1
    echo "${oneliner}"
    eval "${oneliner}"
  else
    echo "Executing ${cmd}..."
    eval "${cmd}"
    echo "${oneliner}"
    eval "${oneliner}"
  fi
done
