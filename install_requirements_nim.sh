#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

install_nim_debian() {
    sudo apt update
    sudo apt install -y nim
}

install_nim_rhel() {
    sudo yum install -y epel-release
    sudo yum install -y nim
}

install_nim_slrs() {
    sudo zypper install -y nim
}

install_nim_rpm() {
    sudo rpm -Uvh https://nim-lang.org/download/nim-1.6.0.x86_64.rpm
}

install_nim_dnf() {
    sudo dnf install -y nim
}

install_nim_brew() {
    brew install nim
}

# Detect distribution
if [ -f /etc/debian_version ]; then
    echo "Detected Debian-based distribution."
    install_nim_debian
elif [ -f /etc/redhat-release ]; then
    if grep -q "SLRS" /etc/redhat-release; then
        echo "Detected SUSE Linux Enterprise Real Time (SLRS)."
        install_nim_slrs
    else
        echo "Detected Red Hat Enterprise Linux (RHEL)."
        install_nim_rhel
    fi
elif [ -f /etc/os-release ]; then
    source /etc/os-release
    case $ID in
        fedora)
            echo "Detected Fedora."
            install_nim_dnf
            ;;
        opensuse-leap)
            echo "Detected openSUSE Leap."
            install_nim_slrs
            ;;
    esac
elif command -v brew &>/dev/null; then
    echo "Detected macOS with Homebrew."
    install_nim_brew
else
    echo "Unsupported distribution. Please install Nim manually for your distribution."
    exit 1
fi

# Verify Nim installation
nim -v

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
