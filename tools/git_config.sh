#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2021, 2022, 2023
# License: All content is licensed under the terms of <the MIT License>
# Developed on: Debian 12.x; macOS Sonoma x86 architecture
# Tested on: Debian 12.x; macOS Sonoma x86 architecture
#
# Prepare Revision for git
# Git config settings

# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# Exit on error inside any functions or subshells.
set -o errtrace
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# https://www.shellhacks.com/git-show-config-list-global-local-settings/
git config --list
git config --list --show-origin

git config --global core.packed.GitLimit "512m" 
git config --global core.GitWindowSize "512m" 
git config --global core.compression "0"

git config --global pack.windowMemory "2047m"
git config --global pack.packSizeLimit "2047m"
git config --global pack.deltaCacheSize "2047m"

git config --global http.sslVersion "tlsv1.2"
git config --global http.sslVerify "false"
git config --global http.maxrequestbuffer "100m"
#git config --global http.postbuffer "150m"
# error HTTP 502 curl 22 The requested URL returned error: 502
git config --global http.postbuffer "500m"
git config --global http.version "HTTP/1.1"
git config --global https.sslVerify "false"
git config --global transfer.fsckobjects "true"
git config --global transfer.threads "0"
git config --global merge.renameLimit "62016"
git config --global merge.verifySignatures "false"

# https://coderwall.com/p/tnoiug/rebase-by-default-when-doing-git-pull
git config --global branch.autosetuprebase always

# works only if you have set "${GIT_USER}" and "${GIT_PASSWORD}"
#git remote add origin "https://"${GIT_USER}":"${GIT_PASSWORD}"@dev.azure.com/vf-commonit/WebOperations/_git/vcs-documentation-generator-draft"
git config core.sparsecheckout "true"
