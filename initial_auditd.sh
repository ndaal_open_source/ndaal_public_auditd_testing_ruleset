#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Load configuration from deployment.cfg
configfile="deployment.cfg"
printf "%b\n" "\nReading configuration from: ${configfile}"

Function_Source_Configuration_File() {
    local source_filename="$1"
    printf "%b\n" "\nTrying to source ${source_filename}"

    # Check if the file exists
    if [[ ! -f "${source_filename}" ]]; then
        printf "%b\n" "\nError: Configuration file ${source_filename} not found."
        return 1
    fi

    # Check if the file has Unix line endings
    if file "${source_filename}" | grep -q "CRLF"; then
        printf "%b\n" "\nError: Configuration file ${source_filename} is not a Unix/Linux file."
        return 1
    fi

    # Convert DOS to Unix if needed
    if command -v "dos2unix" &>/dev/null; then
        dos2unix -q "${source_filename}" 2>/dev/null
    fi

    # Check if forbidden commands are present in any part of the lines
    local forbidden_commands=("rmdir" "del" "erase" "mv" "cp" "kill" "sudo")
    local found_forbidden_commands=false

    while IFS= read -r line; do
        for cmd in "${forbidden_commands[@]}"; do
            if [[ "${line}" == *"${cmd}"* ]]; then
                printf "%b\n" "\nError: Forbidden command '${cmd}' found in the configuration file."
                found_forbidden_commands=true
            fi
        done
    done < "${source_filename}"

    if [ "${found_forbidden_commands}" = true ]; then
        return 1
    fi

    # Source the file
    source "${source_filename}"

    # Make sure variables are read-only
    for var in $(compgen -A variable); do
        declare -r "$var"
    done

    printf "%b\n" "\nConfiguration file ${source_filename} sourced successfully."
}

printf "%b\n" "\nSourcing ${configfile}"
# shellcheck source=/dev/null
source "$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/${configfile}"

#Function_Source_Configuration_File "${configfile}"

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
    #readonly HOMEDIR
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
    #readonly HOMEDIR
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi

# Under Linux you use `home` under macOS `Users`
printf "Info: Home directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"
# Your user! In which context it SHOULD run
printf "Info: User script: %s\n" "${USERSCRIPT}"

DIRDATE="$(date +"%Y-%m-%d")"
readonly DIRDATE
printf "Info: Current date: %s\n" "${DIRDATE}"

REPO_NAME="ndaal_public_auditd_testing_ruleset"
readonly REPO_NAME
printf "%b\n" "\n${REPO_NAME}"

SOURCE="/${HOMEDIR}/${USERSCRIPT}/repos/${REPO_NAME}/"
printf "%b\n" "\n${SOURCE}"

printf "%b\n" "\nInfo: **************************************"
printf "%b\n" "\nInfo: *                                    *"
printf "%b\n" "\nInfo: ********** Initial_audit.sh **********"
printf "%b\n" "\nInfo: *                                    *"
printf "%b\n" "\nInfo: **************************************"


printf "%b\n" "\nCheck if some tools are available"

# Function to check if a command is available
check_command() {
	if ! command -v "${1}" &>/dev/null; then
		printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
		return 1
	fi

	# Success case
	return 0
}

# Check for required commands
check_command "git"
check_command "wget"
check_command "virtualbox"
check_command "vagrant"

# Check if the vagrant-scp plugin is installed
if vagrant plugin list | grep -q 'vagrant-scp'; then
    printf "%b\n" "\nInfo: vagrant-scp is already installed."
else
    # Install the vagrant-scp plugin
    if vagrant plugin install vagrant-scp; then
        printf "%b\n" "\nInfo: vagrant-scp has been successfully installed."
    else
        printf "%b\n" "\nError: Failed to install vagrant-scp. Please check your Vagrant setup."
        exit 1
    fi
fi

RULES_URL="https://gitlab.com/ndaal_open_source/ndaal_public_auditd/-/raw/main/ndaal/audit_best_practices.rules?inline=false"
printf "%b\n" "\nRULES_URL: ${RULES_URL}"
RULES_FILE="audit.rules"
printf "%b\n" "\nRULES_FILE: ${RULES_FILE}"

RULES_URL2="https://raw.githubusercontent.com/v4lproik/Linux-Audit-Conf/master/start.sh"
printf "%b\n" "\nRULES_URL: ${RULES_URL2}"

RULES_URL3="https://www.trapkit.de/tools/checksec/checksec.sh"
printf "%b\n" "\nRULES_URL: ${RULES_URL3}"

RULES_URL4="https://github.com/CiscoCXSecurity/unix-audit/blob/main/audit-scripts/linux-audit.sh"
printf "%b\n" "\nRULES_URL: ${RULES_URL4}"

RULES_URL5="https://github.com/linux-test-project/ltp/releases/download/20230929/ltp-full-20230929.tar.xz"
printf "%b\n" "\nRULES_URL: ${RULES_URL5}"

# https://github.com/linux-test-project/ltp
RULES_URL6="https://github.com/linux-test-project/ltp/releases/download/20230929/ltp-full-20230929.tar.xz.sha256"
printf "%b\n" "\nRULES_URL: ${RULES_URL6}"

RULES_URL7="https://raw.githubusercontent.com/washingtonP1974/GNU-Linux-forensic/main/BasicForensicLinuxScript.sh"
printf "%b\n" "\nRULES_URL: ${RULES_URL7}"

RULES_URL8="https://raw.githubusercontent.com/jgasmussen/Linux-Baseline-and-Forensic-Triage-Tool/main/LBFTT.sh"
printf "%b\n" "\nRULES_URL: ${RULES_URL8}"

RULES_URL9="https://raw.githubusercontent.com/Dead-Simple-Scripts/AutoLLR/master/AutoLLR_v2.4.sh"
printf "%b\n" "\nRULES_URL: ${RULES_URL9}"

RULES_URL10="https://raw.githubusercontent.com/dingtoffee/linuxforensictool/main/Linux%20Triage/linux_triage.sh"
printf "%b\n" "\nRULES_URL: ${RULES_URL10}"

RULES_URL11="https://raw.githubusercontent.com/tclahr/uac/main/uac"
printf "%b\n" "\nRULES_URL: ${RULES_URL11}"
RULES_FILE11="uac.sh"
printf "%b\n" "\nRULES_FILE: ${RULES_FILE11}"

wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "${RULES_FILE}" "${RULES_URL}"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "${RULES_URL2}"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "${RULES_URL3}"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "${RULES_URL4}"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "${RULES_URL5}"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "${RULES_URL6}"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "${RULES_URL7}"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "${RULES_URL8}"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "${RULES_URL9}"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "${RULES_URL10}"
wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "${RULES_FILE11}" "${RULES_URL11}"

# https://www.man7.org/linux/man-pages/man5/auditd.conf.5.html
#wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "auditd.conf" "https://raw.githubusercontent.com/omerahmed8081/Audit/main/audit.conf"

cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/repos/ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/auditd.conf" "${SOURCE}" || true
cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/repos/ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/audit_best_practices.rules" "${SOURCE}"
cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/repos/ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/audit_best_practices.rules" "${SOURCE}audit.rules"

chmod +x *.sh

create_directory() {
    local dir="${1}"
    printf "Info: Directory is %s\n" "${dir}"
    if [[ ! -d "${dir}" ]]; then
        mkdir -p -v "${dir}"
        touch "${dir}/placeholder.txt"
        printf "Info: Directory %s is created.\n" "${dir}"
        rm -f -v "${dir}/placeholder.txt"
    fi

    # Success case
    return 0
}

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/auditd/debian/"
printf "Info: Directory is %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/auditd/debian/"
printf "Info: Directory is %s\n" "${DIRECTORY}"

create_directory "${DIRECTORY}"

printf "%b\n" "\nInfo: copying several files in the vagrant directory for uploading"
cp -f -p -v "${SOURCE}check_auditd.sh" "${DIRECTORY}"
cp -f -p -v "${SOURCE}auditd.conf" "${DIRECTORY}"
cp -f -p -v "${SOURCE}auditd_directory_list.txt" "${DIRECTORY}"
cp -f -p -v "${SOURCE}linux-audit.sh" "${DIRECTORY}"
cp -f -p -v "${SOURCE}ltp-full-20230929.tar.xz" "${DIRECTORY}"
cp -f -p -v "${SOURCE}ltp-full-20230929.tar.xz.sha256" "${DIRECTORY}"

cp -f -p -v "${SOURCE}install_requirements_auditd.sh" "${DIRECTORY}"
cp -f -p -v "${SOURCE}install_requirements_kali.sh" "${DIRECTORY}"
cp -f -p -v "${SOURCE}install_requirements_go.sh" "${DIRECTORY}"
cp -f -p -v "${SOURCE}install_requirements_nim.sh" "${DIRECTORY}"

cp -f -p -v "${SOURCE}archive_logs.sh" "${DIRECTORY}"
cp -f -p -v "${SOURCE}deployment.cfg" "${DIRECTORY}"
cp -f -p -v "${SOURCE}check_dns_servers.sh" "${DIRECTORY}"
cp -f -p -v "${SOURCE}check_ntp_servers.sh" "${DIRECTORY}"
cp -f -p -v "${SOURCE}checksec.sh" "${DIRECTORY}"
printf "%b\n" "\nInfo: editing juhu.sh"
sed -i '' '/^ctrl/ s/^/#/' "juhu.sh" || true
sed -i '' '/^sudo !!/ s/^/#/' "juhu.sh" || true
sed -i '' '/^ssh !!/ s/^/#/' "juhu.sh" || true
sed -i '' '/^top !!/ s/^/#/' "juhu.sh" || true
sed -i '' '/^atop !!/ s/^/#/' "juhu.sh" || true
sed -i '' '/^htop !!/ s/^/#/' "juhu.sh" || true
sed -i '' '/^iftop !!/ s/^/#/' "juhu.sh" || true
sed -i '' '/^[^#]*top/s/^/# /' "juhu.sh" || true
cp -f -p -v "${SOURCE}juhu.sh" "${DIRECTORY}" || true
cp -f -p -v "${SOURCE}check_b.sh" "${DIRECTORY}" || true
cp -f -p -v "${SOURCE}create_priv_rules.sh" "${DIRECTORY}" || true
cp -f -p -v "${SOURCE}AutoLLR_v2.4.sh" "${DIRECTORY}" || true
cp -f -p -v "${SOURCE}linux_triage.sh" "${DIRECTORY}" || true

cp -f -p -v "${SOURCE}unix_collector_new.sh" "${DIRECTORY}"
cp -f -p -v "${SOURCE}BasicForensicLinuxScript.sh" "${DIRECTORY}"
cp -f -p -v "${SOURCE}LBFTT.sh" "${DIRECTORY}"

cp -f -p -v "${SOURCE}journal.conf" "${DIRECTORY}"
cp -f -p -v "${SOURCE}logrotate.conf" "${DIRECTORY}"

printf "%b\n" "\nInfo: editing bash_oneliner.sh"
ls -la "bash_oneliner.sh" || true
sed -i '' '/^ctrl/ s/^/#/' "bash_oneliner.sh" || true
sed -i '' '/^sudo !!/ s/^/#/' "bash_oneliner.sh" || true
sed -i '' '/^ssh !!/ s/^/#/' "bash_oneliner.sh" || true
sed -i '' '/^top !!/ s/^/#/' "bash_oneliner.sh" || true
sed -i '' '/^atop !!/ s/^/#/' "bash_oneliner.sh" || true
sed -i '' '/^htop !!/ s/^/#/' "bash_oneliner.sh" || true
sed -i '' '/^iftop !!/ s/^/#/' "bash_oneliner.sh" || true
sed -i '' '/^[^#]*top/s/^/# /' "bash_oneliner.sh" || true

cp -f -p -v "${SOURCE}bash_oneliner.sh" "${DIRECTORY}" || true

cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/repos/ndaal_public_test_data_compress/install_requirements_rust_on_debian.sh" "${DIRECTORY}"
cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/repos/ndaal_public_test_data_compress/install_requirements_rust_on_debian.sh" "${SOURCE}"

input_file11="audit.rules"
output_file11="audit_new.rules"

# awk '/^-w / {print $0; print "-a always,exit -F arch=b32 -F path="$3" -F perm="$5" -F key="$7; print "-a always,exit -F arch=b64 -F path="$3" -F perm="$5" -F key="$7}' "${input_file11}" > "${output_file11}"
# awk '/^-w / {print $0; print "# Original line commented out"; print "-a always,exit -F arch=b32 -F path="$3" -F perm="$5" -F key="$7; print "-a always,exit -F arch=b64 -F path="$3" -F perm="$5" -F key="$7} !/^-w / {print $0}' "${input_file11}" > "${output_file11}"
# awk '/^-w/ { print; print "# Original line commented out"; gsub(/-w/, "-a always,exit -F arch=b32 -F path=" $3 " -F perm=" $5 " -F key=" $7); print "-a always,exit -F arch=b64 -F path=" $3 " -F perm=" $5 " -F key=" $7 } !/^-w/ { print }' "${input_file11}" > "${output_file11}"
# awk '/^-w/ { print; print "# Original line commented out"; gsub(/-w ([^ ]+) -p ([^ ]+) -k ([^ ]+)/, "-a always,exit -F arch=b64 -F path=" "\\1 -F perm=" "\\2 -F key=" "\\3"); print "-a always,exit -F arch=b32 -F path=" "\\1 -F perm=" "\\2 -F key=" "\\3" } !/^-w/ { print }' "${input_file11}" > "${output_file11}"
awk '/^-w/ {
    path = perm = key = "";
    for (i=1; i<=NF; i++) {
        if ($i == "-w" && i+1 <= NF) path = $++i;
        else if ($i == "-p" && i+1 <= NF) {
            perm_part = $++i;
            sub(/^-/, "", perm_part);
            perm = "-F perm=" perm_part;
        }
        else if ($i == "-k" && i+1 <= NF) {
            key_part = $++i;
            sub(/^-/, "", key_part);
            key = "-F key=" key_part;
        }
    }
    if (index(path, ".") == 0) {
        print;
        print "# Original line commented out";
        print "-a always,exit -F arch=b32 -F path=" path " " perm " " key;
        print "-a always,exit -F arch=b64 -F path=" path " " perm " " key;
    } else {
        print "# Skipping line with a dot in the path: " $0;
    }
}
!/^-w/ { print }' "${input_file11}" > "${output_file11}"



# 
#old_path="/path/to/your/directory"
#new_path="/repos/_cheat_sheets/tldr-pages/pages/linux"
#sed -i '' "s|${old_path}|${new_path}|g" your_file

printf "%b\n" "\nInfo: Granting executing rights for *.sh"
chmod +x ./*.sh

# Function to initialize and start Vagrant VM
Function_Start_Vagrant_VM () {
    local box="${1}"

    rm -f -v "Vagrantfile"
    printf "%b\n" "\nInfo: Initialize Vagrant VM"
    vagrant init "${box}"

    printf "%b\n" "\nInfo: Start Vagrant VM"
    vagrant up
}

# Function to install auditd on the Vagrant VM
Function_Install_Auditd () { 
    printf "%b\n" "\nInfo: Install apt-transport-https"
    vagrant ssh -c "sudo apt-get update && sudo apt-get -y install apt-transport-https" || true
    printf "%b\n" "\nInfo: Install auditd"
    printf "%b\n" "\nInfo: sudo apt-get update && sudo apt-get -y install auditd"
    vagrant ssh -c "sudo apt-get update && sudo apt-get -y install auditd" || true
    printf "%b\n" "\nInfo: sudo apt-get update && sudo apt-get -y install auditd-plugins"
    vagrant ssh -c "sudo apt-get update && sudo apt-get -y install audispd-plugins" || true
    printf "%b\n" "\nInfo: sudo apt-get update && sudo apt-get -y install logrotate"
    vagrant ssh -c "sudo apt-get -y install logrotate" || true
    printf "%b\n" "\nInfo: This option is used to remove archived journal files that are no longer needed."
    printf "%b\n" "\nInfo: In this case, it keeps the latest 5 archived journal files and deletes the rest,"
    printf "%b\n" "\nInfo: helping to manage disk space usage by the systemd journal."
    vagrant ssh -c "sudo journalctl --vacuum-files=5"
    printf "%b\n" "\nInfo: To delete archived journal entries manually, you can use either the –vacuum-size or the –vacuum-time option."
    printf "%b\n" "\nInfo: In the example below, we are deleting any archived journal files, so the journal size comes back to 10MB."
    vagrant ssh -c "sudo journalctl --vacuum-size=10M"
    printf "%b\n" "\nInfo: To check how much disk space is currently taken up by the journal, use the –disk-usage parameter:"
    vagrant ssh -c "sudo journalctl --disk-usage"
    vagrant ssh -c "sudo journalctl --vacuum-time=2h"
    printf "%b\n" "\nInfo: To verify the journal for internal consistency, use the –verify option:"
    vagrant ssh -c "sudo journalctl --verify"
}

Function_Copy_File_with_Vagrant () {
    local VAGRANT_FILENAME="${1}"
    printf "%b\n" "\nInfo: We will copy file ${VAGRANT_FILENAME} with Vagrant"
    printf "%b\n" "\nInfo: scp ${VAGRANT_FILENAME}"
    printf "%b\n" "\nInfo: sudo rm -f -v /home/vagrant/${VAGRANT_FILENAME}"
    vagrant ssh -c "sudo rm -f -v /home/vagrant/${VAGRANT_FILENAME}"
    printf "%b\n" "\nInfo: scp ${VAGRANT_FILENAME} to ${box}:/home/vagrant/"
    vagrant scp "${VAGRANT_FILENAME}" "${box}":/home/vagrant/
}

# Function to download, rename, and upload audit rules to Vagrant VM
Function_Upload_Audit_Rules () {
    local box="${1}"
    local rules_file="${2}"

    printf "%b\n" "\nInfo: Download audit rules file from public ndaal repo"
    rm -f -v "${rules_file}"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "${RULES_FILE}" "${RULES_URL}"

    printf "%b\n" "\nInfo: Define the input and output file names"
    input_file="${rules_file}"
    printf "%b\n" "\nInfo: the audit rule file ${rules_file} will be investigated"
    output_file="audit.rules_investigated.txt"
    printf "%b\n" "\nInfo: We will store the results of ${rules_file} in ${output_file}"

    printf "%b\n" "\nInfo: Extract lines containing " -k " and get the content after the space"
    grep -v '^#' "${input_file}" | grep " -k " | awk '{print $NF}' | sed 's/^[ \t]*//;s/[ \t]*$//' | sort -u > "${DIRECTORY}${output_file}" || true
    ls -la "${DIRECTORY}${output_file}" || true
    cat "${DIRECTORY}${output_file}" || true

    printf "%b\n" "\nInfo: Investigated content extracted and saved in ${output_file}"

    output_file2="audit.rules_investigated2.txt"
    printf "%b\n" "\nInfo: We will store the results of ${rules_file} in ${output_file2}"

    output_file3="audit.rules_investigated3.txt"
    printf "%b\n" "\nInfo: We will store the results of ${rules_file} in ${output_file3}"

    output_file4="audit.rules_investigated3.txt"
    printf "%b\n" "\nInfo: We will store the results of ${rules_file} in ${output_file4}"

    output_file5="audit.rules_investigated5.txt"
    printf "%b\n" "\nInfo: We will store the results of ${rules_file} in ${output_file5}"

    output_file6="audit.rules_investigated6.txt"
    printf "%b\n" "\nInfo: We will store the results of ${rules_file} in ${output_file6}"

    printf "%b\n" "\nWarning: Remove lines starting with # and extract content after -w  till  -p"
    # grep -v '^#' "${input_file}" | grep "^-w " | awk '{print $2}' | sed 's/ -p.*//' | sort -u > "${DIRECTORY}${output_file2}"
    # grep -v '^#' "${input_file}" | grep "^-w " | awk '{print $2}' | sed 's/ -p.*//' | grep -v '/$' | sort -u > "${DIRECTORY}${output_file2}"
    grep -v '^#' "${input_file}" | grep "^-w " | awk '{print $2}' | sed 's/ -p.*//' | grep -v '/$' | grep -vE '\.conf$|\.cfg$|\.pem$|\.sh$|\..conflist$|\.yml$|\.yaml$|\.init$|\.service$|\.socket$|\.rules$' | sort -u > "${DIRECTORY}${output_file2}" || true

    printf "%b\n" "\nWarning: Remove lines starting with # and extract content after -a  till  -"
    # grep -v '^#' "${input_file}" | grep "^-a " | awk '{print $2}' | sed 's/ -p.*//' | sort -u > "${DIRECTORY}${output_file3}"
    # grep -v '^#' "${input_file}" | grep "^-a " | grep -oE '-F dir=[^ ]+' | sed -E 's/^-F dir=//;s/\/-k / /' | sort -u > "${DIRECTORY}${output_file3}"
    # awk '!/^#/ && /-a / { match($0, /-F dir=[^ ]+/); if (RSTART) print substr($0, RSTART+8, RLENGTH-8) }' "${input_file}" | sort -u > "${DIRECTORY}${output_file3}"
    # awk '!/^#/ && /-a / { match($0, /-F dir=[^ ]+/); if (RSTART && substr($0, RSTART+8, 1) == "/") print substr($0, RSTART+8, RLENGTH-8) }' "${input_file}" | sort -u > "${DIRECTORY}${output_file3}"
    # awk '!/^#/ && /-a / { match($0, /-F dir=[^ ]+/); if (RSTART && match(substr($0, RSTART+8), /\/$/)) print substr($0, RSTART+8, RLENGTH-8) }' "${input_file}" | sort -u > "${DIRECTORY}${output_file3}"
    # awk '!/^#/ && /-a / { if (match($0, /-F dir=[^ ]+/)) { match(substr($0, RSTART+8), /\/$/); if (RSTART) print substr($0, RSTART+8, RLENGTH-8) } }' "${input_file}" | sort -u > "${DIRECTORY}${output_file3}"
    # grep -v '^#' "${input_file}" | grep "^-a " | awk '{print $2}' | sed 's/ -k.*//' | grep '/$' | sort -u > "${DIRECTORY}${output_file3}"
    grep -v '^#' "${input_file}" | grep -E "^-a .*-F dir=" | sed -n 's/.*-F dir=\([^ ]*\) -.*/\1/p' | grep '/$' | sort -u > "${DIRECTORY}${output_file5}" || true

    grep -v '^#' "${input_file}" | grep -E "^-a .*-F dir=" | sed -n 's/.*-F dir=\([^ ]*\) -.*/\1/p' | grep -v '/$' | sort -u > "${DIRECTORY}${output_file6}" || true

    ls -la "${DIRECTORY}${output_file2}" || true
    cat "${DIRECTORY}${output_file2}" || true

    output_file10="audit.rules_investigated_conf_list.txt"
    printf "%b\n" "\nWe will store the results of ${rules_file} in ${output_file10}"

    printf "%b\n" "\nWarning: Remove lines starting with # and extract content after -w with .conf"
    # Remove lines starting with #
    sed '/^#/d' "$input_file" |
    # Search for lines containing "-w " and ".conf"
    grep '\-w .*\.conf' |
    # Extract content after "-w " up to ".conf" and append ".conf" at the end
    sed -n 's/.*-w \([^ ]*\)\.conf.*/\1.conf/p' |
    # Eliminate double lines
    sort -u > "${DIRECTORY}${output_file10}" || true

    printf "%b\n" "\nWarning: Remove lines starting with # and extract content after -w with .cnf"
    # Remove lines starting with #
    sed '/^#/d' "$input_file" |
    # Search for lines containing "-w " and ".cnf"
    grep '\-w .*\.cnf' |
    # Extract content after "-w " up to ".cnf" and append ".cnf" at the end
    sed -n 's/.*-w \([^ ]*\)\.cnf.*/\1.cnf/p' |
    # Eliminate double lines
    sort -u >> "${DIRECTORY}${output_file10}" || true

    printf "%b\n" "\nWarning: Remove lines starting with # and extract content after -w with .cfg"
    # Remove lines starting with #
    sed '/^#/d' "$input_file" |
    # Search for lines containing "-w " and ".cfg"
    grep '\-w .*\.cfg' |
    # Extract content after "-w " up to ".cfg" and append ".cfg" at the end
    sed -n 's/.*-w \([^ ]*\)\.cfg.*/\1.cfg/p' |
    # Eliminate double lines
    sort -u >> "${DIRECTORY}${output_file10}" || true

    printf "%b\n" "\nWarning: Remove lines starting with # and extract content after -w with .plist"
    # Remove lines starting with #
    sed '/^#/d' "$input_file" |
    # Search for lines containing "-w " and ".plist"
    grep '\-w .*\.plist' |
    # Extract content after "-w " up to ".plist" and append ".plist" at the end
    sed -n 's/.*-w \([^ ]*\)\.plist.*/\1.plist/p' |
    # Eliminate double lines
    sort -u >> "${DIRECTORY}${output_file10}" || true

    ls -la "${DIRECTORY}${output_file10}" || true
    cat "${DIRECTORY}${output_file10}" || true

    printf "%b\n" "\nInfo: Upload several files to Vagrant VM"

    VAGRANT_UPLOAD="${rules_file}"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="auditd.conf"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="checksec.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="check_auditd.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="auditd_directory_list.txt"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="install_requirements_auditd.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="install_requirements_kali.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="install_requirements_go.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="install_requirements_nim.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="install_requirements_ruby.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="archive_logs.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="deployment.cfg"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="bash_oneliner.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="unix_collector_new.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="BasicForensicLinuxScript.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="LBFTT.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="AutoLLR_v2.4.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="linux_triage.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="check_dns_servers.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="check_ntp_servers.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="juhu.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="check_b.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="create_priv_rules.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="install_requirements_rust_on_debian.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="journal.conf"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="logrotate.conf"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="check_b.sh"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="${output_file}"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="${output_file2}"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="${output_file3}"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="${output_file4}"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="${output_file5}"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="${output_file6}"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    VAGRANT_UPLOAD="${output_file10}"
    Function_Copy_File_with_Vagrant "${VAGRANT_UPLOAD}"

    printf "%b\n" "\nWarning: Removing with: sudo rm -f -v /home/vagrant/backlog_limit_results.txt"
    vagrant ssh -c "sudo rm -f -v /home/vagrant/backlog_limit_results.txt" || true

    printf "%b\n" "\nInfo: Copying with: sudo cp -f -p -v -f /home/vagrant/audit.rules /etc/audit/rules.d/audit.rules"
    vagrant ssh -c "sudo cp -f -p -v -f /home/vagrant/audit.rules /etc/audit/rules.d/audit.rules"

}

Function_Download_Audit_Logs () {
    local box="${1}"

    vagrant scp "${box}":/home/vagrant/log_auditd_test.txt "log_auditd_test.txt"
    vagrant ssh -c "history | sort >> /home/vagrant/history.txt"
    vagrant scp "${box}":/home/vagrant/history.txt "/${HOMEDIR}/${USERSCRIPT}/auditd/debian/"
    vagrant ssh -c "sudo /home/vagrant/archive_logs.sh"
    vagrant scp "${box}":/home/vagrant/*.tar.gz "/${HOMEDIR}/${USERSCRIPT}/auditd/debian/"
    vagrant scp "${box}":/home/vagrant/*.tgz "/${HOMEDIR}/${USERSCRIPT}/auditd/debian/"
    #vagrant scp "${box}":/home/vagrant/*.tar.bz2 "/${HOMEDIR}/${USERSCRIPT}/auditd/debian/"
    #vagrant scp "${box}":/home/vagrant/*.tar.xz "/${HOMEDIR}/${USERSCRIPT}/auditd/debian/"
}   

Function_Install_Tools () {
    printf "%b\n" "\nInfo: Installing tools, artefacts"
    #vagrant ssh -c "sudo /home/vagrant/install_requirements_auditd.sh -dnf" || true
    #vagrant ssh -c "sudo /home/vagrant/install_requirements_auditd.sh -yum" || true
    vagrant ssh -c "sudo /home/vagrant/install_requirements_auditd.sh -debian" || true
    printf "%b\n" "\nInfo: Installing tools, artefacts for Kali"
    #vagrant ssh -c "sudo /home/vagrant/install_requirements_kali.sh -debian" || true
    printf "%b\n" "\nInfo: Finished installing tools, artefacts"
}

Function_Restart_Auditd () {
    local box="${1}"

    printf "%b\n" "\nsudo cp -f -p -v /home/vagrant/audit.rules /etc/audit/rules.d/audit.rules"
    vagrant ssh -c "sudo cp -f -p -v /home/vagrant/audit.rules /etc/audit/rules.d/audit.rules"

    printf "%b\n" "\nsudo /home/vagrant/create_priv_rules.sh"
    vagrant ssh -c "sudo /home/vagrant/create_priv_rules.sh" || true

    printf "%b\n" "\nsudo cp -f -p -v /home/vagrant/priv.rules /etc/audit/rules.d/priv.rules"
    vagrant ssh -c "sudo cp -f -p -v /home/vagrant/priv.rules /etc/audit/rules.d/priv.rules" || true

    printf "%b\n" "\nsudo cp -f -p -v /home/vagrant/auditd.conf /etc/audit/auditd.conf"
    #vagrant ssh -c "sudo cp -f -p -v /home/vagrant/auditd.conf /etc/audit/auditd.conf" || true

    printf "%b\n" "\nsudo cp -f -p -v /home/vagrant/auditd.conf /etc/auditd.conf"
    #vagrant ssh -c "sudo cp -f -p -v /home/vagrant/auditd.conf /etc/auditd.conf" || true

    # Restart service
    printf "%b\n" "\nInfo: Restart auditd"
    vagrant ssh -c "sudo systemctl restart auditd" || true

    # Check service
    printf "%b\n" "\nInfo: Check audit service status"
    vagrant ssh -c "sudo systemctl status auditd.service" || true
}

Function_Check_Auditd_Status () {
    local box="${1}"

    # Check loaded audit rules
    printf "%b\n" "\nInfo: Check loaded audit rules"
    vagrant ssh -c "sudo auditctl -l"

    # Check audit service status
    printf "%b\n" "\nInfo: Check audit service status"
    vagrant ssh -c "sudo auditctl -s"
}

Function_Restart_Journald () {
    local box="${1}"

    printf "%b\n" "\nsudo cp -f -p -v /home/vagrant/journal.conf /etc/systemd/journald.conf"
    vagrant ssh -c "sudo cp -f -p -v /home/vagrant/journal.conf /etc/systemd/journald.conf"

    # Restart service
    printf "%b\n" "\nInfo: Restart journald"
    vagrant ssh -c "sudo systemctl restart systemd-journald" || true
}

Function_Check_Journald_Status () {
    local box="${1}"

    # Check Journald service status
    printf "%b\n" "\nInfo: Check Journald service status"
    vagrant ssh -c "sudo systemctl status systemd-journald"
}

# Function to get the machine name using 'vagrant status'
Function_Get_Machine_Name() {
    vagrant status | awk '/running/{print $1; exit}'
}

# Function Execute_Test
Function_Execute_Test () {
    printf "%b\n" "\nInfo: Granting executing rights for *.sh"
    vagrant ssh -c "sudo chmod +x /home/vagrant/*.sh"

    printf "%b\n" "\nInfo: Executing AutoLLR_v2.4.sh"
    vagrant ssh -c "sudo /home/vagrant/AutoLLR_v2.4.sh" || true

    printf "%b\n" "\nInfo: Executing linux_triage.sh"
    vagrant ssh -c "sudo /home/vagrant/linux_triage.sh" || true

    printf "%b\n" "\nInfo: Executing unix_collector_new.sh"
    vagrant ssh -c "sudo rm -f -r /home/vagrant/collector-bookworm-${DIRDATE}" || true
    vagrant ssh -c "sudo /home/vagrant/unix_collector_new.sh" || true

    printf "%b\n" "\nInfo: Executing uac.sh"
    vagrant ssh -c "sudo /home/vagrant/uac/uac -p full /home/vagrant" || true

    printf "%b\n" "\nInfo: Executing uac.sh"
    vagrant ssh -c "sudo /home/vagrant/ndaal_public_uac_ndaal/uac -p full /home/vagrant" || true

    printf "%b\n" "\nInfo: Executing install_requirements_rust_on_debian.sh"
    vagrant ssh -c "sudo /home/vagrant/install_requirements_rust_on_debian.sh" || true

    printf "%b\n" "\nInfo: Executing install_requirements_go.sh"
    vagrant ssh -c "sudo /home/vagrant/install_requirements_go.sh" || true

    printf "%b\n" "\nInfo: Executing install_requirements_nim.sh"
    vagrant ssh -c "sudo /home/vagrant/install_requirements_nim.sh" || true

    printf "%b\n" "\nInfo: Executing install_requirements_ruby.sh"
    vagrant ssh -c "sudo /home/vagrant/install_requirements_ruby.sh" || true

    printf "%b\n" "\nInfo: Executing bash_oneliner.sh"
    vagrant ssh -c "sudo /home/vagrant/bash_oneliner.sh" || true

    printf "%b\n" "\nInfo: Executing juhu.sh"
    vagrant ssh -c "sudo /home/vagrant/juhu.sh" || true

    #printf "%b\n" "\nInfo: Executing check_b.sh"
    #vagrant ssh -c "sudo /home/vagrant/check_b.sh" || true

    printf "%b\n" "\nInfo: Executing check_auditd.sh"
    vagrant ssh -c "sudo /home/vagrant/check_auditd.sh" || true
}

# Start and provision Debian VMs
cd "${DIRECTORY}" || exit
printf "%b\n" "\nInfo: Changed to the desired directory: ${DIRECTORY}"

printf "%b\n" "\nStart_Vagrant_VM"

#exit 0

Function_Start_Vagrant_VM "debian/bookworm64"
#Function_Start_Vagrant_VM "centos/8"

printf "%b\n" "\nInfo: ********** Install Auditd on the VM **********"

Function_Install_Auditd

# Get the machine name
MACHINE_NAME=$(Function_Get_Machine_Name)
if [ -z "${MACHINE_NAME}" ]; then
    printf "%b\n" "\nError: Vagrant machine not found or not running."
    exit 1
fi

Function_Create_Copy_of_Audit_Logs () {
    cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/auditd/debian/"*.tar.gz "/${HOMEDIR}/${USERSCRIPT}/auditd/" || true
    cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/auditd/debian/"*.tar.bz2 "/${HOMEDIR}/${USERSCRIPT}/auditd/" || true
    cp -f -p -v "/${HOMEDIR}/${USERSCRIPT}/auditd/debian/"*.tar.xz "/${HOMEDIR}/${USERSCRIPT}/auditd/" || true
    ls -la "/${HOMEDIR}/${USERSCRIPT}/auditd/"*.tar.gz || true
    ls -la "/${HOMEDIR}/${USERSCRIPT}/auditd/"*.tar.bz2 || true
    ls -la "/${HOMEDIR}/${USERSCRIPT}/auditd/"*.tar.xz || true
}

printf "%b\n" "\nInfo: ********** Upload and configure audit rules on the VM **********"
Function_Upload_Audit_Rules "${MACHINE_NAME}" "${RULES_FILE}"

printf "%b\n" "\nInfo: ********** Install_Tools on the VM **********"
Function_Install_Tools

printf "%b\n" "\nInfo: ********** Restart auditd on the VM **********"
Function_Restart_Auditd "${MACHINE_NAME}"

printf "%b\n" "\nInfo: ********** Check auditd service status on the VM **********"
Function_Check_Auditd_Status "${MACHINE_NAME}"

printf "%b\n" "\nInfo: ********** Restart journald on the VM **********"
Function_Restart_Journald "${MACHINE_NAME}"

printf "%b\n" "\nInfo: ********** Check journald service status on the VM **********"
Function_Check_Journald_Status "${MACHINE_NAME}"

printf "%b\n" "\nInfo: ********** Execute Test on the VM **********"
Function_Execute_Test

printf "%b\n" "\nInfo: ********** Download Audit Logs on the VM **********"
Function_Download_Audit_Logs "${MACHINE_NAME}"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
