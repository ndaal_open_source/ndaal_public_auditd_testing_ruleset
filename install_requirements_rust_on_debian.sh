#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of <the MIT License>
# Developed on: Debian 12.x; macOS Sonoma x86 architecture
# Tested on: Debian 12.x; macOS Sonoma x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# Exit on error inside any functions or subshells.
set -o errtrace
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
set -o pipefail
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace
#
# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    # script cleanup here
}

# Check if running as root
if [ "${EUID}" -ne "0" ]; then
  echo "Please run as root or using sudo."
  exit 1
fi

# Under Linux you use `home` under macOS `Users`
HOMEDIR="home"
readonly HOMEDIR
echo "HOMEDIR: ${HOMEDIR}"

# Your user ! in which context it SHOULD run
USERSCRIPT="vagrant"
readonly USERSCRIPT
echo "USERSCRIPT: ${USERSCRIPT}"

DIRDATE="$(date +"%Y-%m-%d")"
readonly DIRDATE
echo "Current date: ${DIRDATE}"

echo "List of crates to install"
crates=("tokei" "sha1sum" "sha256sum" "sha512sum" "k12sum" "twoxhash" "b2sum" "b3sum")

echo "Step 1: Update Debian Before Rust Installation"
sudo apt update && sudo apt upgrade -y

echo "Step 2: Install Initial Required Installation Packages For Rust"
sudo apt install -y curl build-essential gcc make

echo "Step 3: Install Rust via cURL and Bash on Debian"
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y

echo "Step 4: Activate Rust Environment on Debian"
source ~/.cargo/env

echo "Step 5: Confirm Rust Installation"
rustc -V

echo "Install crates using a loop"
for crate in "${crates[@]}"; do
  cargo install "$crate"
done

echo "check crates using a loop"
for crate in "${crates[@]}"; do
  "$crate" --v
done

cleanup

script_name1="$(basename "${0}")"
echo "script_name1: ${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
echo "script_path1: ${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
echo "Script path with name: ${script_path_with_name}"
echo "Script finished"
exit 0
