#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

install_ruby_debian() {
    sudo apt update
    sudo apt install -y ruby-full
}

install_ruby_rhel() {
    sudo yum install -y ruby
}

install_ruby_slrs() {
    sudo zypper install -y ruby
}

install_ruby_rpm() {
    sudo rpm -Uvh https://cache.ruby-lang.org/pub/ruby/3.0/ruby-3.0.3-1.el8.x86_64.rpm
}

install_ruby_dnf() {
    sudo dnf install -y ruby
}

install_ruby_brew() {
    brew install ruby
}

# Detect distribution
if [ -f /etc/debian_version ]; then
    printf "Detected Debian-based distribution.\n"
    install_ruby_debian
elif [ -f /etc/redhat-release ]; then
    if grep -q "SLRS" /etc/redhat-release; then
        printf "Info: Detected SUSE Linux Enterprise Real Time (SLRS).\n"
        install_ruby_slrs
    else
        printf "Info: Detected Red Hat Enterprise Linux (RHEL).\n"
        install_ruby_rhel
    fi
elif [ -f /etc/os-release ]; then
    source /etc/os-release
    case $ID in
        fedora)
            printf "Info: Detected Fedora.\n"
            install_ruby_dnf
            ;;
        opensuse-leap)
            printf "Info: Detected openSUSE Leap.\n"
            install_ruby_slrs
            ;;
    esac
elif command -v brew &>/dev/null; then
    printf "Info: Detected macOS with Homebrew.\n"
    install_ruby_brew
else
    printf "Error: Unsupported distribution. Please install Ruby manually for your distribution.\n"
    exit 1
fi

# Verify Ruby installation
ruby --version

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
