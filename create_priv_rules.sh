#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2021, 2022, 2023, 2024
# License: All content is licensed under the terms of <the MIT License>
# Developed on: Debian 12.x; macOS Ventura x86 architecture
# Tested on: Debian 12.x; macOS Ventura x86 architecture
# 
# By default, sphinx-build supports a number of outputs. The ones I’m most likely to use are the following:

# html: to make standalone HTML files
# dirhtml: to make HTML files named index.html in directories
# singlehtml: to make a single large HTML file
# epub: to make an epub
# latexpdf: to make LaTeX and PDF files (default pdflatex)
# text: to make text files
# man: to make manual pages
# gettext: to make PO message catalogs
# doctest: to run all doctests embedded in the documentation (if enabled)
# coverage: to run coverage check of the documentation (if enabled)
# And of those, let’s face it, it’s dirhtml, epub, latexpdf. If you’re building towards a constrained set of 
# targets, you can usually more easily work on styling them and using the particular affordances those formats offer.
# this build process relies heavily on conf.py

# Exit on error. Append "|| true" if you expect an error.
# set -o errexit
# Exit on error inside any functions or subshells.
set -o errtrace
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    rm -f -v priv.rules
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

##- Use of privileged commands (unsuccessful and successful)
## You can run the following commands to generate the rules (don't forget to
## add arch=b32 rules, too):
## https://github.com/linux-audit/audit-userspace/blob/master/rules/31-privileged.rules

find /bin -type f -perm -04000 2>/dev/null | awk '{ printf "-a always,exit -F arch=b32 -F path=%s -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $1 }' > priv.rules
find /bin -type f -perm -04000 2>/dev/null | awk '{ printf "-a always,exit -F arch=b64 -F path=%s -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $1 }' >> priv.rules

find /sbin -type f -perm -04000 2>/dev/null | awk '{ printf "-a always,exit -F arch=b32 -F path=%s -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $1 }' >> priv.rules
find /sbin -type f -perm -04000 2>/dev/null | awk '{ printf "-a always,exit -F arch=b64 -F path=%s -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $1 }' >> priv.rules

find /usr/bin -type f -perm -04000 2>/dev/null | awk '{ printf "-a always,exit -F arch=b32 -F path=%s -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $1 }' >> priv.rules
find /usr/bin -type f -perm -04000 2>/dev/null | awk '{ printf "-a always,exit -F arch=b64 -F path=%s -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $1 }' >> priv.rules

find /usr/sbin -type f -perm -04000 2>/dev/null | awk '{ printf "-a always,exit -F arch=b32 -F path=%s -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $1 }' >> priv.rules
find /usr/sbin -type f -perm -04000 2>/dev/null | awk '{ printf "-a always,exit -F arch=b64 -F path=%s -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $1 }' >> priv.rules

filecap /bin 2>/dev/null | sed '1d' | awk '{ printf "-a always,exit -F path=%s -F arch=b32 -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $2 }' >> priv.rules
filecap /bin 2>/dev/null | sed '1d' | awk '{ printf "-a always,exit -F path=%s -F arch=b64 -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $2 }' >> priv.rules

filecap /sbin 2>/dev/null | sed '1d' | awk '{ printf "-a always,exit -F path=%s -F arch=b32 -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $2 }' >> priv.rules
filecap /sbin 2>/dev/null | sed '1d' | awk '{ printf "-a always,exit -F path=%s -F arch=b64 -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $2 }' >> priv.rules

filecap /usr/bin 2>/dev/null | sed '1d' | awk '{ printf "-a always,exit -F arch=b32 -F path=%s -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $2 }' >> priv.rules
filecap /usr/bin 2>/dev/null | sed '1d' | awk '{ printf "-a always,exit -F arch=b64 -F path=%s -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $2 }' >> priv.rules

filecap /usr/sbin 2>/dev/null | sed '1d' | awk '{ printf "-a always,exit -F arch=b32 -F path=%s -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $2 }' >> priv.rules
filecap /usr/sbin 2>/dev/null | sed '1d' | awk '{ printf "-a always,exit -F arch=b64 -F path=%s -F perm=x -F auid>=500 -F auid!=unset -F key=privileged\n", $2 }' >> priv.rules

echo "Copying with: sudo cp -f -p -v priv.rules /etc/audit/rules.d/priv.rules"
cp -f -p -v "priv.rules" "/etc/audit/rules.d/priv.rules"


script_name1="$(basename "${0}")"
echo "script_name1: ${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
echo "script_path1: ${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
echo "Script path with name: ${script_path_with_name}"
echo "Script finished"
exit 0
