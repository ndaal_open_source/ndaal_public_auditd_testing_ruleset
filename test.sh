#!/bin/bash

input_file="${1}"
output_file="${2}"

while IFS= read -r line
do
  if [[ "$line" == "-w"* ]]; then
    path=$(echo "$line" | awk -F' ' '{print $2}')
    perm=$(echo "$line" | awk -F'-p ' '{print $2}' | sed 's/.*-p //')

    echo "$line" >> "${output_file}"

    echo "-a always,exit -F arch=b32 -F path=${path} -F perm=${perm}" >> "${output_file}"
    echo "-a always,exit -F arch=b64 -F path=${path} -F perm=${perm}" >> "${output_file}"
  else
    echo "${line}" | sed 's/ -k.*//' >> "${output_file}"
  fi
done < "${input_file}"
