#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
    readonly HOMEDIR
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
    readonly HOMEDIR
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi

# Under Linux you use `home` under macOS `Users`
printf "Info: Home directory: %s\n" "${HOMEDIR}"

USERSCRIPT="cloud"
# Your user! In which context it SHOULD run
printf "Info: User script: %s\n" "${USERSCRIPT}"

DIRDATE="$(date +"%Y-%m-%d")"
readonly DIRDATE
printf "Info: Current date: %s\n" "${DIRDATE}"

# Get the current date and time in the desired format
LOGTIME="$(date +"%Y-%m-%d-%H-%M")"

DESTINATION="/${HOMEDIR}/${USERSCRIPT}/"
printf "Info: Destination directory: %s\n" "${DESTINATION}"

LOG_AUDITD="/${HOMEDIR}/${USERSCRIPT}/log_auditd_test.txt"
echo "Extraction log file is: ${LOG_AUDITD}"

echo "**********************************"
echo "*                                *"
echo "********** check auditd **********"
echo "*                                *"
echo "**********************************"

cd "${DESTINATION}" || exit
printf "Changed to the desired directory: %s\n" "${DESTINATION}"

if [ -e "${LOG_AUDITD}" ]; then
    echo "File exists, create a copy"
    cp -f -v "${LOG_AUDITD}" "${DESTINATION}log_auditd_test-${DIRDATE}.txt"
    echo "File copied to ${DESTINATION}log_auditd_test-${DIRDATE}.txt"
else
    echo "File doesn't exist, creating it with touch"
    touch "${LOG_AUDITD}"
    echo "File created at ${LOG_AUDITD}"
    echo "Current date: ${LOGTIME}" >> "${LOG_AUDITD}"
fi

Function_Prepare_Logging_Start () {
    echo "Prepare_Logging_Start"
    echo "Check if some tools are available"
    apt-get update
    command -v "auditd" || { echo "auditd not found. Please install it."; sudo apt-get -y install auditd; }
    command -v "logrotate" || { echo "logrotate not found. Please install it."; sudo apt-get -y install logrotate; }
    command -v "journalctl" || { echo "journalctl not found. Please install it."; sudo apt-get -y install journalctl; }

    sudo systemd-analyze cat-config systemd/journald.conf || true
    file_journald_conf="/etc/systemd/journald.conf"
    echo "configuring some journal setting in ${file_journald_conf}"
    cp -f -p -v "${file_journald_conf}" "${file_journald_conf}-$(date +"%Y-%m-%d").save"
    cat -v -t -e "${file_journald_conf}" || true

    backup_suffix=$(date +"%Y-%m-%d")
    readonly backup_suffix
    echo "Current date: ${backup_suffix}"

    # Create a backup of the configuration file
    cp -f -p -v "${file_journald_conf}" "${file_journald_conf}-${backup_suffix}.save"

    sudo sed -i \
    -e 's/#Storage=auto/Storage=persistent/' \
    -e 's/#Compress/Compress/' \
    -e 's/#Seal=yes/Seal=yes/' \
    -e 's/#SplitMode=uid/SplitMode=uid/' \
    -e 's/#SyncIntervalSec=5m/SyncIntervalSec=5m/' \
    -e 's/#RateLimitIntervalSec=30s/RateLimitIntervalSec=30s/' \
    -e 's/#RateLimitBurst=10000/RateLimitBurst=10000/' \
    -e 's/#SystemMaxUse=/SystemMaxUse=500M/' \
    -e 's/#SystemKeepFree=/SystemKeepFree=250M/' \
    -e 's/#SystemMaxFileSize=/SystemMaxFileSize=8M/' \
    -e 's/#SystemMaxFiles=100/SystemMaxFiles=200/' \
    -e 's/#RuntimeMaxUse=/RuntimeMaxUse=450M/' \
    -e 's/#RuntimeKeepFree=/RuntimeKeepFree=500M/' \
    -e 's/#RuntimeMaxFileSize=/RuntimeMaxFileSize=8M/' \
    -e 's/#RuntimeMaxFiles=100/RuntimeMaxFiles=200/' \
    -e 's/#MaxRetentionSec=/MaxRetentionSec=1day/' \
    -e 's/#MaxFileSec=1month/MaxFileSec=1month/' \
    -e 's/#ForwardToSyslog=yes/ForwardToSyslog=yes/' \
    -e 's/#ForwardToKMsg=no/ForwardToKMsg=yes/' \
    -e 's/#ForwardToConsole=no/ForwardToConsole=yes/' \
    -e 's/#ForwardToWall=yes/ForwardToWall=yes/' \
    -e 's/#TTYPath=\/dev\/console/TTYPath=\/dev\/console/' \
    -e 's/#MaxLevelStore=debug/MaxLevelStore=debug/' \
    -e 's/#MaxLevelSyslog=debug/MaxLevelSyslog=debug/' \
    -e 's/#MaxLevelKMsg=notice/MaxLevelKMsg=notice/' \
    -e 's/#MaxLevelConsole=info/MaxLevelConsole=info/' \
    -e 's/#MaxLevelWall=emerg/MaxLevelWall=emerg/' \
    -e 's/#LineMax=48K/LineMax=48K/' \
    -e 's/#ReadKMsg=yes/ReadKMsg=yes/' \
    -e 's/#ReadSyslog=yes/ReadSyslog=yes/' \
    -e 's/#ReadKernel=no/ReadKernel=yes/' \
    -e 's/#ReadWall=no/ReadWall=yes/' \
    -e 's/#ReadAudit=yes/ReadAudit=yes/' \
    -e 's/#Audit=no/Audit=yes/' \
    "${file_journald_conf}"
    cat -v -t -e "${file_journald_conf}"

    echo "Check if both files exist"
    if [ -e "${file_journald_conf}" ] && [ -e "${file_journald_conf}-${backup_suffix}.save" ]; then
        # Check if 'cat' and 'grep' commands work without errors
        if cat "${file_journald_conf}" | grep -q "Compress"; then
            echo "Pattern 'Compress' found in ${file_journald_conf}. Restarting systemd-journald service."

            # Restart the systemd-journald service
            systemctl restart systemd-journald
        else
            echo "Pattern 'compress' not found in ${file_journald_conf}. No action taken."
        fi
    else
        echo "One or both files do not exist. Exiting without making changes."
    fi

    echo "Finishd Prepare_Logging_Start"
    echo "********************************************************" >> "${LOG_AUDITD}"
}

Function_Install_Tools () {
    
    echo "Installing tools, artefacts"
    sudo chmod +x /home/vagrant/*.sh || true
    sudo ls -la /home/vagrant/*.sh || true
    sudo "/home/vagrant/install_requirements_auditd.sh -debian" || true
    #exit 0
    echo "Finished installing tools, artefacts"
}

Function_Logging_Start () {
    echo "********************************************************" > "${LOG_AUDITD}"
    echo "***** Start ******" >> "${LOG_AUDITD}"
    echo "Current date and time: ${LOGTIME}" >> "${LOG_AUDITD}"
    # https://www.2daygeek.com/check-system-hardware-devices-bus-information-lspci-lsscsi-lsusb-lsblk-linux/
    # https://linuxhandbook.com/lshw-command/
    sudo lshw >> "${LOG_AUDITD}" || true
    echo "Using this method, you will get details of individual RAM slots and the cache, including information like RAM size, clock speed, etc."
    sudo lshw -C memory >> "${LOG_AUDITD}" || true
    echo "By far this is the most useful way you can use the lshw command as it lists all the connected storage devices with all the necessary information."
    sudo lshw -class storage >> "${LOG_AUDITD}" || true
    echo "The system information includes details like system architecture, motherboard name and manufacturer, and number of plug-and-play ports."
    sudo lshw -C system >> "${LOG_AUDITD}" || true
    echo "There are times when one wants to verify the manufacturer of the sound card ."
    sudo lshw -C multimedia >> "${LOG_AUDITD}" || true
    echo "If you want to know which GPU is being used to get the display output, then, you can use the lshw command with the display flag:"
    sudo lshw -C display >> "${LOG_AUDITD}" || true
    echo "If you're looking for a way to get the PCIe bridge information, then, you'd have to use the bridge flag as shown:"
    sudo lshw -C bridge >> "${LOG_AUDITD}" || true
    echo "And if you want to list down the buses in your system, then, you can use the bus flag with the lshw command as shown:"
    sudo lshw -C bus >> "${LOG_AUDITD}" || true
    echo "This will give you details on CPU type, manufacturer, clock speed, number of cores, threads and many more related things."
    sudo lshw -C CPU >> "${LOG_AUDITD}" || true
    echo "To display the network information, you'd have to use the -C to specify the class and the network as shown:"
    sudo lshw -C network >> "${LOG_AUDITD}" || true
    echo "general hardware information, including manufacturing and product serial numbers" >> "${LOG_AUDITD}"
    sudo dmidecode -t system >> "${LOG_AUDITD}" || true
    echo "summary of your BIOS information" >> "${LOG_AUDITD}"
    sudo dmidecode -t bios >> "${LOG_AUDITD}" || true
    
    echo "lscpu is a command-line utility to display information about the CPU architecture."
    echo "It reads the CPU architecture information from sysfs and /proc/cpuinfo files and prints in the terminal."
    echo "The information includes the number of CPUs, threads, cores, sockets, and Non-Uniform Memory Access (NUMA)"
    echo "nodes. It also displays CPU caches and cache sharing, family, model, bogoMIPS, byte order, and stepping."
    sudo lscpu >> "${LOG_AUDITD}" || true
    
    echo "lspci command is used to display information about PCI buses in the system and hardware devices that are connected to PCI and PCI bus."
    sudo lspci -v >> "${LOG_AUDITD}" || true
    
    echo "lsusb stands for list Universal Serial Bus or USB. It’s display information about USB buses in the system and the devices connected to them." 
    echo "This will display a list of all USB devices connected to your computer such as keyboards, mouse, printers, disk drives, network adapters, etc.."
    #command -v "lsusb" || { echo "lsusb not found. Please install it."; exit 1; }
    
    if command -v "lsusb" &> /dev/null; then
        sudo lsusb -v >> "${LOG_AUDITD}" || true
    else
        echo "lsusb command not found. Skipping..."
    fi

    echo "The lsscsi command lists information about SCSI/Sata devices attached to the system."
    #sudo lsscsi >> "${LOG_AUDITD}" || true
    echo "lsblk stands for list block devices. It’s display information about block devices (except RAM disks). Block devices are hard disk partition, flash drives, CD-ROM, optical drives, etc,."
    #sudo lsblk >> "${LOG_AUDITD}" || true
    echo "Executing aureport --success"
    sudo aureport --success >> "${LOG_AUDITD}"
    echo "Executing aureport --failed"
    sudo aureport --failed >> "${LOG_AUDITD}"

    sudo aureport -ts yesterday -te now --summary -i

    echo "Most events sorted by executable" >> "${LOG_AUDITD}"
    sudo aureport -ts today -te now -x --summary -i >> "${LOG_AUDITD}"
    echo "Most events sorted by system call (syscall)" >> "${LOG_AUDITD}"
    sudo aureport -ts today -te now -s --summary -i >> "${LOG_AUDITD}"

    sudo auditctl -l >> "${LOG_AUDITD}"
    echo "auditctl -s to query the current status of the audit daemon" >> "${LOG_AUDITD}"
    sudo auditctl -s >> "${LOG_AUDITD}"

    echo "To check how much disk space is currently taken up by the journal, use the –disk-usage parameter:"
    journalctl --disk-usage
    echo "To delete archived journal entries manually, you can use either the –vacuum-size or the –vacuum-time option."
    echo "In the example below, we are deleting any archived journal files, so the journal size comes back to 10MB."
    journalctl --vacuum-size=10M
    echo "To verify the journal for internal consistency, use the –verify option:"
    journalctl --verify || true

    echo "Journald tracks each log to a specific system boot. To limit the logs shown to the current boot, use the -b switch."
    #journalctl -b --lines 1000 >> "${LOG_AUDITD}"
    echo "To see messages logged within a specific time window, we can use the --since and --until options. The following command shows journal messages logged within the last hour."
    #journalctl --since "1 hour ago" || true
    echo "To see messages logged in the last two days, the following command can be used."
    #journalctl --since "2 days ago" || true

    sudo ps -ef >> "${LOG_AUDITD}" || true
    sudo ps -e -o pid,user,cmd,lstart >> "${LOG_AUDITD}" || true

    echo "********************************************************" >> "${LOG_AUDITD}"
}

Function_Logging () {
    echo "********************************************************" >> "${LOG_AUDITD}"
    echo "Current date and time: ${LOGTIME}" >> "${LOG_AUDITD}"
    sudo tree -d / >> "${LOG_AUDITD}"
    sudo tree / >> "${LOG_AUDITD}"
    echo "The --dereference option ensures that symbolic links are followed,"
    echo "and the information about the target is displayed instead"
    echo "of the link itself. This should help in avoiding the error related"
    echo "to symbolic links."
    sudo ls -a -I -b -h -si -R -l -dereference / >> "${LOG_AUDITD}" || true
    echo "Create a Report from All Process Events" >> "${LOG_AUDITD}"
    sudo aureport -p >> "${LOG_AUDITD}" || true
    echo "Create a Report from All System Call Events" >> "${LOG_AUDITD}"
    sudo aureport -s >> "${LOG_AUDITD}" || true
    echo "Create a Report from All Executable Events" >> "${LOG_AUDITD}"
    sudo aureport -x >> "${LOG_AUDITD}" || true
    echo "Create a Report about Files" >> "${LOG_AUDITD}"
    sudo aureport -f >> "${LOG_AUDITD}" || true
    echo "Create a Report about Users" >> "${LOG_AUDITD}"
    sudo aureport -u >> "${LOG_AUDITD}" || true
    echo "Create a Report about Logins" >> "${LOG_AUDITD}"
    sudo aureport -l -i >> "${LOG_AUDITD}" || true
    echo "Limit a Report to a Certain Time Frame" >> "${LOG_AUDITD}"
    sudo aureport -t >> "${LOG_AUDITD}" || true

}

Function_Logging_Final () {
    sudo auditctl -l >> "${LOG_AUDITD}"
    sudo auditctl -D >> "${LOG_AUDITD}"
    sudo auditctl -l >> "${LOG_AUDITD}"
    echo "Most events sorted by executable" >> "${LOG_AUDITD}"
    sudo aureport -ts today -i -x –summary >> "${LOG_AUDITD}"
    echo "Most events sorted by system call (syscall)" >> "${LOG_AUDITD}"
    #sudo ps -ef >> "${LOG_AUDITD}"
    #sudo ps -e -o pid,user,cmd,lstart >> "${LOG_AUDITD}"
    echo "********************************************************" >> "${LOG_AUDITD}"
    echo "Current date and time: ${LOGTIME}" >> "${LOG_AUDITD}"
    sudo aureport --success >> "${LOG_AUDITD}"
    sudo aureport --failed >> "${LOG_AUDITD}"
    echo "***** End ******" >> "${LOG_AUDITD}"
    echo "********************************************************" >> "${LOG_AUDITD}"
}

# Function to list installed software using dpkg
Function_list_installed_dpkg() {
    if command -v dpkg &>/dev/null; then
        echo "===== dpkg -l ====="
        dpkg -l > "installed_packages_dpkg_$(date +"%Y-%m-%d").txt"
    else
        echo "dpkg not found."
    fi
}

# Function to list installed software using rpm
Function_list_installed_rpm() {
    if command -v rpm &>/dev/null; then
        echo "===== rpm -qa ====="
        rpm -qa > "installed_packages_rpm_$(date +"%Y-%m-%d").txt"
    else
        echo "rpm not found."
    fi
}

# Function to list installed software using yum
Function_list_installed_yum() {
    if command -v yum &>/dev/null; then
        echo "===== yum list installed ====="
        yum list installed > "installed_packages_yum_$(date +"%Y-%m-%d").txt"
    else
        echo "yum not found."
    fi
}

# Function to list installed software using dnf
Function_list_installed_dnf() {
    if command -v dnf &>/dev/null; then
        echo "===== dnf list installed ====="
        dnf list installed > "installed_packages_dnf_$(date +"%Y-%m-%d").txt"
    else
        echo "dnf not found."
    fi
}

# Function to list installed software using zypper
Function_list_installed_zypper() {
    if command -v zypper &>/dev/null; then
        echo "===== zypper search -i -s ====="
        zypper search -i -s > "installed_packages_zypper_$(date +"%Y-%m-%d").txt"
    else
        echo "zypper not found."
    fi
}

# Function to list installed software using brew
Function_list_installed_brew() {
    if command -v brew &>/dev/null; then
        echo "===== brew list ====="
        brew list > "installed_packages_brew_$(date +"%Y-%m-%d").txt"
    else
        echo "brew not found."
    fi
}

# Function to list installed software using pacman
Function_list_installed_pacman() {
    if command -v pacman &>/dev/null; then
        echo "===== pacman -Q ====="
        pacman -Q > "installed_packages_pacman_$(date +"%Y-%m-%d").txt"
    else
        echo "pacman not found."
    fi
}

# Function to list installed software using xbps
Function_list_installed_xbps() {
    if command -v xbps-query &>/dev/null; then
        echo "===== xbps-query -l ====="
        xbps-query -l > "installed_packages_xbps_$(date +"%Y-%m-%d").txt"
    else
        echo "xbps-query not found."
    fi
}

# Function to list installed software using pip
Function_list_installed_pip() {
    if command -v pip &>/dev/null; then
        echo "===== pip list ====="
        pip list > "installed_packages_pip_$(date +"%Y-%m-%d").txt"
    else
        echo "pip not found."
    fi
}

# Function to list installed software using pisi
Function_list_installed_pisi() {
    if command -v pisi &>/dev/null; then
        echo "===== pisi list ====="
        pisi list > "installed_packages_pisi_$(date +"%Y-%m-%d").txt"
    else
        echo "pisi not found."
    fi
}

# Function to list installed software using nix
Function_list_installed_nix() {
    if command -v nix-env &>/dev/null; then
        echo "===== nix-env -q --installed ====="
        nix-env -q --installed > "installed_packages_nix_$(date +"%Y-%m-%d").txt"
    else
        echo "nix-env not found."
    fi
}

# Function to list installed software using yay
Function_list_installed_yay() {
    if command -v yay &>/dev/null; then
        echo "===== yay -Qq ====="
        yay -Qq > "installed_packages_yay_$(date +"%Y-%m-%d").txt"
    else
        echo "yay not found."
    fi
}

# Function to list installed software using equo
Function_list_installed_equo() {
    if command -v equo &>/dev/null; then
        echo "===== equo query list ====="
        equo query list > "installed_packages_equo_$(date +"%Y-%m-%d").txt"
    else
        echo "equo not found."
    fi
}

# Function to list installed software using apt
Function_list_installed_apt() {
    if command -v apt &>/dev/null; then
        echo "===== apt list --installed ====="
        apt list --installed > "installed_packages_apt_$(date +"%Y-%m-%d").txt"
    else
        echo "apt not found."
    fi
}

# Call functions to list installed software for each package manager
Function_list_installed_dpkg
Function_list_installed_rpm
Function_list_installed_yum
Function_list_installed_dnf
Function_list_installed_zypper
Function_list_installed_brew
Function_list_installed_pacman
Function_list_installed_xbps
Function_list_installed_pip
Function_list_installed_pisi
Function_list_installed_nix
Function_list_installed_yay
Function_list_installed_equo
Function_list_installed_apt

Function_Hardening() {
echo "ipv4_sysctl_settings"
sysctl -a >> "${LOG_AUDITD}"
sysctl -w net.ipv4.conf.all.accept_redirects=0
sysctl -w net.ipv4.conf.all.accept_source_route=0
sysctl -w net.ipv4.conf.all.log_martians=1
sysctl -w net.ipv4.conf.all.rp_filter=1
sysctl -w net.ipv4.conf.all.secure_redirects=0
sysctl -w net.ipv4.conf.all.send_redirects=0
sysctl -w net.ipv4.conf.all.shared_media=0
sysctl -w net.ipv4.conf.default.accept_redirects=0
sysctl -w net.ipv4.conf.default.accept_source_route=0
sysctl -w net.ipv4.conf.default.log_martians=1
sysctl -w net.ipv4.conf.default.rp_filter=1
sysctl -w net.ipv4.conf.default.secure_redirects=0
sysctl -w net.ipv4.conf.default.send_redirects=0
sysctl -w net.ipv4.conf.default.shared_media=0
sysctl -w net.ipv4.icmp_echo_ignore_broadcasts=1
sysctl -w net.ipv4.icmp_ignore_bogus_error_responses=1
sysctl -w net.ipv4.ip_forward=0
sysctl -w net.ipv4.tcp_challenge_ack_limit=2147483647
sysctl -w net.ipv4.tcp_invalid_ratelimit=500
sysctl -w net.ipv4.tcp_max_syn_backlog=20480
sysctl -w net.ipv4.tcp_rfc1337=1
sysctl -w net.ipv4.tcp_syn_retries=5
sysctl -w net.ipv4.tcp_synack_retries=2
sysctl -w net.ipv4.tcp_syncookies=1

echo "ipv6_sysctl_settings:"
sysctl -w net.ipv6.conf.all.accept_ra=0
sysctl -w net.ipv6.conf.all.accept_redirects=0
sysctl -w net.ipv6.conf.all.accept_source_route=0
sysctl -w net.ipv6.conf.all.forwarding=0
sysctl -w net.ipv6.conf.all.use_tempaddr=2
sysctl -w net.ipv6.conf.default.accept_ra=0
sysctl -w net.ipv6.conf.default.accept_ra_defrtr=0
sysctl -w net.ipv6.conf.default.accept_ra_pinfo=0
sysctl -w net.ipv6.conf.default.accept_ra_rtr_pref=0
sysctl -w net.ipv6.conf.default.accept_redirects=0
sysctl -w net.ipv6.conf.default.accept_source_route=0
sysctl -w net.ipv6.conf.default.autoconf=0
sysctl -w net.ipv6.conf.default.dad_transmits=0
sysctl -w net.ipv6.conf.default.max_addresses=1
sysctl -w net.ipv6.conf.default.router_solicitations=0
sysctl -w net.ipv6.conf.default.use_tempaddr=2

echo "generic_sysctl_settings:"
sysctl -w fs.protected_fifos=1
sysctl -w fs.protected_hardlinks=1
sysctl -w fs.protected_symlinks=1
sysctl -w fs.suid_dumpable=2
sysctl -w kernel.core_uses_pid=1
sysctl -w kernel.dmesg_restrict=1
sysctl -w kernel.kptr_restrict=2
sysctl -w kernel.panic=60
sysctl -w kernel.panic_on_oops=60
sysctl -w kernel.perf_event_paranoid=3
sysctl -w kernel.randomize_va_space=2
sysctl -w kernel.sysrq=0
sysctl -w kernel.unprivileged_bpf_disabled=2
sysctl -w kernel.yama.ptrace_scope=1
sysctl -w net.core.bpf_jit_harden=0

echo "conntrack_sysctl_settings:"
#sysctl -w net.netfilter.nf_conntrack_max=2000000
#sysctl -w net.netfilter.nf_conntrack_tcp_loose=0
#sysctl -w page_alloc.shuffle=1
sysctl -w kernel.kptr_restrict=1
#sysctl -w kernel.modules_disabled=0
sysctl -w user.max_user_namespaces=31231
}

Function_Hardening

Function_process_conf_files() {
  local root_mount_point
  local home_mount_point

  # Determine the mount points for / and /home
  root_mount_point=$(df -P / | awk 'NR==2 {print $6}')
  #read only root_mount_point
  echo "Root Mount Point: ${root_mount_point}"

  home_mount_point=$(df -P /home | awk 'NR==2 {print $6}')
  #read only home_mount_point
  echo "Home Mount Point: ${home_mount_point}"

  # Search for .conf files on / and /home
  local root_files=($(sudo find "${root_mount_point}" -type f -name "*.conf" 2>/dev/null))
  local home_files=($(sudo find "${home_mount_point}" -type f -name "*.conf" 2>/dev/null))

  Function_process_files_in_directory "${root_files[@]}"
  Function_process_files_in_directory "${home_files[@]}"
}

DIRECTORY=""
#DIRECTORY="/${HOMEDIR}/${USERSCRIPT}/"
echo "the directory is ${DIRECTORY}"
output_file3="audit.rules_investigated_conf_list.txt"
echo "We will import from ${output_file3}"
output_file4="audit.rules_investigated_conf_findings.txt"
echo "We will store the results in ${output_file4}"

Function_Test_Artefacts() {
  local input_file="Test_Artefacts_Input.txt"
  local allow_input_file="allow_input_file_test.txt"
  local deny_input_file="deny_input_file_test.txt"

  touch "${allow_input_file}"
  echo "/usr/sbin/arp " > "${allow_input_file}"

  touch "deny_input_file.txt"
  echo "/sbin/ping" > "${deny_input_file}"

  # Echo variable definitions
  echo "the input_file is ${input_file}"
  echo "the allow_input_file is ${allow_input_file}"
  echo "the deny_input_file is ${deny_input_file}"

  # Check if the files exist
  if [[ ! -f "${allow_input_file}" || ! -f "${deny_input_file}" ]]; then
    echo "Error: ${allow_input_file} or ${deny_input_file} not found."
    return 1
  fi

  echo "Investigate bin and sbin for available binaries"
  sudo find /bin /usr/bin /sbin /usr/sbin -type f -executable -exec file -i '{}' + | grep 'charset=binary' | awk -F: '{print $1}' | sort > "${input_file}"

  # Combine files and eliminate duplicates
  #sort -u "${deny_input_file}" "${input_file}" | grep -vxFf "${deny_input_file}" > "${input_file}"
  #cat "${allow_input_file}" >> "${input_file}"
  
  # Investigate entries in allow_input_file
    while IFS= read -r line; do
        grep -qF "${line}" "${input_file}" || echo "${line}" >> "${input_file}"
    done < "${allow_input_file}"

    # Investigate entries in deny_input_file
    while IFS= read -r line; do
        awk -v pattern="${line}" '$0 !~ pattern' "${input_file}" > "${input_file}.tmp" && mv -v "${input_file}.tmp" "${input_file}"
    done < "${deny_input_file}"

  sort -u -o "${input_file}" "${input_file}"

  # Import result into an array
  mapfile -t binaries_array < "${input_file}"

  # Check if the arrays are not empty
  if [ "${#binaries_array[@]}" -eq 0 ]; then
    echo "Error: No binaries found."
    return 1
  fi

  # Define readonly variables
  filename_test="${binaries_array[0]}"
  path_test="${binaries_array[1]}"

  # Create bash oneliner
  local bash_oneliner=""
  for binary_path in "${binaries_array[@]}"; do
    if command -v "${binary_path}" &>/dev/null; then
      local help_output
      help_output=$(eval "${binary_path} --help" 2>&1)
      echo "${help_output}"
      bash_oneliner+="${binary_path} -> ${binary_path} ${help_output}"$'\n'|| true
      echo "${bash_oneliner}"
    else
      echo "Binary not found: ${binary_path}"
    fi
  done

  echo "Bash oneliner:"
  echo "${bash_oneliner}"
}

# Example usage
#Function_Test_Artefacts

#git clone -v "https://codeberg.org/a13xp0p0v/kernel-hardening-checker"


Function_Process_UAC () {
   echo "Process UAC"

   DIRECTORY="/home/vagrant/uac/"
    echo "${DIRECTORY}"

    if [ ! -d "${DIRECTORY}" ]; then
        # Control will enter here if ${DESTINATION} doesn't exist.
        git clone -v "https://github.com/tclahr/uac"
        echo "the directory ${DIRECTORY} is created"
    fi

   cd "${DIRECTORY}" || exit
   echo "Changed to the desired directory: ${DIRECTORY}"

   cd "${DIRECTORY}artifacts/live_response/packages/" || exit
   echo "Changed to the desired directory: ${DIRECTORY}"

   echo "version: 1.0
artifacts:
  -
    description: Display installed packages.
    supported_os: [linux, macos]
    collector: command
    command: pip list
    output_file: pip_list-installed.txt

" > pip.yaml

   echo "
version: 1.0
artifacts:
  -
    description: Display installed packages.
    supported_os: [linux]
    collector: command
    command: xbps-query -l
    output_file: xbps_list-installed.txt

" > xbps.yaml

   cd "${DIRECTORY}" || exit
   echo "Changed to the desired directory: ${DIRECTORY}"

   ./uac -p full /home/vagrant
}

Function_Process_UAC

Function_Process_UAC_ndaal () {
   echo "Process UAC ndaal"

   DIRECTORY="/home/vagrant/ndaal_public_uac_ndaal/"
    echo "${DIRECTORY}"

    if [ ! -d "${DIRECTORY}" ]; then
        # Control will enter here if ${DESTINATION} doesn't exist.
        git clone -v "https://gitlab.com/vPierre/ndaal_public_uac_ndaal"
        echo "the directory ${DIRECTORY} is created"
    fi

   cd "${DIRECTORY}" || exit
   echo "Changed to the desired directory: ${DIRECTORY}"

   ./uac -p full /home/vagrant
}

Function_Process_UAC_ndaal

Function_Process_autotest() {
   echo "Process autotest"

   DIRECTORY="/home/vagrant/autotest/"
   echo "${DIRECTORY}"

   if [ ! -d "${DIRECTORY}" ]; then
        # Control will enter here if ${DESTINATION} doesn't exist.
        git clone -v "https://github.com/autotest/autotest"
        echo "the directory ${DIRECTORY} is created"
   fi

   cd "${DIRECTORY}" || exit
   printf "Changed to the desired directory: %s\n" "${DIRECTORY}"

   #client/autotest-local --verbose run sleeptest
}

Function_Process_autotest


DIRECTORY="/home/vagrant/"

cd "${DIRECTORY}" || exit
printf "Changed to the desired directory: %s\n" "${DIRECTORY}"

Function_Process_BasicForensicLinuxScript() {
   echo "Process Basic Forensic Linux Script"

   RULES_URL_A1="https://raw.githubusercontent.com/washingtonP1974/GNU-Linux-forensic/main/BasicForensicLinuxScript.sh"
   echo "RULES_URL: ${RULES_URL_A1}"
   wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "${RULES_URL_A1}"
   echo "Granting executing rights for *.sh"
   chmod +x BasicForensicLinuxScript.sh
   sudo /home/vagrant/BasicForensicLinuxScript.sh || true
}

Function_Process_BasicForensicLinuxScript

Function_Process_Lynis() {
   echo "Process Lynis"
   sudo lynis --pentest > "log_lynis_pentest_${DIRDATE}.txt"
   sudo lynis audit system > "log_lynis_audit_system_${DIRDATE}.txt"
}

Function_Process_Lynis

# Set file paths
input_file="${output_file3}"
allow_input_file="allow_input_file.txt"
deny_input_file="deny_input_file.txt"
output_file="audit.rules_investigated_conf_list_new.txt"

touch "${allow_input_file}"
echo "/etc/apparmor/ndaal.conf " > "${allow_input_file}"

touch "deny_input_file.txt"
echo "/etc/apparmor/parser.conf " > "deny_input_file.txt"

# Echo variable definitions
echo "the input_file is ${input_file}"
echo "the allow_input_file is ${allow_input_file}"
echo "the deny_input_file is ${deny_input_file}"
echo "the output_file is ${output_file}"

# Function to check if files exist
check_files_exist() {
    for file in "${@}"; do
        if [ ! -f "${file}" ]; then
            echo "Error: File '${file}' not found. Exiting."
            exit 1
        fi
    done
}

# Check if all files exist
check_files_exist "${input_file}" "${allow_input_file}" "${deny_input_file}"

# Investigate entries in allow_input_file
while IFS= read -r line; do
    grep -qF "${line}" "${input_file}" || echo "${line}" >> "${input_file}"
done < "${allow_input_file}"

# Investigate entries in deny_input_file
while IFS= read -r line; do
    awk -v pattern="${line}" '$0 !~ pattern' "${input_file}" > "${output_file}.tmp" && mv -v "${output_file}.tmp" "${input_file}"
done < "${deny_input_file}"

# Sort and remove duplicates
sort -u "${input_file}" -o "${output_file}"

# Function to process lines from "${DIRECTORY}${output_file3}"
process_lines() {
    local lines=("$@")
    
    # Iterate through each line in "${DIRECTORY}${output_file3}"
    for line in "${lines[@]}"; do
        full_path="${DIRECTORY}${line}"
        echo "**************************************************" >> "${DIRECTORY}${output_file4}"
        echo "*" >> "${DIRECTORY}${output_file4}"
        echo "${full_path}" >> "${DIRECTORY}${output_file4}"
        echo "**************************************************" >> "${DIRECTORY}${output_file4}"
        echo "*" >> "${DIRECTORY}${output_file4}"

        # Check if the file exists
        if [ -e "${full_path}" ]; then
            # Copy the content of the file to "${DIRECTORY}${output_file4}"
            echo "*************************" >> "${DIRECTORY}${output_file4}"
            echo "${full_path}" >> "${DIRECTORY}${output_file4}"
            echo "*************************" >> "${DIRECTORY}${output_file4}"

            echo "Copy the content of the file ${line} to ${DIRECTORY}${output_file4}"
            cat "${full_path}" >> "${DIRECTORY}${output_file4}"

            echo "Append in file ${line} a line with ### This is an auditd test ###"
            echo "### This is an auditd test ###" >> "${full_path}"
            echo "Copy the content of the file ${line} again to ${DIRECTORY}${output_file4}"
            cat "${full_path}" >> "${DIRECTORY}${output_file4}"
            echo "*************************" >> "${DIRECTORY}${output_file4}"

            echo "Remove in file ${line} the line with ### This is an auditd test ###"
            echo "Remove in file ${line} the line with ### This is an auditd test ###" >> "${full_path}"
            sed -i '/### This is an auditd test ###/d' "${full_path}"

            echo "*************************" >> "${DIRECTORY}${output_file4}"
            echo "Copy the content of the file ${line} again to ${DIRECTORY}${output_file4}"
            cat "${full_path}" >> "${DIRECTORY}${output_file4}"
            echo "*************************" >> "${DIRECTORY}${output_file4}"

            echo "Investigate each line for matches with sudo grep, sudo ausearch, and sudo journalctl"
            echo "Use sudo grep for ${line}"
            sudo grep "${line}" /var/log/syslog && echo "/var/log/syslog grep: ${line}" >> "${DIRECTORY}${output_file4}" || true
            sudo grep "${line}" /var/log/auth.log && echo "/var/log/auth.log grep: ${line}" >> "${DIRECTORY}${output_file4}" || true
            sudo grep "${line}" /var/log/audit/audit.log && echo "/var/log/audit/audit.log grep: ${line}" >> "${DIRECTORY}${output_file4}" || true

            echo "Use sudo ausearch for ${line}"
            sudo ausearch -m AVC -m USER_AVC -m SELINUX_ERR -i | grep "${line}" || true
            sudo ausearch -m AVC -m USER_AVC -m SELINUX_ERR -i | grep "${line}" && echo "sudo ausearch: ${line}" >> "${DIRECTORY}${output_file4}" || true

            echo "Use sudo journalctl for ${line}"
            sudo journalctl --no-pager | grep "${line}" && echo "sudo journalctl: ${line}" || true
            sudo journalctl --no-pager | grep "${line}" && echo "sudo journalctl: ${line}" >> "${DIRECTORY}${output_file4}" || true
            echo "Use sudo journalctl _TRANSPORT=audit for ${line}"
            sudo journalctl --no-pager _TRANSPORT=audit | grep  "${line}" && echo "sudo journalctl: ${line}" || true
            sudo journalctl --no-pager _TRANSPORT=audit | grep  "${line}" && echo "sudo journalctl: ${line}" >> "${DIRECTORY}${output_file4}" || true

            echo "*************************" >> "${DIRECTORY}${output_file4}"
        else
            echo "File not found: ${full_path}" >> "${DIRECTORY}${output_file4}"
        fi
    done
}

# Function to perform additional operations on the directory
Function_process_directory() {
    local directory="$1"
    local example_file="${directory}/example.txt"

    echo "Check if example file exists"
    if [ -f "${example_file}" ]; then
        # Display a message indicating that the example file exists
        echo "Example file '${example_file}' already exists. Skipping additional operations."
        return
    fi

    echo "Touch a file in the directory"
    sudo touch "${example_file}"
    
    echo "Echo ### This is an auditd test ### to the example file"
    echo "### This is an auditd test ###" >> "${example_file}"

    echo "Display the content of the example file"
    sudo cat -v -t -e "${example_file}"

    echo "Remove the line containing ### This is an auditd test ###"
    sudo sed -i '/### This is an auditd test ###/d' "${example_file}"

    echo "Display the modified content of the example file"
    sudo cat -v -t -e "${example_file}"

    echo "Remove the example file"
    sudo rm -f -v "${example_file}"
}

# Input and output file paths
input_file="audit.rules_investigated2.txt"
output_file="directory_output_file.txt"

# Check if input file exists
if [ ! -f "${input_file}" ]; then
    echo "Input file '${input_file}' not found."
    exit 1
fi

# Initialize an array from the input file
mapfile -t lines < "${input_file}"

# Loop through each line
for line in "${lines[@]}"; do
    echo "Check if the line meets the specified conditions ${line}"
    #if [[ ! "$line" =~ ^\# && ! "$line" =~ [\;\:\.\?\*] ]]; then
    if [[ ! "${line}" =~ ^\# && ! "${line}" =~ [\;\:\?\*] ]]; then
        echo "Check if ${line} is a directory"
        if [ -d "directory: ${line}" ]; then
            echo "${line}"
            echo "Append to the output file"
            echo "${line}" >> "${output_file}"
            # Call the function to perform additional operations
            Function_process_directory "${line}"
        fi
    fi
done

# Import lines from "${DIRECTORY}${output_file3}" into an array
mapfile -t lines < "${DIRECTORY}${output_file3}"

# Call the function with the lines array
process_lines "${lines[@]}"

#exit 0

Function_process_files_in_directory() {
  local files=("$@")

  for file in "${files[@]}"; do
    echo "Processing file: ${file}"

    # Create a copy of the file with the extension .old
    echo "Creating a copy of the file: ${file}.old"
    sudo cp -f -v "${file}" "${file}.old"

    # Check if the file has a line starting with "#"
    if sudo grep -q "^#" "${file}"; then
      echo "Appending the test entry at the end of the file: ${file}"
      # Append the test entry at the end of the file
      echo "### This is an auditd test ###" | sudo tee -a "${file}" > /dev/null

      # Show the content of the file
      echo "Content of ${file} after appending:"
      sudo  cat -v -t -e "${file}"

      # Remove the appended line
      echo "Removing the appended line from the file: ${file}"
      sudo sed -i.bak '/^### This is an auditd test$/d' "${file}"
    fi

    # Investigate ownership user and group
    original_user=$(sudo stat -c "%U" "${file}")
    original_group=$(sudo stat -c "%G" "${file}")

    # Check if the user 'example' and group 'example' exist
    if ! sudo id -u example &>/dev/null; then
      echo "Creating user: example"
      sudo useradd example
    fi

    if ! sudo getent group example &>/dev/null; then
      echo "Creating group: example"
      sudo groupadd example
    fi

    # Change the file to user 'example' and group 'example'
    echo "Changing ownership of the file to example:example: ${file}"
    sudo chown example:example "${file}" || true

    # Change it back to the original user and group with the same UID and GID
    echo "Changing ownership of the file back to ${original_user}:${original_group}: ${file}"
    sudo chown "${original_user}:${original_group}" "${file}" || true

    # Delete the file
    echo "Deleting the file: ${file}"
    sudo rm -f -v "${file}.old"

    # Move/rename the copy of this file to the original place
    #echo "Moving/rename the copy of the file to the original place: ${file}.old -> ${file}"
    #sudo mv -v "${file}.old" "${file}"

    # Check if the user 'example' and group 'example' exist
    if sudo id -u example &>/dev/null; then
      echo "Removing user: example"
      sudo userdel example
    fi

    if sudo getent group example &>/dev/null; then
      echo "Removing group: example"
      sudo groupdel example
    fi

    echo "Finished processing file: ${file}"
    echo "------------------------------------------"
  done
}

# Function to manage services
Function_manage_services() {
    echo "Listing all services managed by systemctl..."
    #mapfile -t services < <(sudo systemctl list-units --type=service --no-pager --plain | awk '{print $1}') || true
    mapfile -t services < <(sudo systemctl list-unit-files --type=service --no-pager --plain | awk '{print $1}') || true

    for service in "${services[@]}"; do
        echo "Checking status of ${service}..."
        sudo systemctl status "${service}" --full --no-pager || true

        echo "Reloading ${service}..."
        #sudo systemctl reload-or-restart "${service}" --full --no-pager || true

        echo "Restarting ${service}..."
        #sudo systemctl restart "${service}" &>/dev/null || true

        echo "Checking status of ${service} after restart..."
        sudo systemctl status "${service}" --full --no-pager || true
    done
}

# Specify the path to your text file
file_path="/${HOMEDIR}/${USERSCRIPT}/auditd_directory_list.txt"
echo "file_path: ${file_path}"

# Declare an array
declare -a array_auditd_directory_list

# Read each line from the file and append it to the array
while IFS= read -r line; do
    array_auditd_directory_list+=("${line}")
done < "${file_path}"

Function_Prepare_Test () {
    echo "create user and group"

    USER_NAME="example"
    echo "The user we are searching for is: ${USER_NAME}"

    # Check if the user already exists
    if getent user "${USER_NAME}" > /dev/null 2>&1; then
        echo "User ${USER_NAME} already exists."
    else
        echo "Create the user if it doesn't exist"
        sudo useradd -m "${USER_NAME}"
        echo "User ${USER_NAME} created successfully."
    fi

    GROUP_NAME="example"
    echo "The user we are searching for is: ${GROUP_NAME}"

    # Check if the group already exists
    if getent group "${GROUP_NAME}" > /dev/null 2>&1; then
        echo "Group ${GROUP_NAME} already exists."
    else
        echo "Create the group if it doesn't exist"
        sudo groupadd "${GROUP_NAME}"
        echo "Group ${GROUP_NAME} created successfully."
    fi

    # Check if the user is already a member of the group
    if groups "${USER_NAME}" | grep -q "\b${GROUP_NAME}\b"; then
        echo "User ${USER_NAME} is already a member of group ${GROUP_NAME}."
    else
        echo "Add the user ${USER_NAME} to the group ${GROUP_NAME}"
        sudo usermod -aG "${GROUP_NAME}" "${USER_NAME}"
        echo "User ${USER_NAME} added to group ${GROUP_NAME}."
    fi
}

Function_Cleanup_Test () {
    echo "delete user and group"

    USER_NAME="example"
    echo "The user we are searching for is: ${USER_NAME}"

    # Check if the user exists
    if getent user "${USER_NAME}" > /dev/null 2>&1; then
        echo "Delete the user ${USER_NAME}"
        sudo userdel -r "${USER_NAME}"
        echo "User ${USER_NAME} deleted."
    else
        echo "User ${USER_NAME} does not exist."
    fi

    GROUP_NAME="example"
    echo "The user we are searching for is: ${GROUP_NAME}"

    # Check if the group exists
    if getent group "${GROUP_NAME}" &>/dev/null; then
        echo "Delete the group ${GROUP_NAME}"
        sudo groupdel "${GROUP_NAME}"
        echo "Group ${GROUP_NAME} deleted."
    else
        echo "Group ${GROUP_NAME} does not exist."
    fi

}

Function_Show_Elements_of_array_auditd_directory_list () {
for element in "${array_auditd_directory_list[@]}"
    do
        (
        echo "Print the elements of the array array_auditd_directory_list ${element}"
        )
    done
}

Function_Executing_Tests_with_array_auditd_directory_list () {
for element in "${array_auditd_directory_list[@]}"
do
    (
    DIRECTORY="${element}"
    echo "${DIRECTORY}"

    if [ ! -d "${DIRECTORY}" ]; then
        # Control will enter here if ${DESTINATION} doesn't exist.
        mkdir -p -v "${DIRECTORY}"
        echo -e "***** \e[101m The directory ${DIRECTORY} is created \e[49m ******"
    fi
    echo "Print the elements of the array ${element}"
    echo "touch ${element}example.txt"
	sudo touch "${element}example.txt"
    sudo echo "### This is an example for a content modification in a file" > "${element}example.txt"
    sudo cat "${element}example.txt"
    sudo touch "${element}example.sh"
    sudo echo "#!/usr/bin/env bash" > "${element}example.sh"
    sudo echo "echo "hello world"" >> "${element}example.sh"
    sudo cat "${element}example.sh"
    sudo echo "echo "### this is an auditd test text"" >> "${element}example.sh"
    sudo chmod +x "${element}example.sh"
    sudo cat "${element}example.sh"    
    sudo ls -la "${element}example.sh"
    sudo "${element}example.sh"
    sudo echo "ls -la ${element}example.sh"
    sudo chmod -x -r "${element}example.sh"
    sudo ls -la "${element}example.sh"
    sudo echo "chmod 444 ${element}example.sh"
    sudo chmod 444 "${element}example.sh"
    sudo ls -la "${element}example.sh"
    sudo rm -f -v "${element}example.sh"
    sudo cp -v "${element}example.txt" "${element}example_old.txt"
    sudo mv -v "${element}example.txt" "${element}example_new.txt"
    sudo chmod +x "${element}example_new.txt"
    sudo chown -v example "${element}example_new.txt"
    sudo ls -la "${element}example_new.txt"
    sudo chown -c example:example "${element}example_new.txt"
    sudo ls -la "${element}example_new.txt"
    sudo rm -f -v "${element}example_new.txt"
    sudo rm -f -v "${element}example_old.txt"
    sudo mkdir -p -v "${element}example"
    sudo chmod -R -r "${element}example"
    sudo touch "${element}example/example.txt"
    sudo rm -f -v -R "${element}example"
    )
done
}

Function_Prepare_Logging_Start
Function_Install_Tools
Function_Logging_Start

Function_Prepare_Test

Function_process_conf_files
Function_manage_services

Function_Show_Elements_of_array_auditd_directory_list
Function_Executing_Tests_with_array_auditd_directory_list
Function_Cleanup_Test


# Find all binaries on the system
binaries=($(sudo find /bin /usr/bin /sbin /usr/sbin -type f -executable -exec file -i '{}' + | grep 'charset=binary' | awk -F: '{print $1}' | sort))
echo "Checking ${binaries}:"

# Loop through each binary
for binary in "${binaries[@]}"; do
    echo "Checking ${binary}:"
    
    # Check for --help
    if timeout 5s bash -c "${binary} --help" &>/dev/null; then
        echo "--help is supported"
        echo "View records related to a certain command, using the ausearch -c COMM_NAME command, for example, ausearch -c less for all records related to the less command."
        sudo ausearch -x "${binary}" -ts recent >> "${LOG_AUDITD}" || true
    else
        echo "--help is not supported"
    fi

    # Check for --version
    if timeout 5s bash -c "${binary} --version" &>/dev/null; then
        echo "--version is supported"
        echo "View records related to a certain command, using the ausearch -c COMM_NAME command, for example, ausearch -c less for all records related to the less command."
        sudo ausearch -x "${binary}" -ts recent >> "${LOG_AUDITD}" || true
    else
        echo "--version is not supported"
    fi
    
    investigated_file="audit.rules_investigated.txt"

    # Read the investigated content into an array
    readarray -t "array_keys" < "${investigated_file}"

    # Search for each key entry using ausearch
    for key in "${array_keys[@]}"; do
        echo "Searching for entries in the syslog with ausearch with option key: ${key}"
        sudo ausearch -k "${key}"  >> "${LOG_AUDITD}" || true
        if [ $? -ne 0 ]; then
            echo "No entries found for key: ${key}"
        fi
        echo "Entries found for key: ${key}"
    done

    echo "----------------------"
done

Function_Logging

Function_Logging_Final

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
